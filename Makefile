MYLIBDIR =$(HOME)/software/clarisse
all: libs

libs:
	$(MAKE) -C $(MYLIBDIR)/util	
	$(MAKE) -C $(MYLIBDIR)/map	
	$(MAKE) -C $(MYLIBDIR)/marshal
	$(MAKE) -C $(MYLIBDIR)/main

$(PRGS): $(OBJS) 
$(PRGS): $(LIB_UTIL) $(LIB_MAP) $(LIB_MARSHAL)
$(PRGS): % : %.o $(TARGET_LIB) 
	$(CC) $(CFLAGS)  -o $@ $< $(LIBS) $(LDFLAGS)

clean:	
	$(MAKE) -C$(MYLIBDIR)/main clean
	$(MAKE) -C$(MYLIBDIR)/util clean
	$(MAKE) -C$(MYLIBDIR)/map clean
	$(MAKE) -C$(MYLIBDIR)/marshal clean


