#include <pthread.h>
#include <assert.h>
#include <limits.h>
#include "buffer.h"
#include "buffer_pool.h"
#include "target.h"
#include "error.h"
#include "util.h"
#ifdef CLARISSE_CONTROL
#include "cls_ctrl.h"
#endif

extern server_global_state_t server_global_state;
#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif



void flush_dirty_buffer(clarisse_target_t *tgt, buffer_unit_p bu) {
    if (bu->dirty) {
      int n, to_write, l_dirty, r_dirty;
      
#if defined(CLARISSE_TIMING) //|| defined(CLARISSE_CONTROL)
      double t0, t1;
  
      t0 = MPI_Wtime();
#endif  
      
      l_dirty = bu->l_dirty;
      r_dirty = bu->r_dirty;
      
      to_write = r_dirty - l_dirty + 1;

      if ((n = pwrite(tgt->local_handle.fd, bu->buf + l_dirty,  to_write, bu->offset + l_dirty)) != to_write){
	fprintf(stderr, "ERROR AT WRITING THE BUFFER %p into file FD=%d SIZE=%d bu->offset=%lld  r_dirty=%d l_dirty=%d r_dirty - l_dirty + 1=%d n=%d\n",
		bu->buf, tgt->local_handle.fd, to_write, bu->offset,  r_dirty, l_dirty, to_write, n);
	if (n<0) 
	  perror("Error writing the buffer");
	handle_err(MPI_ERR_OTHER, "flush_dirty_buffer error");
       
      }
#ifdef CLARISSE_TASK_DEBUG 
	int myrank;
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);	
	fprintf(stderr, "A: %s: RANK %d WRITE SIZE=%d bu->offset=%lld  r_dirty=%d l_dirty=%d r_dirty - l_dirty + 1=%d\n", __FUNCTION__, myrank,
                r_dirty - l_dirty + 1, bu->offset,  r_dirty, l_dirty, r_dirty - l_dirty + 1);
#endif
	
#if defined(CLARISSE_TIMING) //|| defined(CLARISSE_CONTROL)
      t1 = MPI_Wtime();
#endif
      /*
#ifdef CLARISSE_CONTROL
      clarisse_ctrl_msg_file_write_time_t msg;
      msg.type = CLARISSE_CTRL_MSG_PUBLISH_FILE_WRITE_TIME;
      msg.time = t1 - t0;
      cls_publish((clarisse_ctrl_msg_t *)&msg, sizeof(clarisse_ctrl_msg_file_write_time_t)); 
      #endif      */
#ifdef CLARISSE_TIMING
      local_timing.time_file_write += (t1 - t0);
      local_timing.cnt_file_write ++;
      local_timing.size_file_write += (r_dirty - l_dirty + 1);
#endif  

      bu->dirty = 0;
      bu->r_dirty = -1;
      bu->l_dirty = INT_MAX;
      //tgt->fsize_ondisk = MAX(tgt->fsize_ondisk, bu->offset + bu->r_dirty + 1);
    }
}

/*
void find_flush_drop_buffer(server_iocontext_t * s_ctxt, clarisse_off offset) {
  buffer_unit_p bu;

  bu = dcache_find_page(&s_ctxt->dcache, s_ctxt->local_target->global_descr, offset); 

  if (bu) {
    //printf ("buffer: put page %d\n",p->idx);
    flush_dirty_buffer(s_ctxt->local_target, bu);
    dcache_rm(&s_ctxt->dcache, bu);
    bufpool_put(server_global_state.buf_pool, bu);
  }
}

// drops the buffer (when truncating or deleting the file)
void find_drop_buffer(server_iocontext_t * s_ctxt, clarisse_off offset) {
  buffer_unit_p bu;

  bu = dcache_find_page(&s_ctxt->dcache, s_ctxt->local_target->global_descr, offset); 

  if (bu) {
    dcache_rm(&s_ctxt->dcache, bu);
    bufpool_put(server_global_state.buf_pool, bu);
  }
}
*/

/*
// starts at b * s_ctxt->log_ios_rank (first block presented on this server)
void flush_buffers(ADIO_File fd, server_iocontext_t * o) {
  struct s_ctxt *s_ctxt = (struct s_ctxt *)fd->fs_ptr;
  if (s_ctxt->fsize_real > 0) {
    int b = s_ctxt->block_size;
    //clarisse_off i, final_r = func_1(s_ctxt->fsize - 1, &s_ctxt->subf_distrib);
    clarisse_off i, final_r = func_1(s_ctxt->fsize_real - 1, s_ctxt->subfile_distr);
    for (i = b * s_ctxt->log_ios_rank; i <= final_r; i+= b * s_ctxt->ios_per_file) 
      find_flush_drop_buffer(fd, o, i); 
    //drop_buffer(fd, o, i); 
    s_ctxt->fsize = 0;
  }
}
*/
void flush_buffers(server_iocontext_t * s_ctxt) {
  while (!dllist_is_empty(&s_ctxt->dcache.lru)) {
    buffer_unit_p bu;

    bu = dcache_rm_lru(&s_ctxt->dcache);
      //printf ("bu idx %lld  file_disp %lld\n",bu->idx,file_disp);
    flush_dirty_buffer(s_ctxt->local_target, bu);
    bufpool_put(server_global_state.buf_pool, bu);
  }
}

void flush_dcache_buffers(server_iocontext_t * s_ctxt) {
  buffer_unit_p bu, tmp;
  
  HASH_ITER(hh, s_ctxt->dcache.bufs, bu, tmp) {
    flush_dirty_buffer(s_ctxt->local_target, bu);
    s_ctxt->dcache.cnt_dirty--;
  }
}


void drop_buffers(server_iocontext_t * s_ctxt) {
  while (!dllist_is_empty(&s_ctxt->dcache.lru)) {
    buffer_unit_p bu;

    bu = dcache_rm_lru(&s_ctxt->dcache);
    bufpool_put(server_global_state.buf_pool, bu);
  }
}



/*
// offset MUST be present in the current buffer BECAUSE  i += b * s_ctxt->ios_per_file
void drop_buffers_from(ADIO_File fd, server_iocontext_t *s_cxt, clarisse_off offset) {
  struct s_ctxt *s_ctxt = (struct s_ctxt *)fd->fs_ptr;
  if (s_ctxt->fsize_real == 0) 
    return;
  else {
    int b = s_ctxt->block_size;
    clarisse_off i; 
    s_ctxt->fsize = MIN(s_ctxt->fsize, offset);
    //    int final_r = func_1(s_ctxt->fsize -1,&s_ctxt->subf_distrib);
    int final_r = func_1(s_ctxt->fsize -1, s_ctxt->subfile_distr);
    if (offset % b) {
      buffer_unit_p bu = dcache_find_page(&s_ctxt->dcache, s_ctxt->global_descr, offset / b);
      if (bu) {
	bzero(bu->buf + offset % b, END_BLOCK(offset, b) - offset + 1); 
	bu->r_dirty = offset % b - 1;
      }
      offset = BEGIN_BLOCK(offset, b) + b * s_ctxt->ios_per_file;
    }
    
    // offset is begining of a block
    for (i = offset; i <= final_r; i += b * s_ctxt->ios_per_file) 
      find_drop_buffer(fd, o, i);
    
  }
}
*/
