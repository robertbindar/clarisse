#include <assert.h>
#include <errno.h>
#include "buffer.h"
#include "map.h"
#include "client_iocontext.h"
#include "server_iocontext.h"
#include "buffer_pool.h"
#include "tasks.h"
#include "list.h"
#include "hdr.h"
#include "error.h"
#include "marshal.h"
#include "buffer.h"
#include "net_dt.h"
#include "util.h"
#include "alloc_pool.h"
#include "cls_ctrl.h"
// WB: 
// Bufpool -> dcache + lru
// dirty: 

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern server_global_state_t server_global_state;
extern client_global_state_t client_global_state;
#ifdef CLARISSE_CONTROL
extern cls_controller_t cls_controller;
#endif
static int write_file_partitioning1(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v);
static int write_file_partitioning2(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v);
static int client_comm_write_round(client_iocontext_t *c_ctxt);
int client_pack_view_wr(client_iocontext_t *c_ctxt, struct rq_file_write *rq);
int  client_task_to_write_rq(client_iocontext_t *c_ctxt, struct rq_file_write *rq, struct dllist **off_len_block_list);
int  client_pack_data_listio(client_iocontext_t *c_ctxt, struct rq_file_write *rq, struct dllist **off_len_block_list);
void serv_write_task(server_iocontext_t * s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_write_task_listio(server_iocontext_t * s_ctxt, struct rq_file_write  *r, int client_idx, int source);

static void serv_recv_serve_write_rq(MPI_Comm comm);
static int serv_file_write(server_iocontext_t * s_ctxt, clarisse_off l, clarisse_off r, int client_idx, int source,  char *out_buf); 
void target_write(int client_idx, int source, server_iocontext_t *s_ctxt, iotask_p iot,  clarisse_off l_f,  clarisse_off r_f, char*buf, short last_msg);


int MPI_File_write_at_all(MPI_File fh, MPI_Offset offset, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status) {
  int ret;

#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_write_all[local_timing.cnt_write_all][0] = MPI_Wtime();
#endif

#ifdef CLARISSE_SAVE_TYPE_IN_FILE
  char *type_buf;
  int type_size;
  printf("WRITE_AT_ALL: OFFSET=%lld, COUNT=%d ", offset, count); 
  print_dt(datatype);
  type_buf = clarisse_malloc(1024);
  type_size = decode_pack_dt_out_buf(datatype, &type_buf);
  save_type_in_file("MEMORY", type_buf, type_size, 0);
  clarisse_free(type_buf);
#endif
  client_iocontext_t *c_ctxt;
  c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
  if (c_ctxt) {
    int bytes_xfered, datatype_size;
    bytes_xfered = cls_write_coll(c_ctxt, (char *) buf, count,
				  datatype, CLARISSE_EXPLICIT_OFFSET, offset);
    
    MPI_Type_size(datatype, &datatype_size);
    MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
    ret = MPI_SUCCESS; 
  }
  else
    ret = PMPI_File_write_at_all(fh, offset, buf, count, datatype, status); 

#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS) {
    int size, transfer_count;

    MPI_Get_count(status, datatype, &transfer_count);
    local_timing.time_write_all[local_timing.cnt_write_all][1] = MPI_Wtime();
    MPI_Type_size(datatype, &size);
    local_timing.size_write_all[local_timing.cnt_write_all] = (double) transfer_count * size;
    local_timing.cnt_write_all++;
  }
#endif  

  return ret;
}

int MPI_File_write_all(MPI_File fh, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status) {
  int ret;
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_write_all[local_timing.cnt_write_all][0] = MPI_Wtime();
#endif  
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
  char *type_buf;
  int type_size;
  printf("WRITE_ALL: COUNT=%d ", count); 
  print_dt(datatype);
  type_buf = clarisse_malloc(1024);
  type_size = decode_pack_dt_out_buf(datatype, &type_buf);
  save_type_in_file("MEMORY", type_buf, type_size, 0);
  clarisse_free(type_buf);
#endif
  client_iocontext_t *c_ctxt;
  c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
  if (c_ctxt) {
    int bytes_xfered, datatype_size;
    
    bytes_xfered = cls_write_coll(c_ctxt, (char *) buf, count,
				  datatype, CLARISSE_INDIVIDUAL,  c_ctxt->fp_ind);
    MPI_Type_size(datatype, &datatype_size);
    MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
    ret = MPI_SUCCESS;
  }
  else
    ret = PMPI_File_write_all(fh, buf, count, datatype, status); 
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS) {
    int size, transfer_count;

    MPI_Get_count(status, datatype, &transfer_count);
    local_timing.time_write_all[local_timing.cnt_write_all][1] = MPI_Wtime();
    MPI_Type_size(datatype, &size);
    local_timing.size_write_all[local_timing.cnt_write_all] = (double) transfer_count*size;
    local_timing.cnt_write_all++;
  }
#endif  

  return ret;
}


void scheduling_start(client_iocontext_t *c_ctxt){
  if ((client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM) &&
      (client_global_state.clarisse_pars.global_scheduling == CLARISSE_GLOBAL_SCHEDULING_FCFS)) {
    int myrank;
    
#ifdef CLARISSE_TIMING
    if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS)
      local_timing.time_write_all_wait[local_timing.cnt_write_all][0] = MPI_Wtime();
#endif  
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
    if (myrank == cls_controller.rank){
      clarisse_ctrl_event_t *event;
      clarisse_ctrl_msg_io_request_start_t *msg_request_start;
      clarisse_ctrl_msg_t msg;
      msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
      msg_request_start = (clarisse_ctrl_msg_io_request_start_t *)&msg;
      msg_request_start->client_id = client_global_state.client_id; 
      cls_publish(&msg, sizeof(clarisse_ctrl_msg_io_request_start_t));
      event = cls_wait_event(CLARISSE_CTRL_MSG_IO_SCHEDULE);
      clarisse_free(event);
    }
    MPI_Barrier(c_ctxt->intracomm);
#ifdef CLARISSE_TIMING
    if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS)
      local_timing.time_write_all_wait[local_timing.cnt_write_all][1] = MPI_Wtime();
#endif  
  }
}

void scheduling_stop(){
  
  if ((client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM) &&
      (client_global_state.clarisse_pars.global_scheduling == CLARISSE_GLOBAL_SCHEDULING_FCFS)) {
    int myrank;
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
    if (myrank == cls_controller.rank){
      clarisse_ctrl_msg_t msg;
      msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
      cls_publish(&msg, sizeof(int));
    }
  }
}



void elasticity_start0() {
  if (client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) {
    if (client_global_state.coll_op_seq == client_global_state.load_injection_coll_seq) {
      int myrank;
      MPI_Comm_rank(client_global_state.intracomm, &myrank);
      if (myrank == 0)
	cls_active_server_slow(client_global_state.loaded_active_log_ios_rank,client_global_state.load);
    }
    
    if (client_global_state.active_log_ios_rank  == -1) {
      clarisse_ctrl_event_t *event;
      event = cls_test_event(CLARISSE_CTRL_MSG_IO_STOP);
      
      if (event != NULL) {
	clarisse_ctrl_msg_t *msg;
	clarisse_ctrl_msg_io_stop_t *msg_io_stop;
	clarisse_ctrl_msg_io_current_coll_op_seq_t msg_io_current_coll_op_seq;
	clarisse_ctrl_msg_io_resume_t *msg_io_resume;
	communication_point_t dest;
	printf("%s: Received CLARISSE_CTRL_MSG_IO_STOP\n", __FUNCTION__);
	msg_io_stop = (clarisse_ctrl_msg_io_stop_t *)&event->msg;
	client_global_state.active_log_ios_rank = msg_io_stop->active_log_ios_rank;
	
	msg_io_current_coll_op_seq.type = CLARISSE_CTRL_MSG_IO_CURRENT_COLL_OP_SEQ;
	msg_io_current_coll_op_seq.coll_op_seq = client_global_state.coll_op_seq;
	msg = (clarisse_ctrl_msg_t *)&msg_io_current_coll_op_seq;
	dest.comm = cls_controller.comm;
	dest.rank = cls_controller.rank;
	cls_publish_remote(&dest, msg, sizeof(clarisse_ctrl_msg_io_current_coll_op_seq_t));
	
	event = cls_wait_event(CLARISSE_CTRL_MSG_IO_RESUME);
	printf("%s: Received CLARISSE_CTRL_MSG_IO_RESUME coll_op_seq = %d\n", __FUNCTION__,  client_global_state.coll_op_seq);
	msg_io_resume = (clarisse_ctrl_msg_io_resume_t *)&event->msg;	  
	client_global_state.last_coll_op_seq =  msg_io_resume->last_coll_op_seq;
      }
      
      if (((event == NULL) && (client_global_state.last_coll_op_seq == client_global_state.coll_op_seq)) || 
	  ((event != NULL) && (client_global_state.last_coll_op_seq == client_global_state.coll_op_seq)))  {
	cls_active_server_down( client_global_state.active_log_ios_rank);	
	client_global_state.epoch++;
	client_global_state.last_coll_op_seq = -1;
	//client_global_state.coll_op_seq= 0;
	client_global_state.active_log_ios_rank  = -1;	  
	if (client_global_state.active_nr_servers == 0) 
	  handle_error(MPI_ERR_OTHER, "No active server left\n");
      }
    }
  }
}

void elasticity_stop0()  {
  //printf("client_global_state.global_coll_seq =%d client_global_state.load_detection_coll_seq=%d\n",
  // client_global_state.global_coll_seq, client_global_state.load_detection_coll_seq);
  // When detecting that a server is slow, publish it. 
  if ((client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) &&
      (client_global_state.coll_op_seq == client_global_state.load_detection_coll_seq)) {
    int myrank;
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
    if (myrank == 0)
      cls_active_server_down_publish(client_global_state.loaded_active_log_ios_rank);
  }
}


void elasticity_start1() {
  
  if (client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) {
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS)
    local_timing.time_write_all_wait[local_timing.cnt_write_all][0] = MPI_Wtime();
#endif 
  
    if (client_global_state.coll_op_seq == client_global_state.load_injection_coll_seq) {
      int myrank;
      MPI_Comm_rank(client_global_state.intracomm, &myrank);
      if (myrank == 0) {
	cls_active_server_slow(client_global_state.loaded_active_log_ios_rank, client_global_state.load); // slow is in seconds
	printf("Slow down server %d by load = %8.5f ms\n", client_global_state.loaded_active_log_ios_rank, client_global_state.load);
      }
    }

    //if (client_global_state.active_log_ios_rank  == -1) {
    clarisse_ctrl_event_t *event;
    int myrank, active_log_ios_rank = -1;

    MPI_Comm_rank(client_global_state.intracomm, &myrank);
    if (myrank == cls_controller.rank) {
      event = cls_test_event(CLARISSE_CTRL_MSG_IO_SERVER_DOWN);
      if (event) 
	active_log_ios_rank = ((clarisse_ctrl_msg_io_server_down_t *)(&(event->msg)))->active_log_ios_rank;;
      //printf("Just finished checking the event received =%p active_log_ios_rank =%d\n", event, active_log_ios_rank);
    }
    //printf("%d: Before Bcast\n", myrank);
    MPI_Bcast(&active_log_ios_rank, 1, MPI_INT, cls_controller.rank, client_global_state.intracomm);
    if (active_log_ios_rank != -1) {
      cls_active_server_down(active_log_ios_rank);	
      client_global_state.epoch++;
      if (myrank == 0) 
	printf("SCALED DOWN %d server : client_global_state.active_nr_servers =%d\n",active_log_ios_rank, client_global_state.active_nr_servers);
      if (client_global_state.active_nr_servers == 0) 
	handle_error(MPI_ERR_OTHER, "No active server left\n");
    }
    // if (myrank == 0)
    //  printf("After bcast\n");
    
    //printf("client_global_state.global_coll_seq =%d client_global_state.load_detection_coll_seq=%d\n",
    // client_global_state.global_coll_seq, client_global_state.load_detection_coll_seq);
    // When detecting that a server is slow, publish it. 
    if (client_global_state.coll_op_seq == client_global_state.load_detection_coll_seq) {
      if (myrank == 0)
	cls_active_server_down_publish(client_global_state.loaded_active_log_ios_rank);
    }
#ifdef CLARISSE_TIMING
    if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS)
      local_timing.time_write_all_wait[local_timing.cnt_write_all][1] = MPI_Wtime();
#endif  
  }
}

void elasticity_stop1()  {
  
}


int cls_write_coll(client_iocontext_t *c_ctxt, 
		   void *buf, int count,
		   MPI_Datatype datatype, int file_ptr_type,
		   MPI_Offset offset)
{
#ifdef CLARISSE_CONTROL
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED) {   
    scheduling_start(c_ctxt);
    if (client_global_state.clarisse_pars.elasticity_policy == 0)
      elasticity_start0();
    else
      elasticity_start1();
  }
#endif    
  int err, myrank, bytes_xfered;

  c_ctxt->local_target = clarisse_target_alloc_init("", -1, CLARISSE_TARGET_MEMORY, distrib_get(datatype, 0), (void *)buf);	
  MPI_Comm_rank(c_ctxt->intracomm, &myrank);  
  
  // The case from below is needed for termination if one rank writes 0 bytes and at least another rank writes at least 1 byte
  // Should be eventually a case of the function from below
  if (count == 0) {
    int ph_ios_rank, log_ios_rank;
    int sends; 
    MPI_Request  *wr_requests;

    sends = client_global_state.active_nr_servers;
    wr_requests = (MPI_Request *) clarisse_malloc(sends * sizeof(MPI_Request)); 
    for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {      
      ph_ios_rank = client_global_state.log2ph[log_ios_rank];
      if (ph_ios_rank != CLS_SERVER_INACTIVE) {
	err = clarisse_isend(&myrank, 1, MPI_INT, ph_ios_rank, 
			     RQ_FILE_WRITE_COLL, c_ctxt->cli_serv_comm, &wr_requests[log_ios_rank]);	
	if (err != MPI_SUCCESS)
	  handle_error(err, "cls_write_coll: send error\n");
      }
    }
    err = MPI_Waitall(sends, wr_requests, MPI_STATUSES_IGNORE);
    if (err != MPI_SUCCESS)
      handle_err(err, "cls_write_coll: waitall error\n");
    clarisse_free(wr_requests);
  }
  else {
    clarisse_off l_v, r_v;
    int bufsize, buftype_size;
    MPI_Type_size(datatype, &buftype_size);
    bufsize = buftype_size * count;
    l_v = (file_ptr_type == CLARISSE_INDIVIDUAL) ? 
      func_sup(c_ctxt->fp_ind, c_ctxt->intermediary_target->distr):
      c_ctxt->etype_size * offset;
    r_v = l_v + bufsize - 1;
    if ((c_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING) || 
	(client_global_state.active_nr_servers == 1))
      bytes_xfered = write_file_partitioning1(c_ctxt, l_v, r_v);
    else
      bytes_xfered = write_file_partitioning2(c_ctxt, l_v, r_v);
    if (file_ptr_type == CLARISSE_INDIVIDUAL) 
      c_ctxt->fp_ind = func_1(r_v, c_ctxt->intermediary_target->distr) + 1; // r_f + 1    
    c_ctxt->fp_sys_posn = -1;   /* set it to null. */

  }
  distrib_free(c_ctxt->local_target->distr);
  clarisse_free(c_ctxt->local_target);

#ifdef CLARISSE_CONTROL  
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED) {
    scheduling_stop();
    if (client_global_state.clarisse_pars.elasticity_policy == 0)
      elasticity_stop0();
    else
      elasticity_stop1();
  }
#endif
  client_global_state.coll_op_seq++;
  return bytes_xfered;
}

static void set_crt_page(client_iocontext_t *c_ctxt, clarisse_off l_d){
  if ((clarisse_am_i_coupled_server(c_ctxt)) &&
      (c_ctxt->clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD) &&
      (c_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK)) {
    int_map_t *ph2log;
    int myrank, active_log_ios_rank;
    clarisse_off b, srv_buf_capacity;
    
    MPI_Comm_rank(c_ctxt->intracomm, &myrank);
    HASH_FIND_INT(client_global_state.ph2log, &myrank, ph2log);
    active_log_ios_rank = cls_get_active_log_ios_rank(ph2log->log_ios_rank);
    b = (clarisse_off)c_ctxt->clarisse_pars.buffer_size;
    srv_buf_capacity = (clarisse_off)b * (clarisse_off) client_global_state.active_nr_servers;
    // Any offset from task queue can be used for computing crt_page
    if (l_d % srv_buf_capacity < b * (active_log_ios_rank + 1))
      c_ctxt->crt_page = BEGIN_BLOCK(l_d, srv_buf_capacity) + b * active_log_ios_rank;
    else
      c_ctxt->crt_page = BEGIN_NEXT_BLOCK(l_d, srv_buf_capacity) + b * active_log_ios_rank;
#ifdef CLARISSE_DCACHE_DEBUG
  printf("%s: crt_page=%lld l_d=%lld\n",__FUNCTION__, c_ctxt->crt_page, l_d);
#endif	
  }
}

static int write_file_partitioning1(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v){
  int bytes_xfered;
  clarisse_off l_f, r_f, crt_l_d, b, l_d, r_d;

  bytes_xfered = 0;
  b = (clarisse_off)c_ctxt->clarisse_pars.buffer_size;
 
  l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
  r_f = func_1(r_v, c_ctxt->intermediary_target->distr);
  MPI_Allreduce(&l_f, &l_d, 1, MPI_LONG_LONG_INT, MPI_MIN, c_ctxt->intracomm);
  MPI_Allreduce(&r_f, &r_d, 1, MPI_LONG_LONG_INT, MPI_MAX, c_ctxt->intracomm);
  crt_l_d = l_d;
  
  while (crt_l_d <= r_d) {
    clarisse_off crt_r_d, srv_buf_capacity, crt_l_f, crt_r_f;
    int bytes;   

    srv_buf_capacity = (clarisse_off)b * (clarisse_off) client_global_state.active_nr_servers;
    //num_messages = (int) CEIL_INT(srv_buf_capacity, c_ctxt->clarisse_pars.net_buf_data_size) * client_global_state.active_nr_servers; 
    // Assertion: (crt_r_f - crt_l_f  + 1 ) <= Maximum capacity of server buffer caches 
    crt_r_d = MIN((BEGIN_BLOCK(crt_l_d, b) + srv_buf_capacity - 1), r_d);
    crt_l_f = MAX(l_f, crt_l_d);
    crt_r_f = MIN(r_f, crt_r_d);
    create_io_tasks(c_ctxt, l_v, 
		    crt_l_f, crt_r_f, 0, 0,
		    CLARISSE_STATIC_FILE_PARTITIONING);

    set_crt_page(c_ctxt, crt_l_d);
    bytes = client_comm_write_round(c_ctxt);
    bytes_xfered += bytes;
    crt_l_d = crt_r_d + 1;    
    MPI_Barrier(c_ctxt->intracomm);
    //#endif
  } /* while crt_l_f <= r_f*/
  assert(bytes_xfered == r_v - l_v + 1);
  return bytes_xfered;
}

// This partitioning is unique for each opearations as the data is mapped dynamically on aggregators. That is a future read operation can not retrieve data from these aggregators, as it does not know the mapping 

// client_global_state.active_nr_servers = 3
// c_ctxt->clarisse_pars.bufpool_size = 2
// l_f = 1, r_f =29, b=4
//                            1             2            3    
//               [0123 4567 8901][2345 6789 0123][4567 8901 2345
//                -***|****|****| ****|****|****| ****|**--|----
//part_block_len  --------------- 
//domain_len      0000 0000 1111 
//partit 1        ****|----|----| ****|----|----| ****|----
//partit 2        ----|****|----| ----|****|----| ----|****|----|  
//partit 3        ----|----|****| ----|----|****| ----|----|****|  

static int write_file_partitioning2(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v){
  int bytes_xfered;
  clarisse_off l_f, r_f, b, part_block_len, l_d, r_d, crt_l_d, crt_l_f;

  assert(client_global_state.active_nr_servers > 1);

  bytes_xfered = 0;
  b = (clarisse_off)c_ctxt->clarisse_pars.buffer_size;
  l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
  r_f = func_1(r_v, c_ctxt->intermediary_target->distr);
  MPI_Allreduce(&l_f, &l_d, 1, MPI_LONG_LONG_INT, MPI_MIN, c_ctxt->intracomm);
  MPI_Allreduce(&r_f, &r_d, 1, MPI_LONG_LONG_INT, MPI_MAX, c_ctxt->intracomm);
  l_d = BEGIN_BLOCK(l_d, b);
  part_block_len = BEGIN_NEXT_BLOCK((r_d - l_d) / client_global_state.active_nr_servers, b);
  //r_d = END_BLOCK(r_f, b);
  //fprintf(stderr, "part_block_len =%d\n", part_block_len);

  crt_l_f = l_f;
  crt_l_d = l_d;
  while (crt_l_d <= MIN(l_d + part_block_len - 1, r_d)){
    int  bytes;
    if ((crt_l_f - crt_l_d) % part_block_len < b) {
      clarisse_off crt_r_f;
      /*int  bytes, num_tasks*/;

      crt_r_f = MIN((END_BLOCK(crt_l_f, b) + ((clarisse_off)client_global_state.active_nr_servers - 1) * part_block_len),
		    r_f);
      //fprintf(stderr, "crt_l_f = %lld crt_r_f =%lld\n", crt_l_f, crt_r_f);
      //num_tasks = 
      create_io_tasks(c_ctxt, l_v, crt_l_f, crt_r_f, 
		      l_d, part_block_len,
		      CLARISSE_DYNAMIC_FILE_PARTITIONING); 
      crt_l_f = BEGIN_NEXT_BLOCK(crt_l_f, b);
    }
    set_crt_page(c_ctxt, BEGIN_BLOCK(crt_l_d,b));
    bytes = client_comm_write_round(c_ctxt);
    bytes_xfered += bytes;	

    //fprintf(stderr, "crt_l_f =%d\n", crt_l_f);
    crt_l_d = BEGIN_NEXT_BLOCK(crt_l_d, b);
    MPI_Barrier(c_ctxt->intracomm);
  } /* while crt_l_f <= r_f*/
  
  return bytes_xfered;
}

static void serv_recv_serve_write_rq_task(void *ptr){
  serv_recv_serve_write_rq(*((MPI_Comm *) ptr));
}

int client_drain_task_queues(client_iocontext_t *c_ctxt, int op_type) {
  int err, ph_ios_rank, bytes_xfered;
  MPI_Datatype d;


  //  MPI_Comm_rank(c_ctxt->intracomm, &myrank);  
  //request_queues_set_first(&c_ctxt->task_queues, myrank);
  bytes_xfered = 0;
  while (!request_queues_empty(&c_ctxt->task_queues)) {
    int count;
    iotask_p iot;
    int irq_max_size;
    struct dllist *off_len_block_list;
    char *data_rq;
    MPI_Request *mpi_rq;

    mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
    off_len_block_list = NULL;
    if (c_ctxt->clarisse_pars.collective == CLARISSE_VB_COLLECTIVE)
      irq_max_size = sizeof(struct rq_file_write) + CLARISSE_MAX_VIEW_SIZE + CLARISSE_MAX_TARGET_NAME_SIZE;
    else
      irq_max_size = sizeof(struct rq_file_write) + CLARISSE_MAX_TARGET_NAME_SIZE;
    data_rq = (char *) clarisse_malloc(irq_max_size); 
    
    iot = (iotask_p) request_queues_peek_crt(&c_ctxt->task_queues); 
    ph_ios_rank =  client_global_state.log2ph[iot->log_ios_rank];
    assert(ph_ios_rank != CLS_SERVER_INACTIVE);
    count = client_task_to_write_rq(c_ctxt, (struct rq_file_write *) data_rq, &off_len_block_list);
    if ((c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE) &&
	off_len_block_list)
      off_len_pack(&c_ctxt->net_dt, off_len_block_list);
    // mpi_rq is not used by now (if reallocation is done the pointer will be lost 
    bytes_xfered += count;
    d = net_dt_commit(&c_ctxt->net_dt);	
    save_mpi_rq(&c_ctxt->mpi_rq_map, mpi_rq, d, data_rq, off_len_block_list);
    
    //printf ("%s: sending data req %d to %d[%d]\n",__FUNCTION__, sends,ph_ios_rank,client_global_state.active_nr_servers);
    //printf("net_dt: aggr_len = %d cnt =%d\n", c_ctxt->net_dt.aggr_len, c_ctxt->net_dt.count);  
    err = clarisse_isend(data_rq, 1, d, ph_ios_rank, op_type, c_ctxt->cli_serv_comm, mpi_rq);
    if (err != MPI_SUCCESS)
      handle_err(err, "cls_write_coll: send error\n");
    //free_datatype(d);
    net_dt_reset(&c_ctxt->net_dt); // reinit the net datatype
    request_queues_set_next(&c_ctxt->task_queues);	
  }
  
  return bytes_xfered;
}


static int client_comm_write_round(client_iocontext_t *c_ctxt){
  int err, ph_ios_rank, log_ios_rank, bytes_xfered;

  if (clarisse_am_i_coupled_server(c_ctxt)) {
    // Wait if this page is not in cache and LRU is empty: 
      // Write-back will eventually make progress and free one page
      // File partitioning 2 is not compatible with WRITE BACK
      if (c_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK) {
	server_iocontext_t * s_ctxt;
	
	s_ctxt = server_iocontext_find(c_ctxt->global_descr);
#ifdef CLARISSE_DCACHE_DEBUG
	printf("%s: Before waiting for page crt_page=%lld\n",__FUNCTION__, c_ctxt->crt_page);
	dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif	
	dcache_wait_for_page(&s_ctxt->dcache, c_ctxt->crt_page);
	s_ctxt->client_rt_info[0].crt_page = c_ctxt->crt_page;
      }
      if (c_ctxt->clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD) {
	tpool_add_work(server_global_state.tpool, serv_recv_serve_write_rq_task, 
		     (void *)(&c_ctxt->cli_serv_comm), (void *)0);
    }
  }
  bytes_xfered = client_drain_task_queues(c_ctxt, RQ_FILE_WRITE_COLL);

  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {    
    if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
      // If no other request has been sent, send a termination request. 
      if (!c_ctxt->pending_ios_ret[log_ios_rank]) {
	MPI_Request *mpi_rq;

	mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);    
	ph_ios_rank = client_global_state.log2ph[log_ios_rank];
	err = clarisse_isend(&c_ctxt->global_descr, 1, MPI_INT, ph_ios_rank, RQ_FILE_WRITE_COLL, c_ctxt->cli_serv_comm, mpi_rq);
	if (err != MPI_SUCCESS)
	  handle_err(err, "cls_write_coll: send error\n");
      }
      // Reset pending_ios_ret : for new phase or new operations
      // normally set in the create_tasks, but if create_tasks not called have 
      // to do it here. 	
      c_ctxt->pending_ios_ret[log_ios_rank] = 0;
    }
  }
  //assert(sends + data_sends <= num_messages);
  /* Code of the aggregators */
  if (clarisse_am_i_coupled_server(c_ctxt)) { 
    if (c_ctxt->clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD)
      tpool_wait_for_task_queue_empty(server_global_state.tpool, (void*)0);
    else
      serv_recv_serve_write_rq(c_ctxt->cli_serv_comm);
  }
  err = MPI_Waitall(c_ctxt->mpi_rq_pool.cnt, c_ctxt->mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
  if (err != MPI_SUCCESS)
    handle_err(err, "cls_write_coll: waitall error\n");
  free_mpi_rq(&c_ctxt->mpi_rq_map);   
  
  return bytes_xfered;
}


static int  client_pack_data_vb(client_iocontext_t *c_ctxt, struct rq_file_write *rq) {
  int total_cnt = 0, log_ios_rank;
  iotask_p iot;

  iot = (iotask_p) request_queues_peek_crt(&c_ctxt->task_queues); 
  log_ios_rank = iot->log_ios_rank; 
  rq->l_f = iot->l_f;
  total_cnt = 0; // Added for sending max c_ctxt->clarisse_pars.net_buf_size
  while(1) {
    long long int count, to_transfer, r_dt;

    to_transfer = MIN(iot->count, c_ctxt->clarisse_pars.net_buf_data_size - total_cnt);
    if (to_transfer == iot->count) {
      r_dt = iot->r_dt;
      rq->r_f = iot->r_f;  
    }
    else {
      r_dt =  func_1(func_sup(iot->l_dt, iot->buf_distr) + to_transfer - 1,
		     iot->buf_distr);
      rq->r_f = func_1(func_sup(iot->l_f, c_ctxt->intermediary_target->distr) + to_transfer - 1,
		       c_ctxt->intermediary_target->distr);
    }
    count = sg_mem_nocopy(&c_ctxt->net_dt, iot->buf,  iot->l_dt, r_dt, iot->buf_distr, GATHER_NOCOPY);
    assert(count == to_transfer);
    total_cnt += count;
    iot->count -= count;

    if (iot->count == 0) {
      iot =  (iotask_p)request_queues_rm_queue(&c_ctxt->task_queues, log_ios_rank);
      clarisse_free(iot);
      iot = (iotask_p)request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank);
      if (iot && (total_cnt < c_ctxt->clarisse_pars.net_buf_data_size))
	//if (iot && (total_cnt < c_ctxt->clarisse_pars.net_buf_size))
	continue;
    }
    else {
      iot->l_dt = r_dt + 1;
      assert(iot->l_dt <= iot->r_dt);
      iot->l_f = rq->r_f + 1;
      assert(iot->l_f <= iot->r_f);
    }
    break;
  }
  rq->count = total_cnt;
  if (request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank))
    rq->last_msg = 0;
  else
    rq->last_msg = 1;
  return total_cnt;
}

int client_task_to_write_rq(client_iocontext_t *c_ctxt, struct rq_file_write *rq, struct dllist **off_len_block_list) {
  int nprocs, total_cnt, bytes_xfered;

  total_cnt = sizeof(struct rq_file_write);  
  MPI_Comm_size(c_ctxt->intracomm, &nprocs);		
  rq->nprocs = nprocs;
  rq->global_descr =  c_ctxt->global_descr;
  if (c_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)
    rq->partitioning_stride_factor = client_global_state.active_nr_servers;
  else
    rq->partitioning_stride_factor = 1;

  total_cnt += client_pack_view_wr(c_ctxt, rq);
  if (total_cnt > 0)
    net_dt_update(&c_ctxt->net_dt, (char *)rq, total_cnt); 
  if ((c_ctxt->clarisse_pars.collective == CLARISSE_VB_COLLECTIVE) || 
      ((c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE) &&
      (distrib_contig(c_ctxt->intermediary_target->distr)) &&
       (c_ctxt->intermediary_target->distr->displ == 0)))
    bytes_xfered = client_pack_data_vb(c_ctxt, rq);
  else {
    if (c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE) 
      bytes_xfered = client_pack_data_listio(c_ctxt, rq, off_len_block_list);
    else
      handle_err(MPI_ERR_OTHER,"Not supported collective method");
  }
    
  return bytes_xfered;
}


//****************************************************
//****************************************************
// Aggregator

static void serv_wb_task(void *ptr){
  task_params_t *params;
  buffer_unit_p bu;
  server_iocontext_t * s_ctxt;
  //  int empty_lru_list;
  //sleep(1);

  params = (task_params_t *)ptr;
  bu = params->bu;
  s_ctxt = params->s_ctxt;
#ifdef CLARISSE_DCACHE_DEBUG
  printf("%s: Enter bu=%p dirty=%d cnt_dirty=%d\n",__FUNCTION__, bu, bu->dirty,s_ctxt->dcache.cnt_dirty );
#endif
  assert(s_ctxt->dcache.cnt_dirty > 0 );
  assert(bu->dirty == 1);

  flush_dirty_buffer(s_ctxt->local_target, bu);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  dcache_assigned_wb_to_lru(&s_ctxt->dcache ,bu);
  //clarisse_free(ptr);
  free(ptr);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
}

// create tasks 
void serv_create_write_tasks(server_iocontext_t * s_ctxt){
  // Pass one buffer to one write-back thread
  // Clear the WB hash and move them to assigned_wb hash
  // Buffers have already been removed from LRU list:
  // will be added back when serving request finishes

  //printf ("s_ctxt=%p\n", s_ctxt);
  
  task_params_t *params;
  buffer_unit_p bu;
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  pthread_mutex_lock(&s_ctxt->dcache.dcache_lock);
  for (bu = s_ctxt->dcache.wb_buffers; bu != NULL; /*bu = bu->hh2.next*/){
    buffer_unit_p aux_bu;
    aux_bu = bu;
    params = (task_params_t *) malloc(sizeof(task_params_t));
    //params = (task_params_t *) clarisse_malloc(sizeof(task_params_t));
    params->s_ctxt = s_ctxt;
    params->bu = bu;
    HASH_DELETE(hh2, s_ctxt->dcache.wb_buffers, bu);      
    HASH_ADD(hh2, s_ctxt->dcache.assigned_wb_buffers, offset, sizeof(clarisse_off), bu);
#ifdef CLARISSE_DCACHE_DEBUG
    printf("CALL tpool_add_work bu=%p bu->dirty=%d cnt_dirty=%d\n", bu, bu->dirty,s_ctxt->dcache.cnt_dirty );
#endif
    
    tpool_add_work(server_global_state.tpool, serv_wb_task, (void *)params, (void *)s_ctxt);
    //sleep(2);
    bu = aux_bu->hh2.next;
  }
  pthread_mutex_unlock(&s_ctxt->dcache.dcache_lock);
#ifdef CLARISSE_DCACHE_DEBUG
  printf("%s: s_ctxt->dcache.wb_buffers=%p\n", __FUNCTION__, s_ctxt->dcache.wb_buffers);
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  //HASH_CLEAR(hh2, s_ctxt->dcache.wb_buffers); 
}

// Aggregator : coupled version
// Receiving the RQ_FILE_WRITE_COLL in a loop
 
static void serv_recv_serve_write_rq(MPI_Comm comm) {
  MPI_Status status;
  int arrived,  myrank, nprocs;
  struct rq_file_write *rq;
  short *received_from;
  server_iocontext_t * s_ctxt;

  s_ctxt = NULL;
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &nprocs);
  arrived = 0;
  received_from = clarisse_malloc(nprocs * sizeof(short));
  bzero(received_from, nprocs * sizeof(short));

  rq = (struct rq_file_write *)server_global_state.net_buf;  
  while (arrived < nprocs) {
    int cnt;

    MPI_Recv(server_global_state.net_buf, server_global_state.clarisse_pars.net_buf_size, MPI_BYTE, MPI_ANY_SOURCE, RQ_FILE_WRITE_COLL, comm, &status);
    MPI_Get_count(&status, MPI_BYTE, &cnt);
    //printf("MPI_Recv from %d %d bytes max_expected=%d buf=%p\n",status.MPI_SOURCE, cnt,  server_global_state.clarisse_pars.net_buf_size, server_global_state.net_buf);
    if (cnt  > (int) sizeof(int)) {
      if (!s_ctxt)
	s_ctxt = server_iocontext_find(rq->global_descr);
      s_ctxt->client_rt_info[0].partitioning_stride_factor = rq->partitioning_stride_factor;
      if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
	serv_write_task_listio(s_ctxt, rq, 0, status.MPI_SOURCE);
      else
	serv_write_task (s_ctxt, rq, 0, status.MPI_SOURCE);
      if (!received_from[status.MPI_SOURCE]){
	if (rq->last_msg == 1) {
	  received_from[status.MPI_SOURCE] = 1;
	  arrived++;
	  //printf("!!!!!!!!!!Last DATA message from %d arrived=%d\n", status.MPI_SOURCE, arrived);
	}
      }
    }
    else {
      received_from[status.MPI_SOURCE] = 1;
      arrived++;
      //printf("!!!!!!!!!!Last NULL message from %d arrived=%d\n", status.MPI_SOURCE, arrived);
      /*if ((myrank == 0) && (arrived == nprocs-1)){
	int i;
	for (i = 0; i < nprocs; i++) 
		if (received_from[i] == 0)
			printf("Process 0 has not yet heard from %d\n", i);	
      }
	*/
    }
  }
  clarisse_free(received_from);
  //printf("-------------------------------RECEIVED FROM EVERYBODY\n");

  if (s_ctxt && (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK)) 
    serv_create_write_tasks(s_ctxt);
  if (s_ctxt && (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_DEMAND))
    flush_dcache_buffers(s_ctxt);
}


// Aggregator 

void serv_write_task(server_iocontext_t *s_ctxt, struct rq_file_write  *r, int client_idx, int source) {
  iotask_p iot;
  
  iot = find_iotask(r->global_descr, source, &server_global_state.write_task_queue);
  
  if (!iot) {
    if (!r->last_msg) {
      iot = alloc_init_iotask(s_ctxt, source, r->l_f, r->r_f, client_idx);
      dllist_iat(&server_global_state.write_task_queue, (struct dllist_link *) iot);
    }
    if (r->first_msg && (r->view_size > 0)) {
      MPI_Datatype dt;
      char *buf;

      buf = (char *)(r + 1) + r->target_name_size;
      dt = reconstruct_dt(buf, r->view_size);  
      distrib_update(s_ctxt->client_rt_info[client_idx].remote_view_distr[source], dt, r->displ);
#ifdef CLARISSE_VIEW_DEBUG
      int rank;
      MPI_Comm_rank(s_ctxt->intracomm, &rank);
      fprintf(stderr, "Proc %d (AGGREG) RECEIVED FROM %d ",rank, source);
      distrib_print(s_ctxt->client_rt_info[client_idx].remote_view_distr[source]);
#endif
    } 
  }
  target_write(client_idx, source, s_ctxt, iot, r->l_f, r->r_f, 
	       (char *)(r + 1) + r->view_size + r->target_name_size,
	       r->last_msg);
}

void serv_write_task_listio_llint(server_iocontext_t *s_ctxt, struct rq_file_write  *r, int client_idx, int source) {
  int metadata_size, i, data_ptr;
  char *data;
  clarisse_off *offs;
  clarisse_off *lens;

  data_ptr = 0;
  metadata_size = r->view_size;
  data = (char *)(r + 1) + r->target_name_size;
  offs = (clarisse_off *) (data + r->count);
  lens = (clarisse_off *) (data + r->count + metadata_size / 2);
  
  for (i = 0; i < metadata_size / (2 * (int)sizeof(clarisse_off)); i++) {
    serv_file_write(s_ctxt, offs[i], offs[i] + lens[i] - 1, client_idx, source, data + data_ptr);
    data_ptr += (int)lens[i];
  }
}

void serv_write_task_listio(server_iocontext_t *s_ctxt, struct rq_file_write  *rq, int client_idx, int source) {
  int metadata_size;
  char *data;
  clarisse_off initial_off;
  metadata_size = rq->view_size;
  data = (char *)(rq + 1) + rq->target_name_size;
  initial_off = rq->l_f;

#ifdef CLARISSE_DCACHE_DEBUG
      dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif

  if (metadata_size == 0) {
    serv_file_write(s_ctxt, initial_off, initial_off + rq->count - 1, client_idx, source, data);
  }
  else {
    int  i, data_ptr;
    int *offs;
    int *lens;
    
    data_ptr = 0;
    metadata_size = rq->view_size;
    offs = (int *) (data + rq->count);
    lens = (int *) (data + rq->count + metadata_size / 2);
    for (i = 0; i < metadata_size / (2 * (int)sizeof(int)); i++) {      
      serv_file_write(s_ctxt, initial_off + offs[i], initial_off + offs[i] + lens[i] - 1, client_idx, source, data + data_ptr);
      data_ptr += (int)lens[i];
    }
  }
}

void target_write(int client_idx, int source, server_iocontext_t *s_ctxt, iotask_p iot,  clarisse_off l_f,  clarisse_off r_f, char*buf, short last_msg)
{
  int b, confirmed, total_cnt, total_errn;

  b = server_global_state.buf_pool->buffer_size;
  total_errn = 0;
  total_cnt = 0;
  errno = 0;
#ifdef CLARISSE_TASK_DEBUG
  int rank;
  MPI_Comm_rank(server_global_state.intracomm, &rank);
  fprintf(stderr, "A: Server %d TASK l_f=%lld r_f=%lld\n", rank, l_f, r_f); 
#endif
  while (l_f <= r_f) {
    clarisse_off crt_r_f;
    
    crt_r_f = MIN(r_f, END_BLOCK(l_f, b));
    // total_cnt is added to move through the buffer
    confirmed = serv_file_write(s_ctxt, l_f, crt_r_f, client_idx, source,  buf + total_cnt);
    total_cnt += confirmed;
    total_errn = MAX(total_errn, errno);
    //    l_f = BEGIN_BLOCK(l_f, b) + s_ctxt->ios_per_file * b;
    l_f = BEGIN_BLOCK(l_f, b) + (clarisse_off) s_ctxt->client_rt_info[client_idx].partitioning_stride_factor  * (clarisse_off) b;
#ifdef CLARISSE_TASK_DEBUG
    fprintf(stderr, "A: TASK l_f=%lld r_f=%lld partitioning_stride_factor=%d\n", l_f, r_f,s_ctxt->client_rt_info[client_idx].partitioning_stride_factor);
#endif
  }
  if (last_msg) {
    if (iot) {
      total_cnt += iot->confirmed;
      total_errn = MAX(iot->errn, total_errn);
      dllist_rem(&server_global_state.write_task_queue, (struct dllist_link *) iot);
      clarisse_free(iot);
    }
  }
  else {
    iot->confirmed += total_cnt;
    iot->errn = MAX(iot->errn, total_errn);
  }
}

static int serv_file_write(server_iocontext_t * s_ctxt, clarisse_off l, clarisse_off r, int client_idx, int source,  char *out_buf) {
  int  n, b, ret; 
  distrib *remote_view_distr;
  buffer_unit_p bu;
  clarisse_off beg_block;

  remote_view_distr = s_ctxt->client_rt_info[client_idx].remote_view_distr[source];
  b = server_global_state.buf_pool->buffer_size;
  beg_block = BEGIN_BLOCK(l, b);
  
  n = func_inf(r, remote_view_distr) - func_sup(l, remote_view_distr) + 1;
  if (n <= 0) 
    return 0;
  if ((s_ctxt->clarisse_pars.coupleness == CLARISSE_COUPLED) &&
      (s_ctxt->clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD) &&
      (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK)){
    //printf("beg_block = %lld s_ctxt->crt_page =%lld\n", beg_block,s_ctxt->crt_page);
    assert(beg_block == s_ctxt->client_rt_info[client_idx].crt_page);
  }
  
  // atomically lookup page in the whole dcache (the page can be as well 
  // either in wb_buffers or wb_buffers_assigned 
  bu = dcache_find_page(&s_ctxt->dcache, beg_block);
  //bu = dcache_find_page_wait_if_assigned(&s_ctxt->dcache, beg_block);

  if (!bu) {
    int to_read;
    //printf("++++++++++++++Buffer off %lld not found in cache\n", beg_block);
#ifdef CLARISSE_DCACHE_DEBUG
    dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
    bu = dcache_put_page(&s_ctxt->dcache, server_global_state.buf_pool, s_ctxt->local_target);
    if (bu == NULL) {
      dcache_print(&s_ctxt->dcache, "dcache_put_page returned NULL");
      handle_err(MPI_ERR_OTHER, "OUT OF BUFFERS");
    }
    if (n == b) 
      to_read = 0;
    else 
      to_read = b;
    if (to_read > 0){
      int actually_read;	  
#ifdef CLARISSE_TIMING
      double t0, t1; 
      t0 = MPI_Wtime();
#endif  	  
      // Before < to_read
      if ((actually_read = pread(s_ctxt->local_target->local_handle.fd,
				 /*fd->fd_sys*/ bu->buf, to_read, beg_block)) < 0) {
	perror("pread error");
	handle_err(MPI_ERR_OTHER, "Error at preadn");
      }
#ifdef CLARISSE_TIMING
      t1 = MPI_Wtime();
      local_timing.time_file_read += (t1 - t0);
      local_timing.cnt_file_read ++;
      local_timing.size_file_read += actually_read;
#endif  	  
    }
    bu->dirty = 0;
    bu->l_dirty = b;
    bu->r_dirty = -1;
    bu->global_descr = s_ctxt->local_target->global_descr;
    bu->offset = beg_block;
    dcache_insert(&s_ctxt->dcache, bu);
  }
  
  ret = sg_mem(out_buf, bu->buf - bu->offset/*func_inf(bu->offset, s_ctxt->local_target->distr)*/,  l, r, remote_view_distr,  SCATTER);  
  //ret =  n;
  if (ret != n)
    handle_err(MPI_ERR_OTHER, "sg_mem returned a value different than expected");
  if (n > 0){
    clarisse_off last_r = get_prev_data_r(r, remote_view_distr);
    /*	if (bu->dirty == 0) {
	s_ctxt->dcache.cnt_dirty++;
	bu->dirty = 1;	
	}*/
    if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK)
      pthread_mutex_lock(&s_ctxt->dcache.dcache_lock);
    bu->l_dirty = MIN(bu->l_dirty, get_next_data_l(l, remote_view_distr) % b) ;
    bu->r_dirty = MAX(bu->r_dirty, last_r % b) ;
    if (bu->dirty == 0) {
      bu->dirty = 1;	
      s_ctxt->dcache.cnt_dirty++;
    }
    if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK) {
      buffer_unit_p aux_bu = NULL;
      
      // If it is in the list should be dirty
      HASH_FIND(hh2, s_ctxt->dcache.wb_buffers, &(bu->offset), sizeof(clarisse_off), aux_bu);
      if (!aux_bu) {  
	HASH_FIND(hh2, s_ctxt->dcache.assigned_wb_buffers, &(bu->offset), sizeof(clarisse_off), aux_bu);
	if (!aux_bu) {  
	  dllist_rem(&s_ctxt->dcache.lru, &bu->lru_link);
	  HASH_ADD(hh2, s_ctxt->dcache.wb_buffers, offset, sizeof(clarisse_off), bu);
#ifdef CLARISSE_DCACHE_DEBUG	    
	  printf("%s:Added buf to wb_buffers bu=%p bu->dirty=%d cnt_dirty=%d\n",__FUNCTION__, bu, bu->dirty,s_ctxt->dcache.cnt_dirty );
#endif
	}
      }
      pthread_mutex_unlock(&s_ctxt->dcache.dcache_lock); 
#ifdef CLARISSE_DCACHE_DEBUG
      dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
    }
  } 
  
#ifdef CLARISSE_TASK_DEBUG  
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  //if (rank == 0)
  //debug
  printf("A: l=%lld r=%lld bu->offset=%lld func_inf(offset)=%lld block size=%d char[0]=%c char[%d]=%c n=%d ret=%d\n", l, r, bu->offset,  ((s_ctxt->local_target->distr)?func_inf(bu->offset, s_ctxt->local_target->distr):(bu->offset)), b, out_buf[0], (int)(r - l), out_buf[(int)(r - l)], n, ret);
  int i;
  printf("bu(%p) [l_dirty=%d - r_dirty=%d] =", bu, bu->l_dirty, bu->r_dirty);
  for (i = bu->l_dirty; i <= bu->r_dirty; i++)
    printf("%c", bu->buf[i]);
  printf("\n");

#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, __FUNCTION__);
#endif
#endif
  
  return n;
}
