#ifndef TASKS_H
#define TASKS_H

void create_preexisting_file(char *filename, int n);
void compare_files(char *filename_tp, char *filename_vb );
void setview_w_r(MPI_Offset off, MPI_Datatype d, MPI_Offset displ, MPI_Info info, char *filename, int n, MPI_Datatype mem_type, int phases);
#endif
