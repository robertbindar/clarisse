#ifndef AGGR_H
#define AGGR_H

#include <stdint.h>
#include "mpi.h"

enum {
  PLATFORM_BGQ_INCLUDE_BRIDGE=0,
  PLATFORM_BGQ_EXCLUDE_BRIDGE,
  PLATFORM_ANY_UNIFORM, 
  PLATFORM_ANY_READFROMFILE,
  AGGR_ASSIGNMENT_COUNT
} aggr_assignment;

int get_aggregators(int type, int param, int step, int **aggr_vector, int *flag_iamserver);
void aggr_print(int nr_aggrs,  int *aggr_vector);

#undef MIN
#define MIN(a,b) ((a<b ? a : b))
#undef MAX
#define MAX(a,b) ((a<b ? b : a))
#ifdef BGQ

// Associate the rank with the bridge coordinate
// Used for sorting the ranks by placing the bridge first and non-bridges subsequently (achieved by setting to 1 the last bridge bit (is always 0)
// bridge0, non-bridge0-0, non-bridge0-1, ...
// bridge1, non-bridge1-0, non-bridge1-1, ...
// ....
typedef struct {
  int rank;
  int bridgeCoord;
  int iambridge;
  int distance_to_bridge;
} sortstruct;

typedef struct {
  sortstruct *pset;
  int pset_size;
} pset_info;

int am_i_bridge();
int32_t get_bridge_coords();
int get_count_psets(int iambridge);
void get_pset_info(pset_info **psets);
void aggr_print_psets(int nr_aggr,  int *aggr_vector, int *aggr_per_pset_vector);
#endif // BGQ

#endif  /* AGGR_H_ */
