#include <assert.h>
#include "dcache.h"
#include "error.h"
#include "map.h" 
#include "server_iocontext.h"
#include "hdr.h"
#include "util.h"
#include "buffer.h"

extern server_global_state_t server_global_state;

void dcache_init(dcache_t *h, int max_elements, int concurrency) {
  h->concurrency = concurrency;
  h->bufs = NULL;
  dllist_init(&h->lru);
  h->max_elements = max_elements;
  h->crt_elements = 0;
  h->cnt_dirty = 0;

  if (concurrency == DCACHE_CONCURRENT_ACCESS) {
    int ret;
    if ((ret = pthread_mutex_init(&(h->dcache_lock), NULL)) != 0)	
      fprintf(stderr,"pthread_mutex_init %s",strerror(ret)), exit(-1);	
    if ((ret = pthread_cond_init(&(h->dcache_full), NULL)) != 0)	
      fprintf(stderr,"pthread_cond_init %s",strerror(ret)), exit(-1);
    if ((ret = pthread_cond_init(&(h->not_in_lru), NULL)) != 0)	
      fprintf(stderr,"pthread_cond_init %s",strerror(ret)), exit(-1);  
    if ((ret = pthread_cond_init(&(h->lru_empty), NULL)) != 0)	
      fprintf(stderr,"pthread_cond_init %s",strerror(ret)), exit(-1);  
    if ((ret = pthread_cond_init(&(h->wait_for_assigned_buf), NULL)) != 0)	
      fprintf(stderr,"pthread_cond_init %s",strerror(ret)), exit(-1);  
    h->wb_buffers = NULL;
    h->assigned_wb_buffers = NULL;
  }
}


dcache_t * dcache_alloc_init(int max_elements, int concurrency) {
  dcache_t * h;
 
  h=(dcache_t *) clarisse_malloc(sizeof(dcache_t));
  dcache_init(h, max_elements, concurrency);
  return h;
}

static inline int dcache_empty(dcache_t * h) { 
  return (h->crt_elements == 0); 
}

static inline int dcache_full(dcache_t *h) { 
  return (h->crt_elements == h->max_elements); 
}

void dcache_free(dcache_t *h) {
  buffer_unit_p bu, tmp;

  if (h->concurrency == DCACHE_CONCURRENT_ACCESS)
    pthread_mutex_lock(&h->dcache_lock);
  HASH_ITER(hh, h->bufs, bu, tmp) {
    HASH_DEL(h->bufs, bu);  /* delete; users advances to next */
    clarisse_free(bu->buf);
    clarisse_free(bu);            /* optional- if you want to free  */
  }
  //HASH_CLEAR(hh, h->bufs);
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) {
  //HASH_CLEAR(hh, h->wb_buffers);
    HASH_ITER(hh2, h->wb_buffers, bu, tmp) {
      HASH_DELETE(hh2, h->wb_buffers, bu);  /* delete; users advances to next */
      clarisse_free(bu->buf);
      clarisse_free(bu);            /* optional- if you want to free  */
    }
    HASH_ITER(hh2, h->assigned_wb_buffers, bu, tmp) {
      HASH_DELETE(hh2, h->assigned_wb_buffers, bu);  /* delete; users advances to next */
      clarisse_free(bu->buf);
      clarisse_free(bu);            /* optional- if you want to free  */
    }
    pthread_mutex_unlock(&h->dcache_lock);
  }
}

void dcache_insert(dcache_t * h, buffer_unit_p bu) {
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS)
    pthread_mutex_lock(&h->dcache_lock);
  if (h->crt_elements >=  h->max_elements) {
    fprintf(stderr,"h->crt_elements = %d >= max_elements\n", h->crt_elements); 
    handle_err(MPI_ERR_OTHER, "Hash insert error");
  }
  else {
    h->crt_elements++;
    HASH_ADD_LLINT(h->bufs, offset, bu);
    dllist_iah(&h->lru, &bu->lru_link);
  }
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS)
     pthread_mutex_unlock(&h->dcache_lock);
}

buffer_unit_p dcache_insert_page(dcache_t * h, clarisse_off idx, int global_descr, char *buf){
  buffer_unit_p bu;
  
  bu = (buffer_unit_p) clarisse_malloc(sizeof(buffer_unit));
  bu->buf = buf;
  bu->dirty = 0;
  bu->l_dirty = server_global_state.buf_pool->buffer_size;
  bu->r_dirty = -1;
  bu->global_descr = global_descr;
  bu->offset = idx;
  dcache_insert(h, bu);
  return bu;
}


// if full replace a page
// else retrieve a page (don't put it in the dcache yet) 
buffer_unit_p dcache_put_page(dcache_t *h, buffer_pool_p b, clarisse_target_t *tgt) {
  buffer_unit_p bu;
 
  bu = NULL;
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&h->dcache_lock);
  if (dcache_full(h)) {
    if (h->concurrency == DCACHE_CONCURRENT_ACCESS)
      while(dllist_is_empty(&h->lru)) {
#ifdef CLARISSE_DCACHE_DEBUG
	printf("Waiting for non-empty lru list\n");
#endif
	pthread_cond_wait(&h->dcache_full, &h->dcache_lock);
#ifdef CLARISSE_DCACHE_DEBUG
	printf("Waking up:non-empty lru list\n");
#endif
      }
    // get the last from the lru list
    bu = dcache_rm_lru(h);
    flush_dirty_buffer(tgt, bu); // Pb block inside critical section
  }
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&h->dcache_lock);
  if (!bu) 
    bu = bufpool_get(b);
  return bu;
}


void dcache_assigned_wb_to_lru(dcache_t * h,   buffer_unit_p bu) {
  assert(h->concurrency == DCACHE_CONCURRENT_ACCESS);
  pthread_mutex_lock(&h->dcache_lock);
  h->cnt_dirty--;
  dllist_iah(&h->lru, &bu->lru_link);
  HASH_DELETE(hh2, h->assigned_wb_buffers, bu); 
  //sleep(2);
  pthread_cond_broadcast(&h->dcache_full);
  //pthread_cond_broadcast(&s_ctxt->dcache.not_in_lru);
  pthread_cond_broadcast(&h->lru_empty);
  //pthread_cond_broadcast(&s_ctxt->dcache.wait_for_assigned_buf);
  pthread_mutex_unlock(&h->dcache_lock);

}




buffer_unit_p dcache_find_page(dcache_t * h, clarisse_off off){

  buffer_unit_p bu /*, bu2*/;
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&h->dcache_lock);
  HASH_FIND_LLINT(h->bufs, &off, bu);
  /*
  HASH_FIND_LLINT(h->wb_buffers, &off, bu2);
  if (!bu2)
    // sleep if not in bu2 and not in lru
    while ( bu && 
	    ((bu->lru_link.next == NULL) && (bu->lru_link.prev == NULL)) &&
	    ((&bu->lru_link) != h->lru.head)){
#ifdef CLARISSE_DCACHE_DEBUG
      printf("Waiting, bu=%p not in  lru list\n", bu);
#endif
      pthread_cond_wait(&h->not_in_lru, &h->dcache_lock);
#ifdef CLARISSE_DCACHE_DEBUG
      printf("Waking up: maybe in lru list\n");
#endif
      HASH_FIND_LLINT(h->bufs, &off, bu);
    }
  */
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&h->dcache_lock);

  //if (bu) 
  //  dllist_move_to_head(&h->lru,&(bu->lru_link));//move up in the lru list
  return bu;
}

// If it finds a page it removes it from the LRU list to make it ireplaceble
buffer_unit_p dcache_find_page_rm_lru(dcache_t * h, clarisse_off off){

  buffer_unit_p bu /*, bu2*/;
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&h->dcache_lock);
  HASH_FIND_LLINT(h->bufs, &off, bu);
  // if the element is in the list remove it, if not nothing happens
  if ((bu) && (!dllist_is_empty(&h->lru)))
    dllist_rem(&h->lru, &bu->lru_link);
    
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&h->dcache_lock);
  return bu;
}


// This function waits if a buffer is found in the dcache
// but it has been already assigned to a thread for flushing
buffer_unit_p dcache_find_page_wait_if_assigned(dcache_t * h, clarisse_off off){
  buffer_unit_p bu;
  assert (h->concurrency == DCACHE_CONCURRENT_ACCESS);
  pthread_mutex_lock(&h->dcache_lock);
  HASH_FIND_LLINT(h->bufs, &off, bu);
  if (bu) {
    buffer_unit_p bu2;
    HASH_FIND_LLINT(h->assigned_wb_buffers, &off, bu2);
    assert ((bu2 == NULL) || (bu == bu2));
    while (bu2) {
      pthread_cond_wait(&h->wait_for_assigned_buf, &h->dcache_lock);
      HASH_FIND_LLINT(h->assigned_wb_buffers, &off, bu2);
    }
  }
  pthread_mutex_unlock(&h->dcache_lock);
  return bu;
}



// This version blocks if the page is not in the cache and LRU list is empty
// Previos version of dcache_find_page blocked if a particular block was not 
// in the LRU list
void dcache_wait_for_page(dcache_t * h, clarisse_off off){
  assert (h->concurrency == DCACHE_CONCURRENT_ACCESS);
  buffer_unit_p bu /*, bu2*/;
  pthread_mutex_lock(&h->dcache_lock);
  if (h->crt_elements == h->max_elements) {
    HASH_FIND_LLINT(h->bufs, &off, bu);
    while ((!bu) && dllist_is_empty(&h->lru)){
      pthread_cond_wait(&h->lru_empty, &h->dcache_lock);
      HASH_FIND_LLINT(h->bufs, &off, bu);
    }
  }
  pthread_mutex_unlock(&h->dcache_lock);
}

void dcache_rm(dcache_t * h, buffer_unit_p bu) {
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&h->dcache_lock);
  HASH_FIND_LLINT(h->bufs, &(bu->offset), bu);
  if (bu) { 
    HASH_DEL(h->bufs, bu);
    if (!dllist_is_empty(&h->lru))
      dllist_rem(&h->lru, &bu->lru_link);
    h->crt_elements--;
    if (bu->dirty)
      h->cnt_dirty--;
  }
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&h->dcache_lock);
}




// I am sure bu is in h
// To be used for instance in HASH_ITER (HASH_ITER crashes with dcache_rm)
void dcache_rm2(dcache_t * h, buffer_unit_p bu) {
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&h->dcache_lock);

  HASH_DEL(h->bufs, bu);
  if (!dllist_is_empty(&h->lru))
    dllist_rem(&h->lru, &bu->lru_link);
  h->crt_elements--;
  if (bu->dirty)
    h->cnt_dirty--;

  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&h->dcache_lock);
}


buffer_unit_p  dcache_rm_lru(dcache_t * h) {
  if (dllist_is_empty(&h->lru)) {
    //if (h->crt_elements <= 0) {
    fprintf(stderr,"dcache_rm_lru: h->lru empty <= 0\n"); 
    handle_err(MPI_ERR_OTHER, "dcache_rm_lru error");
    return NULL;
  }
  else {
    struct dllist_link *lk;
    buffer_unit_p bu;
    
    lk = dllist_rem_tail(&h->lru);
    bu = (buffer_unit_p) DLLIST_ELEMENT(lk, buffer_unit, lru_link);
    HASH_DEL(h->bufs, bu);
    h->crt_elements--;
    if (bu->dirty)
      h->cnt_dirty--;
    return bu;
  }
}

/*
buffer_unit_p  dcache_peek_lru(dcache_t * h) {
  buffer_unit_p bu;
  
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&h->dcache_lock);
  if (dllist_is_empty(&h->lru)) {
    bu = NULL;
  }
  else {
    struct dllist_link *lk;
    lk = dllist_tail(&h->lru); 
    bu = (buffer_unit_p) DLLIST_ELEMENT(lk, buffer_unit, lru_link);
  }
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&h->dcache_lock);
  return bu;
}
*/
/*

// Atomically remove from buffer cache and move to bufpool
// necessary for avoiding that a thread thinks that the buffer
// is not available and tries to fill it again

buffer_unit_p dcache_atomic_bufpool_move(dcache_t * h, buffer_pool_p b) {
  assert (h->concurrency == DCACHE_CONCURRENT_ACCESS);
  buffer_unit_p bu;  

  pthread_mutex_lock(&h->dcache_lock);
  bu = dcache_rm_lru(h);
  bufpool_put(b, bu);
  pthread_mutex_unlock(&h->dcache_lock);    
  return bu;
}

//  dcache_atomic_bufpool_move_if_waiting moves the buffer only 
//  if somebody is waiting
//  dcache_atomic_bufpool_move moves the buffer anyhow
buffer_unit_p dcache_atomic_bufpool_move_if_waiting(dcache_t * h, buffer_pool_p b) {
  assert (h->concurrency == DCACHE_CONCURRENT_ACCESS);
  buffer_unit_p bu;  

  pthread_mutex_lock(&b->bufpool_lock);
  if (b->cnt_waiting > 0) {
    pthread_mutex_lock(&h->dcache_lock);
    bu = dcache_rm_lru(h);
    pthread_mutex_unlock(&h->dcache_lock);  
    bufpool_put(b, bu);
  }  
  pthread_mutex_unlock(&b->bufpool_lock);
  return bu;
}

*/
void dcache_atomic_bufpool_move_all(dcache_t * h, buffer_pool_p b) {
  assert (h->concurrency == DCACHE_CONCURRENT_ACCESS);
  //pthread_mutex_lock(&b->bufpool_lock);
  pthread_mutex_lock(&h->dcache_lock);
  while (!dllist_is_empty(&h->lru)) {
    buffer_unit_p bu;  
    bu = dcache_rm_lru(h);
    bufpool_put(b, bu);
  }
  pthread_mutex_unlock(&h->dcache_lock);    
  /*if (b->cnt_waiting > 0) {
    pthread_cond_broadcast(&b->bufpool_not_empty);
  }
  pthread_mutex_unlock(&b->bufpool_lock); */
}



void  dcache_print(dcache_t * h, char* mesg) {
  struct dllist_link *aux;
  buffer_unit_p bu;
  int i, n;

  if (h->concurrency == DCACHE_CONCURRENT_ACCESS)
    pthread_mutex_lock(&h->dcache_lock);

  n = 10;
  printf("%s:%s\n",mesg, __FUNCTION__);
  printf("\tcrt_elements=%d max_elements=%d cnt_dirty=%d\n",  h->crt_elements, h->max_elements, h->cnt_dirty);
  printf("\tCONTENT:");
  i=0;
  for (bu = h->bufs; bu != NULL; bu = bu->hh.next) {
    bu_print(bu);
    if ((++i)==n) {
      printf("\t\tOMMITTING FURTHER ELEMENTS (printed only the first %d)", n);  
      break;
    }
  }
  printf("\n\tWB BUFFERS:");
  i = 0;
  for (bu = h->wb_buffers; bu != NULL; bu = bu->hh2.next) {
    bu_print(bu);
    if ((++i)==n){
      printf("\t\tOMMITTING FURTHER ELEMENTS (printed only the first %d)", n);
      break;
    }
  }
  printf("\n\tASSIGNED WB BUFFERS:");
  i = 0;
  for (bu = h->assigned_wb_buffers; bu != NULL; bu = bu->hh2.next) {
    bu_print(bu);
    if ((++i)==n){
      printf("\t\tOMMITTING FURTHER ELEMENTS (printed only the first %d)", n);
      break;
    }
  }
  printf("\n\tHASH LRU:");
  i = 0;
  for(aux = h->lru.head; aux != NULL; aux = aux->next) {
    bu = (buffer_unit_p) DLLIST_ELEMENT(aux, buffer_unit, lru_link);
    bu_print(bu);
    if ((++i)==10) {
      printf("\t\tOMMITTING FURTHER ELEMENTS (printed only the first %d)", n);
      break;
    }
  }

  printf("\n");
  if (h->concurrency == DCACHE_CONCURRENT_ACCESS)
    pthread_mutex_unlock(&h->dcache_lock);
}




