#ifndef BUFFER_H
#define BUFFER_H

#include <stdio.h>
#include "server_iocontext.h"
#include "buffer_pool.h"
#include "target.h"
#include "clarisse.h"

void flush_buffer(server_iocontext_t * s_ctxt, clarisse_off offset);
void drop_buffer(server_iocontext_t * s_ctxt, clarisse_off offset);
void flush_buffers(server_iocontext_t * s_ctxt);
void flush_dcache_buffers(server_iocontext_t * s_ctxt);
void drop_buffers(server_iocontext_t * s_ctxt);
void flush_dirty_buffer(clarisse_target_t *tgt, buffer_unit_p bu);

#endif /*BUFFER_H_*/
