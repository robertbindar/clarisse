export ENV_FROM_SCRIPT=true
#export CLARISSE_NR_SERVERS=1
FILENAME="abc"
COLL_INDEP="independent" #"independent" "collective"
MPIEXEC="mpiexec -l" 
GDB= #"xterm -e gdb -args"

max_processes=5
min_nr_servers=1
max_nr_servers=4
export CLARISSE_COUPLENESS=intercomm #coupled intercomm dynamic
export CLARISSE_COLLECTIVE=vb #listio #vb  #vb # listio
export CLARISSE_WRITE_TYPE=ondemand #back ondemand onclose
export CLARISSE_AGGR_THREAD=thread
export CLARISSE_FILE_PARTITIONING=static

run_tests="7 8" #"1 2 3 4 5 6 7 8"
declare -a tests=(
"../build/main/test1_open_close" 
"../build/main/test2_write" 
"../build/main/test3_write_read" 
"../build/main/test4_write_read" 
"../build/main/test5_read" 
"../build/main/test6_write_write" 
"../build/main/test7_setview_write_read"
"../build/main/test8_setview_write_read_nproc"
)

declare -a preexisting_file=("false" "true")

if [ "$CLARISSE_COLLECTIVE" == "listio" ]
then
     declare -a vbtype=("lazy")
#Test:                            1    2    3    4    5    6    7    8 
    declare -a buffer_size=(    "16" "16" "32" "32" "32" "16" "32" "16") 
    declare -a bufpool_size=(    "2" " 2"  "2"  "1"  "2"  "2"  "2"  "2")
    declare -a net_buf_size=(   "32" "32" "32" "32" "32" "32" "24" "12")
    declare -a max_dcache_size=( "2" " 2"  "2"  "1"  "2"  "2"  "2"  "2")

else
    declare -a vbtype=("lazy" "eager")
#Test:                             1    2    3    4    5    6    7    8 
    declare -a buffer_size=(      "4" " 4" "4"  "4"  "4"  "4"  "4" "16") 
    declare -a bufpool_size=(     "2" " 2" "2"  "1"  "2"  "2"  "2"  "2")
    declare -a net_buf_size=(     "4" " 4" "4"  "4"  "4"  "8"  "3"  "3")
    declare -a max_dcache_size=(  "2" " 2" "2"  "1"  "2"  "2"  "2"  "2")
fi


#for j in $(seq ${#tests[@]})
for j in ${run_tests}
do
    let j=j-1
    export CLARISSE_BUFFER_SIZE=${buffer_size[${j}]}
    export CLARISSE_BUFPOOL_SIZE=${bufpool_size[${j}]}
    export CLARISSE_NET_BUF_SIZE=${net_buf_size[${j}]}
    export CLARISSE_MAX_DCACHE_SIZE=${max_dcache_size[${j}]}
    #echo $j
    #echo ${CLARISSE_BUFFER_SIZE}
    #echo $CLARISSE_BUFPOOL_SIZE
    #echo $CLARISSE_NET_BUF_SIZE
    for v in `seq $min_nr_servers $max_nr_servers`
    do
	echo CLARISSE_NR_SERVERS=$v
	export CLARISSE_NR_SERVERS=$v
	if [ "$CLARISSE_COUPLENESS" == "intercomm" ]
	then
		min_processes=$((v+1))	
	else
		min_processes=$v
	fi 
	for i in `seq $min_processes $max_processes`
	do
	    #echo i=$i v=$v
	    for k in "${vbtype[@]}"
	    do
		export CLARISSE_LAZY_EAGER_VIEW=$k
		if [ "$j" == "./test8_setview_write_read_nproc" ]
		then
		    for l in "${preexisting_file[@]}"
		    do
			export TEST_PREEXISTING_FILE=$l
			#echo $v 
			${MPIEXEC} -n $i ${tests[${j}]} ${FILENAME} ${COLL_INDEP}
			if [ $? -ne 0 ]
			then
			    exit $?
			fi
		    done
		else
		    #echo $v 
		    ${MPIEXEC} -n $i ${GDB} ${tests[${j}]} ${FILENAME} ${COLL_INDEP}
		    if [ $? -ne 0 ]
		    then
			exit $?
		    fi
		fi
	    done
	done
    done
done
