#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include "target.h"
#include "dcache.h"
#include "buffer_pool.h"
#include "error.h"
#include "hdr.h"
#include "util.h"


void clarisse_target_init(clarisse_target_t *tgt, const char *name, int global_descr, int type, distrib* distr, void * handle){
  tgt->global_descr = global_descr;
  tgt->type = type;
  strcpy(tgt->name, name);
  tgt->distr = distr;
  switch (type) {
  case CLARISSE_TARGET_FILE:
    if (handle)
      tgt->local_handle.fd = *((int *) handle);
    else {
      if ((tgt->local_handle.fd = open(name, O_CREAT|O_RDWR, 0666)) < 0) {
	//char err_msg[1024]; 
	perror(__FUNCTION__);
	fprintf(stderr, "Error when opening file %s\n", name);
	//handle_err(MPI_ERR_OTHER, (char*)err_msg);
	handle_err(MPI_ERR_OTHER, "Error when opening file\n");
      }
    }
    break;
  case CLARISSE_TARGET_FILE_FUTURE:
    if (handle)
      tgt->local_handle.fd = *((int *) handle);
    break;

  case CLARISSE_TARGET_MEMORY:
    tgt->local_handle.buf = (char *) handle;
    break;
  case CLARISSE_TARGET_VIEW:
  case CLARISSE_TARGET_MPI_FILE:
    tgt->local_handle.mpi_fh = handle;
    break;
  }
}


clarisse_target_t * clarisse_target_alloc_init(const char *name, int global_descr, int type, distrib* distr, void * handle){
  clarisse_target_t *tgt;
  
  tgt = (clarisse_target_t *) clarisse_malloc(sizeof(clarisse_target_t));
  clarisse_target_init(tgt, name, global_descr, type, distr, handle);
  return tgt;
}

// Looks up a table 
// if target found, return it 
// else alloc, init, insert into table and return it
clarisse_target_t * clarisse_target_find_alloc_init_add(clarisse_target_t **target_map, char *name, int type, distrib* distr, void * handle){
  clarisse_target_t *tgt = NULL;
  int global_desc;

  global_desc = clarisse_hash((unsigned char *) name);
  HASH_FIND_INT(*target_map, &global_desc, tgt);
  if (!tgt) {
    tgt = clarisse_target_alloc_init(name, global_desc, type, distr, handle);
    HASH_ADD_INT(*target_map, global_descr, tgt); 
  }
  return tgt;
}

clarisse_target_t * clarisse_target_clone(clarisse_target_t *orig_tgt){
  clarisse_target_t *tgt;

  tgt = (clarisse_target_t *) clarisse_malloc(sizeof(clarisse_target_t));
  tgt->global_descr = orig_tgt->global_descr;
  tgt->type = orig_tgt->type;
  strcpy(tgt->name, orig_tgt->name);
  tgt->distr = orig_tgt->distr;
  switch (orig_tgt->type) {
  case CLARISSE_TARGET_FILE:
    tgt->local_handle.fd = orig_tgt->local_handle.fd;
    //tgt->fsize_ondisk = tgt->fsize_real = -1;
    break;
  case CLARISSE_TARGET_MEMORY:
    tgt->local_handle.buf = orig_tgt->local_handle.buf;
    //tgt->fsize_ondisk = tgt->fsize_real = -1;
    break;
  case CLARISSE_TARGET_MPI_FILE:
    tgt->local_handle.mpi_fh = orig_tgt->local_handle.mpi_fh;
    //tgt->fsize_ondisk = orig_tgt->fsize_ondisk; 
    //tgt->fsize_real = orig_tgt->fsize_real;
    break;
  }
  return tgt;
}


/*
void clarisse_target_free_content(clarisse_target_t *tgt){
  free_datatype(tgt->distr->flat->id);
  distrib_free_content(tgt->distr);
}


void clarisse_target_free(clarisse_target_t *tgt){
  clarisse_target_free_content(tgt);
  clarisse_free(tgt);
}
*/
