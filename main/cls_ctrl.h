#ifdef CLARISSE_CONTROL
#ifndef CLS_CTRL_H
#define CLS_CTRL_H

#include "mpi.h"
#include "list.h" 
#include "clarisse_internal.h"
#include "cls_pub_sub.h" 

typedef struct {
  int rank;
  MPI_Comm comm;
  int nprocs;
} cls_controller_t;

typedef struct {
  int rank;
  int scheduled_client;
  dllist request_queue;
} cls_global_controller_t;

typedef struct {
  struct dllist_link link;
  int client_id;
  int rank;
} cls_ctrl_request_t;
 

/*
#define CLARISSE_CTRL_TAG 29092014
#define CLARISSE_CTRL_MSG_DONE 0
*/

#define CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY 1
#define CLARISSE_CTRL_MSG_SUBSCRIBE_LOAD 2
#define CLARISSE_CTRL_MSG_SUBSCRIBE_HEARTBEAT 3

#define CLARISSE_CTRL_MSG_PUBLISH_MEMORY 11
#define CLARISSE_CTRL_MSG_PUBLISH_LOAD 12
#define CLARISSE_CTRL_MSG_PUBLISH_HEARTBEAT 13
#define CLARISSE_CTRL_MSG_PUBLISH_FILE_WRITE_TIME 14
// Controller(C) -> Global Controller (GC) CLARISSE_CTRL_MSG_IO_REQUEST_START
// GC->C CLARISSE_CTRL_MSG_IO_SCHEDULE
//    schedule
// C->GC CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED
#define CLARISSE_CTRL_MSG_IO_REQUEST_START 20
#define CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED 21
#define CLARISSE_CTRL_MSG_IO_SCHEDULE 22 

// LC-> C  CLARISSE_CTRL_MSG_IO_SERVER_DOWN(active_log_ios_rank) 
// C -> LC CLARISSE_CTRL_MSG_IO_STOP(active_log_ios_rank)
// LC: compute the new server map
// LC -> C CLARISSE_CTRL_MSG_IO_CURRENT_COLL_OP_SEQ(coll_op_seq)
// C: last_coll_op_seq = MAX_ALL(coll_op_seq)
// C -> LC CLARISSE_CTRL_MSG_IO_RESUME(last_coll_op_seq)
#define CLARISSE_CTRL_MSG_IO_SERVER_DOWN 30
#define CLARISSE_CTRL_MSG_IO_STOP 31
#define CLARISSE_CTRL_MSG_IO_RESUME 32
#define CLARISSE_CTRL_MSG_IO_CURRENT_COLL_OP_SEQ 33 



typedef struct {  
  int type;
  int memory;
} clarisse_ctrl_msg_memory_t;

typedef struct {  
  int type;
  double time;
} clarisse_ctrl_msg_file_write_time_t;

typedef struct {  
  int type;
  int client_id;
} clarisse_ctrl_msg_io_request_start_t;

// resilience and load balance
typedef struct {  
  int type;
  int active_log_ios_rank;
} clarisse_ctrl_msg_io_server_down_t;

typedef struct {  
  int type;
  int active_log_ios_rank;
} clarisse_ctrl_msg_io_stop_t;

typedef struct {  
  int type;
  int last_coll_op_seq;
} clarisse_ctrl_msg_io_resume_t;

typedef struct {  
  int type;
  int coll_op_seq;
} clarisse_ctrl_msg_io_current_coll_op_seq_t;

typedef struct {  
  int type;
  int last_coll_op_seq;
} clarisse_ctrl_msg_io_last_coll_op_seq_t;


/*
enum {
  CLARISSE_CTRL_EVENT_DONE = 0,
  CLARISSE_CTRL_EVENT_MEMORY, 
  CLARISSE_CTRL_EVENT_LOAD,
  CLARISSE_CTRL_EVENT_COUNT
};
*/

int cls_ctrl_init();
int cls_ctrl_finalize();

#endif  /* CLS_CTRL_H */
#endif /* CLARISSE_CONTROL */
