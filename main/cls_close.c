#include "client_iocontext.h"
#include "server_iocontext.h"
#include "buffer_pool.h"
#include "hdr.h"
#include "buffer.h"
#include "util.h"
#include "error.h"

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern server_global_state_t server_global_state;
extern client_global_state_t client_global_state;
extern int i_am_client;
extern int i_am_server;

int cli_close(client_iocontext_t *c_ctxt);
void serv_close(client_iocontext_t *c_ctxt);

int MPI_File_close(MPI_File *fh) {
  int ret = MPI_SUCCESS;
#ifdef CLARISSE_TIMING
  local_timing.time_close[local_timing.cnt_close][0] = MPI_Wtime();
#endif   
  
  if (i_am_client) {
    client_iocontext_t *c_ctxt;

#ifdef CLARISSE_DEBUG
    printf("MPI_File_close\n");
#endif
    c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)*fh);
    
    if (c_ctxt) {
      if (clarisse_am_i_coupled_server(c_ctxt)){
	serv_close(c_ctxt);
      }  
      ret = cli_close(c_ctxt);  
    }
    else
      ret = PMPI_File_close(fh);
  }
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_close < CLARISSE_MAX_CNT_TIMED_OPS) {
    local_timing.time_close[local_timing.cnt_close][1] = MPI_Wtime();
    local_timing.cnt_close++;
  }
#endif  
  
  return ret;
}

int  cli_close(client_iocontext_t *c_ctxt) {
  int ret = 0;

  if ((c_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) &&
      (c_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_CLOSE)) {
    int myrank;
    MPI_Comm_rank(client_global_state.intracomm, &myrank);  
    MPI_Barrier(client_global_state.intracomm);
    if (myrank == 0) {

      int log_ios_rank, i;
      MPI_Request *requests;
      int errn;
      MPI_Status status;
      char **irq;
      
      //printf("CLIENT sends FLUSH to server\n"); 
      requests = (MPI_Request *) clarisse_malloc(client_global_state.active_nr_servers * sizeof(MPI_Request));
      irq = clarisse_malloc(sizeof(char *) * client_global_state.active_nr_servers); 
      for (log_ios_rank = 0, i = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++) {
	if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	  struct rq_file_flush *rq;
	  
	  rq = clarisse_malloc(sizeof(struct rq_file_flush)); 
	  rq->global_descr = c_ctxt->global_descr;
	  irq[i] = (char *) rq; 
	  
	  ret = MPI_Isend(irq[i], sizeof(struct rq_file_flush), MPI_BYTE, client_global_state.log2ph[log_ios_rank], RQ_FILE_FLUSH, client_global_state.initial_cli_serv_intercomm, requests + i);
	  if (ret != MPI_SUCCESS) {
	    handle_error(ret, "Error when sending the reqs");
	    return ret;
	  }
	  i++;
	}
      }
      errn = 0;
      for (log_ios_rank = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++) {
	if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	  struct rt_file_flush fd_ret;
	  ret = MPI_Recv(&fd_ret, sizeof(struct rt_file_delete), MPI_BYTE,  client_global_state.log2ph[log_ios_rank], RT_FILE_FLUSH, client_global_state.initial_cli_serv_intercomm, &status);
	  if (ret != MPI_SUCCESS) {
	    handle_error(ret, "cls_close: Error receiving the flush rets");
	    return ret;
	  }
	  errn = MIN(errn, fd_ret.errn);
	}
      }
      MPI_Waitall(client_global_state.active_nr_servers, requests, MPI_STATUSES_IGNORE);
      if (ret != MPI_SUCCESS)
	handle_error(ret, "cls_close: waitall error\n");
      clarisse_free(requests);
      if (errn == 0)
	ret = MPI_SUCCESS;
      else
	ret = MPI_ERR_FILE;
    }
  }
  //MPI_Barrier(client_global_state.intracomm);
  //cls_adio_close(c_ctxt);
  HASH_DEL(client_global_state.c_ctxt_map, c_ctxt);  
  // Wait for flushing to end
  if (!(c_ctxt->mpi_access_mode & MPI_MODE_RDONLY))
    MPI_Barrier(c_ctxt->intracomm);
  client_iocontext_free(c_ctxt); 
  return ret;
}

void serv_close(client_iocontext_t *c_ctxt) {
  server_iocontext_t *s_ctxt;
  clarisse_target_t *tgt;

  s_ctxt = server_iocontext_find_del(c_ctxt->global_descr);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif

  if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK){
    tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 1);
    tpool_wait_for_task_queue_empty(server_global_state.tpool, (void*)s_ctxt);
    tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 0);
    //    tpool_task_queue_destroy(server_global_state.tpool, (void*)s_ctxt);
    dcache_atomic_bufpool_move_all(&s_ctxt->dcache, server_global_state.buf_pool);
#ifdef CLARISSE_DCACHE_DEBUG
    dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  }
  else
    flush_buffers(s_ctxt);
  // Find and free target
  HASH_FIND_INT(server_global_state.active_targets, &c_ctxt->global_descr, tgt);
  HASH_DEL(server_global_state.active_targets, tgt);
  distrib_free(tgt->distr);
  clarisse_free(tgt);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  // Free context
  server_iocontext_free(s_ctxt);
  //printf("******************************************CLOSE finish\n");
}
