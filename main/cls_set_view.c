#include <assert.h>
#include "client_iocontext.h"
#include "server_iocontext.h"
#include "hdr.h"
#include "error.h"
#include "marshal.h"
#include "util.h"

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern server_global_state_t server_global_state;
extern client_global_state_t client_global_state;

void serv_setview(client_iocontext_t *c_ctxt);

int MPI_File_set_view(MPI_File fh, MPI_Offset disp, MPI_Datatype etype, MPI_Datatype filetype, const char *datarep, MPI_Info info) {
  int ret;
  client_iocontext_t *c_ctxt;

#ifdef CLARISSE_TIMING
  local_timing.time_setview[local_timing.cnt_setview][0] = MPI_Wtime();
#endif   

#ifdef CLARISSE_SAVE_TYPE_IN_FILE
  printf("SETVIEW: DISPL=%lld ", disp); 
  print_dt(filetype);
#endif
  
  c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
  if (c_ctxt) {
    int log_ios_rank, etype_size;
    
    c_ctxt->disp = disp;
    c_ctxt->etype = etype;
    c_ctxt->filetype = filetype;
    MPI_Type_size(etype, &etype_size);
    c_ctxt->etype_size = (MPI_Count) etype_size;
    distrib_free(c_ctxt->intermediary_target->distr);
    c_ctxt->intermediary_target->distr = distrib_get(filetype, disp);
#ifdef CLARISSE_VIEW_DEBUG
    int rank;
    MPI_Comm_rank(fd->comm, &rank);
    fprintf(stderr, "Proc %d (NON-AGGREG) ",rank);
    distrib_print(c_ctxt->intermediary_target->distr);
#endif
    if ((c_ctxt->clarisse_pars.lazy_eager_view == CLARISSE_LAZY_VIEW) ||
	(c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)){
      for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++)
	c_ctxt->view_send[log_ios_rank] = 0;
    } 
    else {
      int ret, nprocs, myrank, i;  
      MPI_Request *requests;
      char *buf;
      struct rq_file_set_view  *rq;
      char **irq;
      
      MPI_Comm_size(c_ctxt->intracomm, &nprocs);
      MPI_Comm_rank(c_ctxt->intracomm, &myrank);
      requests = (MPI_Request *) clarisse_malloc(client_global_state.active_nr_servers * sizeof(MPI_Request));
      irq = clarisse_malloc(sizeof(char *) * client_global_state.active_nr_servers); 
      for (log_ios_rank = 0, i = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
	if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	  int total_cnt;
	  
	  total_cnt = sizeof(struct rq_file_set_view);
	  irq[i] = (char *) clarisse_malloc(sizeof(struct rq_file_set_view) + CLARISSE_MAX_VIEW_SIZE + CLARISSE_MAX_TARGET_NAME_SIZE); 
	  rq = (struct rq_file_set_view *)irq[i];
	  rq->global_descr = c_ctxt->global_descr;
	  rq->rank = myrank;
	  rq->nprocs = nprocs;
	  if (c_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)
	    rq->partitioning_stride_factor = client_global_state.active_nr_servers;
	  else
	    rq->partitioning_stride_factor = 1; 	
	  rq->displ = disp;
	  //	  rq->log_ios_rank = log_ios_rank;
	  
	  if (c_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) {
	    strcpy(((char *)rq) + total_cnt, c_ctxt->filename);
	    rq->target_name_size = strlen(c_ctxt->filename) + 1;
	    assert(rq->target_name_size < CLARISSE_MAX_TARGET_NAME_SIZE);	  
	    total_cnt += (strlen(c_ctxt->filename) + 1);
	  }
	  else
	    rq->target_name_size = 0;
	  buf = ((char*)rq) + total_cnt;
	  rq->view_size = decode_pack_dt_out_buf(filetype, &buf);
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
	  save_type_in_file("VIEW", buf, rq->view_size, rq->displ);
#endif
	  if (rq->view_size > CLARISSE_MAX_VIEW_SIZE)
	    handle_error(MPI_ERR_OTHER,"SETVIEW EAGER: View size larger than allowed\n");
	  total_cnt += rq->view_size;
	  
	  ret = MPI_Isend(irq[i], total_cnt, MPI_BYTE, client_global_state.log2ph[log_ios_rank], RQ_FILE_SET_VIEW, c_ctxt->cli_serv_comm, requests + i);
	  if (ret != MPI_SUCCESS) {
	    handle_err(ret, "Error when sending the reqs");
	    return ret;
	  }
	  i++; // only if the server is active
	  c_ctxt->view_send[log_ios_rank] = 1;
	}
      }
      assert (i == client_global_state.active_nr_servers);
      if (clarisse_am_i_coupled_server(c_ctxt)) { 
	serv_setview(c_ctxt);
      }
      
      MPI_Waitall(client_global_state.active_nr_servers, requests, MPI_STATUSES_IGNORE);
      if (ret != MPI_SUCCESS)
	handle_err(ret, "setview: waitall error\n");
      for (log_ios_rank = 0, i = 0; log_ios_rank <  client_global_state.clarisse_pars.nr_servers; log_ios_rank++) 
	if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	  clarisse_free(irq[i]);
	  i++;
	}
      clarisse_free(irq);
      clarisse_free(requests);
    }
    ret = MPI_SUCCESS;
  }
  else {// ROMIO 
    if (client_global_state.clarisse_pars.collective == CLARISSE_ROMIO_COLLECTIVE)
      ret = PMPI_File_set_view(fh, disp, etype, filetype, datarep, info);
    else {
      fprintf(stderr, "%s: Clarisse error: the file has not been correctly open and the set_view method is not ROMIO\n", __FUNCTION__);
      ret = MPI_ERR_OTHER;
    }
  }
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_setview < CLARISSE_MAX_CNT_TIMED_OPS) {
    local_timing.time_setview[local_timing.cnt_setview][1] = MPI_Wtime();
    local_timing.cnt_setview++;
  }
#endif  
  
  return ret;
}


void serv_setview(client_iocontext_t *c_ctxt) {
  int nprocs, i;
  MPI_Comm_size(c_ctxt->intracomm, &nprocs);
  
  for (i = 0; i < nprocs; i++) {
    int count, ret;
    struct rq_file_set_view *r;
    MPI_Status s;
    server_iocontext_t * s_ctxt;
    MPI_Datatype dt;
    clarisse_target_t *tgt;
    
    ret = MPI_Recv(server_global_state.net_buf, server_global_state.clarisse_pars.net_buf_size, MPI_BYTE, MPI_ANY_SOURCE, RQ_FILE_SET_VIEW, c_ctxt->cli_serv_comm, &s);
    if (ret != MPI_SUCCESS)
      handle_err(ret, "Error in receiving");
    r = (struct rq_file_set_view *)server_global_state.net_buf;
    HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
    s_ctxt = server_iocontext_find(r->global_descr);
    if (!s_ctxt)
       s_ctxt = server_iocontext_init(r->global_descr);
    server_iocontext_update(s_ctxt, tgt, 0 /*client_idx*/, r->partitioning_stride_factor, r->nprocs);

    MPI_Get_count(&s, MPI_BYTE, &count);
    dt = reconstruct_dt((char *)(r + 1) + r->target_name_size, r->view_size);
    distrib_update(s_ctxt->client_rt_info[0].remote_view_distr[r->rank], dt, r->displ);
#ifdef CLARISSE_VIEW_DEBUG
    int rank;
    MPI_Comm_rank(fd->cli_serv_comm, &rank);
    fprintf(stderr, "Proc %d (AGGREG) RECEIVED FROM %d ", rank, r->rank);
    distrib_print(s_ctxt->client_rt_info[0].remote_view_distr[r->rank]);
#endif
  }	    
}
