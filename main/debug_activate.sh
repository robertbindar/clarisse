

if [ "$#" -ne 1 ]
then
    echo "Run this script with one argument:" $0 "[vm/howard]"
    exit
fi

rm Makefile
ln -s Makefile.$1.debug Makefile
rm ../map/Makefile
ln -s ../map/Makefile.$1.debug ../map/Makefile
rm ../marshal/Makefile
ln -s ../marshal/Makefile.$1.debug ../marshal/Makefile
rm ../util/Makefile
ln -s ../util/Makefile.$1.debug ../util/Makefile
make clean
make
