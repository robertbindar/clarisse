#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include "client_iocontext.h"
#include "error.h"
#include "util.h"
#include "server.h"
#include "hdr.h"
#include "aggr.h"
#ifdef CLARISSE_CONTROL
#include "cls_ctrl.h"
#endif 

#ifdef CLARISSE_TIMING
clarisse_timing_t local_timing;
#endif

server_global_state_t server_global_state;
client_global_state_t client_global_state;
int i_am_client = 0;
int i_am_server = 0;
int *client_nprocs = NULL;
int nr_clients = 1;

#ifdef CLARISSE_MEM_COUNT
extern clarisse_mem_count_t mem_count;
extern int multithreaded;
#endif 

char ** get_server_params(clarisse_params *clarisse_pars) {
  int i;
  char **argvv, *argv;

  argvv = (char **) clarisse_malloc(CLARISSE_SERVER_PARAMS_CNT * sizeof(char *));
  argv = (char *) clarisse_malloc(CLARISSE_MAX_PARAM_SIZE * CLARISSE_SERVER_PARAMS_CNT);
  for (i = 0; i < CLARISSE_SERVER_PARAMS_CNT; i++)
    argvv[i] = argv + i * CLARISSE_MAX_PARAM_SIZE;
  sprintf(argvv[0], "%d", clarisse_pars->buffer_size);
  sprintf(argvv[1], "%d", clarisse_pars->bufpool_size);
  sprintf(argvv[3], "%d", clarisse_pars->net_buf_size);
  sprintf(argvv[4], "%d", clarisse_pars->net_datatype_count);
  sprintf(argvv[5], "%d", clarisse_pars->net_datatype_incr);
  sprintf(argvv[6], "%d", clarisse_pars->metadata_transfer_size);
  sprintf(argvv[7], "%hd", clarisse_pars->io_sched_policy);
  sprintf(argvv[8], "%hd", clarisse_pars->lazy_eager_view);
  sprintf(argvv[9], "%hd", clarisse_pars->prefetch);
  sprintf(argvv[10], "%hd", clarisse_pars->write_type);
  argvv[11] = NULL;
  return argvv;
}



void clarisse_param_init() {
  char *val;
  
  if ((val = getenv("CLARISSE_SERVER_PATH")))
    strcpy(client_global_state.clarisse_pars.server_path, val);
  else
    strcpy(client_global_state.clarisse_pars.server_path,"\0");

  if ((val = getenv("CLARISSE_BUFFER_SIZE")))
    client_global_state.clarisse_pars.buffer_size = atoi(val); 
  else
    client_global_state.clarisse_pars.buffer_size = CLARISSE_DEFAULT_BUFFER_SIZE;
  
  if ((val = getenv("CLARISSE_BUFPOOL_SIZE")))
    client_global_state.clarisse_pars.bufpool_size = atoi(val); 
  else
    client_global_state.clarisse_pars.bufpool_size = CLARISSE_DEFAULT_BUFPOOL_SIZE;
  
  if ((val = getenv("CLARISSE_MAX_DCACHE_SIZE")))
    client_global_state.clarisse_pars.max_dcache_size = atoi(val); 
  else
    client_global_state.clarisse_pars.max_dcache_size = CLARISSE_DEFAULT_MAX_DCACHE_SIZE;

  if ((val = getenv("CLARISSE_METADATA_TRANSFER_SIZE")))
    client_global_state.clarisse_pars.metadata_transfer_size = atoi(val); 
  else
    client_global_state.clarisse_pars.metadata_transfer_size = CLARISSE_DEFAULT_NET_DATATYPE_INCR;

  // Add maximum view size and max metadata size
  if ((val = getenv("CLARISSE_NET_BUF_SIZE"))) 
    client_global_state.clarisse_pars.net_buf_data_size = atoi(val);
  else 
    client_global_state.clarisse_pars.net_buf_data_size = CLARISSE_DEFAULT_NET_BUF_DATA_SIZE;
  client_global_state.clarisse_pars.net_buf_size =  client_global_state.clarisse_pars.net_buf_data_size + client_global_state.clarisse_pars.metadata_transfer_size + CLARISSE_MAX_VIEW_SIZE + CLARISSE_MAX_TARGET_NAME_SIZE;
  

  if ((val = getenv("CLARISSE_NET_DATATYPE_COUNT")))
    client_global_state.clarisse_pars.net_datatype_count = atoi(val); 
  else
    client_global_state.clarisse_pars.net_datatype_count = CLARISSE_DEFAULT_NET_DATATYPE_COUNT;

  if ((val = getenv("CLARISSE_NET_DATATYPE_INCR")))
    client_global_state.clarisse_pars.net_datatype_incr = atoi(val); 
  else
    client_global_state.clarisse_pars.net_datatype_incr = CLARISSE_DEFAULT_NET_DATATYPE_INCR;

  if ((val = getenv("CLARISSE_NR_SERVERS")))
    client_global_state.clarisse_pars.nr_servers = atoi(val); 
  else
    client_global_state.clarisse_pars.nr_servers = CLARISSE_DEFAULT_NR_SERVERS;
  client_global_state.active_nr_servers = client_global_state.clarisse_pars.nr_servers;
  if ((val = getenv("CLARISSE_IO_SCHED_POLICY")))
    client_global_state.clarisse_pars.io_sched_policy = atoi(val); 
  else
    client_global_state.clarisse_pars.io_sched_policy = CLARISSE_DEFAULT_IO_SCHED_POLICY;

  if ((val = getenv("CLARISSE_LAZY_EAGER_VIEW")))
    if (!strcmp(val, "lazy"))
      client_global_state.clarisse_pars.lazy_eager_view = CLARISSE_LAZY_VIEW; 
    else
      if (!strcmp(val, "eager"))
	client_global_state.clarisse_pars.lazy_eager_view = CLARISSE_EAGER_VIEW; 
      else
	client_global_state.clarisse_pars.lazy_eager_view = CLARISSE_DEFAULT_LAZY_EAGER_VIEW;
  else
    client_global_state.clarisse_pars.lazy_eager_view = CLARISSE_DEFAULT_LAZY_EAGER_VIEW;

  if ((val = getenv("CLARISSE_PREFETCH")))
    if (!strcmp(val, "enable"))
      client_global_state.clarisse_pars.prefetch = CLARISSE_PREFETCH;
    else
      client_global_state.clarisse_pars.prefetch = CLARISSE_NO_PREFETCH;
  else
    client_global_state.clarisse_pars.prefetch = CLARISSE_DEFAULT_PREFETCH;

  if ((val = getenv("CLARISSE_WRITE_TYPE")))
    if (!strcmp(val, "back")) 
      client_global_state.clarisse_pars.write_type = CLARISSE_WRITE_BACK;
    else
      if (!strcmp(val, "ondemand"))
	client_global_state.clarisse_pars.write_type = CLARISSE_WRITE_ON_DEMAND;
      else
	client_global_state.clarisse_pars.write_type = CLARISSE_WRITE_ON_CLOSE;
  else
    client_global_state.clarisse_pars.write_type = CLARISSE_DEFAULT_WRITE_TYPE;

  if ((val = getenv("CLARISSE_COUPLENESS"))) {
    if (!strcmp(val, "dynamic"))
      client_global_state.clarisse_pars.coupleness = CLARISSE_DYN_PROCESS;
    else
      if (!strcmp(val, "intercomm"))
	client_global_state.clarisse_pars.coupleness = CLARISSE_INTERCOMM;
      else
	if (!strcmp(val, "coupled"))
	  client_global_state.clarisse_pars.coupleness = CLARISSE_COUPLED;
	else
	  handle_err(MPI_ERR_OTHER, "CLARISSE_COUPLENESS: Unsupported value");
  }
  else
    client_global_state.clarisse_pars.coupleness = CLARISSE_COUPLED;

  if ((val = getenv("CLARISSE_SERVER_MAP"))){
    if (!strcmp(val, "romio"))
      client_global_state.clarisse_pars.server_map = CLARISSE_SERVER_MAP_ROMIO;
    else
      if (!strcmp(val, "topology_exclude_bridge")) {
	client_global_state.clarisse_pars.server_map = CLARISSE_SERVER_MAP_TOPOLOGY_EXCLUDE_BRIDGE;
        if ((val = getenv("CLARISSE_BGQ_AGGREGATOR_SELECTION_STEP")))
	   client_global_state.clarisse_pars.aggregator_selection_step = atoi(val);
	else
	   handle_err(MPI_ERR_OTHER, "CLARISSE_SERVER_MAP=topology_exclude_bridge: define CLARISSE_BGQ_AGGREGATOR_SELECTION_STEP as an environment var");
	if ((val = getenv("CLARISSE_BGQ_BG_NODES_PSET")))
           client_global_state.clarisse_pars.bg_nodes_pset = atoi(val);
        else
           handle_err(MPI_ERR_OTHER, "CLARISSE_SERVER_MAP=topology_exclude_bridge: define CLARISSE_BGQ_BG_NODES_PSET as an environment var");
      }
      else 
	if (!strcmp(val, "topology_include_bridge")) {
	  client_global_state.clarisse_pars.server_map = CLARISSE_SERVER_MAP_TOPOLOGY_INCLUDE_BRIDGE;	
          if ((val = getenv("CLARISSE_BGQ_BG_NODES_PSET")))
            client_global_state.clarisse_pars.bg_nodes_pset = atoi(val);
          else           
             handle_err(MPI_ERR_OTHER, "CLARISSE_SERVER_MAP=topology_include_bridge: define CLARISSE_BGQ_BG_NODES_PSET as an environment var");
        }
        else {
	  if (!strcmp(val, "random"))
	    client_global_state.clarisse_pars.server_map = CLARISSE_SERVER_MAP_RANDOM;
	  else
	    if (!strcmp(val, "uniform"))
	  	client_global_state.clarisse_pars.server_map = CLARISSE_SERVER_MAP_UNIFORM;
  	    else
		handle_err(MPI_ERR_OTHER, "CLARISSE_SERVER_MAP: Unsupported value");
	}
  }
  else
    client_global_state.clarisse_pars.server_map = CLARISSE_DEFAULT_SERVER_MAP;
 
  if ((val = getenv("CLARISSE_FILE_PARTITIONING"))){
    if (!strcmp(val, "dynamic"))
      client_global_state.clarisse_pars.file_partitioning = CLARISSE_DYNAMIC_FILE_PARTITIONING;
    else
      if (!strcmp(val, "static"))
      	client_global_state.clarisse_pars.file_partitioning = CLARISSE_STATIC_FILE_PARTITIONING;
      else
        handle_err(MPI_ERR_OTHER, "CLARISSE_FILE_PARTITIONING: Unsupported value");
  }
  else
    client_global_state.clarisse_pars.file_partitioning = CLARISSE_DEFAULT_FILE_PARTITIONING;

  if ((val = getenv("CLARISSE_COLLECTIVE"))){
    if (!strcmp(val, "vb"))
      client_global_state.clarisse_pars.collective = CLARISSE_VB_COLLECTIVE;
    else
      if (!strcmp(val, "romio"))
	client_global_state.clarisse_pars.collective = CLARISSE_ROMIO_COLLECTIVE;
      else
	if (!strcmp(val, "listio"))
	  client_global_state.clarisse_pars.collective = CLARISSE_LISTIO_COLLECTIVE;
	else
	  handle_err(MPI_ERR_OTHER, "CLARISSE_COLLECTIVE: Unsupported value");
  }
  else
    client_global_state.clarisse_pars.collective = CLARISSE_VB_COLLECTIVE;
  
  if ((val = getenv("CLARISSE_AGGR_THREAD")))
    if (!strcmp(val, "thread"))
      client_global_state.clarisse_pars.aggr_thread = CLARISSE_AGGR_THREAD; 
    else
      if (!strcmp(val, "process"))
	client_global_state.clarisse_pars.aggr_thread = CLARISSE_AGGR_PROCESS; 
      else
	client_global_state.clarisse_pars.aggr_thread = CLARISSE_DEFAULT_AGGR_THREAD;
  else
    client_global_state.clarisse_pars.aggr_thread = CLARISSE_DEFAULT_AGGR_THREAD;
  if ((val = getenv("CLARISSE_CONTROL")))
    if (!strcmp(val, "enable"))
      client_global_state.clarisse_pars.control = CLARISSE_CONTROL_ENABLED;
    else
      client_global_state.clarisse_pars.control = CLARISSE_CONTROL_DISABLED;
  else
    client_global_state.clarisse_pars.control = CLARISSE_DEFAULT_CONTROL;

  if ((val = getenv("CLARISSE_GLOBAL_SCHEDULING")))
    if (!strcmp(val, "fcfs"))
      client_global_state.clarisse_pars.global_scheduling = CLARISSE_GLOBAL_SCHEDULING_FCFS;
    else
      client_global_state.clarisse_pars.global_scheduling = CLARISSE_GLOBAL_SCHEDULING_NULL;
  else
    client_global_state.clarisse_pars.global_scheduling = CLARISSE_DEFAULT_GLOBAL_SCHEDULING;

  if ((val = getenv("CLARISSE_ELASTICITY")))
    if (!strcmp(val, "enable"))
      client_global_state.clarisse_pars.elasticity = CLARISSE_ELASTICITY_ENABLED;
    else
      client_global_state.clarisse_pars.elasticity = CLARISSE_ELASTICITY_DISABLED;
  else
    client_global_state.clarisse_pars.elasticity = CLARISSE_DEFAULT_ELASTICITY;

  if ((val = getenv("CLARISSE_READ_FUTURE")))
    if (!strcmp(val, "enable"))
      client_global_state.clarisse_pars.read_future = CLARISSE_READ_FUTURE_ENABLED;
    else
      client_global_state.clarisse_pars.read_future = CLARISSE_READ_FUTURE_DISABLED;
  else
    client_global_state.clarisse_pars.read_future = CLARISSE_DEFAULT_READ_FUTURE;
  
  client_global_state.load_injection_coll_seq = -1;
  client_global_state.load_detection_coll_seq = -1;
   client_global_state.loaded_active_log_ios_rank  = -1;
  if ((val = getenv("CLARISSE_LOAD_INJECTION_PHASE"))) {
    int err = 0, myrank;
    int load_injection_phase, load_detection_phase, active_log_ios_rank;
    double load; // seconds

    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    if (client_global_state.clarisse_pars.control != CLARISSE_CONTROL_ENABLED) {
      if (myrank == 0)
	fprintf(stderr,"CLARISSE_LOAD_INJECTION_PHASE envvar given but CLARISSE_CONTROL is DISABLED\n");
      err = 1;
    }
    else 
      load_injection_phase = atoi(val);
    if ((val = getenv("CLARISSE_SERVER_LOAD")))
      load = atof(val);
    else {
      if (myrank == 0)
	fprintf(stderr,"CLARISSE_LOAD_INJECTION_PHASE envvar given but CLARISSE_SERVER_LOAD not given\n");
	err = 1;
    }
    if ((val = getenv("CLARISSE_LOADED_SERVER")))
      active_log_ios_rank = atoi(val);
    else {
      if (myrank == 0)
	fprintf(stderr,"CLARISSE_LOAD_INJECTION_PHASE envvar given but CLARISSE_LOADED_SERVER not given\n");
      err = 1;
    }
    if ((val = getenv("CLARISSE_LOAD_DETECTION_PHASE")))
      load_detection_phase = atoi(val);
    else {
      if (myrank == 0)
	fprintf(stderr,"CLARISSE_LOAD_INJECTION_PHASE envvar given but CLARISSE_LOAD_DETECTION_PHASE not given\n");
      err = 1;
    }
    if (err) {
      PMPI_Finalize();
      exit(1);
    }
    if (myrank == 0)
      printf(" load_injection_phase=%d\n load_detection_phase=%d\n active_log_ios_rank=%d\n load=%7.5f\n",load_injection_phase, load_detection_phase, active_log_ios_rank, load);
    cls_set_load_injection_coll_seq(load_injection_phase, active_log_ios_rank, load);
    cls_set_load_detection_coll_seq(load_detection_phase);
  }

  // Add maximum view size and max metadata size
  if ((val = getenv("CLARISSE_ELASTICITY_POLICY"))) 
    client_global_state.clarisse_pars.elasticity_policy = atoi(val);
  else 
    client_global_state.clarisse_pars.elasticity_policy = 0;
  

#ifdef CLARISSE_MEM_COUNT 
  if ((client_global_state.clarisse_pars.write_type == CLARISSE_WRITE_BACK) ||
      (client_global_state.clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD))
    multithreaded = 1;
#endif
}


void clarisse_param_print(clarisse_params *clarisse_pars){
  
  printf("CLARISSE PARAMS:\n");
  if ((clarisse_pars->collective == CLARISSE_VB_COLLECTIVE) ||
      (clarisse_pars->collective == CLARISSE_LISTIO_COLLECTIVE)){
    if (clarisse_pars->collective == CLARISSE_LISTIO_COLLECTIVE)
      printf("\t  CLARISSE_COLLECTIVE = listio\n");
    else {
      printf("\t  CLARISSE_COLLECTIVE = view-based\n");
      printf("\t  CLARISSE_LAZY_EAGER_VIEW = %s\n", ((clarisse_pars->lazy_eager_view == CLARISSE_LAZY_VIEW)?"lazy":"eager"));
    }
    if (client_global_state.clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD)
      printf("\t  CLARISSE_AGGR_THREAD = thread\n");
    else
      printf("\t  CLARISSE_AGGR_THREAD = process\n");
    printf("\t  CLARISSE_BUFFER_SIZE = %d\n",clarisse_pars->buffer_size);
    printf("\t  CLARISSE_NET_BUF_SIZE = %d\n",clarisse_pars->net_buf_size);
    printf("\t  CLARISSE_NR_SERVERS = %d\n",clarisse_pars->nr_servers);
    printf("\t  CLARISSE_MAX_DCACHE_SIZE = %d\n", clarisse_pars->max_dcache_size);
    printf("\t  CLARISSE_BUFPOOL_SIZE = %d\n", clarisse_pars->bufpool_size);
    printf("\t  CLARISSE_WRITE_TYPE = ");
    switch(clarisse_pars->write_type){
    case CLARISSE_WRITE_ON_DEMAND: printf("ondemand\n"); break;
    case CLARISSE_WRITE_ON_CLOSE: printf("onclose\n"); break;
    case CLARISSE_WRITE_BACK: printf("back\n"); break;
    default: printf("N/A (ERROR)\n");
    }
    printf("\t  CLARISSE_SERVER_MAP = ");
    switch(clarisse_pars->server_map){
    case CLARISSE_SERVER_MAP_ROMIO: printf("romio\n"); break;
    case CLARISSE_SERVER_MAP_TOPOLOGY_EXCLUDE_BRIDGE: 
	printf("topology_exclude_bridge\n"); 
	printf("\t  CLARISSE_BGQ_BG_NODES_PSET = %d\n", clarisse_pars->bg_nodes_pset);
	printf("\t  CLARISSE_BGQ_AGGREGATOR_SELECTION_STEP = %d\n", clarisse_pars->aggregator_selection_step);
	break;
    case CLARISSE_SERVER_MAP_TOPOLOGY_INCLUDE_BRIDGE: 
	printf("topology_include_bridge\n"); 
        printf("\t  CLARISSE_BGQ_BG_NODES_PSET = %d\n", clarisse_pars->bg_nodes_pset);
	break;
    case CLARISSE_SERVER_MAP_RANDOM: printf("random\n"); break; 
    case CLARISSE_SERVER_MAP_UNIFORM: printf("uniform\n"); break;
    default: printf("N/A (ERROR)\n");
    }
    printf("\t  CLARISSE_FILE_PARTITIONING = ");
    if (clarisse_pars->file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)
      printf("static\n");
    else
      printf("dynamic\n"); 
    printf("\t  CLARISSE_COUPLENESS = ");
    switch(clarisse_pars->coupleness){
    case CLARISSE_COUPLED: printf("coupled\n"); break;
    case CLARISSE_INTERCOMM: printf("intercomm\n"); break;
    case CLARISSE_DYN_PROCESS: printf("dynamic\n"); break; 
    default: printf("N/A (ERROR)\n");
    }
    printf("\t  CLARISSE_CONTROL = ");
    if (clarisse_pars->control == CLARISSE_CONTROL_ENABLED)
      printf("enabled\n");
    else
      printf("disabled\n");
    printf("\t  CLARISSE_ELASTICITY = ");
    if (clarisse_pars->elasticity == CLARISSE_ELASTICITY_ENABLED) {
      printf("enabled\n"); 
      printf("\t  CLARISSE_ELASTICITY_POLICY = %d\n", clarisse_pars->elasticity_policy);
    }
    else
      printf("disabled\n");
    printf("\t  CLARISSE_GLOBAL_SCHEDULING = ");
    if (clarisse_pars->global_scheduling == CLARISSE_GLOBAL_SCHEDULING_FCFS)
      printf("fcfs\n");
    else
      printf("NULL\n");

    printf("\t  CLARISSE_READ_FUTURE = ");
    if (clarisse_pars->read_future == CLARISSE_READ_FUTURE_ENABLED)
      printf("enabled\n");
    else
      printf("disabled\n");
    
  }
  else 
    printf("\t  CLARISSE_COLLECTIVE = ROMIO\n");	    
  	 		
}

// VERIFICATION OF CORRECTNESS
static void clarisse_params_verification(clarisse_params *clarisse_pars){
  int err, myrank, nprocs;
  /*
  if (clarisse_pars->buffer_size + clarisse_pars->metadata_transfer_size + CLARISSE_MAX_VIEW_SIZE + CLARISSE_MAX_TARGET_NAME_SIZE > clarisse_pars->net_buf_size)
    handle_err(MPI_ERR_OTHER, "Parameter error: buffer_size > net_buf_size");
  */

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  err = 0;
  assert(clarisse_pars->metadata_transfer_size >= sizeof(struct rq_file_set_view));
  assert(clarisse_pars->metadata_transfer_size >= sizeof(struct rq_file_write));
  assert(clarisse_pars->metadata_transfer_size >= sizeof(struct rq_file_read));
  assert(clarisse_pars->metadata_transfer_size >= sizeof(struct rt_file_write));
  assert(clarisse_pars->metadata_transfer_size >= sizeof(struct rt_file_read));
  
  // For ROMIO params should be always coupled
  if (clarisse_pars->server_map == CLARISSE_SERVER_MAP_ROMIO){
    if (clarisse_pars->coupleness != CLARISSE_COUPLED) {
      err = 1;
      if (myrank == 0) 
	fprintf(stderr,"For CLARISSE_SERVER_MAP_ROMIO coupleness != CLARISSE_COUPLED\n");
    }
  }
  // Coupled versions can not have more servers than launched processes
  if (clarisse_pars->coupleness == CLARISSE_COUPLED) {
    if (clarisse_pars->nr_servers > nprocs){
      err = 1;
      if (myrank == 0) 
	fprintf(stderr,"For CLARISSE_COUPLED the number of servers = %d is larger than nprocs = %d\n", clarisse_pars->nr_servers, nprocs); 
    }
  } 
 
  // For intercomm versions can not have more or equal servers than launched processes
  if (clarisse_pars->coupleness == CLARISSE_INTERCOMM) {
    if (clarisse_pars->nr_servers >= nprocs) {
      err = 1;
      if (myrank == 0) 	
	fprintf(stderr,"For CLARISSE_INTERCOMM the number of servers = %d  is larger or equal than nprocs = %d\n", clarisse_pars->nr_servers, nprocs); 
    }
  } 

  // At each server for each file I cannot allocate more than the size of the buffer pool 
  if (clarisse_pars->max_dcache_size > clarisse_pars->bufpool_size){
    err = 1;
    if (myrank == 0) 
      fprintf(stderr, "At each server for each file it is not possible to allocate (max_dcache_size) more than the size of the buffer pool (bufpool_size): max_dcache_size=%d bufpool_size=%d\n", clarisse_pars->max_dcache_size, clarisse_pars->bufpool_size);
  }

 // At each server for each file I cannot allocate more than the size of the buffer pool 
  if (clarisse_pars->max_dcache_size * clarisse_pars->buffer_size < clarisse_pars->net_buf_data_size){
    err = 1;
    if (myrank == 0)
      fprintf(stderr, "At each server for each file net_buf cannot larger than the dcache capacity: max_dcache_size=%d buf_size=%d, net_buf_data_size=%d\n", clarisse_pars->max_dcache_size, clarisse_pars->buffer_size, clarisse_pars->net_buf_data_size);
  } 

  if ((clarisse_pars->collective == CLARISSE_LISTIO_COLLECTIVE) &&
      (clarisse_pars->net_buf_data_size <= 2 * (int) sizeof(int))){
    err = 1;
    if (myrank == 0)
      fprintf(stderr, "For LISTIO_COLLECTIVE net_buf must be larger than a offset-length pair to acomodate at least one byte: net_buf_data_size = %d must be > %d\n", clarisse_pars->net_buf_data_size, 2 * (int) sizeof(int));
  } 

  // control does not work with ROMIO
  if ((clarisse_pars->collective == CLARISSE_ROMIO_COLLECTIVE) &&
      (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED)){
    err = 1;
    if (myrank == 0)
      fprintf(stderr, "CONTROL is not  implemented for ROMIO_COLLECTIVE \n");
  } 

  // Dynamic file partitioning  is not compatible with WRITE_BACK or WRITE_ON_CLOSE 
  if ((clarisse_pars->file_partitioning == CLARISSE_DYNAMIC_FILE_PARTITIONING) && ((clarisse_pars->write_type == CLARISSE_WRITE_BACK) || (clarisse_pars->write_type == CLARISSE_WRITE_ON_CLOSE))) {
    err = 1;
    if (myrank == 0)
      fprintf(stderr, "DYNAMIC_FILE_PARTITIONING is not compatible with WRITE_BACK or WRITE_ONCLOSE\n");
    
  }

  if (clarisse_pars->coupleness == CLARISSE_INTERCOMM) {
    int i, total_client_processes = 0;
    for (i = 0; i < nr_clients; i++)
      total_client_processes += client_nprocs[i];
    
    if (clarisse_pars->nr_servers + total_client_processes != nprocs) {
      err = 1;
      if (myrank == 0) 	
	fprintf(stderr,"For CLARISSE_INTERCOMM the number of server processes + number of client processes = %d + %d is DIFFERENT than nprocs = %d\n", clarisse_pars->nr_servers, total_client_processes, nprocs); 
    }
  } 

  //if (clarisse_pars->server_map == CLARISSE_SERVER_MAP_TOPOLOGY){
  //   client_global_state.clarisse_pars.aggregator_selection_step

// elasticity and global scheduling do not work without control
  if (((clarisse_pars->elasticity == CLARISSE_ELASTICITY_ENABLED) || 
      (clarisse_pars->global_scheduling != CLARISSE_GLOBAL_SCHEDULING_NULL)) &&
      (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_DISABLED)){
    err = 1;
    if (myrank == 0)
      fprintf(stderr, "ELASTICITY and/or GLOBAL_SCHEDULING do not work without CNTROL\n");
  } 

 // read_future does not work without intercomm
  if ((clarisse_pars->read_future == CLARISSE_READ_FUTURE_ENABLED) && 
      ((clarisse_pars->coupleness != CLARISSE_INTERCOMM) ||
       (clarisse_pars->file_partitioning != CLARISSE_STATIC_FILE_PARTITIONING))){
    err = 1;
    if (myrank == 0)
      fprintf(stderr, "READ_FUTURE does work only for CLARISSE_COUPLENESS=intercomm and CLARISSE_FILE_PARTITIONING=static\n");
  }  


  // At each server for each file I cannot allocate more than the size of the buffer pool (not sharing a buffer among clients by now)
  if (clarisse_pars->max_dcache_size * nr_clients > clarisse_pars->bufpool_size){
    err = 1;
    if (myrank == 0)
      fprintf(stderr, "At each server I have less than one buffer that can allocate to the dcache of each client: max_dcache_size=%d * nr_clients=%d > bufpool_size=%d\n", clarisse_pars->max_dcache_size, nr_clients, clarisse_pars->bufpool_size);
  } 

  if (err) {
    PMPI_Finalize();
    exit(1);
  }
    
}


int MPI_Init (int *argc, char ***argv){
  int ret;
  char *val;
  short int control, collective, aggr_thread;

  if ((val = getenv("CLARISSE_CONTROL")))
    if (!strcmp(val, "enable"))
      control = CLARISSE_CONTROL_ENABLED;
    else
      control = CLARISSE_CONTROL_DISABLED;
  else
    control = CLARISSE_DEFAULT_CONTROL;

  if ((val = getenv("CLARISSE_COLLECTIVE"))){
    if (!strcmp(val, "vb"))
      collective = CLARISSE_VB_COLLECTIVE;
    else
      if (!strcmp(val, "romio"))
	collective = CLARISSE_ROMIO_COLLECTIVE;
      else
	if (!strcmp(val, "listio"))
	  collective = CLARISSE_LISTIO_COLLECTIVE;
	else
	  handle_err(MPI_ERR_OTHER, "CLARISSE_COLLECTIVE: Unsupported value");
  }
  else
    collective = CLARISSE_VB_COLLECTIVE;

  if ((val = getenv("CLARISSE_AGGR_THREAD")))
    if (!strcmp(val, "thread"))
      aggr_thread = CLARISSE_AGGR_THREAD; 
    else
      if (!strcmp(val, "process"))
	aggr_thread = CLARISSE_AGGR_PROCESS; 
      else
	aggr_thread = CLARISSE_DEFAULT_AGGR_THREAD;
  else
    aggr_thread = CLARISSE_DEFAULT_AGGR_THREAD;

  if ((control == CLARISSE_CONTROL_ENABLED) || ((collective != CLARISSE_ROMIO_COLLECTIVE) && 
							  (aggr_thread == CLARISSE_AGGR_THREAD))) { 
    int provided;
    ret = PMPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided != MPI_THREAD_MULTIPLE)
      handle_err(MPI_ERR_OTHER, "The provided mode different from MPI_THREAD_MULTIPLE).");
  }
  else
    ret = PMPI_Init(argc, argv);

  ret = cls_init();
  return ret;
}

int cls_init() {
  //printf("CLARISSE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  int myrank;

  client_global_state_init();
  if ((client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM) && 
      (client_nprocs == NULL)) {
    int nprocs;
    
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs); 
    nr_clients = 1;
    client_nprocs = (int *) clarisse_malloc(sizeof(int));
    client_nprocs[0] = nprocs - client_global_state.clarisse_pars.nr_servers;
  }
  //client_global_state_init();
  clarisse_params_verification(&client_global_state.clarisse_pars);

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);  
#ifdef CLARISSE_PARAM_PRINT
    if (myrank == 0) clarisse_param_print(&client_global_state.clarisse_pars);
#endif


#ifdef CLARISSE_TIMING
  clarisse_timing_init(&local_timing);
#endif

  /*#ifdef CLARISSE_MEM_COUNT
  clarisse_mem_count_init(&mem_count);
#endif
  */
  server_global_state.initialized = 0;

  switch (client_global_state.clarisse_pars.coupleness){
  case CLARISSE_COUPLED: {
    i_am_client = 1;
    if (client_global_state.clarisse_pars.server_map != CLARISSE_SERVER_MAP_ROMIO) {
      i_am_server = cls_assign_servers(client_global_state.clarisse_pars.server_map, client_global_state.clarisse_pars.nr_servers, NULL, NULL, NULL);
    }
#ifdef CLARISSE_CONTROL
    if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED)
      cls_ctrl_init();
#endif
    break;	
  }
  case CLARISSE_INTERCOMM: {
    MPI_Comm comm;
    int *client_leaders;
    //int total;
    // If the user has not set it, there is 1 client with nprocs - nr_server processe
    client_leaders = (int *) clarisse_malloc(nr_clients * sizeof(int));
    client_global_state.client_id = cls_assign_servers(client_global_state.clarisse_pars.server_map, client_global_state.clarisse_pars.nr_servers, NULL, &client_global_state.server_leader, client_leaders);
    /*MPI_Reduce(&client_global_state.client_id, &total, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    if (myrank == 0)
    printf("INIT: Total servers=%d\n", total);*/
    //printf("I am %d and %s (%d)\n", myrank, ((client_global_state.client_id == CLARISSE_CLIENT_MEMBERSHIP_KEY)?"client":"server"), client_global_state.client_id);
    MPI_Comm_split(MPI_COMM_WORLD, client_global_state.client_id, myrank, &comm);
    if (client_global_state.client_id > 0) 
      i_am_client = 1;
    else
      i_am_client = 0;
    MPI_Comm_split(MPI_COMM_WORLD, i_am_client, myrank, &client_global_state.allclients_intracomm);

    if (client_global_state.client_id == CLARISSE_SERVER_MEMBERSHIP_KEY){
      int /*serv_procs,*/ local_leader;
      MPI_Comm *intercomms;
      int i;

      intercomms = (MPI_Comm *)clarisse_malloc(nr_clients * sizeof(MPI_Comm));
      i_am_server = 1;
      local_leader = 0;
      for (i = 0; i < nr_clients; i++) {
	int err;
	err = MPI_Intercomm_create(comm, local_leader, MPI_COMM_WORLD, client_leaders[i], 1, &(intercomms[i]));
	if (err != MPI_SUCCESS)
	  handle_err(err, "MPI_Intercomm_create in server\n");
      }
      // MPI_Comm_size(intercomms[0], &serv_procs);
      // printf("Server procs count=%d\n", serv_procs);
      server_global_state_init(&client_global_state.clarisse_pars, comm, nr_clients, intercomms);
      

#ifdef CLARISSE_CONTROL
      if (server_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED) 
	cls_ctrl_init();
#endif
      //cls_ctrl_init uses the client_global_state to get the servers
      client_global_state_free();
     
      server_operate();
      MPI_Finalize();
      exit(0);
    }
    else { // CLIENT
      int /*cli_procs,*/ local_leader, err;
      i_am_client = 1;
      client_global_state.intracomm = comm;
      local_leader = 0;
      err = MPI_Intercomm_create(comm, local_leader , MPI_COMM_WORLD, client_global_state.server_leader, 1, &client_global_state.initial_cli_serv_intercomm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Intercomm_create in client\n");
      //MPI_Comm_size(client_global_state.initial_cli_serv_intercomm, &cli_procs);
      //printf("Client procs count=%d\n", cli_procs);
    
      clarisse_free(client_leaders);
#ifdef CLARISSE_CONTROL
      if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED)
	cls_ctrl_init();
#endif
    }
    break;
  }
  case CLARISSE_DYN_PROCESS: {
    i_am_client = 1;
    i_am_server = 0;
      /*    if (ahpfs_global_info.connect) {
	    MPI_Group group_self,new_group_self;
	    MPI_Comm_group(MPI_COMM_SELF, &group_self);
	    
	    strcpy(serv_name,"ioserver");
	    ahpfs_global_info.pool_comm = clarisse_malloc(sizeof(MPI_Comm *) * ahpfs_global_info.nr_ios);
	    for (i=0; i < ahpfs_global_info.nr_ios; i++){
	    ahpfs_global_info.pool_comm[i] = (MPI_Comm) clarisse_malloc(sizeof(MPI_Comm));
	    merr = MPI_Lookup_name( serv_name, MPI_INFO_NULL, port );
	    if (merr) {
	    MPI_Error_string(err, string, &len);
	    fprintf(stderr, "Error in MPI_Lookup_name:%s\n", string);
	    }
	    err = MPI_Comm_connect(port, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &ahpfs_global_info.pool_comm[i]);
	    MPI_Group_union (group_self,ahpfs_global_info.pool_comm[i],&new_group_self);    
	    if (myrank == 0) err = MPI_Send(argv, NR_OF_ARGS * 255 ,MPI_CHAR,0,RQ_INIT_IO,new_group_self);
	    }
	    ahpfs_global_info.intercomm = group_self;
	    } else
      */
    if (strlen(client_global_state.clarisse_pars.server_path)) {
	char **argvv;
	int err;
	int *errcodes;
	
	errcodes = clarisse_malloc(client_global_state.clarisse_pars.nr_servers * sizeof(int));
	
	argvv = get_server_params(&client_global_state.clarisse_pars);
	
	err = MPI_Comm_spawn(client_global_state.clarisse_pars.server_path, 
			     argvv,
			     client_global_state.clarisse_pars.nr_servers, 
			     MPI_INFO_NULL, 0, MPI_COMM_WORLD, 
			     &(client_global_state.initial_cli_serv_intercomm), 
			     errcodes);
	clarisse_free(*argvv);
	clarisse_free(argvv);
	clarisse_free(errcodes);
	if (err != MPI_SUCCESS)
	  handle_err(err, "MPI_Comm_spawn error");
    }
    else
       handle_err(MPI_ERR_OTHER, "Server path not defined (e.g. in CLARISSE_SERVER_PATH environment var)");
    break;
  } // case 
  } //switch
  return 0;
}




int cls_finalize() {
  int ret, myrank;

#ifdef CLARISSE_TIMING
  //if (i_am_client)
  clarisse_timing_finalize(&local_timing);
#endif
#ifdef CLARISSE_MEM_COUNT
  //if (i_am_client)
  clarisse_mem_count_finalize(&mem_count);
#endif
#ifdef CLARISSE_CONTROL
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED)
    cls_ctrl_finalize();
#endif
  //MPI_Barrier(MPI_COMM_WORLD);
  if (i_am_client) {
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
#ifdef CLARISSE_PARAM_PRINT
    if (myrank == 0) clarisse_param_print(&client_global_state.clarisse_pars);
#endif
    
    if ((client_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS) ||
	(client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM)) {
      //int  myrank;  
      
      //MPI_Barrier(client_global_state.intracomm);
      //MPI_Comm_rank(client_global_state.intracomm, &myrank);
      //if (myrank == 0) {
      MPI_Request *requests;
      int j; //log_ios_rank;
      int_map_t *m, *tmp;
      //requests = (MPI_Request *) clarisse_malloc(client_global_state.active_nr_servers * sizeof(MPI_Request));
      requests = (MPI_Request *) clarisse_malloc(client_global_state.clarisse_pars.nr_servers * sizeof(MPI_Request));
      //printf("client_global_state.clarisse_pars.nr_servers=%d\n",client_global_state.clarisse_pars.nr_servers);
      if (requests == NULL)
	handle_error(MPI_ERR_NO_MEM, "Out of memory");
      j = 0;
      /*
      for(log_ios_rank = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++) {
	if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	  ret = MPI_Isend(&myrank, 1, MPI_INT, client_global_state.log2ph[log_ios_rank] , RQ_FINALIZE_IO, client_global_state.initial_cli_serv_intercomm, requests + j);
	  if (ret != MPI_SUCCESS) {
	    handle_err(ret, "Error when sending the reqs");
	    return ret;
	  }
	  j++;
	}
	}*/
      HASH_ITER(hh, client_global_state.ph2log, m, tmp) {
	ret = MPI_Isend(&myrank, 1, MPI_INT, j, RQ_FINALIZE_IO, client_global_state.initial_cli_serv_intercomm, requests + j);
	if (ret != MPI_SUCCESS) {
	  handle_err(ret, "Error when sending the reqs");
	  return ret;
	}
	j++;
      }

      //MPI_Waitall(client_global_state.active_nr_servers, requests, MPI_STATUSES_IGNORE);
      MPI_Waitall(client_global_state.clarisse_pars.nr_servers, requests, MPI_STATUSES_IGNORE);
      clarisse_free(requests);
    }
    MPI_Barrier(client_global_state.intracomm);
    if (myrank == 0) {
      int nprocs;
      char *view_type, *test, *delete_file;
      MPI_Comm_size(client_global_state.intracomm, &nprocs);
      view_type = getenv("CLARISSE_LAZY_EAGER_VIEW");
      test = getenv("CLARISSE_TEST");
      delete_file  = getenv("TEST_PREEXISTING_FILE");
      if (test){
	printf("%s passed with %d process(es)",  test, nprocs);
	if (client_global_state.clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
	  printf( " listio");
	else
	  printf( " view: %s",(view_type)   ? view_type   : "lazy(default) or dynamically modified");
	if (delete_file)
	  printf( "preexisting file:%s\n",delete_file);
	else
	  printf("\n" );
      }
    }

    client_global_state_free();      
  }
  if (i_am_server) 
    server_global_state_free();

  if (client_nprocs)
    clarisse_free(client_nprocs);
  return 0;
}

int MPI_Finalize (){
  int ret; 

  ret = cls_finalize();
  if (ret != 0)
    handle_err(ret, "Error in cls_finalize");
  ret = PMPI_Finalize();
#ifdef CLARISSE_MEM_DEBUG  
  clarisse_print_leaks();
#endif
  return ret;
}


#ifdef CLARISSE_TIMING

void clarisse_timing_init(clarisse_timing_t *timing){
  timing->cnt_open = 0;
  timing->cnt_close = 0;
  timing->cnt_setview = 0;
  timing->cnt_write_all = 0;
  timing->cnt_read_all = 0;

  timing->cnt_file_write = 0;
  timing->cnt_file_read = 0;
  timing->time_file_write = 0;
  timing->time_file_read = 0;
  timing->size_file_write = 0;
  timing->size_file_read = 0;
  timing->initial_time = MPI_Wtime();
}

void clarisse_timing_finalize_client(clarisse_timing_t *timing){
  int i, myrank, nprocs;
  //double tmp_t; 
  double avg_time_open = 0, avg_time_close = 0, avg_time_setview = 0, avg_time_write_all = 0, avg_time_read_all = 0;
  double max_time_open, max_time_close, max_time_setview, max_time_write_all, max_time_read_all;
  double min_time_open, min_time_close, min_time_setview, min_time_write_all, min_time_read_all;
  double tmp_sz;
  double avg_sz_write_all = 0, avg_sz_read_all = 0;
  double min_sz_write_all, min_sz_read_all;
  double max_sz_write_all, max_sz_read_all;
  //double tmp_thru;
  double avg_thru_write_all = 0, avg_thru_read_all = 0;
  double min_thru_write_all, min_thru_read_all;
  double max_thru_write_all, max_thru_read_all;

  MPI_Comm_rank(client_global_state.intracomm, &myrank);
  MPI_Comm_size(client_global_state.intracomm, &nprocs);
  // OPEN
  
  //if (myrank == 0) clarisse_param_print(&client_global_state.clarisse_pars);
  
  if (myrank == 0)
    printf("NUMBER OF CLIENT PROCESSES=%d\n", nprocs);
  if ((myrank == 0) && (timing->cnt_open > 0)){
    printf("\nINDIVIDUAL OPEN STATISTICS\n_____________________________\n");
    printf("%s%23s%12s%12s\n", "CLIENT", "TIME","START", "FINISH");
  }
  for (i = 0; i < timing->cnt_open; i++){
    double t0, t1, tmp_t;

    MPI_Reduce(&timing->time_open[i][0], &t0, 1, MPI_DOUBLE, MPI_MIN, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->time_open[i][1], &t1, 1, MPI_DOUBLE, MPI_MAX, 0, client_global_state.intracomm); 
    tmp_t = (t1 - t0);
    avg_time_open += tmp_t;
    if (i == 0)
      max_time_open = min_time_open = tmp_t;
    else {
      max_time_open = MAX(max_time_open, tmp_t);
      min_time_open = MIN(min_time_open, tmp_t);
    }
    if ((myrank == 0) && (timing->cnt_open > 0))
      printf("%6d%10s %12.6f%12.6f%12.6f\n", client_global_state.client_id, "OPEN",tmp_t, t0 - timing->initial_time, t1 - timing->initial_time);
  }
  if ((myrank == 0) && (timing->cnt_open > 0)) {
    printf("TOTAL OPEN STATISTICS\n_____________________________\n");
    printf("%28s%13s%13s%13s\n", "AVG", "MIN", "MAX", "NROPS");
    avg_time_open /= timing->cnt_open;
    printf("%10s TIME %12.6f %12.6f %12.6f %12d\n", "OPEN",avg_time_open, min_time_open, max_time_open, timing->cnt_open);
  }
  // CLOSE
 if ((myrank == 0) && (timing->cnt_close > 0)){
   printf("\nINDIVIDUAL CLOSE STATISTICS\n_____________________________\n");
   printf("%s%23s%12s%12s\n", "CLIENT","TIME", "START", "FINISH");
 } 
 for (i = 0; i < timing->cnt_close; i++){
    double t0, t1, tmp_t;
 
    MPI_Reduce(&timing->time_close[i][0], &t0, 1, MPI_DOUBLE, MPI_MIN, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->time_close[i][1], &t1, 1, MPI_DOUBLE, MPI_MAX, 0, client_global_state.intracomm); 
    
    tmp_t = t1 - t0;
    avg_time_close += tmp_t;
    if (i == 0)
      max_time_close = min_time_close = tmp_t;
    else {
      max_time_close = MAX(max_time_close, tmp_t);
      min_time_close = MIN(min_time_close, tmp_t);
    }
    if ((myrank == 0) && (timing->cnt_close > 0))
      printf("%6d%10s %12.6f%12.6f%12.6f\n", client_global_state.client_id, "CLOSE",tmp_t, t0 - timing->initial_time, t1 - timing->initial_time);
  }
  if ((myrank == 0) && (timing->cnt_close > 0)) {
    printf("TOTAL CLOSE STATISTICS\n_____________________________\n");
    printf("%28s%13s%13s%13s\n", "AVG", "MIN", "MAX", "NROPS");
    avg_time_close /= timing->cnt_close;
    printf("%10s TIME %12.6f %12.6f %12.6f %12d\n", "CLOSE", avg_time_close, min_time_close, max_time_close, timing->cnt_close);
  }  
  //SETVIEW
  if ((myrank == 0) && (timing->cnt_setview > 0)){
    printf("\nINDIVIDUAL SETVIEW STATISTICS\n_____________________________\n");
    printf("%s%28s%12s%12s\n", "CLIENT", "TIME","START", "FINISH");
  }    
  for (i = 0; i < timing->cnt_setview; i++){
    double t0, t1, tmp_t;

    MPI_Reduce(&timing->time_setview[i][0], &t0, 1, MPI_DOUBLE, MPI_MIN, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->time_setview[i][1], &t1, 1, MPI_DOUBLE, MPI_MAX, 0, client_global_state.intracomm); 
    tmp_t = t1 - t0;
    avg_time_setview += tmp_t;
    if (i == 0)
      max_time_setview = min_time_setview = tmp_t;
    else {
      max_time_setview = MAX(max_time_setview, tmp_t);
      min_time_setview = MIN(min_time_setview, tmp_t);
    }
    if ((myrank == 0) && (timing->cnt_setview > 0))
      printf("%d%10s TIME %12.6f%12.6f%12.6f\n", client_global_state.client_id, "SETVIEW",tmp_t, t0 - timing->initial_time, t1 - timing->initial_time);  
  }
  if ((myrank == 0) && (timing->cnt_setview > 0)) {
    printf("TOTAL SETVIEW STATISTICS\n_____________________________\n");
    printf("%28s%13s%13s%13s\n", "AVG", "MIN", "MAX", "NROPS");
    avg_time_setview /= timing->cnt_setview;
    printf("%10s TIME %12.6f %12.6f %12.6f %12d\n", "SETVIEW", avg_time_setview, min_time_setview, max_time_setview, timing->cnt_setview);
  }
  //WRITE
  if ((myrank == 0) && (timing->cnt_write_all > 0)){
    printf("\nINDIVIDUAL WRITE STATISTICS\n_____________________________\n");
    printf("%s%23s%12s%12s%12s%12s%12s\n", "CLIENT", "TIME","SIZE","THRU","START", "FINISH", "WAITING");
  }
  for (i = 0; i < timing->cnt_write_all; i++){
    double thru, tmp_t0, tmp_t1, tmp_t0_wait, tmp_t1_wait;
    MPI_Reduce(&timing->time_write_all[i][0], &tmp_t0, 1, MPI_DOUBLE, MPI_MIN, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->time_write_all[i][1], &tmp_t1, 1, MPI_DOUBLE, MPI_MAX, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->size_write_all[i], &tmp_sz, 1, MPI_DOUBLE, MPI_SUM, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->time_write_all_wait[i][0], &tmp_t0_wait, 1, MPI_DOUBLE, MPI_MIN, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->time_write_all_wait[i][1], &tmp_t1_wait, 1, MPI_DOUBLE, MPI_MAX, 0, client_global_state.intracomm); 
    thru = tmp_sz / 1024 / 1024 / (tmp_t1 -  tmp_t0);
    tmp_sz /= nprocs;

    avg_time_write_all += (tmp_t1 -  tmp_t0);
    avg_sz_write_all += tmp_sz;
    avg_thru_write_all += thru;
    if (i == 0) {
      max_time_write_all = min_time_write_all = (tmp_t1 -  tmp_t0);
      max_sz_write_all = min_sz_write_all = tmp_sz;
      max_thru_write_all = min_thru_write_all = thru;	    
    }
    else {
      max_time_write_all = MAX(max_time_write_all, (tmp_t1 -  tmp_t0));
      min_time_write_all = MIN(min_time_write_all, (tmp_t1 -  tmp_t0));
      max_sz_write_all = MAX(max_sz_write_all, tmp_sz);
      min_sz_write_all = MIN(min_sz_write_all, tmp_sz);
      max_thru_write_all = MAX(max_thru_write_all, thru);
      min_thru_write_all = MIN(min_thru_write_all, thru);
    }
    if ((myrank == 0) && (timing->cnt_write_all > 0))
      printf("%6d%10s %12.6f%12.0f%12.3f%12.6f%12.6f%12.6f\n", client_global_state.client_id, "WRITE_ALL",  (tmp_t1 -  tmp_t0), tmp_sz,thru, tmp_t0 - timing->initial_time, tmp_t1 - timing->initial_time, (tmp_t1_wait -  tmp_t0_wait));
  }
  if ((myrank == 0) && (timing->cnt_write_all > 0)) {
    printf("TOTAL WRITE STATISTICS\n_____________________________\n");
    printf("%28s%13s%13s%13s\n", "AVG", "MIN", "MAX", "NROPS");
    avg_time_write_all /= timing->cnt_write_all;
    printf("%10s TIME %12.6f %12.6f %12.6f %12d\n","WRITE_ALL" , avg_time_write_all, min_time_write_all, max_time_write_all,  timing->cnt_write_all);
    avg_sz_write_all /= timing->cnt_write_all;
    printf("%10s SIZE %12.0f %12.0f %12.0f %12d\n","WRITE_ALL", avg_sz_write_all, min_sz_write_all, max_sz_write_all,  timing->cnt_write_all);
    avg_thru_write_all /= timing->cnt_write_all;
    printf("%10s THRU %12.3f %12.3f %12.3f %12d\n","WRITE_ALL", avg_thru_write_all, min_thru_write_all, max_thru_write_all, timing->cnt_write_all);
  } 
  // READ
  if ((myrank == 0) && (timing->cnt_read_all > 0)){
    printf("\nINDIVIDUAL READ STATISTICS\n_____________________________\n");
    printf("%s%23s%12s%12s%12s%12s\n", "CLIENT", "TIME","SIZE","THRU","START", "FINISH");
  }
  for (i = 0; i < timing->cnt_read_all; i++){
    double thru, tmp_t0, tmp_t1;
    MPI_Reduce(&timing->time_read_all[i][0], &tmp_t0, 1, MPI_DOUBLE, MPI_MIN, 0, client_global_state.intracomm); 
    MPI_Reduce(&timing->time_read_all[i][1], &tmp_t1, 1, MPI_DOUBLE, MPI_MAX, 0, client_global_state.intracomm);     
    MPI_Reduce(&timing->size_read_all[i], &tmp_sz, 1, MPI_DOUBLE, MPI_SUM, 0, client_global_state.intracomm); 
    thru = tmp_sz / 1024 / 1024 / (tmp_t1 -  tmp_t0);
    tmp_sz /= nprocs;
   
    avg_time_read_all += (tmp_t1 -  tmp_t0);
    avg_sz_read_all += tmp_sz;
    avg_thru_read_all += thru;
    if (i == 0) {
      max_time_read_all = min_time_read_all = (tmp_t1 -  tmp_t0);
      max_sz_read_all = min_sz_read_all = tmp_sz;
      max_thru_read_all = min_thru_read_all = thru;	    
    }
    else {
      max_time_read_all = MAX(max_time_read_all, (tmp_t1 -  tmp_t0));
      min_time_read_all = MIN(min_time_read_all, (tmp_t1 -  tmp_t0));
      max_sz_read_all = MAX(max_sz_read_all, tmp_sz);
      min_sz_read_all = MIN(min_sz_read_all, tmp_sz);
      max_thru_read_all = MAX(max_thru_read_all, thru);
      min_thru_read_all = MIN(min_thru_read_all, thru);
    }
    if ((myrank == 0) && (timing->cnt_read_all > 0))
      printf("%6d%10s %12.6f%12.0f%12.3f%12.6f%12.6f\n", client_global_state.client_id, "READ_ALL", (tmp_t1 -  tmp_t0), tmp_sz, thru, tmp_t0 - timing->initial_time, tmp_t1 - timing->initial_time);
  }
  if ((myrank == 0) && (timing->cnt_read_all > 0)) {
    printf("TOTAL READ STATISTICS\n_____________________________\n");
    printf("%28s%13s%13s%13s\n", "AVG", "MIN", "MAX", "NROPS");
    avg_time_read_all /= timing->cnt_read_all;
    printf("%10s TIME %12.6f %12.6f %12.6f %12d\n", "READ_ALL", avg_time_read_all, min_time_read_all, max_time_read_all, timing->cnt_read_all);
    avg_sz_read_all /= timing->cnt_read_all;
    printf("%10s SIZE %12.0f %12.0f %12.0f %12d\n", "READ_ALL", avg_sz_read_all, min_sz_read_all, max_sz_read_all, timing->cnt_read_all);
    avg_thru_read_all /= timing->cnt_read_all;
    printf("%10s THRU %12.3f %12.3f %12.3f %12d\n", "READ_ALL", avg_thru_read_all, min_thru_read_all, max_thru_read_all, timing->cnt_read_all);
  }
  
}

void clarisse_timing_finalize_server(clarisse_timing_t *timing, MPI_Comm intracomm){
  int myrank, nprocs;
  double thru, tmp_t, min_thru, max_thru, op_proc, tmp_thru;
  int tmp_cnt, tmp_procs, local_op;
  double tmp_sz;

  MPI_Comm_rank(intracomm, &myrank);
  MPI_Comm_size(intracomm, &nprocs);
  // STORAGE ACTIVITY
  if (myrank == 0){
    printf("\nSTORAGE STATISTICS\n_____________________________\n");
    printf("%21s%12s%12s%12s%12s%10s%10s\n", "TIME","SIZE","AGGR THRU","MIN THRU","MAX THRU", "N_PROCS", "OP/PROC");
  }
  
  local_op = (timing->cnt_file_write > 0) ? 1: 0;
  MPI_Reduce(&local_op, &tmp_procs, 1, MPI_INT, MPI_SUM, 0, intracomm);
  MPI_Reduce(&timing->cnt_file_write, &tmp_cnt, 1, MPI_INT, MPI_SUM, 0, intracomm);
  thru = ((timing->cnt_file_write > 0) ? (timing->size_file_write / timing->time_file_write / 1024 / 1024) : 0);
  MPI_Reduce(&timing->time_file_write, &tmp_t, 1, MPI_DOUBLE, MPI_SUM, 0, intracomm); 
  MPI_Reduce(&timing->size_file_write, &tmp_sz, 1, MPI_DOUBLE, MPI_SUM, 0, intracomm); 
  MPI_Reduce(&thru, &tmp_thru, 1, MPI_DOUBLE, MPI_SUM, 0, intracomm); 
  if (timing->cnt_file_write == 0)
    thru = 0;
  MPI_Reduce(&thru, &max_thru, 1, MPI_DOUBLE, MPI_MAX, 0, intracomm); 
  if (timing->cnt_file_write == 0)
    thru = DBL_MAX;
  MPI_Reduce(&thru, &min_thru, 1, MPI_DOUBLE, MPI_MIN, 0, intracomm); 
  

  if (myrank == 0) {
    if (tmp_procs > 0) {
      tmp_t /= tmp_procs;
      tmp_sz /= tmp_procs;
      op_proc= ((double) tmp_cnt) / ((double)tmp_procs);
    }
    else {
      min_thru = 0;
      op_proc = 0;
   }
    printf("%10s %10.6f%12.0f%12.3f%12.3f%12.3f%10d%10.3f\n", "FILE WRITE",tmp_t,tmp_sz,tmp_thru, min_thru, max_thru, tmp_procs, op_proc);
  }
  // READ
  local_op = (timing->cnt_file_read > 0) ? 1: 0;
  MPI_Reduce(&local_op, &tmp_procs, 1, MPI_INT, MPI_SUM, 0, intracomm);
  MPI_Reduce(&timing->cnt_file_read, &tmp_cnt, 1, MPI_INT, MPI_SUM, 0, intracomm);
  thru = ((timing->cnt_file_read > 0) ? (timing->size_file_read / timing->time_file_read / 1024 / 1024) : 0);
  MPI_Reduce(&timing->time_file_read, &tmp_t, 1, MPI_DOUBLE, MPI_SUM, 0, intracomm); 
  MPI_Reduce(&timing->size_file_read, &tmp_sz, 1, MPI_DOUBLE, MPI_SUM, 0, intracomm); 
  MPI_Reduce(&thru, &tmp_thru, 1, MPI_DOUBLE, MPI_SUM, 0, intracomm); 
  if (timing->cnt_file_read == 0)
    thru = 0;
  MPI_Reduce(&thru, &max_thru, 1, MPI_DOUBLE, MPI_MAX, 0, intracomm); 
  if (timing->cnt_file_read == 0)
    thru = DBL_MAX;
  MPI_Reduce(&thru, &min_thru, 1, MPI_DOUBLE, MPI_MIN, 0, intracomm); 

  if (myrank == 0) {
    if (tmp_procs > 0) {
      tmp_t /= tmp_procs;
      tmp_sz /= tmp_procs;
      op_proc= ((double) tmp_cnt) / ((double)tmp_procs);
    }
    else {
      min_thru = 0;
      op_proc = 0;
    }
    printf("%10s %10.6f%12.0f%12.3f%12.3f%12.3f%10d%10.3f\n", "FILE READ",tmp_t,tmp_sz,tmp_thru, min_thru, max_thru, tmp_procs, op_proc);
  }
}

void clarisse_timing_finalize(clarisse_timing_t *timing) {
  if (i_am_client)
    clarisse_timing_finalize_client(timing);
  /*
  if ((i_am_server) && (server_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM)) {
    clarisse_timing_finalize_server(timing, server_global_state.intracomm);
    MPI_Barrier(server_global_state.intracomm);
  }
  else
  */
  //  clarisse_timing_finalize_server(timing, MPI_COMM_WORLD);
 
}


#endif

#ifdef CLARISSE_MEM_COUNT
void clarisse_mem_count_init(clarisse_mem_count_t *mem_count){
  mem_count->crt_allocated_memory = 0;
  mem_count->max_allocated_memory = 0;
}

void clarisse_mem_count_finalize(clarisse_mem_count_t *mem_count){
  double max_max_allocated_memory, min_max_allocated_memory;
  int myrank;
  // MAX MEMORY
  //printf("MAX ALLOCATED MEMORY (MB) = %7.0f\n",
  //         mem_count->max_allocated_memory / 1024 / 1024);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Reduce(&mem_count->max_allocated_memory, &max_max_allocated_memory, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD); 
  MPI_Reduce(&mem_count->max_allocated_memory, &min_max_allocated_memory, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD); 
  
  if (myrank == 0){
    printf("\nMEMORY STATISTICS\n_____________________________\n");
    printf("MIN MAX ALLOCATED MEMORY = %7.0f MBytes (%9.0f bytes)\n", 
	   min_max_allocated_memory / (1024 * 1024), min_max_allocated_memory);
    printf("MAX MAX ALLOCATED MEMORY = %7.0f MBytes (%9.0f bytes)\n", 
	   max_max_allocated_memory / (1024 * 1024), max_max_allocated_memory);
  } 
  
}


#endif
