#ifndef HDR_H
#define HDR_H

#include <stdio.h>
#include "buffer_pool.h"
#include "server_iocontext.h"

enum message_types {
  RQ_FINALIZE_IO,
  RQ_FILE_WRITE,
  RQ_FILE_WRITE_COLL,
  RQ_FILE_READ,
  RQ_FILE_READ_COLL,
  RQ_FILE_READ_COLL_FUTURE,
  RQ_FILE_DELETE,
  RQ_FILE_SET_VIEW,
  RQ_FILE_FLUSH,
  RT_FILE_WRITE,
  RT_FILE_WRITE_COLL,
  RT_FILE_READ,
  RT_FILE_READ_COLL,
  RT_FILE_DELETE,
  RT_FILE_FLUSH,
  RQ_SERVER_DELAY,
  RT_SERVER_DELAY,
  CLARISSE_COUNT_MESSAGE_TYPES
};


struct rq_file_set_view {
  int global_descr;
  int partitioning_stride_factor;
  int nprocs;
  int rank;
  clarisse_off displ;
  int view_size;
  int target_name_size;
};

struct rq_file_write {
  int global_descr;
  int partitioning_stride_factor; // replaces ios_per_file, is ios_per_file for static partitioning, 1 for dynamic partitioning
  int nprocs;
  int count;
  clarisse_off l_f;
  clarisse_off r_f;
  short last_msg;
  short first_msg;
  int view_size;
  int target_name_size;
  clarisse_off displ;
};

struct rq_file_read {
  int global_descr;
  int partitioning_stride_factor; // replaces ios_per_file, is ios_per_file for static partitioning, 1 for dynamic partitioning
  int nprocs;
  int count;
  clarisse_off l_f;
  clarisse_off r_f;
  short first_msg;
  short last_msg; 
  int view_size;
  int target_name_size;
  clarisse_off displ;
};

struct rq_file_flush {
  int global_descr;
};

struct rt_file_write {
  int confirmed;
  int errn;
};

struct rt_file_read {
  int confirmed;
  int errn;
  int last_msg;
};

struct rt_file_delete {
  int errn;
};

struct rt_file_flush {
  int errn;
};


enum{
  CLARISSE_EXPLICIT_OFFSET=0,
  CLARISSE_INDIVIDUAL,
  CLARISSE_SHARED
};


void clarisse_param_init();
int clarisse_isend(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request);
int cls_write_coll(client_iocontext_t *c_ctxt, 
		   void *buf, int count,
		   MPI_Datatype datatype, int file_ptr_type,
		   MPI_Offset offset);
int cls_write(client_iocontext_t *c_ctxt, 
	      void *buf, int count,
	      MPI_Datatype datatype, int file_ptr_type,
	      MPI_Offset offset);
int cls_read_coll(client_iocontext_t *c_ctxt, 
		   void *buf, int count,
		   MPI_Datatype datatype, int file_ptr_type,
		   MPI_Offset offset);
int cls_read(client_iocontext_t *c_ctxt, 
	     void *buf, int count,
	     MPI_Datatype datatype, int file_ptr_type,
	     MPI_Offset offset);

int cls_read_coll_future(client_iocontext_t *c_ctxt, 
		   void *buf, int count,
		   MPI_Datatype datatype, int file_ptr_type,
		   MPI_Offset offset);

#endif
