#include <assert.h>
#include "tasks.h"
#include "client_iocontext.h"
#include "server_iocontext.h"
#include "map.h"
#include "hdr.h"

extern client_global_state_t client_global_state;

iotask_p find_iotask(int global_descr, int proc_rank, dllist *task_queue){
  struct dllist_link *aux;
  for (aux = task_queue->head; aux != NULL; aux = aux->next) {
    iotask_p iot;

    iot = (iotask_p)aux;
    if ((global_descr == iot->s_ctxt->local_target->global_descr) && 
	(proc_rank == iot->proc_rank)) 
      return iot;
  }
  return NULL;
}

iotask_p alloc_init_iotask(server_iocontext_t *s_ctxt, int proc_rank, clarisse_off l_f, clarisse_off r_f, int client_idx) {
  iotask_p iot;
  
  iot =  (iotask_p) clarisse_malloc(sizeof(iotask));
  iot->s_ctxt = s_ctxt;
  iot->proc_rank = proc_rank;
  iot->l_f = iot->crt_l_f = l_f;
  iot->r_f = r_f;
  iot->confirmed = 0;
  iot->errn = 0;
  iot->client_idx = client_idx;
  return iot;
}

// l_v argument is necessary, as the first byte from memory is written to  
// l_v byte in view (as a relative pointer for computing l_dt and r_dt)
// partioning distrib is the distribution of data that is sent in current round 


int create_io_tasks(client_iocontext_t *c_ctxt, 
		    clarisse_off l_v, clarisse_off l_f, clarisse_off r_f,
		    clarisse_off l_d, clarisse_off part_block_len, 
		    int file_partitioning) {	
  clarisse_off crt_l_v, crt_l_f, crt_r_v, crt_r_f;
  int  b, log_ios_rank, active_log_ios_rank, i, num_tasks; 

#ifdef CLARISSE_TASK_DEBUG  
  int myrank;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
#endif
#ifdef CLARISSE_TASK_DEBUG
  //if (myrank == 0)
  printf("C: create_io_tasks: l_v=%lld, l_f=%lld r_f=%lld nr_servers=%d nr_active_servers=%d\n",l_v, l_f, r_f, client_global_state.clarisse_pars.nr_servers, client_global_state.active_nr_servers);
#endif
  b = c_ctxt->clarisse_pars.buffer_size; 
  num_tasks = 0;
  c_ctxt->pending_ios = 0;
  for (i = 0; i < c_ctxt->clarisse_pars.nr_servers; i++)
    c_ctxt->pending_ios_ret[i] = 0;
  crt_l_f = l_f;
  if (file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)
    active_log_ios_rank = (l_f / b) % client_global_state.active_nr_servers ;
  else
    active_log_ios_rank = (l_f - l_d) / part_block_len;
  assert(active_log_ios_rank < client_global_state.active_nr_servers);
  log_ios_rank = client_global_state.active_log_servers[active_log_ios_rank];
  //printf("log_ios_rank=%d nr_servers=%d\n", log_ios_rank, client_global_state.active_nr_servers);
  while (crt_l_f <= r_f) {
    crt_r_f = MIN(r_f, END_BLOCK(crt_l_f, b));
    crt_l_v = func_sup(crt_l_f, c_ctxt->intermediary_target->distr);
    crt_r_v = func_inf(crt_r_f, c_ctxt->intermediary_target->distr); 
    if (crt_l_v <= crt_r_v) {
      iotask_p iot = (iotask_p) clarisse_malloc(sizeof(iotask));
      iot->count = crt_r_v - crt_l_v + 1;
      iot->l_f = iot->crt_l_f = crt_l_f;
      iot->r_f = crt_r_f;
      iot->l_dt = func_1(crt_l_v - l_v, c_ctxt->local_target->distr); 
      iot->r_dt = func_1(crt_r_v - l_v, c_ctxt->local_target->distr);
      iot->buf_distr = c_ctxt->local_target->distr;
      iot->buf = c_ctxt->local_target->local_handle.buf; 
      iot->log_ios_rank = log_ios_rank;
      
      if (c_ctxt->view_send[log_ios_rank] == 0) {
	//c_ctxt->view_send[log_ios_rank] = 1;
	iot->first = 1;
      } 
      else
	iot->first = 0;
      
      if (!c_ctxt->pending_ios_ret[log_ios_rank]) {
	c_ctxt->pending_ios_ret[log_ios_rank] = 1;
	c_ctxt->pending_ios++;
      }
      request_queues_insert(&c_ctxt->task_queues, log_ios_rank , (void*)iot);
      num_tasks++;
#ifdef CLARISSE_TASK_DEBUG
      //if (myrank == 0)
	fprintf(stderr, 
		"C: create_io_task: Proc %d TASK l_dt=%lld r_dt=%lld l_v=%lld r_v=%lld l_f=%lld r_f=%lld log_ios_rank=%d first_char=%c last char=%c\n",
		myrank, iot->l_dt, iot->r_dt, crt_l_v, crt_r_v, crt_l_f, crt_r_f, log_ios_rank, iot->buf[iot->l_dt], iot->buf[iot->r_dt]); 
#endif
    }
    if (file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING){
      crt_l_f = BEGIN_NEXT_BLOCK(crt_l_f, b);
      active_log_ios_rank = (active_log_ios_rank + 1) % client_global_state.active_nr_servers;
    }
    else {
      crt_l_f =  BEGIN_BLOCK(crt_l_f, b) + part_block_len; 
      active_log_ios_rank ++;
      assert(active_log_ios_rank <= client_global_state.active_nr_servers);
    }
    log_ios_rank = client_global_state.active_log_servers[active_log_ios_rank];
	
#ifdef CLARISSE_TASK_DEBUG
    //if (myrank == 0)
    //  printf("C: crt_l_f=%lld log_ios_rank=%d\n", crt_l_f, log_ios_rank);
#endif
  } // while (crt_l_f <= r_f)
#ifdef CLARISSE_TASK_DEBUG  
  //if (myrank == 0)
    printf("C: num_tasks=%d\n", num_tasks);
#endif
  return num_tasks;
}


// for the file_partitioning2, which was not working
int OLDcreate_io_tasks(client_iocontext_t *c_ctxt, 
		    clarisse_off l_v, clarisse_off l_f, clarisse_off r_f,
		    distrib *partitioning_distr) {	
  clarisse_off crt_l_v, crt_l_f, crt_r_v, crt_r_f, begin_next_block;
  int  b, log_ios_rank, i, num_tasks; 

#ifdef CLARISSE_TASK_DEBUG  
  int myrank;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
#endif
#ifdef CLARISSE_TASK_DEBUG
  if (myrank == 0){
    printf("create_io_tasks: l_v=%lld, l_f=%lld r_f=%lld nr_servers=%d\n",l_v, l_f, r_f, client_global_state.active_nr_servers);
    distrib_print(partitioning_distr);
    }
#endif
  b = c_ctxt->clarisse_pars.buffer_size; 
  num_tasks = 0;
  c_ctxt->pending_ios = 0;
  for (i = 0; i < c_ctxt->clarisse_pars.nr_servers; i++) {
    c_ctxt->pending_ios_ret[i] = 0;
  }
  crt_l_f = l_f;
  if (distrib_contig(partitioning_distr)) 
    if (c_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)
      log_ios_rank = (l_f / b) % client_global_state.active_nr_servers ;
    else
      log_ios_rank = 0;
  else {
    log_ios_rank = find_bin_search(MIN((crt_l_f - partitioning_distr->displ) % ((partitioning_distr->flat->indices[1] - partitioning_distr->flat->indices[0]) * partitioning_distr->flat->count), partitioning_distr->flat->extent - 1 ), partitioning_distr, MAP_CURRENT_BLOCK);
    //assert(log_ios_rank == 0);
    //printf("log_ios_rank=%d nr_servers=%d\n", log_ios_rank, client_global_state.active_nr_servers);
  }
  while (crt_l_f <= r_f) {
    crt_r_f = MIN(r_f, END_BLOCK(crt_l_f, b));
    crt_l_v = func_sup(crt_l_f, c_ctxt->intermediary_target->distr);
    crt_r_v = func_inf(crt_r_f, c_ctxt->intermediary_target->distr); 
    if (crt_l_v <= crt_r_v) {
      iotask_p iot = (iotask_p) clarisse_malloc(sizeof(iotask));
      iot->count = crt_r_v - crt_l_v + 1;
      iot->l_f = crt_l_f;
      iot->r_f = crt_r_f;
      iot->l_dt = func_1(crt_l_v - l_v, c_ctxt->local_target->distr); 
      iot->r_dt = func_1(crt_r_v - l_v, c_ctxt->local_target->distr);
      iot->buf_distr = c_ctxt->local_target->distr;
      iot->buf = c_ctxt->local_target->local_handle.buf; 
      iot->log_ios_rank = log_ios_rank;
      
      if (c_ctxt->view_send[log_ios_rank] == 0) {
	c_ctxt->view_send[log_ios_rank] = 1;
	iot->first = 1;
      } 
      else
	iot->first = 0;
      
      if (!c_ctxt->pending_ios_ret[log_ios_rank]) {
	c_ctxt->pending_ios_ret[log_ios_rank] = 1;
	c_ctxt->pending_ios++;
      }
      request_queues_insert(&c_ctxt->task_queues, log_ios_rank , (void*)iot);
      num_tasks++;
#ifdef CLARISSE_TASK_DEBUG
      if (myrank == 0)
	fprintf(stderr, 
	      "create_io_task: Proc %d TASK l_dt=%lld r_dt=%lld l_v=%lld r_v=%lld l_f=%lld r_f=%lld log_ios_rank=%d first_char=%c\n",
		myrank, iot->l_dt, iot->r_dt, crt_l_v, crt_r_v, crt_l_f, crt_r_f, log_ios_rank, iot->buf[iot->l_dt]); 
#endif
    }
    
    //crt_l_f = BEGIN_NEXT_BLOCK(crt_l_f, b);
    begin_next_block = BEGIN_NEXT_BLOCK(crt_l_f, b);
    crt_l_f = func_1(func_sup(begin_next_block, 
			      partitioning_distr),
		     partitioning_distr);
#ifdef CLARISSE_TASK_DEBUG
    if (myrank == 0)
      printf("crt_l_f=%lld\n", crt_l_f);
#endif
    if (distrib_contig(partitioning_distr)) 
      if (c_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)     //|| (begin_next_block != crt_l_f))
	log_ios_rank = (log_ios_rank + 1) % client_global_state.active_nr_servers;
      else
	log_ios_rank =  (crt_l_f - BEGIN_BLOCK(l_f, b)) 
	  / (partitioning_distr->flat->extent / client_global_state.active_nr_servers) 
	  % client_global_state.active_nr_servers;
    else {
      // find_bin_search returns the index of the aggregator
      // TO DO: simplify the formula
      log_ios_rank = find_bin_search(MIN((crt_l_f - partitioning_distr->displ) % ((partitioning_distr->flat->indices[1] - partitioning_distr->flat->indices[0]) * partitioning_distr->flat->count), partitioning_distr->flat->extent - 1 ), 
				     partitioning_distr, MAP_CURRENT_BLOCK);
      //printf("log_ios_rank=%d nr_servers=%d cnt_passes=%d log_ios_rank2=%d\n", log_ios_rank, client_global_state.active_nr_servers, cnt_passes, cnt_passes/ client_global_state.active_nr_servers);
      //assert(log_ios_rank == cnt_passes / client_global_state.active_nr_servers);
    }
    //cnt_passes ++;
  } // while (crt_l_f <= r_f)
#ifdef CLARISSE_TASK_DEBUG  
  if (myrank == 0)
      printf("num_tasks=%d\n", num_tasks);
#endif
  assert(num_tasks <= client_global_state.active_nr_servers * c_ctxt->clarisse_pars.bufpool_size);
  return num_tasks;
}
