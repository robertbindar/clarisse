#include "util.h"
#ifdef CLARISSE_CONTROL
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include "mpi.h"
#include "error.h"
#include "cls_ctrl.h"
#include "clarisse.h"
#include "client_iocontext.h"

int handler_forward_request(void *eprops, clarisse_ctrl_event_t *event) {
  int err;
  clarisse_ctrl_event_props_t *ep;

  ep = (clarisse_ctrl_event_props_t *) eprops;
  printf("Forwarding requests\n");
  err = MPI_Send(&event->msg, event->msg_size, MPI_BYTE, ep->dest.rank, CLARISSE_CTRL_TAG, ep->dest.comm);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");
  return 0;
}

int scheduled_client = 0;
dllist request_queue = DLLIST_INITIALIZER;

int handler_request_start(void *eprops, clarisse_ctrl_event_t *event) {
  if (scheduled_client > 0) {
    clarisse_ctrl_event_t *aux_event;
    aux_event = clarisse_malloc(sizeof(clarisse_ctrl_event_t));
    memcpy(aux_event, event, sizeof(clarisse_ctrl_event_t));
    dllist_iat(&request_queue, &aux_event->link);
  }
  else {
    int err;
    clarisse_ctrl_msg_t msg;
    
    scheduled_client = 1;
    printf("%s: Received request start: SCHEDULE \n", __FUNCTION__);
    msg.type = CLARISSE_CTRL_MSG_IO_SCHEDULE; 
    err = MPI_Send(&msg, sizeof(int), MPI_BYTE, event->source.rank, CLARISSE_CTRL_TAG, event->source.comm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");
  }
  CLARISSE_UNUSED(eprops);
  return 0;
}

int handler_request_terminated(void *eprops, clarisse_ctrl_event_t *event) {

  if (dllist_is_empty(&request_queue)) 
    scheduled_client = 0;
  else {
    int err;
    clarisse_ctrl_msg_t msg;
    dllist_link *elm = dllist_rem_head(&request_queue);
    clarisse_ctrl_event_t *saved_event = DLLIST_ELEMENT(elm, clarisse_ctrl_event_t, link);

    printf("%s: Received request terminated: SCHEDULE \n", __FUNCTION__);
    msg.type = CLARISSE_CTRL_MSG_IO_SCHEDULE; 
    err = MPI_Send(&msg, sizeof(int), MPI_BYTE, saved_event->source.rank, CLARISSE_CTRL_TAG, saved_event->source.comm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");

  }
  CLARISSE_UNUSED(eprops);
  CLARISSE_UNUSED(event);
  return 0;
}




void test_schedule(){
  MPI_Comm comms[1];
  clarisse_ctrl_event_props_t eprops;
  int myrank;
  

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

  comms[0] = MPI_COMM_WORLD;
  cls_pub_sub_init(1, comms);
  
  if (myrank == 0) {
    eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
    eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
    eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
    eprops.dest_type = CLARISSE_EVENT_DYNAMIC_DESTINATION;
    eprops.handler = handler_request_start;
    cls_subscribe(&eprops);  

    eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
    eprops.handler = handler_request_terminated;
    cls_subscribe(&eprops);   
  }
  
  if (myrank > 0) {  
    clarisse_ctrl_event_t *event;
    clarisse_ctrl_msg_io_request_start_t *msg_request_start;
    clarisse_ctrl_msg_t msg;

    eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
    eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
    eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
    eprops.dest.rank = 0;
    eprops.dest.comm = MPI_COMM_WORLD;
    eprops.dest_type = CLARISSE_EVENT_USE_DESTINATION;
    eprops.handler = handler_forward_request;
    cls_subscribe(&eprops);

    eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
    eprops.handler = handler_forward_request;
    cls_subscribe(&eprops);

    eprops.type = CLARISSE_CTRL_MSG_IO_SCHEDULE;
    eprops.handler = NULL;
    cls_subscribe(&eprops);

    msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
    msg_request_start = (clarisse_ctrl_msg_io_request_start_t *)&msg;
    msg_request_start->client_id = myrank; 
    cls_publish(&msg, sizeof(clarisse_ctrl_msg_io_request_start_t));
    event = cls_wait_event(CLARISSE_CTRL_MSG_IO_SCHEDULE);
    clarisse_free(event);
    printf("Client id %d SCHEDULED!", myrank);
    sleep(1);
    msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
    cls_publish(&msg, sizeof(int));
  }
  sleep(2);
  MPI_Barrier(MPI_COMM_WORLD);
  cls_pub_sub_finalize();
}
#endif


int main(int argc, char **argv){
#ifdef CLARISSE_CONTROL  
  int provided;
  PMPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  test_schedule();
  PMPI_Finalize();
#else
  CLARISSE_UNUSED(argc);
  CLARISSE_UNUSED(argv);
#endif
  return 0;
}


