#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"


// A client writes, another client reads


#define WR 0
#define WRRD 1
#define RD 2

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void my_comp(double sleep_time){
  double t0;
  
  t0 = MPI_Wtime();
  while ((MPI_Wtime() - t0) < sleep_time);
}

void write_read_ntimes(int access_type, char *filename, int n, MPI_Datatype mem_type, int phases, double sleep_time) {
  int myrank, err, i, mem_type_size, nprocs;
  MPI_File fh;
  MPI_Status status;
  MPI_Aint lb, mem_type_extent;
  char *buf;
  MPI_Comm intracomm = cls_get_client_intracomm();
  
  MPI_Comm_size(intracomm, &nprocs);
  MPI_Comm_rank(intracomm, &myrank);
  MPI_Type_get_extent(mem_type, &lb, &mem_type_extent);
  MPI_Type_size(mem_type, &mem_type_size);

  buf = malloc(mem_type_extent * n);
  
  for (i = 0; i < mem_type_extent * n; i++){
    buf[i] = 'a' + i % 16;
  }
  if (myrank == 0)
    MPI_File_delete(filename, MPI_INFO_NULL);
  if ((access_type == WRRD) || (access_type == WR)){
    MPI_Barrier(intracomm);
    err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
			MPI_INFO_NULL, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
    
    MPI_Comm allclients_intracomm = cls_get_allclients_intracomm();
    MPI_Barrier(cls_get_allclients_intracomm());
    double start_time = MPI_Wtime();
    
    for (i = 0; i < phases; i++) {
      MPI_Offset off = (MPI_Offset)n*(MPI_Offset)myrank + (MPI_Offset)i*(MPI_Offset)n*(MPI_Offset)nprocs;
      err = MPI_File_write_at_all(fh, off, buf, n, mem_type, &status);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_write_all");
      my_comp(sleep_time);
    }
    double finish_time = MPI_Wtime();
    MPI_Barrier(allclients_intracomm);
    double min_start_time, max_finish_time, min_start_time_all = 0;
    MPI_Reduce(&start_time, &min_start_time, 1, MPI_DOUBLE, MPI_MIN, 0, intracomm); 
    MPI_Reduce(&finish_time, &max_finish_time, 1, MPI_DOUBLE, MPI_MAX, 0, intracomm);
    MPI_Allreduce(&start_time, &min_start_time_all, 1, MPI_DOUBLE, MPI_MIN, allclients_intracomm); 
    if (myrank == 0)
      printf("Filename %s min_start_time=%9.6f max_finish_time=%9.6f total_time=%9.6f\n", filename, min_start_time - min_start_time_all, max_finish_time - min_start_time_all, max_finish_time - min_start_time);

    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
    
  }
  if ((access_type == WRRD) || (access_type == RD)){
    MPI_Barrier(intracomm);
    err = MPI_File_open(intracomm, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
    
    for (i = 0; i < phases; i++) {
      //double t0,t1,tmp_t0,tmp_t1;
      MPI_Offset off = (MPI_Offset)n*(MPI_Offset)myrank + (MPI_Offset)i*(MPI_Offset)n*(MPI_Offset)nprocs;
      //t0 = MPI_Wtime();
      err = MPI_File_read_at_all(fh, off, buf, n, mem_type, &status);
      /*t1 = MPI_Wtime();
	MPI_Reduce(&t0, &tmp_t0, 1, MPI_DOUBLE, MPI_MIN, 0, intracomm); 
	MPI_Reduce(&t1, &tmp_t1, 1, MPI_DOUBLE, MPI_MAX, 0, intracomm); 
	if (myrank == 0)
	printf("Read time = %9.6f\n", tmp_t1 -  tmp_t0);
      */
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_read_all");
    }
    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
  }
  free(buf);
}


void remove_aggrs(int n) {
  int i, nr_aggr;
  
  nr_aggr = cls_get_nr_active_servers();
  for (i = 0; i < n; i++) {
    int log_ios_rank, flag;
    
    flag = 1;
    while(flag) {
      log_ios_rank = ((int)random()) % nr_aggr;
      flag = cls_active_server_down(log_ios_rank);
      /*      int myrank;
      MPI_Comm intracomm = cls_get_client_intracomm();
      MPI_Comm_rank(intracomm, &myrank);
      if (myrank == 0) {
	printf("Shut down %d flag=%d\n", log_ios_rank, flag);
	cls_print_servers();
	}*/
    }
    
  }
}

void test0(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time) {
  write_read_ntimes(access_type, filename, n, type, phases, sleep_time);
}

void test1(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time) {
  char *val;

  if ((val = getenv("CLARISSE_COLLECTIVE"))  && (!strcmp(val, "romio")))
    test0(access_type, filename, n, type, phases, sleep_time);
  else {

    int nr_aggr, i;
    
    nr_aggr = cls_get_nr_active_servers();
    
    for (i = nr_aggr; i >= 64; i = i / 2) {
      int myrank;
      MPI_Comm intracomm = cls_get_client_intracomm();
      
      MPI_Comm_rank(intracomm, &myrank);
      if (myrank == 0)
	printf("Write n times crt_nr_aggr=%d cls_get_nr_active_servers()=%d\n",i,cls_get_nr_active_servers() ); 
      write_read_ntimes(WR, filename, n, type, phases, sleep_time);
      remove_aggrs(i/2);
    }
  }
}


void test2(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time) {
  char *val;
  double load;
  
  if ((val = getenv("CLARISSE_COUPLENESS"))  && (!strcmp(val, "intercomm"))){

    int  i, active_log_ios_rank, myrank; //nr_aggr,
    MPI_Comm intracomm = cls_get_client_intracomm();
  
    MPI_Comm_rank(intracomm, &myrank);
    //nr_aggr = cls_get_nr_active_servers();
    for (i = 0; i <= 1; i++) {
      
      for (load = 1e-6; load  < 1e-3; load *= 2) {
	if (myrank == 0) {
	  for (active_log_ios_rank = 0; active_log_ios_rank < i; active_log_ios_rank++)
	    cls_active_server_slow(active_log_ios_rank, load);
	  printf("write_read_n_times load=%10.6f\n", load );
	}
	MPI_Barrier(intracomm);
	write_read_ntimes(access_type, filename, n, type, phases, sleep_time);
	if (i == 0) break;
      }
    }
  }
  else 
    printf("test 2 works only for INTERCOMM coupleness\n");
}

int main(int argc, char **argv)
{
  int myrank, nprocs, n, phases, sleep_time, nr_servers, my_nprocs, client_id;
  int client_nprocs[2];
  char *env;
  char filename[256];
  setenv("CLARISSE_COUPLENESS", "intercomm", 1 /*overwrite*/);
  setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
  setenv("CLARISSE_MAX_DCACHE_SIZE", "1", 1 /*overwrite*/);

  env = getenv("CLARISSE_NR_SERVERS");
  if (!env) {
    fprintf(stderr, "%s error: CLARISSE_NR_SERVERS env var not specified\n", __FILE__);
    exit(1);
  }  
  nr_servers = atoi(env);
  env = getenv("MY_NPROCS");
  if (!env) {
    fprintf(stderr, "%s error: MY_NPROCS env var not specified\n", __FILE__);
    exit(1);
  }
  my_nprocs = atoi(env);
  env = getenv("CLARISSE_COUPLENESS");
  if ((!env)  || (strcmp(env, "intercomm"))){
    fprintf(stderr, "%s error: CLARISSE_COUPLENESS must be intercomm\n", __FILE__);
    exit(1);
  }

  client_nprocs[0] = (my_nprocs - nr_servers) / 2;
  client_nprocs[1] = my_nprocs - nr_servers - client_nprocs[0];
  cls_set_clients(2, client_nprocs);
  //printf("client_nprocs[0] = %d client_nprocs[1]=%d\n", client_nprocs[0], client_nprocs[1]);

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  
  assert(my_nprocs == nprocs);


  if (argc < 5) {
    if (myrank == 0) {
      fprintf(stderr, "Missing argument, call %s filename access_size_per_proc phases sleep_time\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  sprintf(filename, "%s", argv[1]);
  n = atoi(argv[2]);
  phases = atoi(argv[3]);
  sleep_time = atoi(argv[4]);

  client_id = cls_get_client_id();
  if (client_id == 1){

    test0(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test0(WRRD, filenames[client_id - 1], n, MPI_BYTE, phases, (double)sleep_time);
    //test1(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test2(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
  }
  if (client_id == 2)  {
    //sleep(120);
    test0(RD, filename, n, MPI_BYTE, phases, (double)sleep_time);
    // test0(WRRD, filenames[client_id - 1], n, MPI_BYTE, phases, (double)sleep_time);
    //test1(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test2(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
  }
  
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
  return 0; 
}
