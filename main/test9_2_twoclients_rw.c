#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"


#define SIZE 1024

#define WR 0
#define WRRD 1
#define RD 2

int lcm(int x, int y); 

char buf1[SIZE+1]="abcdefghijklmnopqrstuvwxz";
char buf2[SIZE+1]="\0";

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void setview_exec(int access_type, MPI_Datatype d, MPI_Offset displ, MPI_Info info, char *filename, int n) {
  int myrank, err;
  MPI_File fh;
  MPI_Status status;
  MPI_Comm intracomm = cls_get_client_intracomm();
  int i;
  
  for (i = 0; i < n; i++)
    buf1[i] = 'a' + i % 16;
  bzero(buf2, n + 1);
  MPI_Comm_rank(intracomm, &myrank);  
  if (myrank == 0)
    MPI_File_delete(filename, MPI_INFO_NULL);
  if ((access_type == WRRD) || (access_type == WR)){
    MPI_Barrier(intracomm);
    //    create_preexisting_file(filename, 16 * n);
  
    err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
			info, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
    
    err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_set_view");
    
    err = MPI_File_write_all(fh, buf1, n, MPI_BYTE, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_write_all");
  
  
    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
  }

 if ((access_type == WRRD) || (access_type == RD)){
   MPI_Barrier(intracomm);
     
   err = MPI_File_open(intracomm, filename, MPI_MODE_RDWR ,
		      info, &fh);
   if (err != MPI_SUCCESS)
     handle_error(err,"MPI_File_open");
   
   err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
   if (err != MPI_SUCCESS)
     handle_error(err,"MPI_File_set_view"); 
   
   err = MPI_File_read_at_all(fh, 0, buf2, n, MPI_CHAR, &status);
   if (err != MPI_SUCCESS)
     handle_error(err,"MPI_File_read_all");
   
   err = MPI_File_close(&fh);
   if (err != MPI_SUCCESS)
     handle_error(err,"MPI_File_close");
   /*   
   if (strncmp(buf1, buf2, n)) {
     buf1[n]='\0';
     buf2[n]='\0';
     fprintf(stderr, "Test %s File %s: \n\tWritten buf=%s\n\tRead buf=%s\n", 
	     __FILE__, filename, buf1, buf2);
     handle_error(err,"Read a different value than written");
   }
   */
 }


}


#define DO_NOT_ADD_RANK_TO_DISPL 0
#define ADD_RANK_TO_DISPL 1

void setview_test1(int argc, char **argv, MPI_Datatype d, MPI_Offset displ, int n, int add_rank, int nprocs[]) {
  int myrank, client_id, lcm_c;
  MPI_Comm intracomm;
  char nr_serv[16];
  
  lcm_c = lcm(nprocs[1], nprocs[2]);
 
  sprintf(nr_serv, "%d", nprocs[0]);
  setenv("CLARISSE_NR_SERVERS", nr_serv, 1 /*overwrite*/);
  cls_set_clients(2, &(nprocs[1]));
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(cls_get_allclients_intracomm(), &myrank);
  if (argc < 2) {
    if (myrank == 0) {
      fprintf(stderr, "Missing argument, call %s filename\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (lcm_c * n > SIZE) {
    if (myrank == 0) {
      fprintf(stderr, "Trying to write %d bytes / proc > SIZE = %d\n", lcm_c * n , SIZE);
    }
    MPI_Finalize();
    exit(1);
  }
  else 
    if (myrank == 0) {
      printf("Client 1: nprocs = %d read %d bytes (%d bytes / proc)\n", nprocs[1],  lcm_c * n, lcm_c * n / nprocs[1] );
      printf("Client 2: nprocs = %d write %d bytes (%d bytes / proc)\n", nprocs[2],  lcm_c * n, lcm_c * n / nprocs[2] );
    }

  intracomm = cls_get_client_intracomm();
  MPI_Comm_rank(intracomm, &myrank);
  
  client_id = cls_get_client_id();
  if (client_id == 1) {
    if (add_rank == ADD_RANK_TO_DISPL)
      displ = displ + myrank * n * (lcm_c / nprocs[1]);
    setview_exec(RD, d, displ, MPI_INFO_NULL, argv[1], n * (lcm_c / nprocs[1]) );
  }
  if (client_id == 2) {
    if (add_rank == ADD_RANK_TO_DISPL)
      displ = displ + myrank * n * (lcm_c / nprocs[2]);
    sleep(2);
    setview_exec(WR, d, displ, MPI_INFO_NULL, argv[1], n * (lcm_c / nprocs[2]));
  }
  free_datatype(d);
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
}




void setview_test2(int argc, char **argv, MPI_Datatype d1, MPI_Offset displ1, int n, MPI_Datatype d2, MPI_Offset displ2, int nprocs[]) {
  int myrank, client_id, lcm_c;
  MPI_Comm intracomm;
  char nr_serv[16];
  
  lcm_c = lcm(nprocs[1], nprocs[2]);
  sprintf(nr_serv, "%d", nprocs[0]);
  setenv("CLARISSE_NR_SERVERS", nr_serv, 1 /*overwrite*/);
  cls_set_clients(2, &(nprocs[1]));
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(cls_get_allclients_intracomm(), &myrank);
  if (argc < 2) {
    if (myrank == 0) {
      fprintf(stderr, "Missing argument, call %s filename\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (lcm_c * n > SIZE) {
    if (myrank == 0) {
      fprintf(stderr, "Trying to write %d bytes / proc > SIZE = %d\n", lcm_c * n , SIZE);
    }
    MPI_Finalize();
    exit(1);
  }
  else 
    if (myrank == 0) {
      printf("Client 1: nprocs = %d read %d bytes (%d bytes / proc)\n", nprocs[1],  lcm_c * n, lcm_c * n / nprocs[1] );
      printf("Client 2: nprocs = %d write %d bytes (%d bytes / proc)\n", nprocs[2],  lcm_c * n, lcm_c * n / nprocs[2] );
    }

  intracomm = cls_get_client_intracomm();
  MPI_Comm_rank(intracomm, &myrank);
  
  client_id = cls_get_client_id();
  if (client_id == 1) {
    setview_exec(RD, d1, displ1, MPI_INFO_NULL, argv[1], n * (lcm_c / nprocs[1]) );
  }
  if (client_id == 2) {
    sleep(2);
    setview_exec(WR, d2, displ2, MPI_INFO_NULL, argv[1], n * (lcm_c / nprocs[2]));
  }
  free_datatype(d1);
  free_datatype(d2);
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
}



int main(int argc, char **argv)
{
  char *env;
  int nprocs[2];

  setenv("CLARISSE_COUPLENESS", "intercomm", 1 /*overwrite*/);
  setenv("CLARISSE_READ_FUTURE", "enable", 1 /*overwrite*/);
  setenv("CLARISSE_FILE_PARTITIONING","static", 1 /*overwrite*/);
  setenv("CLARISSE_COLLECTIVE","vb", 1 /*overwrite*/);
  setenv("CLARISSE_WRITE_TYPE","ondemand", 1 /*overwrite*/);
  setenv("CLARISSE_AGGR_THREAD","thread", 1 /*overwrite*/);
  setenv("CLARISSE_MAX_DCACHE_SIZE", "1", 1 /*overwrite*/);
  env = getenv("ENV_FROM_SCRIPT");
  if (!env || strncmp(env, "true", 4)) { 
    //  setenv("CLARISSE_BUFPOOL_SIZE", "16", 1 /*overwrite*/);
    setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
    //setenv("CLARISSE_LAZY_EAGER_VIEW", "eager", 1 /*overwrite*/);
    env = getenv("CLARISSE_COLLECTIVE");
    if (!env || strncmp(env, "listio", 6)) { 
      setenv("CLARISSE_BUFFER_SIZE", "16", 1 /*overwrite*/);
      //setenv("CLARISSE_NET_BUF_SIZE", "3", 1 /*overwrite*/);
      setenv("CLARISSE_NET_BUF_SIZE", "16", 1 /*overwrite*/);
     }
    else {
      setenv("CLARISSE_BUFFER_SIZE", "16", 1 /*overwrite*/);
      setenv("CLARISSE_NET_BUF_SIZE", "12", 1 /*overwrite*/);
    }
  }
  // nprocs[0]: nr processes in servers 
  // nprocs[1]: nr_processes in client 1
  // nprocs[2]: nr_processes in client 2
  //nprocs[0] = nprocs[1] = nprocs[2] = 1;
  //setview_test1(argc, argv, MPI_BYTE, 0, 1, ADD_RANK_TO_DISPL, nprocs);
  
  //nprocs[0] = 2; nprocs[1] = nprocs[2] = 1;
  //setview_test1(argc, argv, MPI_BYTE, 0, 1, ADD_RANK_TO_DISPL, nprocs);
  //setview_test1(argc, argv, MPI_BYTE, 0, 17, ADD_RANK_TO_DISPL, nprocs);
  //setview_test1(argc, argv, MPI_BYTE, 0, 256, ADD_RANK_TO_DISPL, nprocs);

  //nprocs[0] = 1; nprocs[1] = 2; nprocs[2] = 1; 
  //setview_test1(argc, argv, MPI_BYTE, 0, 1, ADD_RANK_TO_DISPL, nprocs);
  //setview_test1(argc, argv, MPI_BYTE, 0, 17, ADD_RANK_TO_DISPL, nprocs);
  //setview_test1(argc, argv, MPI_BYTE, 0, 256, ADD_RANK_TO_DISPL, nprocs);

  nprocs[0] = 3; nprocs[1] = 2; nprocs[2] = 1; 
  setview_test1(argc, argv, MPI_BYTE, 0, 256, ADD_RANK_TO_DISPL, nprocs);
  
  /*
  nprocs[0] = 1; nprocs[1] = 2; nprocs[2] = 2; 
  
  client_id = cls_get_client_id();
  intracomm = cls_get_client_intracomm();
  if (client_id == 1) {

  }
  if (client_id == 2) {

  }
  setview_test2(argc, argv, MPI_BYTE, myrank1 * 256, , , 256, nprocs);
  */
  


  //setview_test1(argc, argv, MPI_BYTE, 10, 1, ADD_RANK_TO_DISPL, nprocs);
  //setview_test1(argc, argv, get_vec_nstride(cls_get_client_intracomm()), 0, 2, ADD_RANK_TO_DISPL, nprocs);
  //  setview_test1(argc, argv, get_vec_nstride(intracomm), 9, 2, ADD_RANK_TO_DISPL, nprocs);
  //if (nprocs == 4) setview_test1(argc, argv, get_darray(intracomm), 0, 64/nprocs, DO_NOT_ADD_RANK_TO_DISPL, nprocs);
  

  return 0; 
}


int lcm(int x, int y) {
  int myx, myy;

  myx = x;
  myy = y;
  if (x == 0) {
    return y;
  }
 
  while (y != 0) {
    if (x > y) {
      x = x - y;
    }
    else {
      y = y - x;
    }
  }
 
  return (int)  (((long long int) myx * (long long int) myy) / x) ;
}
