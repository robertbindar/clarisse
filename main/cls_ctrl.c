#ifdef CLARISSE_CONTROL
#include <sys/time.h>
#include <errno.h>
#include <assert.h>
#include "mpi.h"
#include "client_iocontext.h"
#include "server_iocontext.h"
#include "cls_pub_sub.h" 
#include "cls_ctrl.h" 
#include "error.h"
#include "util.h"

#ifdef CLARISSE_BEACON
#include "beacon.h"
BEACON_beep_handle_t beacon_handle;
void load_handler(BEACON_receive_topic_t * caught_topic)
{
  fprintf(stderr,
	  "Received topic details: Name=%s, Beep name=%s, Hostname=%s, Scope = %s, Seqnum=%d Message: %s\n",
	  caught_topic->topic_name, caught_topic->beep_name, caught_topic->incoming_src.hostname, caught_topic->topic_scope, caught_topic->seqnum, caught_topic->topic_payload);     
}
#endif 

#define PUBLISH_INTERVAL_SEC 1

extern client_global_state_t  client_global_state;
extern server_global_state_t  server_global_state;
extern int i_am_client;
cls_controller_t cls_controller;
cls_global_controller_t cls_global_controller;


int cls_handler_forward_request(void *eprops, clarisse_ctrl_event_t *event) {
  int err;
  clarisse_ctrl_event_props_t *ep;

  ep = (clarisse_ctrl_event_props_t *) eprops;
#ifdef CLARISSE_DEBUG_CTRL
  printf("Forwarding requests\n");
#endif
  err = MPI_Send(&event->msg, event->msg_size, MPI_BYTE, ep->dest.rank, CLARISSE_CTRL_TAG, ep->dest.comm);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");
  return 0;
}

int cls_handler_request_start(void *eprops, clarisse_ctrl_event_t *event) {
  if (cls_global_controller.scheduled_client > 0) {
    clarisse_ctrl_event_t *aux_event;
    aux_event = clarisse_malloc(sizeof(clarisse_ctrl_event_t));
    memcpy(aux_event, event, sizeof(clarisse_ctrl_event_t));
    dllist_iat(&cls_global_controller.request_queue, &aux_event->link);
  }
  else {
    int err;
    clarisse_ctrl_msg_t msg;
    
    cls_global_controller.scheduled_client = 1;
#ifdef CLARISSE_DEBUG_CTRL
    printf("%s: Received request start: SCHEDULE \n", __FUNCTION__);
#endif
    msg.type = CLARISSE_CTRL_MSG_IO_SCHEDULE; 
    err = MPI_Send(&msg, sizeof(int), MPI_BYTE, event->source.rank, CLARISSE_CTRL_TAG, event->source.comm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");
  }
  CLARISSE_UNUSED(eprops);
  return 0;
}

int cls_handler_request_terminated(void *eprops, clarisse_ctrl_event_t *event) {

  if (dllist_is_empty(&cls_global_controller.request_queue)) 
    cls_global_controller.scheduled_client = 0;
  else {
    int err;
    clarisse_ctrl_msg_t msg;
    dllist_link *elm = dllist_rem_head(&cls_global_controller.request_queue);
    clarisse_ctrl_event_t *saved_event = DLLIST_ELEMENT(elm, clarisse_ctrl_event_t, link);
#ifdef CLARISSE_DEBUG_CTRL
    printf("%s: Received request terminated: SCHEDULE \n", __FUNCTION__);
#endif
    msg.type = CLARISSE_CTRL_MSG_IO_SCHEDULE; 
    err = MPI_Send(&msg, sizeof(int), MPI_BYTE, saved_event->source.rank, CLARISSE_CTRL_TAG, saved_event->source.comm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");

  }
  CLARISSE_UNUSED(eprops);
  CLARISSE_UNUSED(event);
  return 0;
}


int received_coll_op_seq = 0;
int last_coll_op_seq = -1;
// C -> LC CLARISSE_CTRL_MSG_IO_STOP(active_log_ios_rank)
int cls_handler_io_server_down(void *eprops, clarisse_ctrl_event_t *event) {
  int err;
  int rank, nprocs;

#ifdef CLARISSE_DEBUG_CTRL
  printf("cls_handler_io_server_down\n");
#endif
  received_coll_op_seq = 0;
  last_coll_op_seq = -1;

  event->msg.type = CLARISSE_CTRL_MSG_IO_STOP;
  MPI_Comm_size(client_global_state.intracomm, &nprocs);
  for (rank = 0; rank < nprocs; rank++) {
    err = MPI_Send(&event->msg, event->msg_size, MPI_BYTE, rank, CLARISSE_CTRL_TAG, client_global_state.intracomm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");
  }
  CLARISSE_UNUSED(eprops);
  return 0;
}

// C -> LC CLARISSE_CTRL_MSG_IO_RESUME(last_coll_op_seq)
int cls_handler_io_current_coll_op_seq(void *eprops, clarisse_ctrl_event_t *event) {
  int nprocs;
  clarisse_ctrl_msg_io_current_coll_op_seq_t *msg_io_current_coll_op_seq;

#ifdef CLARISSE_DEBUG_CTRL
  printf("cls_handler_io_current_coll_op_seq\n");
#endif
  msg_io_current_coll_op_seq = (clarisse_ctrl_msg_io_current_coll_op_seq_t *) &event->msg;
  
  MPI_Comm_size(client_global_state.intracomm, &nprocs);
  received_coll_op_seq++;
  last_coll_op_seq = MAX(last_coll_op_seq, msg_io_current_coll_op_seq->coll_op_seq);

  if (received_coll_op_seq == nprocs) {
    int err, rank; 
    clarisse_ctrl_msg_io_last_coll_op_seq_t msg_io_last_coll_op_seq;
    clarisse_ctrl_msg_t *msg;
    int msg_size = sizeof(clarisse_ctrl_msg_io_last_coll_op_seq_t);
    
    msg = (clarisse_ctrl_msg_t *) &msg_io_last_coll_op_seq;
    msg_io_last_coll_op_seq.type = CLARISSE_CTRL_MSG_IO_RESUME;
    msg_io_last_coll_op_seq.last_coll_op_seq = last_coll_op_seq;
    
    for (rank = 0; rank < nprocs; rank++) {
      err = MPI_Send(msg, msg_size, MPI_BYTE, rank, CLARISSE_CTRL_TAG, client_global_state.intracomm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Send error");
    }
    received_coll_op_seq = 0;
    last_coll_op_seq = -1;
  }
  CLARISSE_UNUSED(eprops);
  return 0;
}

int cls_ctrl_init(){
#ifndef CLARISSE_BEACON
  
  int myrank_global, myrank;
  clarisse_ctrl_event_props_t eprops;
  cls_global_controller.rank = client_global_state.global_controller_rank = 
    client_global_state.server_leader;
  if (i_am_client) {
    cls_controller.rank = client_global_state.controller_rank;
    cls_controller.comm = client_global_state.intracomm;
    //cls_global_controller.rank = client_global_state.global_controller_rank;
 
  }
  else {
    cls_controller.rank = server_global_state.controller_rank;
    cls_controller.comm = server_global_state.intracomm;
    //cls_global_controller.rank = server_global_state.global_controller_rank;
  }
  MPI_Comm_size(cls_controller.comm, &cls_controller.nprocs);
  MPI_Comm_rank(cls_controller.comm, &myrank);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank_global);
  cls_global_controller.scheduled_client = 0;
  dllist_init(&cls_global_controller.request_queue);

  int nr_comms = 0;
  MPI_Comm comms[2];
  
  if  (((myrank_global == cls_global_controller.rank) ||
       (myrank == cls_controller.rank)) && 
       (client_global_state.clarisse_pars.elasticity != CLARISSE_ELASTICITY_ENABLED))
    comms[nr_comms++] = MPI_COMM_WORLD;
  if (client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) {
    if (i_am_client) 
      comms[nr_comms++] = client_global_state.intracomm;
    else
      comms[nr_comms++] = server_global_state.intracomm;
  }
  if (nr_comms > 0)
    cls_pub_sub_init(nr_comms, comms);
  
  /*
  if ((client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) ||
      (myrank_global == cls_global_controller.rank) ||
      (myrank == cls_controller.rank)) {
    MPI_Comm comms[2];
    comms[0] = MPI_COMM_WORLD;
    if (i_am_client) 
      comms[1] = client_global_state.intracomm;
    else
      comms[1] = server_global_state.intracomm;
    cls_pub_sub_init(2, comms);
  }
  */
  
  // Global controller subscribes to rq_start and rq_terminated
  if (myrank_global == cls_global_controller.rank) {
    eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
    eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
    eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
    eprops.dest_type = CLARISSE_EVENT_DYNAMIC_DESTINATION;
    eprops.handler = cls_handler_request_start;
    cls_subscribe(&eprops);  

    eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
    eprops.handler = cls_handler_request_terminated;
    cls_subscribe(&eprops); 
  }
  if (i_am_client) {
    eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
    eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
    
    if (myrank == cls_controller.rank){
      eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
      eprops.dest.rank = cls_global_controller.rank;
      eprops.dest.comm = MPI_COMM_WORLD;
      eprops.dest_type = CLARISSE_EVENT_USE_DESTINATION;
      eprops.handler = cls_handler_forward_request;
      cls_subscribe(&eprops);
      
      eprops.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
      eprops.handler = cls_handler_forward_request;
      cls_subscribe(&eprops);
      
      eprops.type = CLARISSE_CTRL_MSG_IO_SCHEDULE;
      eprops.handler = NULL;
      cls_subscribe(&eprops);

      if (client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) {
	eprops.type = CLARISSE_CTRL_MSG_IO_SERVER_DOWN;
	if (client_global_state.clarisse_pars.elasticity_policy == 0) {
	  eprops.handler = cls_handler_io_server_down;
	  cls_subscribe(&eprops);
	  eprops.type = CLARISSE_CTRL_MSG_IO_CURRENT_COLL_OP_SEQ;
	  eprops.handler = cls_handler_io_current_coll_op_seq;
	}
	else
	  eprops.handler = NULL; // elasticity2
	cls_subscribe(&eprops);	
      }
    }
    if (client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) {
      if (client_global_state.clarisse_pars.elasticity_policy == 0) {
	eprops.type = CLARISSE_CTRL_MSG_IO_STOP;
	eprops.handler = NULL;
	cls_subscribe(&eprops);
	eprops.type = CLARISSE_CTRL_MSG_IO_RESUME;
	eprops.handler = NULL;
	cls_subscribe(&eprops);
      }
    }
  }

#endif
  return 0;
}

int cls_ctrl_finalize(){
  cls_pub_sub_finalize();
  return 0;
}

#endif
