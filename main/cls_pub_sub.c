#ifdef CLARISSE_CONTROL
#include <sys/time.h>
#include <errno.h>
#include <assert.h>
#include "mpi.h"
#include "client_iocontext.h"
#include "cls_pub_sub.h" 
#include "error.h"
#include "util.h"

#ifdef CLARISSE_BEACON
#include "beacon.h"
BEACON_beep_handle_t beacon_handle;
void load_handler(BEACON_receive_topic_t * caught_topic)
{
  fprintf(stderr,
	  "Received topic details: Name=%s, Beep name=%s, Hostname=%s, Scope = %s, Seqnum=%d Message: %s\n",
	  caught_topic->topic_name, caught_topic->beep_name, caught_topic->incoming_src.hostname, caught_topic->topic_scope, caught_topic->seqnum, caught_topic->topic_payload);     
}
#endif 

#define PUBLISH_INTERVAL_SEC 1

int publisher_running = 0;
int subscriber_running = 0;

pthread_t sub_tid, pub_tid;

dllist event_queue = DLLIST_INITIALIZER;
pthread_mutex_t mutex_event_queue = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condvar_wait_any_event = PTHREAD_COND_INITIALIZER;

// The events for which there is no waiter && which do not have an associated handler are saved in this queue
// 
dllist local_event_queue = DLLIST_INITIALIZER;
pthread_mutex_t mutex_local_event_queue = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condvar_local_wait_event = PTHREAD_COND_INITIALIZER;

clarisse_ctrl_event_props_t *eprops_table = NULL;
pthread_mutex_t mutex_eprops_table = PTHREAD_MUTEX_INITIALIZER;
MPI_Comm *comm_table;
int comm_count;


void* publisher_thread(/*void *ptr*/){
  struct timespec timeout;
  int done = 0;

  clock_gettime(CLOCK_REALTIME, &timeout);
  timeout.tv_sec += PUBLISH_INTERVAL_SEC;
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("Publisher started\n");
#endif  
  pthread_mutex_lock (&mutex_event_queue);
  while (!done) {
    int err;
#ifdef CLARISSE_PUB_SUB_DEBUG
    printf("Blocking waiting for an event\n");
#endif
    err = pthread_cond_timedwait (&condvar_wait_any_event,&mutex_event_queue, &timeout);
    if ((err != 0) && (err != ETIMEDOUT)){
	perror("pthread_cond_timedwait");
	exit(1);
    }   
    if (err == ETIMEDOUT) {
#ifdef CLARISSE_PUB_SUB_DEBUG
      printf("TIMEOUT, check again the condition\n");
#endif 
      timeout.tv_sec += PUBLISH_INTERVAL_SEC;
    }
 
    while (!dllist_is_empty(&event_queue)) {
      dllist_link *elm = dllist_rem_head(&event_queue);
      clarisse_ctrl_event_t *event = DLLIST_ELEMENT(elm, clarisse_ctrl_event_t, link);
      clarisse_ctrl_event_props_t *ep;

      pthread_mutex_unlock (&mutex_event_queue);
      if (event->msg.type == CLARISSE_CTRL_MSG_DONE)
	done = 1;
	
      pthread_mutex_lock (&mutex_eprops_table);
      HASH_FIND_INT(eprops_table, &event->msg.type, ep);
      if (ep && (ep->handler)) 
	ep->handler_refcount++;
      pthread_mutex_unlock(&mutex_eprops_table);
      if (ep && (ep->handler)) 
	ep->handler((void *)ep, event);
      pthread_mutex_lock (&mutex_eprops_table);
      if (ep && (ep->handler)) 
	ep->handler_refcount--;
      pthread_mutex_unlock(&mutex_eprops_table);
      clarisse_free(event);
      pthread_mutex_lock(&mutex_event_queue);
    }
  }
  pthread_mutex_unlock(&mutex_event_queue);
#ifdef CLARISSE_PUB_SUB_DEBUG  
  printf("PUBLISHER DONE\n");
#endif
  return NULL;
}



void * subscriber_thread(/*void *ptr*/)
{
  int done = 0;
  //int *pval = (int*)ptr;
  //t0 = MPI_Wtime();
  while (!done) {
    clarisse_ctrl_msg_t msg;
    clarisse_ctrl_event_t *event;
    MPI_Status status;
    int err, msg_size;
    int comm_idx = 0;
#ifdef CLARISSE_PUB_SUB_DEBUG
    printf("SUBSCRIBER recving ...\n");
#endif
    if (comm_count > 1) {
      int received = 0;
      while (!received) {
	comm_idx = (comm_idx + 1) % comm_count;
	err = MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm_table[comm_idx], &received,&status);
	if (err != MPI_SUCCESS)
	  handle_error(err, "MPI_Iprobe error\n");	
      }
    }
    err = MPI_Recv(&msg, CLARISSE_CTRL_MAX_MSG_SIZE, MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG, comm_table[comm_idx], &status);
    if (err != MPI_SUCCESS)
	handle_error(err, "MPI_Recv error\n");
#ifdef CLARISSE_PUB_SUB_DEBUG    
    printf("Received event =%d \n", msg.type);
#endif
    if (msg.type == CLARISSE_CTRL_MSG_DONE) 
      done = 1;
    else {
      clarisse_ctrl_event_props_t *ep;
      int null_handler;
      event = (clarisse_ctrl_event_t *) clarisse_malloc(sizeof(clarisse_ctrl_event_t));
      MPI_Get_count(&status, MPI_BYTE, &msg_size);
      memcpy(&(event->msg), &msg, msg_size);
      event->msg_size = msg_size;
      event->source.rank = status.MPI_SOURCE;
      event->source.comm = comm_table[comm_idx];
      
#ifdef CLARISSE_PUB_SUB_DEBUG
      printf("1:Try to acquire mutex_eprops_table\n");
#endif
      pthread_mutex_lock (&mutex_eprops_table);
#ifdef CLARISSE_PUB_SUB_DEBUG
      printf("1:Acquired mutex_eprops_table\n");
#endif
      HASH_FIND_INT(eprops_table, &event->msg.type, ep);
      if (ep->handler == NULL) 
	null_handler = 1;
      else 
	null_handler = 0;
      pthread_mutex_unlock(&mutex_eprops_table);
#ifdef CLARISSE_PUB_SUB_DEBUG
      printf("1:Released mutex_eprops_table\n");
      printf("null_handler=%d\n", null_handler);
#endif
      if (null_handler) {
	pthread_mutex_lock(&mutex_local_event_queue);
	dllist_iat(&local_event_queue, &event->link);
	pthread_cond_broadcast(&condvar_local_wait_event);
	pthread_mutex_unlock(&mutex_local_event_queue);
       }
      else {
 	pthread_mutex_lock(&mutex_event_queue);
	dllist_iat(&event_queue, &event->link);
	pthread_cond_signal(&condvar_wait_any_event);
	pthread_mutex_unlock(&mutex_event_queue);
     }
    }
  }
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("SUBSCRIBER DONE\n");
#endif  
  return NULL;
}


int handler_done(void *eprops, clarisse_ctrl_event_t *event) {
  int err;
  clarisse_ctrl_event_props_t *ep;
  
  ep = (clarisse_ctrl_event_props_t *) eprops;
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("Sending DONE\n");
#endif
  err = MPI_Send(&event->msg, event->msg_size, MPI_BYTE, ep->dest.rank, CLARISSE_CTRL_TAG, ep->dest.comm);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");
  return 0;
}


int cls_pub_sub_init(int nr_comms, MPI_Comm comms[]){
  
#ifdef CLARISSE_BEACON
  int ret;
  BEACON_beep_t binfo;
  /* Specify the beep information needed by the BEACON_Connect */
  memset(&binfo, 0, sizeof(binfo));
  strcpy(binfo.beep_version, "1.0");
  //    sprintf(beep_name, "beacon_pub_%d", getpid());
  strcpy(binfo.beep_name, "beacon_clarisse");
  
  /* Connect to BEACON using BEACON_Connect */
  ret = BEACON_Connect(&binfo, &beacon_handle);
  if (ret != BEACON_SUCCESS) {
    printf("BEACON_Connect is not successful ret=%d\n", ret);
    exit(-1);
  }
#else 
  int ret, i;
  clarisse_ctrl_event_props_t eprops;

  comm_count = nr_comms;
  comm_table = (MPI_Comm *)clarisse_malloc(nr_comms * sizeof(MPI_Comm));
  for (i = 0; i < nr_comms; i++) comm_table[i] = comms[i];

  if (subscriber_running == 0) {
    if ((ret = pthread_create(&sub_tid, NULL, subscriber_thread, NULL)) != 0){	
      fprintf(stderr,"pthread_create %d",ret);
      exit(-1);
    }
    subscriber_running = 1;
  }
  if (publisher_running == 0) {
    if ((ret = pthread_create(&pub_tid, NULL, publisher_thread, NULL)) != 0) {	
      fprintf(stderr,"pthread_create %d",ret); 
      exit(-1); 
    }
    publisher_running = 1;   
  }
  eprops.type = CLARISSE_CTRL_MSG_DONE;
  eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
  eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
  //MPI_Comm_rank(MPI_COMM_WORLD, &eprops.dest.rank);
  MPI_Comm_rank(comms[0], &eprops.dest.rank);
  eprops.dest.comm = comms[0];//MPI_COMM_WORLD;
  eprops.dest_type = CLARISSE_EVENT_USE_DESTINATION;
  eprops.handler = handler_done;
  cls_subscribe(&eprops);

#endif
  return 0;
}

int cls_pub_sub_finalize(){
#ifdef CLARISSE_BEACON
  BEACON_Disconnect(beacon_handle);
#else 
  int ret;
  clarisse_ctrl_event_props_t *tmp, *eprops;

  if (publisher_running) {
    clarisse_ctrl_msg_t msg;
    msg.type = CLARISSE_CTRL_MSG_DONE;
    cls_publish(&msg, sizeof(int));
  
#ifdef CLARISSE_PUB_SUB_DEBUG
    printf("Signaled publisher termination");
#endif
  }
  if (subscriber_running) {
    ret = pthread_join(sub_tid, NULL);
    if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); }
  }
  if (publisher_running) {
    ret = pthread_join(pub_tid, NULL);
    if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); }

    clarisse_free(comm_table);
    HASH_ITER(hh, eprops_table, eprops, tmp) {
      HASH_DEL(eprops_table, eprops);  
      clarisse_free(eprops);          
    }
  }
  
#endif
  return 0;
}

int cls_publish(clarisse_ctrl_msg_t *msg, int msg_size){
#ifdef CLARISSE_BEACON
  BEACON_topic_properties_t eprop;
  int ret;
  
  strncpy(eprop.topic_payload, msg->payload, msg_size);
  //sprintf(eprop.topic_payload,"PAYLOAD");
  strcpy(eprop.topic_scope, "global"); 
  ret = BEACON_Publish(beacon_handle, "LOAD", &eprop);
  if (ret != BEACON_SUCCESS) {
    printf("BEACON_Publish failed with ret =%d\n", ret);
    exit(-1);
  }
#else 
  assert(publisher_running != 0);
  clarisse_ctrl_event_t *event;
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("Publish\n");  
#endif
  event = (clarisse_ctrl_event_t *) clarisse_malloc(sizeof(clarisse_ctrl_event_t));
  memcpy(&(event->msg), msg, msg_size);
  event->msg_size = msg_size;

  //Next code similar for publish and subscriber thread (make a function)
  clarisse_ctrl_event_props_t *ep;
  int null_handler;
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("2:Try to acquire mutex_eprops_table\n");
#endif
  pthread_mutex_lock (&mutex_eprops_table);
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("2:Acquired mutex_eprops_table\n");
#endif
  HASH_FIND_INT(eprops_table, &event->msg.type, ep);
  if (ep->handler == NULL) 
    null_handler = 1;
  else 
    null_handler = 0;
  pthread_mutex_unlock(&mutex_eprops_table);
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("2:Released mutex_eprops_table\n");
#endif
  if (null_handler) {
    pthread_mutex_lock(&mutex_local_event_queue);
    dllist_iat(&local_event_queue, &event->link);
    pthread_cond_broadcast(&condvar_local_wait_event);
    pthread_mutex_unlock(&mutex_local_event_queue);
   }
  else {
    pthread_mutex_lock(&mutex_event_queue);
    dllist_iat(&event_queue, &event->link);
    pthread_cond_signal(&condvar_wait_any_event);
    pthread_mutex_unlock(&mutex_event_queue);
  }
#endif
  return 0;
}




int cls_publish_remote(communication_point_t *dest, clarisse_ctrl_msg_t *msg, int msg_size){
  int err;

#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("Publish remote\n");  
#endif
   err = MPI_Send(msg, msg_size, MPI_BYTE, dest->rank, CLARISSE_CTRL_TAG, dest->comm);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");
  return 0;
}




// Returns  
//    0 : success 
//   -1 : error (by now handler active for existing subscription 

clarisse_subscribe_handle_t cls_subscribe(clarisse_ctrl_event_props_t *eprops){
#ifdef CLARISSE_BEACON
  int ret;
  BEACON_subscribe_handle_t shandle;
  char filter_string[] = "topic_scope=global,topic_name=LOAD";
    
  ret = BEACON_Subscribe(&shandle, beacon_handle, filter_string);
  if (ret != BEACON_SUCCESS) {
    printf("BEACON_Subscribe failed ret=%d!\n", ret);
    exit(-1);
  }
  BEACON_Signal(&shandle, beacon_handle, load_handler);
  return 0;
#else
  clarisse_ctrl_event_props_t *ep;
  int found = 0;
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("3:Try to acquire mutex_eprops_table\n");
#endif
  pthread_mutex_lock (&mutex_eprops_table);
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("3:Acquired mutex_eprops_table\n");
#endif
  HASH_FIND_INT(eprops_table, &eprops->type, ep);
  if (ep) {
    if (ep->handler_refcount > 0) {
#ifdef CLARISSE_PUB_SUB_DEBUG
      printf("3:Released mutex_eprops_table\n");
#endif
      pthread_mutex_unlock(&mutex_eprops_table);
      return -1;
    }
    else
      found = 1;
  }
  else
    ep = (clarisse_ctrl_event_props_t *) clarisse_malloc(sizeof(clarisse_ctrl_event_props_t));
  
  ep->type = eprops->type;
  ep->priority = eprops->priority;
  ep->scope = eprops->scope;
  ep->dest_type = eprops->dest_type;
  ep->dest.rank = eprops->dest.rank;
  ep->dest.comm = eprops->dest.comm;
  ep->handler = eprops->handler;
  ep->handler_refcount = 0;
  if (!found)
    HASH_ADD_INT(eprops_table, type, ep);
  pthread_mutex_unlock (&mutex_eprops_table);    
#ifdef CLARISSE_PUB_SUB_DEBUG
  printf("3:Released mutex_eprops_table\n");
#endif
  assert( (publisher_running != 0) && (subscriber_running !=0 ));
  return 0;
#endif
}


clarisse_ctrl_event_t *cls_wait_event(int event_type){
  int posted = 0;
  clarisse_ctrl_event_t *e;

  pthread_mutex_lock (&mutex_local_event_queue);
  while (!posted) {
    struct dllist_link *aux;
    for(aux = local_event_queue.head; aux != NULL; aux = aux->next) {
      e = (clarisse_ctrl_event_t *) DLLIST_ELEMENT(aux, clarisse_ctrl_event_t, link);
      if (e->msg.type == event_type) {
#ifdef CLARISSE_PUB_SUB_DEBUG
	printf("%s: found_event %d\n", __FUNCTION__, event_type);
#endif
	dllist_rem(&local_event_queue, aux);
	posted = 1;
	break;
      }
    }
    if (!posted) {
      int err;
      err = pthread_cond_wait(&condvar_local_wait_event, &mutex_local_event_queue);
      if (err != 0){
	perror("pthread_cond");
	exit(1);
      }  
    }
  }
  pthread_mutex_unlock(&mutex_local_event_queue);    
  return e;
}


// Verifies if a event has arrived. 
// If positive, it removes the event from the event queue. 
clarisse_ctrl_event_t *cls_test_event(int event_type){
  int posted = 0;
  clarisse_ctrl_event_t *e = NULL;
  struct dllist_link *aux;

  pthread_mutex_lock (&mutex_local_event_queue);  
  for(aux = local_event_queue.head; aux != NULL; aux = aux->next) {
    e = (clarisse_ctrl_event_t *) DLLIST_ELEMENT(aux, clarisse_ctrl_event_t, link);
    if (e->msg.type == event_type) {
#ifdef CLARISSE_PUB_SUB_DEBUG
      printf("%s: found_event %d\n", __FUNCTION__, event_type);
#endif
      dllist_rem(&local_event_queue, aux);
      posted = 1;
      break;
    }
  }
  pthread_mutex_unlock(&mutex_local_event_queue);  
  if (posted)
    return e;
  else 
    return NULL;
}


#endif
