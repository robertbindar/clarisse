#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"

#define WR 0
#define WRRD 1

char filename[256];

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void test_8_2_0(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time);
void test_8_2_1(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time);
void test_8_2_2(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time);
// Start with all servers and put down a server after the first phase

void test_8_2_3(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time);
// Start with all servers
// Inject load in phase i 
// and remap servers  in phase i + 1
void test_8_2_4(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time, int load_injection_phase, int load_detection_phase, double load);
// Same as test_8_2_ 4, but using the API
// Start with all servers
// Inject load in phase i 
// and remap servers  in phase i + 1
void test_8_2_5(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time, int load_injection_phase, int load_detection_phase, double load);

int main(int argc, char **argv)
{
  int nprocs, n, phases, sleep_time, myrank, load_injection_phase, load_detection_phase = 3; // If -1 no load detection
  double load;

  setenv("CLARISSE_NR_SERVERS", "2", 1 /*overwrite*/);
  setenv("CLARISSE_COUPLENESS", "intercomm", 1 /*overwrite*/);
  setenv("CLARISSE_CONTROL", "enable", 1 /*overwrite*/);
  setenv("CLARISSE_ELASTICITY", "enable", 1 /*overwrite*/);

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);

  if (argc < 8) {    
    if (myrank == 0) {
      fprintf(stderr, "Missing argument, call %s filename access_size_per_proc phases sleep_time(seconds) load(seconds) load_injection_phase load_detection_phase\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  sprintf(filename, "%s", argv[1]);
  n = atoi(argv[2]);
  phases = atoi(argv[3]);
  sleep_time = atoi(argv[4]);
  load = atof(argv[5]);
  load_injection_phase = atoi(argv[6]);
  load_detection_phase = atoi(argv[7]); // If -1 no load detection
  if (myrank == 0)
    printf("\n%s PARAMS\n\tACCESS_SIZE = %d\n\tPHASES = %d\n\tSLEEP_TIME = %d seconds\n\tLOAD = %13.3f seconds\n\tLOAD_INJECTION_PHASE = %d\n\tLOAD_DETECTION_PHASE = %d\n\n", __FILE__, n, phases, sleep_time, load, load_injection_phase, load_detection_phase); 
  //load = load / 1000;
  if (cls_is_client()) {
    //test_8_2_0(WRRD, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test_8_2_1(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test_8_2_2(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test_8_2_3(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    
    test_8_2_4(WR, filename, n, MPI_BYTE, phases, (double)sleep_time, load_injection_phase, load_detection_phase, load);
    //test_8_2_5(WR, filename, n, MPI_BYTE, phases, (double)sleep_time, load_injection_phase, load_detection_phase, load);
  }
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
  return 0; 
}
