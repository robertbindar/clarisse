#ifndef TASKS_H
#define TASKS_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "map.h"
#include "list.h"
#include "server_iocontext.h"
#include "client_iocontext.h"
#include "clarisse.h"
#include "util.h"


typedef struct iotask {
  struct dllist_link link;
  int proc_rank; // process requesting this task
  int count;       // data size
  server_iocontext_t * s_ctxt;  // context to which this iotask is associated
  clarisse_off l_f;
  clarisse_off  r_f;
  clarisse_off crt_l_f; // used when the task is decomposed for transfer
  clarisse_off l_dt; // mapped to file as : map_1(buf_view, map_1(view, l_dt))
  clarisse_off r_dt;
  distrib *buf_distr; // distribution of data in memory 
  int confirmed;
  int errn;
  int log_ios_rank;
  char *buf;
  int first;
  int client_idx; // parallel client to which this task is associated
} iotask, *iotask_p;

typedef struct {
  server_iocontext_t *s_ctxt; 
  buffer_unit_p bu;
} task_params_t;

iotask_p alloc_init_iotask(server_iocontext_t *s_ctxt, int proc_rank, clarisse_off l_f, clarisse_off r_f, int client_idx);
iotask_p find_iotask(int global_descr, int proc_rank, dllist *task_queue);
int create_io_tasks(client_iocontext_t *c_ctxt, 
		    clarisse_off l_v, clarisse_off l_f, clarisse_off r_f,
		    clarisse_off l_d, clarisse_off part_block_len, 
		    int file_partitioning);
#endif /*TASKS_H_*/
