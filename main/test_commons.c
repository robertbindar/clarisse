#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "test_datatypes.h"
#include "server_iocontext.h"
#include "client_iocontext.h"

extern client_global_state_t client_global_state;
extern int i_am_client;

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void setview_w_r(MPI_Offset off, MPI_Datatype d, MPI_Offset displ, MPI_Info info, char *filename, int n, MPI_Datatype mem_type, int phases) {
  int err, i, mem_type_size;
  MPI_File fh;
  MPI_Status status;
  MPI_Aint lb, mem_type_extent;
  char *buf;
  
  MPI_Type_get_extent(mem_type, &lb, &mem_type_extent);
  MPI_Type_size(mem_type, &mem_type_size);

  buf = malloc(mem_type_extent * n);
  
  MPI_File_delete(filename, MPI_INFO_NULL);
  err = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");
  err = MPI_File_seek(fh, off, MPI_SEEK_SET);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_seek");
  for (i = 0; i < phases; i++) {
    err = MPI_File_write_all(fh, buf, n, mem_type, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_write_all");
  }
  ////////////////////////
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");
  err = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");
  ////////////////////////////

  err = MPI_File_seek(fh, off, MPI_SEEK_SET);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_seek");
  for (i = 0; i < phases; i++) {
    err = MPI_File_read_all(fh, buf, n, mem_type, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_read_all");
  }
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");
  
  free(buf);
 }

void create_preexisting_file(char *filename, int n){
  if (i_am_client){
    char *env;
    int rank;
    
    MPI_Comm_rank(client_global_state.intracomm, &rank);
    env = getenv("TEST_PREEXISTING_FILE");
    if (env && !strncmp(env, "true", 4)) {
      MPI_Barrier(client_global_state.intracomm);
      if (rank == 0) {
	int fd, i;
	char *buf;
	
	if ((fd = open(filename, O_CREAT|O_WRONLY, 0666)) < 0){
	  perror("Open");
	  handle_error(MPI_ERR_OTHER,"Creating preexisting file");
	}
	buf = (char *) malloc(n);
	for (i = 0; i < n; i++) buf[i]='_';  
	if (write(fd, buf, n) < n) 
	  handle_error(MPI_ERR_OTHER, "Wrote less than the number of bytes in the preexisting file");
	close(fd);
      }
      MPI_Barrier(client_global_state.intracomm);
    }
  }
}

void compare_files(char *filename_tp, char *filename_vb ){

  if (i_am_client){
    int rank;
    
    MPI_Comm_rank(client_global_state.intracomm, &rank); 
    if (rank == 0) {
      int err = MPI_SUCCESS;
      struct stat st_tp, st_vb;
      stat(filename_tp, &st_tp);
      stat(filename_vb, &st_vb);
      if (st_tp.st_size != st_vb.st_size) {
	fprintf(stderr, "Error when comparing %s (size = %d) and %s files (size = %d)\n", filename_tp, (int)st_tp.st_size, filename_vb, (int)st_vb.st_size);  
	handle_error(err,"Different file sizes produced by TP and VB");
      }
      else {
	int fd_tp, fd_vb;
	char *buf_tp, *buf_vb;
	if ((fd_tp = open(filename_tp, O_RDONLY)) < 0){
	  perror("Open");
	  handle_error(err,"TP file");
	}
	if ((fd_vb = open(filename_vb, O_RDONLY)) < 0){
	  perror("Open");
	  handle_error(err,"VB file");
	}
	buf_tp = (char *) malloc(st_tp.st_size);
	buf_vb = (char *) malloc(st_vb.st_size);
	if (read(fd_tp, buf_tp, st_tp.st_size) < st_tp.st_size) 
	  handle_error(err, "Read less than the whole TP file");
	if (read(fd_vb, buf_vb, st_vb.st_size) < st_vb.st_size) 
	  handle_error(err, "Read less than the whole VB file");
	
	close(fd_tp); 
	close(fd_vb);
	
	if (strncmp(buf_tp, buf_vb, st_tp.st_size)) {
	  fprintf(stderr, "Error when comparing %s and %s files\n", filename_tp, filename_vb); 
	  fprintf(stderr, "Test %s: \n", 
		  __FILE__);
	  handle_error(err,"Different files produced by VB and TP");
	}
	free(buf_vb); 
	free(buf_tp);
	
      }
    }
    MPI_Barrier(client_global_state.intracomm);
  }
}

