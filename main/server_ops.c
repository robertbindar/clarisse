#include <float.h>
#include "mpi.h"
#include "assert.h"
#include "clarisse.h"
#include "hdr.h"
#include "uthash.h"
#include "util.h" 
#include "error.h"
#include "marshal.h"
#include "buffer.h"
#include "dcache.h"
#include "tasks.h"

double delay = 0;

extern server_global_state_t server_global_state;

void serv_op_write_coll(struct rq_file_write * r, int client_idx, int source, int cnt);
void serv_op_write(struct rq_file_write * r, int client_idx, int source, int cnt);
void serv_op_read_coll(struct rq_file_read * r, int client_idx, int source, int cnt);
void serv_op_read(struct rq_file_read * r, int client_idx, int source, int cnt);
void serv_op_read_coll_future(struct rq_file_read * r, int client_idx, int source, int cnt);
void serv_op_flush(struct rq_file_flush * r, int client_idx, int source);
void serv_op_setview(struct rq_file_set_view * r, int client_idx);
void serv_op_delete(char * r, int client_idx, int source);
void serv_op_delay(double t, int client_idx, int source);
void serv_write_task(server_iocontext_t *s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_write_task_listio(server_iocontext_t * s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_read_task(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_read_future_task(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_read_task_listio(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_create_write_tasks(server_iocontext_t * s_ctxt);
int serv_read_n_shares(int client_idx);
void flush_dcache_buffers_future(server_iocontext_t *s_ctxt); // cls_read_coll.c

void mysleep(double sleep_time_sec){
  double t0;
  
  t0 = MPI_Wtime();
  while ((MPI_Wtime() - t0) < sleep_time_sec);
}

void server_init(int argc, char *argv[]){
  CLARISSE_UNUSED(argc); 
  CLARISSE_UNUSED(argv);

  /*
  clarisse_params clarisse_pars;
  MPI_Comm intracomm, *intercomms;

  clarisse_pars.buffer_size = atoi(argv[1]);
  clarisse_pars.bufpool_size = atoi(argv[2]);
  clarisse_pars.net_buf_size = atoi(argv[4]);
  clarisse_pars.net_datatype_count = atoi(argv[5]);
  clarisse_pars.net_datatype_incr = atoi(argv[6]);
  clarisse_pars.metadata_transfer_size = atoi(argv[7]);
  clarisse_pars.io_sched_policy = (short int) atoi(argv[8]);
  clarisse_pars.lazy_eager_view = (short int) atoi(argv[9]);
  clarisse_pars.prefetch = (short int) atoi(argv[10]);
  clarisse_pars.write_type  = (short int) atoi(argv[11]);

  clarisse_pars.coupleness = CLARISSE_DYN_PROCESS;
  
  //MPI_Comm_get_parent(&intercomm);
  //  if (server_global_state.initial_cli_serv_intercomm == MPI_COMM_NULL)
  //  handle_error(MPI_ERR_OTHER, "Server inter-comm is MPI_COMM_NULL\n");
  intracomm = MPI_COMM_WORLD;
  //server_global_state_init(&clarisse_pars, intracomm, 1, intercomms);
  */
}

void server_finalize(){
  server_global_state_free();
}




void server_operate(){
  int again;
  int client_idx = 0;

  again = 1;
  while (again) { 
    MPI_Status status;
    int cnt;
    int received;
    
    received = 0;
    while (!received) {
      int err;

      client_idx = (client_idx + 1) % server_global_state.no_clients;
      err = MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, server_global_state.intercomms[client_idx], &received,&status);
      if (err != MPI_SUCCESS)
	handle_error(err, "MPI_Iprobe error\n");      
    }
    MPI_Recv(server_global_state.net_buf, server_global_state.clarisse_pars.net_buf_size, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, server_global_state.intercomms[client_idx], &status );	
    MPI_Get_count(&status, MPI_BYTE, &cnt);

    switch (status.MPI_TAG) { 
      //double max = 0, min = DBL_MAX, avg = 0;
      //int cntops = 0;
    case RQ_FINALIZE_IO: 
      server_global_state.finalized_cnt++;
      if (server_global_state.finalized_cnt == server_global_state.total_client_procs){
	again = 0;
	//	printf("WRITE OP TIMINGS: %d ops avg = %9.6f min = %9.6f max = %9.6f\n",cntops, avg/ cntops, min, max);
      }
      break;
      //case RQ_FILE_OPEN:
      //case RQ_FILE_CLOSE:
    case RQ_FILE_SET_VIEW: 
      serv_op_setview((struct rq_file_set_view *)server_global_state.net_buf, client_idx);
      break;
    case RQ_FILE_WRITE: 
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Write rq from %d\n", status.MPI_SOURCE);
#endif
      serv_op_write((struct rq_file_write *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt);
      break;
    case RQ_FILE_WRITE_COLL: 
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Write coll rq from %d crt delay=%f\n", status.MPI_SOURCE, delay);
#endif
      
      mysleep(delay);
      //double t0, t1;
      //t0 = MPI_Wtime();
      serv_op_write_coll((struct rq_file_write *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt);
      //t1 = MPI_Wtime();
      //max = MAX(max, t1 - t0);
      //min = MIN(min, t1 - t0);
      //avg += (t1 - t0);
      //cntops++;

      break;
    case RQ_FILE_READ:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Read rq from %d\n", status.MPI_SOURCE);
#endif 
      serv_op_read_coll((struct rq_file_read *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt);
      break;
    case RQ_FILE_READ_COLL:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Read COLL rq from %d\n", status.MPI_SOURCE);
#endif 
      serv_op_read_coll((struct rq_file_read *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt);
      break;

    case RQ_FILE_READ_COLL_FUTURE:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Read COLL future rq from %d\n", status.MPI_SOURCE);
#endif 
      serv_op_read_coll_future((struct rq_file_read *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt);
      break;

    case RQ_FILE_FLUSH:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Flush rq from %d\n", status.MPI_SOURCE);
#endif 
      serv_op_flush((struct rq_file_flush *)server_global_state.net_buf, client_idx, status.MPI_SOURCE);
     
      break;
      //case RQ_FILE_FTRUNCATE:
    case RQ_FILE_DELETE:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Delete rq from %d\n", status.MPI_SOURCE);
#endif
      serv_op_delete((char *)server_global_state.net_buf, client_idx, status.MPI_SOURCE);
      break;
      //case RQ_FILE_GETSIZE:
    case RQ_SERVER_DELAY:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Server delay rq from %d for %f seconds\n", status.MPI_SOURCE, 
	     *((double *) server_global_state.net_buf));
#endif
      
      serv_op_delay(*((double *) server_global_state.net_buf), client_idx, status.MPI_SOURCE);
     
      break;
    default: 
      // Unexpected message type  
#ifdef CLARISSE_SERVER_DEBUG      
      fprintf(stderr,"Unexpected message \n");
#endif
      MPI_Abort(MPI_COMM_WORLD, 1); 
    } 
    
    /* if (!chash_empty(&ios_state.read_req_list)) { 
      ios_task_p i;
      i = (ios_task_p) chash_peek_crt(&ios_state.read_req_list);
      read_n_shares(i);
      if (i->l_f > i->r_f) 
	free(chash_remove_crt(&ios_state.read_req_list));
      else 
	chash_set_next(&ios_state.read_req_list);
    }
    */
  }
}
  

void serv_op_write_coll(struct rq_file_write * r, int client_idx, int source, int cnt){

  server_iocontext_t *s_ctxt;
  int global_descr;

  if (cnt == sizeof(int)) 
    global_descr = *((int *) r);
  else
    global_descr = r->global_descr;
  s_ctxt = server_iocontext_find(global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(global_descr);
#ifdef CLARISSE_DCACHE_DEBUG
    dcache_print(&s_ctxt->dcache, "serv_op_write_coll BEGINNING");
#endif      
  if (cnt > (int) sizeof(int)) {
    
    if (s_ctxt->local_target == NULL) {
      clarisse_target_t *tgt;
      char *filename;
      
      assert (r->first_msg > 0);
      HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
      if (!tgt) {
	filename = (char *)(r + 1) /*+ r->view_size*/;
	tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
	HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
      }
      server_iocontext_update_target(s_ctxt, tgt);
    }
    if (r->first_msg)
      server_iocontext_update_client_rt_info(s_ctxt, client_idx, r->partitioning_stride_factor, r->nprocs);
#ifdef CLARISSE_DCACHE_DEBUG
    dcache_print(&s_ctxt->dcache, "serv_op_write_coll before serv_write_task");
#endif
    if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
      serv_write_task_listio(s_ctxt, r, client_idx, source);
    else
      serv_write_task(s_ctxt, r, client_idx, source);
  }
  //#ifdef CLARISSE_DCACHE_DEBUG
  //dcache_print(&s_ctxt->dcache, "serv_op_write  MIDDLE");
  //#endif
  // collective write-back 
  //printf("r->last_msg=%d write_ops[%d]=%d\n", r->last_msg, source, s_ctxt->crt_write_ops[source]);
  if ((cnt == sizeof(int) || (r->last_msg == 1)) && 
      ((s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK) ||
       (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_DEMAND))) { 
    if (s_ctxt->client_rt_info[client_idx].crt_write_ops[source] == 0) {
      s_ctxt->client_rt_info[client_idx].crt_write_ops[source] = 1;
      s_ctxt->client_rt_info[client_idx].crt_arrived_writes++;
#ifdef CLARISSE_SERVER_DEBUG      
     printf("%s crt_arrived_writes=%d nr_clients=%d\n", __FUNCTION__, s_ctxt->client_rt_info[client_idx].crt_arrived_writes, server_global_state.client_nprocs[client_idx]);
#endif
      if (s_ctxt->client_rt_info[client_idx].crt_arrived_writes == server_global_state.client_nprocs[client_idx]) {
	short int * tmp;
	
	tmp = s_ctxt->client_rt_info[client_idx].crt_write_ops;
	s_ctxt->client_rt_info[client_idx].crt_write_ops = s_ctxt->client_rt_info[client_idx].next_write_ops; 
	s_ctxt->client_rt_info[client_idx].crt_arrived_writes = s_ctxt->client_rt_info[client_idx].next_arrived_writes;
	s_ctxt->client_rt_info[client_idx].next_write_ops = tmp;
	memset((void *)s_ctxt->client_rt_info[client_idx].next_write_ops, 0, server_global_state.client_nprocs[client_idx] * sizeof(short int));
	s_ctxt->client_rt_info[client_idx].next_arrived_writes = 0;

	if (s_ctxt->clarisse_pars.read_future == CLARISSE_READ_FUTURE_ENABLED) {
	  flush_dcache_buffers_future(s_ctxt);
	}
	else {
	  if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK) 	
	    serv_create_write_tasks(s_ctxt);
	  if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_DEMAND){
#ifdef CLARISSE_SERVER_DEBUG      
	    printf("Flush dcache buffers\n");
#endif
	    flush_dcache_buffers(s_ctxt);
	  }
	}
      }
    }
    else { // if (s_ctxt->client_rt_info[client_idx].crt_write_ops[source] == 0)
      assert(s_ctxt->client_rt_info[client_idx].next_write_ops[source] == 0);
      s_ctxt->client_rt_info[client_idx].next_write_ops[source] = 1;
      s_ctxt->client_rt_info[client_idx].next_arrived_writes++;
      assert(s_ctxt->client_rt_info[client_idx].next_arrived_writes < server_global_state.client_nprocs[client_idx]);
    }
  }
#ifdef CLARISSE_DCACHE_DEBUG
    dcache_print(&s_ctxt->dcache, "serv_op_write_coll END");
#endif 
} 


void serv_op_write(struct rq_file_write * r, int client_idx, int source, int cnt){

  server_iocontext_t *s_ctxt;
  int global_descr;

  global_descr = r->global_descr;
  s_ctxt = server_iocontext_find(global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(global_descr);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, "serv_op_write BEGINNING");
#endif      
  if (s_ctxt->local_target == NULL) {
    clarisse_target_t *tgt;
    char *filename;
    
    assert (r->first_msg > 0);
    HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
    if (!tgt) {
      filename = (char *)(r + 1) /*+ r->view_size*/;
      tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
      HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
    }
    server_iocontext_update_target(s_ctxt, tgt);
  }
  if (r->first_msg)
    server_iocontext_update_client_rt_info(s_ctxt, client_idx, r->partitioning_stride_factor, r->nprocs);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, "serv_op_write before serv_write_task");
#endif
  if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
    serv_write_task_listio(s_ctxt, r, client_idx, source);
  else
    serv_write_task(s_ctxt, r, client_idx, source);
  
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, "serv_op_write END");
#endif 
  CLARISSE_UNUSED(cnt);
} 




void serv_op_read_coll(struct rq_file_read * r, int client_idx, int source, int cnt){
  if (cnt > (int) sizeof(int)) {
    clarisse_target_t *tgt;
    server_iocontext_t *s_ctxt;
    int err;
 
    s_ctxt = server_iocontext_find(r->global_descr);
    if (!s_ctxt)
       s_ctxt = server_iocontext_init(r->global_descr);

    HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
    if (!tgt) {
      char *filename;
      
      assert (r->first_msg > 0);
      filename = (char *)(r + 1) /*+ r->view_size*/;
      tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
      HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
    }
   
    server_iocontext_update_target(s_ctxt, tgt);
    if (r->first_msg)
      server_iocontext_update_client_rt_info(s_ctxt, client_idx, r->partitioning_stride_factor, r->nprocs);

    if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
      serv_read_task_listio(s_ctxt, r, client_idx, source);
    else {
      serv_read_task(s_ctxt, r, client_idx, source);
      serv_read_n_shares(client_idx);
    }
    err = MPI_Waitall(s_ctxt->client_rt_info[client_idx].mpi_rq_pool.cnt, s_ctxt->client_rt_info[client_idx].mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
    if (err != MPI_SUCCESS)
      handle_err(err, "SERV waitall error\n");
    free_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map); 
  }
}


void serv_op_read_coll_future(struct rq_file_read * r, int client_idx, int source, int cnt){
  clarisse_target_t *tgt;
  server_iocontext_t *s_ctxt;
 
  s_ctxt = server_iocontext_find(r->global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(r->global_descr);

  HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
  if (!tgt) {
    char *filename;
    
    assert (r->first_msg > 0);
    filename = (char *)(r + 1); 
    tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE_FUTURE, NULL, NULL); 
    HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
  }
   
  server_iocontext_update_target(s_ctxt, tgt);
  if (r->first_msg)
    server_iocontext_update_client_rt_info(s_ctxt, client_idx, r->partitioning_stride_factor, r->nprocs);

  // Check if the write is available
  // insert in a queue otherwise

  if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
    serv_read_task_listio(s_ctxt, r, client_idx, source);
  else {
    serv_read_future_task(s_ctxt, r, client_idx, source);
    //serv_read_n_shares(client_idx);
  }
  /*
  err = MPI_Waitall(s_ctxt->client_rt_info[client_idx].mpi_rq_pool.cnt, s_ctxt->client_rt_info[client_idx].mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
  if (err != MPI_SUCCESS)
    handle_err(err, "SERV waitall error\n");
  free_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map); 
  */
  CLARISSE_UNUSED(cnt);
}

void serv_op_setview(struct rq_file_set_view * r, int client_idx) {
  MPI_Datatype dt;
  server_iocontext_t *s_ctxt;
  int global_descr;
  
  global_descr = r->global_descr;
  s_ctxt = server_iocontext_find(global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(global_descr);
      
  if (s_ctxt->local_target == NULL) {
    clarisse_target_t *tgt;
    char *filename;
    
    HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
    if (!tgt) {
      filename = (char *)(r + 1);
      tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
      HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
    }
    server_iocontext_update(s_ctxt, tgt, client_idx, r->partitioning_stride_factor, r->nprocs);
  }

  dt = reconstruct_dt((char *)(r + 1) + r->target_name_size, r->view_size);
  distrib_update(s_ctxt->client_rt_info[client_idx].remote_view_distr[r->rank], dt, r->displ);
}

void serv_op_delete(char * filename, int client_idx, int source){
  clarisse_target_t *tgt;
  int global_descr;
  struct rt_file_delete fd_ret;
  int ret;

  global_descr = clarisse_hash((unsigned char *)filename);
  HASH_FIND_INT(server_global_state.active_targets, &global_descr, tgt);
  if (tgt) {
    server_iocontext_t *s_ctxt;

    HASH_DEL(server_global_state.active_targets, tgt);
    clarisse_free(tgt);
    s_ctxt = server_iocontext_find(global_descr);
    if (s_ctxt) {
      if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK){
	tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 1);
	tpool_wait_for_task_queue_empty(server_global_state.tpool, (void*)s_ctxt);
	tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 0);
	//    tpool_task_queue_destroy(server_global_state.tpool, (void*)s_ctxt);
	dcache_atomic_bufpool_move_all(&s_ctxt->dcache, server_global_state.buf_pool);
#ifdef CLARISSE_DCACHE_DEBUG
	dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
      }
      else
	drop_buffers(s_ctxt);
      s_ctxt->local_target = NULL;
    }
  }
  fd_ret.errn = 0;
  ret = MPI_Send(&fd_ret, sizeof(struct rt_file_delete), MPI_BYTE,  source, RT_FILE_DELETE, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_delete: Error receiving the rets");
  }
}

void serv_op_flush(struct rq_file_flush *r, int client_idx, int source){
  clarisse_target_t *tgt;
  struct rt_file_flush fd_ret;
  int ret, global_descr;

  global_descr = r->global_descr;
  HASH_FIND_INT(server_global_state.active_targets, &global_descr, tgt);
  if (tgt) {
    server_iocontext_t *s_ctxt;

    s_ctxt = server_iocontext_find(global_descr);
    if (s_ctxt) {
      if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK){
	tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 1);
	tpool_wait_for_task_queue_empty(server_global_state.tpool, (void*)s_ctxt);
	tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 0);
	//    tpool_task_queue_destroy(server_global_state.tpool, (void*)s_ctxt);
	dcache_atomic_bufpool_move_all(&s_ctxt->dcache, server_global_state.buf_pool);
#ifdef CLARISSE_DCACHE_DEBUG
	dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
      }
      else 
	flush_buffers(s_ctxt);
    }
  }
  fd_ret.errn = 0;
  ret = MPI_Send(&fd_ret, sizeof(struct rt_file_flush), MPI_BYTE,  source, RT_FILE_FLUSH, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_delete: Error receiving the rets");
  }
}

void serv_op_delay(double t, int client_idx, int source) {
  int ret = 0;
  delay = t;
  ret = MPI_Send(&ret, 1, MPI_INT,  source, RT_SERVER_DELAY, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_delete: Error receiving the rets");
  }
}
