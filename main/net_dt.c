#include "net_dt.h"
#include "error.h"
#include "util.h"

/* Functions for manipulating the hindexed datatype used for transfers for
avoiding memcpy.
*/

void net_dt_alloc(struct net_dt *net_dt){
  if ((net_dt->block_lens = (int *) clarisse_malloc(net_dt->max_count*sizeof(int))) == NULL)
    handle_error(MPI_ERR_NO_MEM, "Out of memory");;
  if ((net_dt->displs = (MPI_Aint *) clarisse_malloc(net_dt->max_count*sizeof(MPI_Aint))) == NULL)
    handle_error(MPI_ERR_NO_MEM, "Out of memory");;
}

void net_dt_init(struct net_dt *net_dt, int max_count, int incr) {
  net_dt->count = 0;
  net_dt->max_count = max_count;
  net_dt->incr = incr;
  net_dt->aggr_len = 0;
}

void net_dt_init_alloc (struct net_dt *net_dt, int max_count, int incr) {
  net_dt_init(net_dt, max_count, incr);
  net_dt_alloc(net_dt);
}

void net_dt_free(struct net_dt *net_dt){
  clarisse_free(net_dt->block_lens);
  clarisse_free(net_dt->displs);
}

void net_dt_reset(struct net_dt *net_dt){
  net_dt->count = 0;
  net_dt->aggr_len = 0;
}

static void net_dt_realloc(struct net_dt *net_dt) {
  net_dt->max_count += net_dt->incr;
  if ((net_dt->block_lens = (int *) clarisse_realloc(net_dt->block_lens, net_dt->max_count *sizeof(int)) ) == NULL)
    handle_error(MPI_ERR_NO_MEM, "Out of memory");
  if ((net_dt->displs = (MPI_Aint *) clarisse_realloc(net_dt->displs, net_dt->max_count*sizeof(MPI_Aint))) == NULL)
    handle_error(MPI_ERR_NO_MEM, "Out of memory");
}

void net_dt_update(struct net_dt *net_dt, char *buf, int len) {
  MPI_Aint a;

  //printf("net_dt_update: len = %d cnt =%d\n", len, net_dt->count);  
  if (net_dt->count == net_dt->max_count)
    net_dt_realloc(net_dt);
  net_dt->block_lens[net_dt->count] = len;
  MPI_Address(buf, &a);
  if (net_dt->count == 0) {
    net_dt->base_address = a;
    net_dt->displs[net_dt->count] = 0;
  }
  else
    net_dt->displs[net_dt->count] = a -  net_dt->base_address;
  net_dt->count++;
  net_dt->aggr_len += len;
}

void net_dt_fuse(struct net_dt *net_dt_dest, struct net_dt *net_dt_src){
  int i;
  for (i = 0; i < net_dt_src->count; i++) {
    if (net_dt_dest->count == net_dt_dest->max_count)
      net_dt_realloc(net_dt_dest);
    net_dt_dest->block_lens[net_dt_dest->count] = net_dt_src->block_lens[i];
    net_dt_dest->displs[net_dt_dest->count] = net_dt_src->displs[i] +
      net_dt_src->base_address - net_dt_dest->base_address;
    net_dt_dest->count++;
  }
}

MPI_Datatype net_dt_commit(struct net_dt *net_dt) {
    MPI_Datatype d;
    int err = MPI_Type_hindexed(net_dt->count, net_dt->block_lens, net_dt->displs, MPI_BYTE, &d);
    if (err != MPI_SUCCESS)
        handle_error(err, "Error at creating the hindexed\n");
    MPI_Type_commit(&d);
    return d;
}




