#include "client_iocontext.h"
#include "server_iocontext.h"
#include "hdr.h"
#include "util.h"

extern client_global_state_t client_global_state;
extern int i_am_client;

int MPI_File_seek(MPI_File fh, MPI_Offset offset, int whence) {
  int ret = MPI_SUCCESS;
  
  if (i_am_client) {
    client_iocontext_t *c_ctxt;
    c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
    
    if (c_ctxt) {
      switch (whence) {
      case MPI_SEEK_SET:
	c_ctxt->fp_ind = func_1(offset, c_ctxt->intermediary_target->distr);
	break;
      case MPI_SEEK_CUR:  
	c_ctxt->fp_ind += func_1(offset, c_ctxt->intermediary_target->distr); 
	break;
      case MPI_SEEK_END:
	c_ctxt->fp_ind = lseek(c_ctxt->fd_sys, offset, SEEK_END);
	break;
      }
      ret = MPI_SUCCESS;
    }
    else
      ret = PMPI_File_seek(fh, offset, whence);
  }
  return ret;
}
