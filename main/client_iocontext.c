#include <assert.h>
#include "client_iocontext.h"
#include "buffer_pool.h"
#include "error.h"
#include "hdr.h"
#include "clarisse.h"
#include "util.h"

extern server_global_state_t server_global_state;
extern client_global_state_t client_global_state;

int am_i_bridge();

void client_iocontext_init(client_iocontext_t *c_ctxt, clarisse_target_t *local_tgt,clarisse_target_t *intermediary_tgt) {
  int i;

  memcpy(&c_ctxt->clarisse_pars, &client_global_state.clarisse_pars, sizeof(clarisse_params));
  
  /*
  switch (client_global_state.clarisse_pars.server_map) {
  case CLARISSE_SERVER_MAP_ROMIO: // treated in adio_map.c 

    break;
  case CLARISSE_SERVER_MAP_UNIFORM:
    c_ctxt->clarisse_pars.nr_servers = client_global_state.clarisse_pars.nr_servers;
    cls_assign_servers(client_global_state.clarisse_pars.server_map, client_global_state.clarisse_pars.nr_servers, NULL);
    break;
  case CLARISSE_SERVER_MAP_TOPOLOGY_EXCLUDE_BRIDGE:
  case CLARISSE_SERVER_MAP_TOPOLOGY_INCLUDE_BRIDGE:

  case CLARISSE_SERVER_MAP_RANDOM:
    break;
  default:
    handle_err(MPI_ERR_OTHER, "CLARISSE_SERVER_MAP not supported\n");
  }
  */
  
  c_ctxt->local_target = local_tgt;
  c_ctxt->intermediary_target = intermediary_tgt; 
  //c_ctxt->remote_targets = remote_tgts;
  net_dt_init_alloc(&c_ctxt->net_dt, 
		    c_ctxt->clarisse_pars.net_datatype_count, 
		    c_ctxt->clarisse_pars.net_datatype_incr);
  c_ctxt->net_buf = (char *) clarisse_malloc(c_ctxt->clarisse_pars.net_buf_size);
  c_ctxt->mpi_rq_map = NULL;
  request_queues_init(&c_ctxt->task_queues, c_ctxt->clarisse_pars.nr_servers, c_ctxt->clarisse_pars.io_sched_policy);
    // Move that to state (or unify all these into a state struct)
  c_ctxt->pending_ios_ret  = (short *)clarisse_malloc(c_ctxt->clarisse_pars.nr_servers * sizeof(short));
  c_ctxt->view_send  = (short *)clarisse_malloc(c_ctxt->clarisse_pars.nr_servers * sizeof(short));
  c_ctxt->pending_ios  = 0;
  //c_ctxt->ph2log = NULL;

  for (i = 0;i < c_ctxt->clarisse_pars.nr_servers; i++) {
    c_ctxt->pending_ios_ret[i] = 0;
    c_ctxt->view_send[i] = 0;
  }
  alloc_pool_init(&c_ctxt->mpi_rq_pool, c_ctxt->clarisse_pars.nr_servers, c_ctxt->clarisse_pars.nr_servers, sizeof(MPI_Request));
  //  c_ctxt->coll_op_seq = 0;
  //c_ctxt->last_coll_op_seq = -1;
  //c_ctxt->epoch = 0;
  //c_ctxt->active_log_ios_rank  = -1;
}

void client_iocontext_free(client_iocontext_t *c_ctxt) {
  //int log_ios_rank;
  //my_fprintf(stderr, "free_client_iocontext\n");
  distrib_free(c_ctxt->intermediary_target->distr);
  clarisse_free(c_ctxt->intermediary_target);
 
  /*for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++){
    int_map_t *m;
    //free_datatype(c_ctxt->remote_targets[log_ios_rank].distr->flat->id);
    //distrib_free(c_ctxt->remote_targets[log_ios_rank].distr);
    HASH_FIND_INT(c_ctxt->ph2log, &c_ctxt->log2ph[log_ios_rank], m);
    HASH_DEL(c_ctxt->ph2log, m);
    clarisse_free(m);
    }*/
  //clarisse_free(c_ctxt->remote_targets);
  clarisse_free(c_ctxt->pending_ios_ret);
  clarisse_free(c_ctxt->view_send);

  // Next one not necessary (is freed in the generic close)
  //  distrib_free(c_ctxt->local_target->distr);
  net_dt_free(&c_ctxt->net_dt);
  request_queues_free(&c_ctxt->task_queues);
  c_ctxt->pending_ios = 0;
  clarisse_free(c_ctxt->net_buf);
  //clarisse_free(c_ctxt->log2ph);
  alloc_pool_free(&c_ctxt->mpi_rq_pool);
  clarisse_free(c_ctxt);
}



void client_iocontext_add(client_iocontext_t **c_ctxt_map,
			  void *key,
			  client_iocontext_t *c_ctxt) {
  c_ctxt->key = key;
  HASH_ADD_PTR(*c_ctxt_map, key, c_ctxt); 
}


client_iocontext_t *client_iocontext_find(client_iocontext_t *c_ctxt_map,
					void *key){
  client_iocontext_t *elem;
  HASH_FIND_PTR(c_ctxt_map, &key, elem);
  return elem;
}

void client_iocontext_find_delete_free(client_iocontext_t **c_ctxt_map,
				       void *key) {
  client_iocontext_t *elem;
  HASH_FIND_PTR(*c_ctxt_map, &key, elem);
  client_iocontext_free(elem);
}


void client_global_state_init(){
  clarisse_param_init();
  client_global_state.c_ctxt_map = NULL;
  if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED){
    client_global_state.initial_cli_serv_intercomm = MPI_COMM_WORLD;
    client_global_state.intracomm = MPI_COMM_WORLD;
  }
  else {
    client_global_state.initial_cli_serv_intercomm = MPI_COMM_NULL;
    client_global_state.intracomm = MPI_COMM_NULL;
  }
  client_global_state.log2ph = NULL;
  client_global_state.ph2log = NULL;
  client_global_state.controller_rank = 0;
  client_global_state.global_controller_rank = 0; // must be a rank in MPI_COMM_WORLD
  client_global_state.coll_op_seq = 0;
  client_global_state.last_coll_op_seq = -1;
  client_global_state.epoch = 0;
  client_global_state.active_log_ios_rank  = -1;
}


void client_global_state_free() {
  if ((client_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS) || 
      (client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM))
    {
      if (client_global_state.initial_cli_serv_intercomm != MPI_COMM_NULL)
	MPI_Comm_free(&client_global_state.initial_cli_serv_intercomm);
      if (client_global_state.intracomm != MPI_COMM_NULL)
	MPI_Comm_free(&client_global_state.intracomm);
      if (client_global_state.allclients_intracomm != MPI_COMM_NULL)
	MPI_Comm_free(&client_global_state.allclients_intracomm);
    }
  if (client_global_state.log2ph) {
    
    /*
    for (log_ios_rank = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++){
      int_map_t *m;
      HASH_FIND_INT(client_global_state.ph2log, &client_global_state.log2ph[log_ios_rank], m);
      if (m) {
	HASH_DEL(client_global_state.ph2log, m);
	clarisse_free(m);
      }
    }
    */
    int_map_t *m, *tmp;
    HASH_ITER(hh, client_global_state.ph2log, m, tmp) {
      HASH_DEL(client_global_state.ph2log, m);
      clarisse_free(m);
    }
    clarisse_free(client_global_state.log2ph);
    client_global_state.log2ph = NULL;
  }
  if (client_global_state.active_log_servers)
    clarisse_free(client_global_state.active_log_servers);
}
