#include <assert.h>
#include "client_iocontext.h"
#include "buffer_pool.h"
#include "hdr.h"
#include "tasks.h"
#include "error.h"
#include "util.h"
#ifdef CLARISSE_CONTROL
#include "cls_ctrl.h"
#endif

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern client_global_state_t client_global_state;
extern server_global_state_t server_global_state;
extern int i_am_client;
extern int i_am_server;


void cli_open(client_iocontext_t * c_ctxt);
void serv_open(client_iocontext_t * c_ctxt);

int MPI_File_open(MPI_Comm comm, const char *filename, int amode, MPI_Info info, MPI_File *fh)
{
  int ret = MPI_SUCCESS;  
  
#ifdef CLARISSE_TIMING
  local_timing.time_open[local_timing.cnt_open][0] = MPI_Wtime();
#endif   
  if (i_am_client){
    char rvalue[MPI_MAX_INFO_VAL];   
    int rflag;   
    char *val;
    const char *fname;
#ifdef CLARISSE_DEBUG
    printf("MPI_File_open\n");
#endif
    if (((val = getenv("CLARISSE_DEVNULL"))!= NULL ) && (!strcmp(val, "enable")))
      fname="/dev/null";
    else
      if ((val = getenv("BTIO_FILENAME")))
	fname = val;
      else
	fname = filename;
    
    ///////////////////////////////////// comm becomes 
    if (client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM)
      comm = client_global_state.intracomm; 
    //ret = PMPI_File_open(comm, fname, amode, info, fh);

    rflag = 0;
    if ((val = getenv("CLARISSE_COLLECTIVE"))) {
      if (!strcmp(val, "romio")){
	rflag = 1;
	strcpy(rvalue, val);
      }
    }
    // An info can overwrite an environment var for romio
    if (info != MPI_INFO_NULL)
      MPI_Info_get(info,"clarisse_collective", MPI_MAX_INFO_VAL, rvalue, &rflag);   
    if (rflag && (!strcmp(rvalue, "romio")) && (comm != MPI_COMM_SELF)) {
      //      cls_adio_set_generic_functions(*fh);
      ret = PMPI_File_open(comm, fname, amode, info, fh);

    }
    else {
      client_iocontext_t *c_ctxt;
      int old_mask;

      c_ctxt = (client_iocontext_t *) clarisse_malloc(sizeof(client_iocontext_t));
      //fh = clarisse_malloc(sizeof(MPI_File));
      //fh = MPIO_File_create(sizeof(MPI_File));
      *fh = (MPI_File)c_ctxt; // in ADIO MPI_File is fd
      client_iocontext_add(&client_global_state.c_ctxt_map, (void *) *fh, c_ctxt);
      c_ctxt->fh = *fh; 
      c_ctxt->filename = filename;
      c_ctxt->fp_ind = 0;
      c_ctxt->fp_sys_posn = 0;
      c_ctxt->disp = 0;
      c_ctxt->etype = MPI_BYTE;
      c_ctxt->filetype = MPI_BYTE;
      c_ctxt->etype_size = 1;
      if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED) {
	c_ctxt->intracomm = comm;
	c_ctxt->cli_serv_comm = comm; 
      }
      else {
	c_ctxt->intracomm = client_global_state.intracomm;
	c_ctxt->cli_serv_comm = client_global_state.initial_cli_serv_intercomm;
      }
      c_ctxt->posix_access_mode = 0;
      if (amode & MPI_MODE_RDONLY)
	c_ctxt->posix_access_mode = c_ctxt->posix_access_mode | O_RDONLY; // For read/write modify
      if (amode & MPI_MODE_RDWR)
	c_ctxt->posix_access_mode = c_ctxt->posix_access_mode | O_RDWR;
      if (amode & MPI_MODE_WRONLY)
	c_ctxt->posix_access_mode = c_ctxt->posix_access_mode | O_RDWR; // For read-modify-write
      if (amode & MPI_MODE_CREATE)
	c_ctxt->posix_access_mode = c_ctxt->posix_access_mode | O_CREAT;
      if (amode & MPI_MODE_EXCL)
	c_ctxt->posix_access_mode = c_ctxt->posix_access_mode | O_EXCL;
      
      old_mask = umask(022);
      umask(old_mask);
      c_ctxt->perm = old_mask ^ 0666;
      c_ctxt->mpi_access_mode = amode; 
      cli_open(c_ctxt);   
      
    // this should be moved into init
    // pb: may not know which are servers and which are clients
#if defined(CLARISSE_CONTROL) && defined(CLARISSE_BEACON)
      if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED) {
	int myrank;
	MPI_Comm_rank(client_global_state.intracomm, &myrank);
	if (myrank == 0)
	  cls_ctrl_subscribe();
      }
#endif
      if (clarisse_am_i_coupled_server(c_ctxt)) {
	MPI_Comm *comm;
#if defined(CLARISSE_CONTROL) && (!defined(CLARISSE_BEACON))
	//cls_ctrl_publisher_start();
#endif	
	i_am_server = 1;
	comm = (MPI_Comm *) clarisse_malloc(sizeof(MPI_Comm));
	comm[0] =  c_ctxt->intracomm;
	server_global_state_init(&c_ctxt->clarisse_pars, c_ctxt->intracomm, 1, comm);
	serv_open(c_ctxt);
      }
      ret = MPI_SUCCESS;
    } // NO ROMIO 
  }
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_open < CLARISSE_MAX_CNT_TIMED_OPS) {
    local_timing.time_open[local_timing.cnt_open][1] = MPI_Wtime();
    local_timing.cnt_open++;
  }
#endif  
    
  return ret;
}

void cli_open(client_iocontext_t * c_ctxt){
  clarisse_target_t *local_tgt, *intermediary_tgt;
  int global_descr;
  
  assert((client_global_state.clarisse_pars.collective == CLARISSE_VB_COLLECTIVE)
	 || (client_global_state.clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE));
  c_ctxt->fd_sys = -1; // To be open by the aggregator only
  c_ctxt->fp_ind = c_ctxt->fp_sys_posn = 0;
  /*    
  //  cls_adio_open(c_ctxt);
  c_ctxt->fd_sys = open(c_ctxt->filename, c_ctxt->posix_access_mode, c_ctxt->perm);
  if (c_ctxt->fd_sys < 0) {
    perror("Error opening file\n");
    handle_error(MPI_ERR_OTHER, "POSIX file open error\n");
  }
  
  if (c_ctxt->posix_access_mode & MPI_MODE_APPEND)
    c_ctxt->fp_ind = c_ctxt->fp_sys_posn = lseek(c_ctxt->fd_sys, 0, SEEK_END);
  */
  global_descr = clarisse_hash((unsigned char *)c_ctxt->filename);
  local_tgt =  NULL; // will be set in read or write (memory data type)
  intermediary_tgt = clarisse_target_alloc_init(c_ctxt->filename, 
						global_descr, 
						CLARISSE_TARGET_VIEW, 
						distrib_get(MPI_BYTE, 0),
						(void *) c_ctxt->fh);  
  client_iocontext_init(c_ctxt, local_tgt, intermediary_tgt);
  c_ctxt->global_descr =  global_descr;
}


// only for the coupled version

void serv_open(client_iocontext_t *c_ctxt) {
  server_iocontext_t *s_ctxt;
  clarisse_target_t *aggr_tgt;
  distrib *distr;
  int nprocs;

  MPI_Comm_size(c_ctxt->intracomm, &nprocs);
  distr = distrib_get(MPI_BYTE, 0);
  aggr_tgt = clarisse_target_alloc_init(c_ctxt->filename, c_ctxt->global_descr, CLARISSE_TARGET_FILE, distr, NULL); //(void *) c_ctxt->fh);  
  c_ctxt->fd_sys = aggr_tgt->local_handle.fd;
  if (c_ctxt->posix_access_mode & MPI_MODE_APPEND)
    c_ctxt->fp_ind = c_ctxt->fp_sys_posn = lseek(c_ctxt->fd_sys, 0, SEEK_END);

  aggr_tgt->global_descr = c_ctxt->global_descr;
  HASH_ADD_INT(server_global_state.active_targets, global_descr, aggr_tgt);

  
  //s_ctxt =  server_iocontext_alloc_init(aggr_tgt, server_global_state.clarisse_pars.nr_servers, nprocs);
  s_ctxt =  server_iocontext_init(aggr_tgt->global_descr);
  server_iocontext_update(s_ctxt, aggr_tgt, 0 /*client_idx*/, server_global_state.clarisse_pars.nr_servers, nprocs);
  s_ctxt->local_target->local_handle.fd = c_ctxt->fd_sys; 
}

