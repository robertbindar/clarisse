#ifndef DCACHE_H
#define DCACHE_H

#include <stdlib.h>
#include "list.h"
#include "buffer_pool.h"
#include "clarisse.h"
#include "target.h"

#define DCACHE_NONCONCURRENT_ACCESS 0
#define DCACHE_CONCURRENT_ACCESS 1


typedef struct{
  int concurrency;
  buffer_unit_p bufs;
  int max_elements;
  int crt_elements;
  int cnt_dirty;
  struct dllist lru;
  
  pthread_mutex_t dcache_lock;
  pthread_cond_t  dcache_full;
  pthread_cond_t  not_in_lru; // a certain page is not in lru 
  pthread_cond_t  lru_empty;  // lru is empty
  pthread_cond_t  wait_for_assigned_buf;  // wait if a buffer is assigned
  buffer_unit_p wb_buffers;
  buffer_unit_p assigned_wb_buffers;
} dcache_t;

void dcache_init(dcache_t * h, int max_elements, int concurrency);
dcache_t * dcache_alloc_init(int max_elements, int concurrency);
void dcache_free(dcache_t * h_p);
void dcache_insert(dcache_t * h_p, buffer_unit_p bu);
void dcache_rm(dcache_t * h_p, buffer_unit_p bu);
void dcache_rm2(dcache_t * h, buffer_unit_p bu);
buffer_unit_p dcache_rm_lru(dcache_t * h_p);
//buffer_unit_p  dcache_peek_lru(dcache_t * h);
void  dcache_print(dcache_t * h_p, char* mesg);
//buffer_unit_p dcache_atomic_bufpool_move(dcache_t * h_p, buffer_pool_p b);
//buffer_unit_p dcache_atomic_bufpool_move_if_waiting(dcache_t * h_p, buffer_pool_p b);
void dcache_atomic_bufpool_move_all(dcache_t * h_p, buffer_pool_p b);

buffer_unit_p dcache_find_page(dcache_t * h, clarisse_off off);
buffer_unit_p dcache_find_page_rm_lru(dcache_t * h, clarisse_off off);
buffer_unit_p dcache_find_page_wait_if_assigned(dcache_t * h, clarisse_off off);
buffer_unit_p dcache_put_page(dcache_t *h, buffer_pool_p b, clarisse_target_t *tgt);
void dcache_wait_for_page(dcache_t * h, clarisse_off off);
void dcache_assigned_wb_to_lru(dcache_t * h, buffer_unit_p bu);

#endif
