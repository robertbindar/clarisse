#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "test_datatypes.h"
#include "marshal.h"

// Test setview, write and read_at 7 character (Two blocks). 

char filename_vb[256];
char filename_tp[256];

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void setview_exec(MPI_Datatype d, MPI_Offset displ, MPI_Info info, char *filename, int n, MPI_Datatype mem_type, int phases) {
  int rank, err, i, mem_type_size;
  MPI_File fh;
  MPI_Status status;
  MPI_Aint lb, mem_type_extent;
  char *buf1;
  char *buf2;
  //MPI_Offset off;

  MPI_Type_get_extent(mem_type, &lb, &mem_type_extent);
  MPI_Type_size(mem_type, &mem_type_size);

  buf1 = malloc(mem_type_extent * n);
  buf2 = malloc(mem_type_extent * n);

  for (i = 0; i < mem_type_extent * n; i++){
    buf1[i] = 'a' + i % 16;
    if (mem_type_size != mem_type_extent)
      buf2[i] = 'a' + i % 16;
  }
  if (mem_type_size == mem_type_extent)
    bzero(buf2, mem_type_extent * n);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_File_delete(filename, MPI_INFO_NULL);
  err = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");
  
  
  for (i = 0; i < phases; i++) {
    err = MPI_File_write_all(fh, buf1, n, mem_type, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_write_all");
  }

  err = MPI_File_seek(fh, 0, MPI_SEEK_SET);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_seek");

  for (i = 0; i < phases; i++) {
    err = MPI_File_read_all(fh, buf2, n, mem_type, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_read_all");
  }

  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");

  if (strncmp(buf1, buf2, n)) {
    //  fprintf(stderr, "File: %s: \n\tWritten buf=%s\n\tRead buf=%s\n", 
    //filename, buf1, buf2);
    fprintf(stderr, "File: %s: \n", filename);
    handle_error(err,"TEST 7 failed: read a different value than written");
  }

  free(buf1);
  free(buf2);
}



void setview_test(MPI_Datatype d, MPI_Offset displ, int n, MPI_Datatype mem_type, int phases) {
  MPI_Info info;

  MPI_Info_create (&info);
   
  MPI_Info_set(info, "clarisse_collective", "romio");
  setview_exec(d, displ, info, filename_tp, n, mem_type, phases);
  
  //printf("VB...\n");
  MPI_Info_set(info, "clarisse_collective", "listio_vb");
  setview_exec(d, displ, info, filename_vb, n, mem_type, phases);
  MPI_Info_free(&info);

  free_datatype(d);
  free_datatype(mem_type);
}

int main(int argc, char **argv)
{
  int rank, nprocs, size, phases;
  MPI_Datatype type, mem_type;
  long long int displ;

  //  setenv("CLARISSE_BUFFER_SIZE", "4", 1 /*overwrite*/);
  setenv("CLARISSE_BUFPOOL_SIZE", "1", 1 /*overwrite*/);
  //setenv("CLARISSE_LAZY_EAGER_VIEW", "eager", 1 /*overwrite*/);

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (argc < 4) {
    if (rank == 0) {
      fprintf(stderr, "Missing argument, call %s filename view_replay_file size_to_write_read\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }


  sprintf(filename_vb, "%s_vb", argv[1]);
  sprintf(filename_tp, "%s_tp", argv[1]);
  size = atoi(argv[3]);

  type = get_type_from_file(argv[2], &displ);
  if (argc == 5) {
    long long int mem_displ;
    mem_type = get_type_from_file(argv[4], &mem_displ);
  }
  else
    mem_type = MPI_BYTE;
 
  phases = 10; // fails for 4, works with 3
  phases = 1;
  setview_test(type, displ, size, mem_type, phases);
  
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
  return 0; 
}
