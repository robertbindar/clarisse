#include "util.h"
#ifdef CLARISSE_CONTROL
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include "mpi.h"
#include "error.h"
#include "cls_ctrl.h"
#include "clarisse.h"
#include "client_iocontext.h"

extern client_global_state_t client_global_state;
#endif
int main(int argc, char **argv){
#ifdef CLARISSE_CONTROL  
  int myrank, nprocs, nr_servers, i;
  int *server_map;

  setenv("CLARISSE_CONTROL", "enable", 1 /*overwrite*/);
  setenv("CLARISSE_COUPLENESS","coupled", 1 /*overwrite*/);

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (nprocs < 2) {
    fprintf(stderr, "Error: run with at least 2 processes\n");
    MPI_Finalize();
    exit(1);
  }
  nr_servers = nprocs / 2;
  server_map = malloc(nr_servers * sizeof(int));
  for (i = 0; i < nr_servers; i++)
    server_map [i] = i * 2 + 1; 
  cls_reassign_servers(CLARISSE_SERVER_MAP_CUSTOM, nr_servers, server_map);
  if (myrank == 0)
    cls_print_servers();
  /*  if (clarisse_am_i_server())
    cls_ctrl_publisher_start();

  for (i=0;i<4;i++) {
    if (myrank % 2) {
      clarisse_ctrl_msg_t msg;
      clarisse_ctrl_msg_props_t props;
      msg.type = CLARISSE_CTRL_MSG_PUBLISH_FILE_WRITE_TIME;
      msg.payload.clarisse_ctrl_msg_file_write_time.time = (double)i;
      props.dest.rank = client_global_state.controller_rank;
      props.dest.comm = client_global_state.intracomm;
      props.priority = CLARISSE_EVENT_PRIORITY_LOW;
      props.scope = CLARISSE_EVENT_SCOPE_PROCESS;
      cls_ctrl_publish(&msg, &props);
    }
    sleep(1);
  }
  */

  free(server_map);  
  MPI_Finalize();
#else
  CLARISSE_UNUSED(argc);
  CLARISSE_UNUSED(argv);
#endif
  return 0;
}


