#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "clarisse.h"
// Read_at 7 character (Two blocks), assumes that a file containing 
//  "abcdefg" exists. 

#define SIZE 7
#define COLLECTIVE_IO 0 
#define INDEPENDENT_IO 1

int coll_indep;
char filename[256];
char buf1[SIZE+1]="abcdefg";
char buf2[SIZE+1]="\0";

void handle_error(int errcode, char *str);

int main(int argc, char **argv)
{
    int myrank, err;
    MPI_File fh;
    MPI_Status status;
    char cmd[128];
    char *env;

    env = getenv("ENV_FROM_SCRIPT");
    if (!env || strncmp(env, "true", 4)) { 
      setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
      env = getenv("CLARISSE_COLLECTIVE");
      if (!env || strncmp(env, "listio", 6)) { 
	setenv("CLARISSE_BUFFER_SIZE", "4", 1 /*overwrite*/);
	setenv("CLARISSE_NET_BUF_SIZE","4", 1);
      }
      else {
	setenv("CLARISSE_BUFFER_SIZE", "32", 1 /*overwrite*/);
	setenv("CLARISSE_NET_BUF_SIZE","32", 1);
      }
    }
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(cls_get_client_intracomm(), &myrank);

    if (argc < 3) {
      if (myrank == 0) {
	fprintf(stderr, "Missing argument, call %s filename collective/independent\n", argv[0]);
      }
      MPI_Finalize();
      exit(1);
    }
    if (strcmp(argv[2], "collective") && strcmp(argv[2], "independent")){
      if (myrank == 0) {
	fprintf(stderr, "Wrong collective/independent argument argument, call %s filename collective/independent\n", argv[0]);
      }
      MPI_Finalize();
      exit(1);
    }
    if (!strcmp(argv[2], "collective"))
      coll_indep = COLLECTIVE_IO;
    if (!strcmp(argv[2], "independent"))
      coll_indep = INDEPENDENT_IO;
    strcpy(filename, argv[1]);
    sprintf(cmd, "echo abcdefg > %s", filename);
    system(cmd);

    
    if (cls_is_client()){
      MPI_Comm intracomm = cls_get_client_intracomm();
      err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE |MPI_MODE_RDWR ,
			  MPI_INFO_NULL, &fh);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_open");
      if (coll_indep == INDEPENDENT_IO) {
	MPI_Barrier(intracomm);
	err = MPI_File_read_at(fh, 0, buf2, SIZE, MPI_BYTE, &status);
      }
      else
	err = MPI_File_read_at_all(fh, 0, buf2, SIZE, MPI_BYTE, &status);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_read_all");
      
      err = MPI_File_close(&fh);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_close");
      
      if (strncmp(buf1, buf2, SIZE))
	handle_error(err,"TEST 5 failed: read a different value than expected");
    }
    setenv("CLARISSE_TEST", __FILE__, 1);
    MPI_Finalize();
    return 0; 
}
