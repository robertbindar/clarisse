#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "clarisse.h"

char filename[256]; //="abc";

void handle_error(int errcode, char *str);

void test0(){
  int err;
  MPI_File fh;
  int  myrank;  
  MPI_Comm intracomm = cls_get_client_intracomm();

  MPI_Comm_rank(intracomm, &myrank);
  if (myrank == 0)  
    MPI_File_delete(filename, MPI_INFO_NULL);
  MPI_Barrier(intracomm);
  err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      MPI_INFO_NULL, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");
}

void test1(){
  int err;
  char filename1[256];
  char filename2[256];
  MPI_File fh1,fh2;
  int  myrank;   
  MPI_Comm intracomm = cls_get_client_intracomm();
  
  sprintf(filename1,"%s1",filename);
  sprintf(filename2,"%s2",filename);
  MPI_Comm_rank(intracomm, &myrank);
  if (myrank == 0)  {
    MPI_File_delete(filename1, MPI_INFO_NULL);
    MPI_File_delete(filename2, MPI_INFO_NULL);
  }
  MPI_Barrier(intracomm);
  
  err = MPI_File_open(intracomm, filename1, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      MPI_INFO_NULL, &fh1);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_open(intracomm, filename2, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      MPI_INFO_NULL, &fh2);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  

  err = MPI_File_close(&fh1);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");

  err = MPI_File_close(&fh2);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");

}


int main(int argc, char **argv)
{
  int rank;

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  
  if (argc < 2) {
    if (rank == 0) {
      fprintf(stderr, "Missing argument, call %s filename\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  strcpy(filename, argv[1]);

  if (cls_is_client()){
    test0();
    test1();
  }
     
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
  return 0; 
}
