#include <assert.h>
#include "buffer.h"
#include "map.h"
#include "client_iocontext.h"
#include "server_iocontext.h"
#include "buffer_pool.h"
#include "tasks.h"
#include "list.h"
#include "hdr.h"
#include "error.h"
#include "marshal.h"
#include "buffer.h"
#include "net_dt.h"
#include "util.h"
#include "clarisse_internal.h"

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern client_global_state_t client_global_state;


int  client_recvret_read_round(client_iocontext_t *c_ctxt);
int client_task_to_read_rq(client_iocontext_t *c_ctxt, struct rq_file_read *rq, int log_ios_rank, struct dllist **off_len_block_list);
static void client_sendreq_read_round2(client_iocontext_t *c_ctxt);
static void client_sendreq_read_round_listio2(client_iocontext_t *c_ctxt);

int client_file_access(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v, int count, int access_type);

int MPI_File_read_at(MPI_File fh, MPI_Offset offset, void * buf, int count,
                         MPI_Datatype datatype, MPI_Status *status) {
  int ret;
  /*
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_read < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_read[local_timing.cnt_read][0] = MPI_Wtime();
#endif   
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
  char *type_buf;
  int type_size;
  printf("READ_AT: OFFSET=%lld, COUNT=%d ", offset, count); 
  print_dt(datatype);
  type_buf = clarisse_malloc(1024);
  type_size = decode_pack_dt_out_buf(datatype, &type_buf);
  save_type_in_file("MEMORY", type_buf, type_size, 0);
  clarisse_free(type_buf);
#endif
  */
  client_iocontext_t *c_ctxt;
  c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
  if (c_ctxt) {
    int bytes_xfered, datatype_size;

    bytes_xfered = cls_read(c_ctxt, (char *) buf, count,
			      datatype, CLARISSE_EXPLICIT_OFFSET, offset);
    
    MPI_Type_size(datatype, &datatype_size);
    MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
    ret = MPI_SUCCESS; 
  }
  else
    ret = PMPI_File_read_at(fh, offset, buf, count, datatype, status); 
  /*
#ifdef CLARISSE_TIMING
    if (local_timing.cnt_read < CLARISSE_MAX_CNT_TIMED_OPS) {
      int size, transfer_count;
    
      MPI_Get_count(status, datatype, &transfer_count);
      local_timing.time_read[local_timing.cnt_read][1] = MPI_Wtime();
      MPI_Type_size(datatype, &size);
      local_timing.size_read[local_timing.cnt_read] = (double) transfer_count * size;
      local_timing.cnt_read++;
    }
#endif  
  */
  return ret;

}

int MPI_File_read(MPI_File fh, void *buf, int count, MPI_Datatype datatype, MPI_Status *status){
  int ret;
  /*
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_read < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_read[local_timing.cnt_read][0] = MPI_Wtime();
#endif
  */
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
  char *type_buf;
  int type_size;
  printf("READ: COUNT=%d ", count); 
  print_dt(datatype);
  type_buf = clarisse_malloc(1024);
  type_size = decode_pack_dt_out_buf(datatype, &type_buf);
  save_type_in_file("MEMORY", type_buf, type_size, 0);
  clarisse_free(type_buf);
#endif
  client_iocontext_t *c_ctxt;
  c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
  if (c_ctxt) {
    int bytes_xfered, datatype_size;

    bytes_xfered = cls_read(c_ctxt, (char *) buf, count,
			    datatype, CLARISSE_INDIVIDUAL,  c_ctxt->fp_ind);
    MPI_Type_size(datatype, &datatype_size);
    MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
    ret = MPI_SUCCESS;
  }
  else
    ret = PMPI_File_read(fh, buf, count, datatype, status); 
  /*
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_read < CLARISSE_MAX_CNT_TIMED_OPS) {
    int size, transfer_count;

    MPI_Get_count(status, datatype, &transfer_count);
    local_timing.time_read[local_timing.cnt_read][1] = MPI_Wtime();
    MPI_Type_size(datatype, &size);
    local_timing.size_read[local_timing.cnt_read] = (double) transfer_count * size;
    local_timing.cnt_read++;
  }
#endif  
  */
  return ret;

}

int cls_read(client_iocontext_t *c_ctxt, 
	     void *buf, int count,
	     MPI_Datatype datatype, int file_ptr_type,
	     MPI_Offset offset)
{
  int bytes_xfered, buftype_size;
  clarisse_off l_v, r_v;

  c_ctxt->local_target = clarisse_target_alloc_init("", -1, CLARISSE_TARGET_MEMORY, distrib_get(datatype, 0), (void *)buf);
  MPI_Type_size(datatype, &buftype_size);
  // If not individual it can be explicit offset, use it 
  l_v = (file_ptr_type == CLARISSE_INDIVIDUAL) ? 
    func_sup(c_ctxt->fp_ind, c_ctxt->intermediary_target->distr):
    c_ctxt->etype_size * offset;
  r_v = l_v +  buftype_size * count - 1;


  if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED)
    bytes_xfered = client_file_access(c_ctxt, l_v, r_v, count, CLARISSE_READ);
  else {
    int err;
    clarisse_off l_f, r_f;
      
    bytes_xfered = 0;
    l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
    r_f = func_1(r_v, c_ctxt->intermediary_target->distr);
    create_io_tasks(c_ctxt, l_v, l_f, r_f, 0, 0, CLARISSE_STATIC_FILE_PARTITIONING);
    if (c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
      client_sendreq_read_round_listio2(c_ctxt);
    else
      client_sendreq_read_round2(c_ctxt);
    
    err = MPI_Waitall(c_ctxt->mpi_rq_pool.cnt, c_ctxt->mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
    if (err != MPI_SUCCESS)
      handle_error(err, "cls_read: waitall error\n");
    free_mpi_rq(&c_ctxt->mpi_rq_map);   
    bytes_xfered += client_recvret_read_round(c_ctxt);
  }

  distrib_free(c_ctxt->local_target->distr);
  clarisse_free(c_ctxt->local_target);
  MPI_Type_size(datatype, &buftype_size);
  if (bytes_xfered != buftype_size * count)
    handle_error(MPI_ERR_OTHER, "cls_read: bytes_xfered  != bufsize\n");    
  if (file_ptr_type == CLARISSE_INDIVIDUAL) 
    c_ctxt->fp_ind = func_1(r_v, c_ctxt->intermediary_target->distr) + 1; 
  
  return bytes_xfered;
}

static void client_sendreq_read_round2(client_iocontext_t *c_ctxt){
  int err, ph_ios_rank, log_ios_rank/*, myrank*/;
  char *data_rq;
  MPI_Request *mpi_rq;

  //MPI_Comm_rank(c_ctxt->intracomm, &myrank);
  //request_queues_set_first(&c_ctxt->task_queues, myrank);  
  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
    ph_ios_rank = client_global_state.log2ph[log_ios_rank];
    if (ph_ios_rank != CLS_SERVER_INACTIVE) {
      if (c_ctxt->pending_ios_ret[log_ios_rank]) {
	struct rq_file_read *rq;
	int irq_max_size;
	struct dllist *off_len_block_list;
	MPI_Datatype d;

	mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
	off_len_block_list = NULL;
	irq_max_size = sizeof(struct rq_file_read) + CLARISSE_MAX_VIEW_SIZE + CLARISSE_MAX_TARGET_NAME_SIZE;
	data_rq = (char *) clarisse_malloc(irq_max_size);
	rq = (struct rq_file_read *)data_rq; 
	client_task_to_read_rq(c_ctxt, rq, log_ios_rank, &off_len_block_list);
	// mpi_rq is not used by now (if reallocation is done the pointer will be lost 
	d = net_dt_commit(&c_ctxt->net_dt);	
	save_mpi_rq(&c_ctxt->mpi_rq_map, mpi_rq, d, data_rq, off_len_block_list);
	//printf("Send request to %d\n", ph_ios_rank);
	err = MPI_Isend(data_rq, 1, d, ph_ios_rank, RQ_FILE_READ, c_ctxt->cli_serv_comm, mpi_rq);
	if (err != MPI_SUCCESS)
	  handle_error(err, "coll_read: send error\n");
	net_dt_reset(&c_ctxt->net_dt); // reinit the net datatype
      } 
    }
  }
}


static void client_sendreq_read_round_listio2(client_iocontext_t *c_ctxt){
  int err, ph_ios_rank, log_ios_rank/*, myrank*/;
  char *data_rq;
  MPI_Request *mpi_rq;

  //MPI_Comm_rank(c_ctxt->intracomm, &myrank);  
  //request_queues_set_first(&c_ctxt->task_queues, myrank);  
  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
    ph_ios_rank = client_global_state.log2ph[log_ios_rank];
    if (ph_ios_rank != CLS_SERVER_INACTIVE) {
      if (c_ctxt->pending_ios_ret[log_ios_rank]) {
	//iotask_p iot;
	struct rq_file_read *rq;
	int irq_max_size;
	struct dllist *off_len_block_list;
	MPI_Datatype d;
	//clarisse_off crt_l_f;
      
	irq_max_size = sizeof(struct rq_file_read) + CLARISSE_MAX_TARGET_NAME_SIZE;      
      //iot = (iotask_p)request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank);
      //request_queues_init_enum(&c_ctxt->task_queues);
      //iot = (iotask_p)request_queues_enum(&c_ctxt->task_queues);
      //crt_l_f = iot->l_f;
      //while (iot) {
      
	request_queues_init_enum(&c_ctxt->task_queues, log_ios_rank);
	//c_ctxt->crt_iot = request_queues_enum(&c_ctxt->task_queues);
	//c_ctxt->crt_l_f = ((iotask_p)c_ctxt->crt_iot)->l_f;
	//while (c_ctxt->crt_iot){
	while(c_ctxt->task_queues.crt_elems[log_ios_rank]) {
	  mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
	  off_len_block_list = NULL;
	  data_rq = (char *) clarisse_malloc(irq_max_size);
	  rq = (struct rq_file_read *)data_rq; 
	  client_task_to_read_rq(c_ctxt, rq, log_ios_rank, &off_len_block_list);
	  off_len_pack(&c_ctxt->net_dt, off_len_block_list);
	  // mpi_rq is not used by now (if reallocation is done the pointer will be lost 
	  d = net_dt_commit(&c_ctxt->net_dt);	
	  save_mpi_rq(&c_ctxt->mpi_rq_map, mpi_rq, d, data_rq, off_len_block_list);
	  //printf("Send request to %d\n", ph_ios_rank);
	  err = MPI_Isend(data_rq, 1, d, ph_ios_rank, RQ_FILE_READ, c_ctxt->cli_serv_comm, mpi_rq);
	  if (err != MPI_SUCCESS)
	    handle_error(err, "coll_read: send error\n");
	  net_dt_reset(&c_ctxt->net_dt); // reinit the net datatype
	}
      } 
    }
  }
}
