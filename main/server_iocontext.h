#ifndef SERVER_IOCONTEXT_H
#define SERVER_IOCONTEXT_H

#include <stdio.h>
#include "client_iocontext.h"
#include "list.h"
#include "map.h"
#include "request_queues.h"
#include "clarisse.h"
#include "dcache.h"
#include "buffer_pool.h"
#include "target.h"
#include "thr_pool.h"
#include "client_iocontext.h"


typedef struct {
  //MPI_Comm intercomm; // inter-communicator
  int partitioning_stride_factor;
  int client_nprocs;
  distrib **remote_view_distr; // Remote views  
  short int *crt_write_ops; // For intercomm and dynamic record where i received from
  int crt_arrived_writes; // For current collective 
  short int *next_write_ops; // For next collective I record where I received from 
  int next_arrived_writes;    // count of writes received so far
  struct net_dt net_dt;
  mpi_rq_map_t *mpi_rq_map;
  alloc_pool_t mpi_rq_pool; 
  clarisse_off crt_page;
} client_rt_info_t;

typedef struct {
  UT_hash_handle hh;
  clarisse_off block_off;
  dllist task_list;
} future_access_t;


// server iocontext
typedef struct {
  UT_hash_handle hh;
  int global_descr;
  dcache_t dcache; // used buffer pool 
  clarisse_params clarisse_pars; // params 
  clarisse_target_t *local_target;
  //MPI_Comm intracomm;
  
  client_rt_info_t *client_rt_info;
  future_access_t *read_futures;
  future_access_t *write_futures;
} server_iocontext_t;

// global stager state
typedef struct server_global_state_t {
  int initialized;
  clarisse_target_t *active_targets;
  server_iocontext_t *s_ctxt_map;
  struct dllist write_task_queue; // each task is associated to a context
  request_queues read_task_queue; // 
  buffer_pool_p buf_pool; // Free buffer pool for the stager 
  tpool_t tpool;
  clarisse_params clarisse_pars; 
  char *net_buf;

  int no_clients;
  int total_client_procs;
  MPI_Comm intracomm;
  MPI_Comm *intercomms; // no_clients
  int *client_nprocs; // number of clients
  int finalized_cnt;
  int controller_rank;
  int global_controller_rank;
} server_global_state_t;

server_iocontext_t * server_iocontext_init(int global_descr);
server_iocontext_t * server_iocontext_alloc_init(clarisse_target_t *local_target, int partitioning_stride_factor, int nprocs);
void server_iocontext_update(server_iocontext_t *s_ctxt, clarisse_target_t *local_target, int client_idx, int partitioning_stride_factor, int client_nprocs);
void server_iocontext_update_client_rt_info(server_iocontext_t *s_ctxt, int client_idx, int partitioning_stride_factor, int client_nprocs);
void server_iocontext_update_target(server_iocontext_t *s_ctxt, clarisse_target_t *local_target);
server_iocontext_t * server_iocontext_find(int global_descr);
server_iocontext_t * server_iocontext_find_del(int global_descr);
void server_iocontext_free(server_iocontext_t *s_ctxt);


void server_global_state_init(clarisse_params *clarisse_pars, MPI_Comm intracomm, int nr_clients, MPI_Comm *intercomms);
void server_global_state_free();


#endif




