#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"

#define WR 0
#define WRRD 1

char filename[256];

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

extern double delay;

void my_comp(double sleep_time){
  double t0;
  
  t0 = MPI_Wtime();
  while ((MPI_Wtime() - t0) < sleep_time);
}

void write_read_ntimes(int access_type, char *filename, int n, MPI_Datatype mem_type, int phases, double sleep_time) {
  int myrank, err, i, mem_type_size, nprocs;
  MPI_File fh;
  MPI_Status status;
  MPI_Aint lb, mem_type_extent;
  char *buf;
  MPI_Comm intracomm = cls_get_client_intracomm();
  
  MPI_Comm_size(intracomm, &nprocs);
  MPI_Comm_rank(intracomm, &myrank);
  MPI_Type_get_extent(mem_type, &lb, &mem_type_extent);
  MPI_Type_size(mem_type, &mem_type_size);

  buf = malloc(mem_type_extent * n);
  
  for (i = 0; i < mem_type_extent * n; i++){
    buf[i] = 'a' + i % 16;
  }
  if (myrank == 0)
    MPI_File_delete(filename, MPI_INFO_NULL);
  MPI_Barrier(intracomm);
  err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		     MPI_INFO_NULL, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  for (i = 0; i < phases; i++) {
    MPI_Offset off = (MPI_Offset)n*(MPI_Offset)myrank + (MPI_Offset)i*(MPI_Offset)n*(MPI_Offset)nprocs;
    err = MPI_File_write_at_all(fh, off, buf, n, mem_type, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_write_all");
    my_comp(sleep_time);
  }
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");
  
  if (access_type == WRRD) {
    MPI_Barrier(intracomm);
    err = MPI_File_open(intracomm, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
    
    for (i = 0; i < phases; i++) {
      //double t0,t1,tmp_t0,tmp_t1;
      MPI_Offset off = (MPI_Offset)n*(MPI_Offset)myrank + (MPI_Offset)i*(MPI_Offset)n*(MPI_Offset)nprocs;
      //t0 = MPI_Wtime();
      err = MPI_File_read_at_all(fh, off, buf, n, mem_type, &status);
      /*t1 = MPI_Wtime();
	MPI_Reduce(&t0, &tmp_t0, 1, MPI_DOUBLE, MPI_MIN, 0, intracomm); 
	MPI_Reduce(&t1, &tmp_t1, 1, MPI_DOUBLE, MPI_MAX, 0, intracomm); 
	if (myrank == 0)
	printf("Read time = %9.6f\n", tmp_t1 -  tmp_t0);
      */
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_read_all");
    }
    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
  }
  free(buf);
}


void remove_aggrs(int n) {
  int i, nr_aggr;
  
  nr_aggr = cls_get_nr_active_servers();
  for (i = 0; i < n; i++) {
    int log_ios_rank, flag;
    
    flag = 1;
    while(flag) {
      log_ios_rank = ((int)random()) % nr_aggr;
      flag = cls_active_server_down(log_ios_rank);
      /*      int myrank;
      MPI_Comm intracomm = cls_get_client_intracomm();
      MPI_Comm_rank(intracomm, &myrank);
      if (myrank == 0) {
	printf("Shut down %d flag=%d\n", log_ios_rank, flag);
	cls_print_servers();
	}*/
    }
    
  }
}

void test_8_2_0(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time) {
  int myrank;

  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);
  if (myrank == 0)
    printf("\n%s PARAMS\n\tFILE_NAME = %s\n\tACCESS_SIZE = %d\n\tPHASES = %d\n\tSLEEP_TIME = %f seconds\n\n", __FILE__, filename, n, phases, sleep_time);
  write_read_ntimes(access_type, filename, n, type, phases, sleep_time);
}

void test_8_2_1(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time) {
  char *val;

  if ((val = getenv("CLARISSE_COLLECTIVE"))  && (!strcmp(val, "romio")))
    test_8_2_0(access_type, filename, n, type, phases, sleep_time);
  else {

    int nr_aggr, i;
    
    nr_aggr = cls_get_nr_active_servers();
    
    //    for (i = nr_aggr; i >= 64; i = i / 2) {
    // MAX is used for small number of aggregators
    for (i = nr_aggr; i >= nr_aggr / 2; (i -= MAX(8 * nr_aggr / 128, 1)) ) {
      int myrank;
      MPI_Comm intracomm = cls_get_client_intracomm();
      
      MPI_Comm_rank(intracomm, &myrank);
      if (myrank == 0)
	printf("Write n times crt_nr_aggr=%d cls_get_nr_active_servers()=%d\n",i,cls_get_nr_active_servers() ); 
      write_read_ntimes(WR, filename, n, type, phases, sleep_time);

      //remove_aggrs(i/2);
      remove_aggrs(8 * nr_aggr / 128);
    }
  }
}


void test_8_2_2(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time) {
  char *val;
  
  if ((val = getenv("CLARISSE_COUPLENESS"))  && (!strcmp(val, "intercomm"))){
    double load;
    int  i, active_log_ios_rank, myrank; //nr_aggr,
    MPI_Comm intracomm = cls_get_client_intracomm();
  
    MPI_Comm_rank(intracomm, &myrank);
    //nr_aggr = cls_get_nr_active_servers();
    for (i = 0; i <= 1; i++) {
      for (load = 1e-6; load  < 1e-3; load *= 2) {
	if (myrank == 0) {
	  for (active_log_ios_rank = 0; active_log_ios_rank < i; active_log_ios_rank++)
	    cls_active_server_slow(active_log_ios_rank, load);
	  printf("write_read_n_times load=%10.6f\n", load );
	}
	MPI_Barrier(intracomm);
	write_read_ntimes(access_type, filename, n, type, phases, sleep_time);
	if (i == 0) break;
      }
    }
  }
  else 
    printf("test_8_2_ 2 works only for INTERCOMM coupleness\n");
}

// Start with all servers and put down a server after the first phase

void test_8_2_3(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time) {
  char *val;
  int myrank;
  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);

  if ((val = getenv("CLARISSE_COUPLENESS"))  && (!strcmp(val, "intercomm"))) {
      //&&
      //(val = getenv("CLARISSE_ELASTICITY"))  && (!strcmp(val, "enable"))){
    int myrank, i, active_log_ios_rank;
    MPI_Comm intracomm = cls_get_client_intracomm();
  
    MPI_Comm_rank(intracomm, &myrank);

    active_log_ios_rank = 0;
    for (i = 0; i < phases; i++) {
      if (myrank == 0)
	printf("Phase %d: nr_active_servers = %d\n", i, cls_get_nr_active_servers());
      write_read_ntimes(access_type, filename, n, type, 1, 0);
      if ((i == 2) && (myrank == 0)) 
	cls_active_server_down_publish(active_log_ios_rank);
      my_comp(sleep_time);
    }
    if (phases == 1)
      sleep(1);
  }
  else {
    
    if (myrank == 0)
      //printf("%s works only for CLARISSE_COUPLENESS=intercomm and CLARISSE_ELASTICITY=enable \n", __FUNCTION__);
      printf("%s works only for CLARISSE_COUPLENESS=intercomm\n", __FUNCTION__);
  }
}

// Start with all servers
// Inject load in phase i 
// and remap servers  in phase i + 1

void test_8_2_4(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time, int load_injection_phase, int load_detection_phase, double load) {
  char *val;
  int myrank;
  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);

  if ((val = getenv("CLARISSE_COUPLENESS"))  && (!strcmp(val, "intercomm"))) {
      //&&
      //(val = getenv("CLARISSE_ELASTICITY"))  && (!strcmp(val, "enable"))){
    int myrank, i, active_log_ios_rank;
    MPI_Comm intracomm = cls_get_client_intracomm();
  
    MPI_Comm_rank(intracomm, &myrank);

    active_log_ios_rank = 0;
    for (i = 0; i < phases; i++) {
      if (myrank == 0)
	printf("Phase %d: nr_active_servers = %d\n", i, cls_get_nr_active_servers());
      if ((i == load_injection_phase) && (myrank == 0)) 
	cls_active_server_slow(active_log_ios_rank, load);
      
      write_read_ntimes(access_type, filename, n, type, 1, 0);
      
      if ((i == load_detection_phase) && (myrank == 0)) 
	cls_active_server_down_publish(active_log_ios_rank);
      my_comp(sleep_time);
    }
    if (phases == 1)
      sleep(1);
  }
  else {
    
    if (myrank == 0)
      //printf("%s works only for CLARISSE_COUPLENESS=intercomm and CLARISSE_ELASTICITY=enable \n", __FUNCTION__);
      printf("%s works only for CLARISSE_COUPLENESS=intercomm\n", __FUNCTION__);
  }
}

// Same as test_8_2_ 4, but using the API
// Start with all servers
// Inject load in phase i 
// and remap servers  in phase i + 1

void test_8_2_5(int access_type, char * filename, int n, MPI_Datatype type, int phases, double sleep_time, int load_injection_phase, int load_detection_phase, double load) {
  char *val;
  int myrank;
  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);

  if ((val = getenv("CLARISSE_COUPLENESS"))  && (!strcmp(val, "intercomm"))) {
      //&&
      //(val = getenv("CLARISSE_ELASTICITY"))  && (!strcmp(val, "enable"))){
    int myrank, i, active_log_ios_rank;
    MPI_Comm intracomm = cls_get_client_intracomm();
  
    MPI_Comm_rank(intracomm, &myrank);

    active_log_ios_rank = 0;

    cls_set_load_injection_coll_seq(load_injection_phase, active_log_ios_rank, load);
    cls_set_load_detection_coll_seq(load_detection_phase);

    for (i = 0; i < phases; i++) {
      if (myrank == 0)
	printf("Phase %d: nr_active_servers = %d\n", i, cls_get_nr_active_servers());
      write_read_ntimes(access_type, filename, n, type, 1, 0);
      my_comp(sleep_time);
    }
    if (phases == 1)
      sleep(1);
  }
  else {
    
    if (myrank == 0)
      //printf("%s works only for CLARISSE_COUPLENESS=intercomm and CLARISSE_ELASTICITY=enable \n", __FUNCTION__);
      printf("%s works only for CLARISSE_COUPLENESS=intercomm\n", __FUNCTION__);
  }
}


int _main(int argc, char **argv)
{
  int nprocs, n, phases, sleep_time, myrank, load_injection_phase, load_detection_phase = 3; // If -1 no load detection
  double load;

  setenv("CLARISSE_NR_SERVERS", "2", 1 /*overwrite*/);
  setenv("CLARISSE_COUPLENESS", "intercomm", 1 /*overwrite*/);
  setenv("CLARISSE_CONTROL", "enable", 1 /*overwrite*/);
  setenv("CLARISSE_ELASTICITY", "enable", 1 /*overwrite*/);

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);

  if (argc < 8) {    
    if (myrank == 0) {
      fprintf(stderr, "Missing argument, call %s filename access_size_per_proc phases sleep_time(seconds) load(seconds) load_injection_phase load_detection_phase\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  sprintf(filename, "%s", argv[1]);
  n = atoi(argv[2]);
  phases = atoi(argv[3]);
  sleep_time = atoi(argv[4]);
  load = atof(argv[5]);
  load_injection_phase = atoi(argv[6]);
  load_detection_phase = atoi(argv[7]); // If -1 no load detection
  if (myrank == 0)
    printf("\n%s PARAMS\n\tACCESS_SIZE = %d\n\tPHASES = %d\n\tSLEEP_TIME = %d seconds\n\tLOAD = %13.3f seconds\n\tLOAD_INJECTION_PHASE = %d\n\tLOAD_DETECTION_PHASE = %d\n\n", __FILE__, n, phases, sleep_time, load, load_injection_phase, load_detection_phase); 
  if (cls_is_client()) {
    //test_8_2_0(WRRD, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test_8_2_1(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test_8_2_2(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    //test_8_2_3(WR, filename, n, MPI_BYTE, phases, (double)sleep_time);
    
    //test_8_2_4(WR, filename, n, MPI_BYTE, phases, (double)sleep_time, load_injection_phase, load_detection_phase, load);
    test_8_2_5(WR, filename, n, MPI_BYTE, phases, (double)sleep_time, load_injection_phase, load_detection_phase, load);
  }
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
  return 0; 
}
