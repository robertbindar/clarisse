#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "test_datatypes.h"

// Test setview, write and read_at 7 character (Two blocks). 

#define SIZE 7

char filename_vb[256];
char filename_tp[256];

char buf1[SIZE+1]="abcdefg";
char buf2[SIZE+1]="\0";

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void setview_exec(MPI_Datatype d1, MPI_Offset displ1, MPI_Datatype d2, MPI_Offset displ2, MPI_Info info, char *filename, int n) {
  int rank, err;
  MPI_File fh;
  MPI_Status status;
  
  bzero(buf2, n+1);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_File_delete(filename, MPI_INFO_NULL);
  err = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  

  err = MPI_File_set_view(fh, displ1, MPI_BYTE, d1, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");

  err = MPI_File_set_view(fh, displ2, MPI_BYTE, d2, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");

  err = MPI_File_set_view(fh, displ2, MPI_BYTE, d2, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");


  err = MPI_File_write_all(fh, buf1, n, MPI_BYTE, &status);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_write_all");
  
  err = MPI_File_read_at_all(fh, 0, buf2, n, MPI_BYTE, &status);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_read_all");
  
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");

  if (strncmp(buf1, buf2, n)) {
    fprintf(stderr, "File: %s: \n\tWritten buf=%s\n\tRead buf=%s\n", 
	    filename, buf1, buf2);
    handle_error(err,"TEST 7 failed: read a different value than written");
  }
  
}



void setview_test(MPI_Datatype d1, MPI_Offset displ1, MPI_Datatype d2, MPI_Offset displ2, int n) {
  MPI_Info info;

  MPI_Info_create (&info);

  MPI_Info_set(info, "clarisse_collective", "listio_vb");
  setview_exec(d1, displ1, d2, displ2, info, filename_vb, n);
  MPI_Info_free(&info);

  free_datatype(d1);
  free_datatype(d2);
}

int main(int argc, char **argv)
{
  int rank;
  int nprocs;


  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (argc < 2) {
    if (rank == 0) {
      fprintf(stderr, "Missing argument, call %s filename\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  sprintf(filename_vb, "%s_vb", argv[1]);
  sprintf(filename_tp, "%s_tp", argv[1]);

  /*
  setview_test(MPI_FLOAT, 0, MPI_FLOAT, 1, 7);
  setview_test(MPI_FLOAT, 1, get_contig(), 0, 7); 
  setview_test(MPI_FLOAT, 1, get_lb_contig(), 1, 7);
  */
  setview_test(get_simple_vec(), 1, MPI_FLOAT, 1, 7);

  setenv("CLARISSE_TEST", __FILE__, 1);  
  MPI_Finalize();
  return 0; 
}
