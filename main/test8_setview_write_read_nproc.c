#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"

// Test setview at different offsets, write and read_at (Two blocks). 

#define SIZE 1024
#define COLLECTIVE_IO 0 
#define INDEPENDENT_IO 1

int coll_indep;
char filename_vb[256];
char filename_tp[256];

char buf1[SIZE+1]="abcdefghijklmnopqrstuvwxz";
char buf2[SIZE+1]="\0";

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void setview_exec(MPI_Datatype d, MPI_Offset displ, MPI_Info info, char *filename, int n) {
  int myrank, err;
  MPI_File fh;
  MPI_Status status;
  MPI_Comm intracomm = cls_get_client_intracomm();
  
  bzero(buf2, n + 1);
  MPI_Comm_rank(intracomm, &myrank);  
  if (myrank == 0)
    MPI_File_delete(filename, MPI_INFO_NULL);
  MPI_Barrier(intracomm);
  create_preexisting_file(filename, 16 * n);
  
  err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");
  if (coll_indep == INDEPENDENT_IO)
      err = MPI_File_write(fh, buf1, n, MPI_BYTE, &status);
  else
    err = MPI_File_write_all(fh, buf1, n, MPI_BYTE, &status);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_write_all");
  
  
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");
   
  err = MPI_File_open(intracomm, filename, MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view"); 
  
  if (coll_indep == INDEPENDENT_IO)
    err = MPI_File_read_at(fh, 0, buf2, n, MPI_BYTE, &status);
  else
    err = MPI_File_read_at_all(fh, 0, buf2, n, MPI_BYTE, &status);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_read_all");
  
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");
  
  if (strncmp(buf1, buf2, n)) {
    buf1[n]='\0';
    buf2[n]='\0';
    fprintf(stderr, "Test %s File %s: \n\tWritten buf=%s\n\tRead buf=%s\n", 
	    __FILE__, filename, buf1, buf2);
    handle_error(err,"Read a different value than written");
  }
  
}


#define DO_NOT_ADD_RANK_TO_DISPL 0
#define ADD_RANK_TO_DISPL 1

void setview_test(MPI_Datatype d, MPI_Offset displ, int n, int add_rank) {
  MPI_Info info;
  int myrank;
  MPI_Comm intracomm = cls_get_client_intracomm();

  MPI_Comm_rank(intracomm, &myrank);

  if (add_rank == ADD_RANK_TO_DISPL)
    displ = displ + myrank;
  
  MPI_Info_create (&info);
  
  MPI_Info_set(info, "clarisse_collective", "romio");
  setview_exec(d, displ, info, "abc_tp", n);
  
  MPI_Info_set(info, "clarisse_collective", "listio_vb");
  setview_exec(d, displ, info, "abc_vb", n);
  
  MPI_Info_free(&info);

  free_datatype(d);
  //sleep(1);
  compare_files(filename_tp, filename_vb);
}

int main(int argc, char **argv)
{
  int myrank, nprocs;
  char *env;

  env = getenv("ENV_FROM_SCRIPT");
  if (!env || strncmp(env, "true", 4)) { 
    //  setenv("CLARISSE_BUFPOOL_SIZE", "16", 1 /*overwrite*/);
    setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
    //setenv("CLARISSE_LAZY_EAGER_VIEW", "eager", 1 /*overwrite*/);
    env = getenv("CLARISSE_COLLECTIVE");
    if (!env || strncmp(env, "listio", 6)) { 
      setenv("CLARISSE_BUFFER_SIZE", "16", 1 /*overwrite*/);
      //setenv("CLARISSE_NET_BUF_SIZE", "3", 1 /*overwrite*/);
      setenv("CLARISSE_NET_BUF_SIZE", "16", 1 /*overwrite*/);
     }
    else {
      setenv("CLARISSE_BUFFER_SIZE", "16", 1 /*overwrite*/);
      setenv("CLARISSE_NET_BUF_SIZE", "12", 1 /*overwrite*/);
    }
  }
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (argc < 3) {
    if (myrank == 0) {
      fprintf(stderr, "Missing argument, call %s filename collective/independent\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (strcmp(argv[2], "collective") && strcmp(argv[2], "independent")){
    if (myrank == 0) {
      fprintf(stderr, "Wrong collective/independent argument argument, call %s filename collective/independent\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (!strcmp(argv[2], "collective"))
    coll_indep = COLLECTIVE_IO;
  if (!strcmp(argv[2], "independent"))
    coll_indep = INDEPENDENT_IO;
  sprintf(filename_vb, "%s_vb", argv[1]);
  sprintf(filename_tp, "%s_tp", argv[1]);


  if (cls_is_client()) {
    int i;
    MPI_Comm intracomm = cls_get_client_intracomm();
    
    MPI_Comm_rank(intracomm, &myrank);
    MPI_Comm_size(intracomm, &nprocs);

    for (i=0;i<1;i++) {
      
      setview_test(MPI_BYTE, 0, 1, ADD_RANK_TO_DISPL);
      
      if (cls_get_nr_active_servers() > 1) {
	//cls_print_servers();
	cls_log_server_down(0);

	//cls_log_server_down(2);
	//cls_log_server_down(1);
	//cls_log_server_up(2);
	//cls_log_server_up(0);
	//cls_log_server_up(1);
      }
      
      
      setview_test(MPI_BYTE, 10, 1, ADD_RANK_TO_DISPL); 
      setview_test(get_vec_nstride(intracomm), 0, 2, ADD_RANK_TO_DISPL);
      
      setview_test(get_vec_nstride(intracomm), 9, 2, ADD_RANK_TO_DISPL);
      
      if (nprocs == 4) setview_test(get_darray(intracomm), 0, 64/nprocs, DO_NOT_ADD_RANK_TO_DISPL);
      
    }
  }
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
  return 0; 
}
