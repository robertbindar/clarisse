#include <assert.h>
#include "target.h"
#include "server_iocontext.h"
#include "client_iocontext.h"
#include "error.h"
#include "util.h"
#include "tasks.h" 
#include "marshal.h"
#include "hdr.h"

extern client_global_state_t client_global_state;
extern server_global_state_t server_global_state;
extern int i_am_client;
extern int i_am_server;

// djb2 hashing Mckenzie et al. Selecting a Hashing Algorithm, SP&E 20(2):209-224, Feb 1990
// truncated to int
int clarisse_hash(unsigned char *str)
{
  unsigned long hash = 5381;
  int c;
  
  while ((c = *str++))
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  
  return (int) hash;
}


int clarisse_am_i_coupled_server(client_iocontext_t *c_ctxt) {
  if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED) {
    int_map_t *m;
    int myrank;
    MPI_Comm_rank(c_ctxt->cli_serv_comm, &myrank);
    HASH_FIND_INT(client_global_state.ph2log, &myrank, m);
    if (m)
      if (client_global_state.log2ph[m->log_ios_rank] != CLS_SERVER_INACTIVE)
	return 1;
      else
	return 0;
    else
      return 0;
  }
  else
    return 0;
}

int clarisse_am_i_server() {
  int myrank;
  int_map_t *m;

  if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED) 
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
  else
    // dynamic processes, needs to be worked out
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  HASH_FIND_INT(client_global_state.ph2log, &myrank, m);
  if (m)
    if (client_global_state.log2ph[m->log_ios_rank] != CLS_SERVER_INACTIVE)
      return 1;
    else
      return 0;
  else
    return 0;
}


MPI_Datatype get_rr_datatype_rank_i(int b, int nr_servers, int log_ios_rank){
  int block_lengths[3] = {1, b, 1}; 
  MPI_Aint displacements[3] = {0, log_ios_rank * b, nr_servers * b};
  MPI_Datatype t, typelist[3] = {MPI_LB, MPI_BYTE, MPI_UB};
  MPI_Type_struct(3, block_lengths, displacements, typelist, &t);
  return t;
}

// Waitall

int clarisse_isend(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request){
  if ((i_am_client && (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED)) || (i_am_server && (server_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED))) 
    return MPI_Isend(buf, count, datatype, dest, tag, comm, request);
  else
    return MPI_Issend(buf, count, datatype, dest, tag, comm, request);
}




void off_len_pack_llint(struct net_dt *net_dt,struct dllist *off_len_block_list) { 
  struct dllist_link *aux;
#ifdef CLARISSE_TASK_DEBUG  
    printf("C: off_len_pack:" );
#endif
  for(aux = off_len_block_list->head; aux != NULL; aux = aux->next) {
    off_len_block_llint_node_t *o = DLLIST_ELEMENT(aux, off_len_block_llint_node_t, link);
    net_dt_update(net_dt, (char *)o->offs, o->metadata_size / 2);
#ifdef CLARISSE_TASK_DEBUG  
    int i;
    for(i = 0; i < o->metadata_size / 2 / (int) sizeof(clarisse_off); i++)
      printf("off[%d]=%lld ", i, o->offs[i]);
#endif 
  }
  for(aux = off_len_block_list->head; aux != NULL; aux = aux->next) {
    off_len_block_llint_node_t *o = DLLIST_ELEMENT(aux, off_len_block_llint_node_t, link);
    net_dt_update(net_dt, (char *)o->lens, o->metadata_size / 2);
#ifdef CLARISSE_TASK_DEBUG  
    int i;
    for(i = 0; i < o->metadata_size / 2 / (int) sizeof(clarisse_off); i++)
      printf("lens[%d]=%lld ", i, o->lens[i]);
#endif 
  }
#ifdef CLARISSE_TASK_DEBUG  
    printf("\n" );
#endif  
} 


void off_len_pack(struct net_dt *net_dt,struct dllist *off_len_block_list) { 
  int first = 1;
  struct dllist_link *aux;
#ifdef CLARISSE_TASK_DEBUG  
    printf("C: off_len_pack:" );
#endif
  for(aux = off_len_block_list->head; aux != NULL; aux = aux->next) {
    off_len_block_node_t *o = DLLIST_ELEMENT(aux, off_len_block_node_t, link);
    clarisse_off first_initial_off;
    net_dt_update(net_dt, (char *)o->offs, o->metadata_size / 2);
    if (first) {
      first = 0;
      first_initial_off = o->initial_off;
    }
    else {
      int i;
      for(i = 0; i < o->metadata_size / 2 / (int) sizeof(int); i++)
	o->offs[i] = o->offs[i] + (int)(o->initial_off - first_initial_off);
    }
      
#ifdef CLARISSE_TASK_DEBUG  
    int i;
    for(i = 0; i < o->metadata_size / 2 / (int) sizeof(int); i++)
      printf("off[%d]=%d ", i, o->offs[i]);
#endif 
  }
  for(aux = off_len_block_list->head; aux != NULL; aux = aux->next) {
    off_len_block_node_t *o = DLLIST_ELEMENT(aux, off_len_block_node_t, link);
    net_dt_update(net_dt, (char *)o->lens, o->metadata_size / 2);
#ifdef CLARISSE_TASK_DEBUG  
    int i;
    for(i = 0; i < o->metadata_size / 2 / (int) sizeof(int); i++)
      printf("lens[%d]=%d ", i, o->lens[i]);
#endif 
  }
#ifdef CLARISSE_TASK_DEBUG  
    printf("\n" );
#endif  
} 


void save_mpi_rq(mpi_rq_map_t **mpi_rq_map, MPI_Request *mpi_rq, MPI_Datatype mpi_datatype, char *data_rq, struct dllist *off_len_block_list) {
  mpi_rq_map_t *mrm;
  
  mrm = (mpi_rq_map_t *) clarisse_malloc(sizeof(mpi_rq_map_t));
  mrm->off_len_block_list = off_len_block_list;
  mrm->mpi_rq = mpi_rq;
  mrm->mpi_datatype = mpi_datatype;
  mrm->data_rq = data_rq;
  HASH_ADD_PTR(*mpi_rq_map, mpi_rq, mrm);

}

void free_mpi_rq(mpi_rq_map_t **mpi_rq_map) {
  mpi_rq_map_t *m, *tmp;
  HASH_ITER(hh, *mpi_rq_map, m, tmp){
    HASH_DEL(*mpi_rq_map, m);
    if (m->off_len_block_list) {
      while (!dllist_is_empty(m->off_len_block_list)) {
	off_len_block_node_t *ol = (off_len_block_node_t *)dllist_rem_head(m->off_len_block_list);
	clarisse_free(ol->offs);
	clarisse_free(ol->lens);
	clarisse_free(ol);
      }
      clarisse_free(m->off_len_block_list);
    }
    free_datatype(m->mpi_datatype);
    clarisse_free(m->data_rq);
    clarisse_free(m);
  }
}



int client_pack_view_wr(client_iocontext_t *c_ctxt, struct rq_file_write *rq){
  int cnt;
  int log_ios_rank; 
  iotask_p iot;

  iot = (iotask_p) request_queues_peek_crt(&c_ctxt->task_queues); 
  //iot = (iotask_p) request_queues_peek_first(&c_ctxt->task_queues, rq->log_ios_rank);
  log_ios_rank = iot->log_ios_rank;
  //assert(log_ios_rank == rq->log_ios_rank);
  cnt = 0;
  if (c_ctxt->view_send[log_ios_rank] == 0) { 
    c_ctxt->view_send[log_ios_rank] = 1;
    rq->first_msg = iot->first;
    if (c_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) {
      rq->target_name_size = strlen(c_ctxt->filename) + 1;
      assert(rq->target_name_size < CLARISSE_MAX_TARGET_NAME_SIZE);
      strcpy(((char *)(rq + 1)) + cnt, c_ctxt->filename);
      cnt += rq->target_name_size;
    }
    else
      rq->target_name_size = 0;
    if (c_ctxt->clarisse_pars.collective == CLARISSE_VB_COLLECTIVE) {
      if (c_ctxt->clarisse_pars.lazy_eager_view == CLARISSE_LAZY_VIEW){
	char *buf = ((char*)(rq + 1) + cnt);
	rq->view_size = decode_pack_dt_out_buf(c_ctxt->filetype, &buf);
	if (rq->view_size > CLARISSE_MAX_VIEW_SIZE)
	  handle_error(MPI_ERR_OTHER,"WRITE LAZY VIEW: View size larger than allowed\n");
	rq->displ = c_ctxt->disp;
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
	save_type_in_file("VIEW", buf, rq->view_size, rq->displ);
#endif
	cnt += rq->view_size;
      }
      else
	rq->view_size = 0;
    }
    else
      rq->view_size = 0; 
  } 
  else {
    rq->view_size = 0;
    rq->first_msg = 0;
    rq->target_name_size = 0;
  }
  return cnt;
}

int client_pack_view_rd(client_iocontext_t *c_ctxt, struct rq_file_read *rq, int log_ios_rank){
  int cnt;
  iotask_p iot;

  //iot = (iotask_p) request_queues_peek_crt(&c_ctxt->task_queues); 
  iot = (iotask_p) request_queues_peek_first(&c_ctxt->task_queues, log_ios_rank);
  cnt = 0;
  if (c_ctxt->view_send[log_ios_rank] == 0) { 
    c_ctxt->view_send[log_ios_rank] = 1;
    rq->first_msg = iot->first;
    if (c_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) {
      rq->target_name_size = strlen(c_ctxt->filename) + 1;
      assert(rq->target_name_size < CLARISSE_MAX_TARGET_NAME_SIZE);
      strcpy(((char *)(rq + 1)) + cnt, c_ctxt->filename);
      cnt += rq->target_name_size;
    }
    else
      rq->target_name_size = 0;
    if (c_ctxt->clarisse_pars.collective == CLARISSE_VB_COLLECTIVE) {
      if (c_ctxt->clarisse_pars.lazy_eager_view == CLARISSE_LAZY_VIEW){
	char *buf = ((char*)(rq + 1) + cnt);
	rq->view_size = decode_pack_dt_out_buf(c_ctxt->filetype, &buf);
	if (rq->view_size > CLARISSE_MAX_VIEW_SIZE)
	  handle_error(MPI_ERR_OTHER,"WRITE LAZY VIEW: View size larger than allowed\n");
	rq->displ = c_ctxt->disp;
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
	save_type_in_file("VIEW", buf, rq->view_size, rq->displ);
#endif
	cnt += rq->view_size;
      }
      else
	rq->view_size = 0;
    }
    else
      rq->view_size = 0;
  } 
  else {
    rq->view_size = 0;
    rq->first_msg = 0;
    rq->target_name_size = 0;
  }
  return cnt;
}


int  client_pack_data_listio_llint(client_iocontext_t *c_ctxt, struct rq_file_write *rq, struct dllist **off_len_block_list) {
  int total_cnt, data_cnt, metadata_cnt, log_ios_rank;
  iotask_p iot;

  total_cnt = 0; // Added for sending max c_ctxt->clarisse_pars.net_buf_size  
  data_cnt = 0;
  metadata_cnt = 0;
  iot = (iotask_p) request_queues_peek_crt(&c_ctxt->task_queues); 
  log_ios_rank = iot->log_ios_rank; 
  rq->l_f = iot->l_f;
  rq->view_size = 0;
  assert(total_cnt + 2 * (int) sizeof(clarisse_off) < c_ctxt->clarisse_pars.net_buf_data_size);
  
  *off_len_block_list = (struct dllist *) clarisse_malloc(sizeof(struct dllist));
  dllist_init(*off_len_block_list);

  // while I can send at least one offset-len list + 1 byte
  while (iot && (total_cnt + 2 * (int) sizeof(clarisse_off) < c_ctxt->clarisse_pars.net_buf_data_size)) {
    long long int count, r_dt;
    long long int *offs, *lens;
    clarisse_off l_f;
    int cnt_blocks, max_size, metadata_size, to_transfer;
    off_len_block_llint_node_t *olbn;

    olbn = (off_len_block_llint_node_t *)clarisse_malloc(sizeof(off_len_block_llint_node_t));
    l_f = iot->l_f;
    max_size = c_ctxt->clarisse_pars.net_buf_data_size - total_cnt;
    cnt_blocks = get_offs_lens2(&iot->l_f, iot->r_f, c_ctxt->intermediary_target->distr, &offs, &lens, max_size, &to_transfer,&metadata_size);
    assert(cnt_blocks > 0);
    olbn->offs = offs;
    olbn->lens = lens;
    olbn->metadata_size = metadata_size;
    dllist_iat(*off_len_block_list, &olbn->link);
    metadata_cnt += metadata_size;
    total_cnt += (to_transfer + metadata_size);
    data_cnt += to_transfer;

    if (to_transfer == iot->count) {
      r_dt = iot->r_dt;
      rq->r_f = iot->r_f;  
    }
    else {
      r_dt =  func_1(func_sup(iot->l_dt, iot->buf_distr) + to_transfer - 1,
		     iot->buf_distr);
      rq->r_f = func_1(func_sup(l_f, c_ctxt->intermediary_target->distr) + to_transfer - 1,
		       c_ctxt->intermediary_target->distr);
    }
    count = sg_mem_nocopy(&c_ctxt->net_dt, iot->buf, iot->l_dt, r_dt, iot->buf_distr, GATHER_NOCOPY);
    assert(count == to_transfer);
    iot->count -= count;

    if (iot->count == 0) {
      iot =  (iotask_p)request_queues_rm_queue(&c_ctxt->task_queues, log_ios_rank);
      clarisse_free(iot);
      iot = (iotask_p)request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank);
    }
    else {
      iot->l_dt = r_dt + 1;
      assert(iot->l_dt <= iot->r_dt);
      assert(l_f <= iot->r_f);
    }
  }
  rq->view_size = metadata_cnt;
  rq->count = data_cnt;
  if (request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank))
    rq->last_msg = 0;
  else
    rq->last_msg = 1;

  assert(metadata_cnt + data_cnt == total_cnt);
  //  return total_cnt;
  return data_cnt;
}


int  client_pack_data_listio(client_iocontext_t *c_ctxt, struct rq_file_write *rq, struct dllist **off_len_block_list) {
  int total_cnt, data_cnt, metadata_cnt, log_ios_rank;
  iotask_p iot;
  short first = 1;

  total_cnt = 0; // Added for sending max c_ctxt->clarisse_pars.net_buf_size  
  data_cnt = 0;
  metadata_cnt = 0;
  iot = (iotask_p) request_queues_peek_crt(&c_ctxt->task_queues); 
  log_ios_rank = iot->log_ios_rank; 
  //rq->l_f = iot->l_f;
  rq->view_size = 0;
  /*if (c_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) {
      rq->target_name_size = strlen(c_ctxt->filename) + 1;
      assert(rq->target_name_size < CLARISSE_MAX_TARGET_NAME_SIZE);
      strcpy(((char *)(rq + 1)), c_ctxt->filename);
      //cnt += rq->target_name_size;
  }
  else
    rq->target_name_size = 0;
  */
  assert(total_cnt + 2 * (int) sizeof(int) < c_ctxt->clarisse_pars.net_buf_data_size);
  
  *off_len_block_list = (struct dllist *) clarisse_malloc(sizeof(struct dllist));
  dllist_init(*off_len_block_list);

  // while I can send at least one offset-len list + 1 byte
  while (iot && (total_cnt + 2 * (int) sizeof(int) < c_ctxt->clarisse_pars.net_buf_data_size)) {
    long long int count, r_dt;
    clarisse_off l_f;
    int cnt_blocks, max_size, to_transfer;
    off_len_block_node_t *olbn;

    olbn = (off_len_block_node_t *)clarisse_malloc(sizeof(off_len_block_node_t));
    l_f = iot->l_f;
    max_size = c_ctxt->clarisse_pars.net_buf_data_size - total_cnt;
    cnt_blocks = get_offs_lens5(&iot->l_f, iot->r_f, c_ctxt->intermediary_target->distr, &olbn->initial_off, &olbn->offs, &olbn->lens, max_size, &to_transfer, &olbn->metadata_size);
    if (first) {
      rq->l_f = olbn->initial_off;
      first = 0;
    }
    assert(cnt_blocks > 0);
    dllist_iat(*off_len_block_list, &olbn->link);
    metadata_cnt += olbn->metadata_size;
    total_cnt += (to_transfer + olbn->metadata_size);
    data_cnt += to_transfer;

    if (to_transfer == iot->count) {
      r_dt = iot->r_dt;
      rq->r_f = iot->r_f;  
    }
    else {
      r_dt =  func_1(func_sup(iot->l_dt, iot->buf_distr) + to_transfer - 1,
		     iot->buf_distr);
      rq->r_f = func_1(func_sup(l_f, c_ctxt->intermediary_target->distr) + to_transfer - 1,
		       c_ctxt->intermediary_target->distr);
    }
    count = sg_mem_nocopy(&c_ctxt->net_dt, iot->buf, iot->l_dt, r_dt, iot->buf_distr, GATHER_NOCOPY);
    assert(count == to_transfer);
    iot->count -= count;

    if (iot->count == 0) {
      iot =  (iotask_p)request_queues_rm_queue(&c_ctxt->task_queues, log_ios_rank);
      clarisse_free(iot);
      iot = (iotask_p)request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank);
    }
    else {
      iot->l_dt = r_dt + 1;
      assert(iot->l_dt <= iot->r_dt);
      assert(l_f <= iot->r_f);
    }
  } // while
  // l_f is the first offset
  rq->view_size = metadata_cnt;
  rq->count = data_cnt;
  if (request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank))
    rq->last_msg = 0;
  else
    rq->last_msg = 1;

  assert(metadata_cnt + data_cnt == total_cnt);
  //  return total_cnt;
  return data_cnt;
}


// only in read: packs off-len lists s.t. the maximum data AND max off-len list 
// is less than net buf size
int  client_pack_metadata_listio_llint(client_iocontext_t *c_ctxt, int log_ios_rank, struct rq_file_read *rq, struct dllist **off_len_block_list) {
  int total_cnt, max_size, total_to_receive;
  iotask * crt_iot;

  total_cnt = 0; // Added for sending max c_ctxt->clarisse_pars.net_buf_size 
  total_to_receive = 0;
  rq->view_size = 0;
  assert(total_cnt + 2 * (int) sizeof(clarisse_off) <= c_ctxt->clarisse_pars.net_buf_data_size);
  
  *off_len_block_list = (struct dllist *) clarisse_malloc(sizeof(struct dllist));
  dllist_init(*off_len_block_list);

  // while I can send at least one offset-len list
  max_size = c_ctxt->clarisse_pars.net_buf_data_size;
  crt_iot = (iotask *)request_queues_peek_crt_queue(&c_ctxt->task_queues, log_ios_rank);
  assert((crt_iot->l_f <= crt_iot->crt_l_f) && (crt_iot->crt_l_f <= crt_iot->r_f));
  while ((crt_iot) && 
	 (total_cnt + 2 * (int) sizeof(clarisse_off) <= c_ctxt->clarisse_pars.net_buf_data_size) && 
	 (max_size > 0)) {
    long long int *offs, *lens;
    int cnt_blocks, metadata_size, to_receive;
    off_len_block_llint_node_t *olbn;

    olbn = (off_len_block_llint_node_t *)clarisse_malloc(sizeof(off_len_block_llint_node_t));
    
    cnt_blocks = get_offs_lens4(&crt_iot->crt_l_f, crt_iot->r_f, c_ctxt->intermediary_target->distr, &offs, &lens, max_size, &to_receive,&metadata_size);
    assert(cnt_blocks > 0);
    olbn->offs = offs;
    olbn->lens = lens;
    olbn->metadata_size = metadata_size;
    dllist_iat(*off_len_block_list, &olbn->link);
    total_cnt += metadata_size;
    total_to_receive += to_receive;
    if (crt_iot->crt_l_f > crt_iot->r_f) {
      request_queues_enum(&c_ctxt->task_queues, log_ios_rank);
      crt_iot = (iotask *)request_queues_peek_crt_queue(&c_ctxt->task_queues, log_ios_rank);
      assert((crt_iot->l_f <= crt_iot->crt_l_f) && (crt_iot->crt_l_f <= crt_iot->r_f));
    }
    max_size = MIN(c_ctxt->clarisse_pars.net_buf_data_size - total_cnt,
		   max_size - to_receive);
  } // while
  if (crt_iot) 
    rq->last_msg = 0;
  else 
    rq->last_msg = 1;
  rq->view_size = total_cnt;
  rq->count = total_to_receive;

  return total_cnt;
}



// only in read: packs off-len lists s.t. the maximum data AND max off-len list 
// is less than net buf size
int  client_pack_metadata_listio(client_iocontext_t *c_ctxt, int log_ios_rank, struct rq_file_read *rq, struct dllist **off_len_block_list) {
  int total_cnt, max_size, total_to_receive, first;
  iotask * crt_iot;

  total_cnt = 0; // Added for sending max c_ctxt->clarisse_pars.net_buf_size 
  total_to_receive = 0;
  rq->view_size = 0;
  assert(total_cnt + 2 * (int) sizeof(int) <= c_ctxt->clarisse_pars.net_buf_data_size);
  
  *off_len_block_list = (struct dllist *) clarisse_malloc(sizeof(struct dllist));
  dllist_init(*off_len_block_list);

  // while I can send at least one offset-len list
  first = 1;
  max_size = c_ctxt->clarisse_pars.net_buf_data_size;
  crt_iot = (iotask *)request_queues_peek_crt_queue(&c_ctxt->task_queues, log_ios_rank);
  assert((crt_iot->l_f <= crt_iot->crt_l_f) && (crt_iot->crt_l_f <= crt_iot->r_f));
  while ( (crt_iot) && 
	  (total_cnt + 2 * (int) sizeof(int) <= c_ctxt->clarisse_pars.net_buf_data_size) &&  
	 //(max_size > 0)) {
	 (max_size >= (int) (2 *sizeof(int)))) {
    int cnt_blocks, to_receive;
    off_len_block_node_t *olbn;

    olbn = (off_len_block_node_t *)clarisse_malloc(sizeof(off_len_block_node_t));    
    cnt_blocks = get_offs_lens6(&crt_iot->crt_l_f, crt_iot->r_f, c_ctxt->intermediary_target->distr, &olbn->initial_off, &olbn->offs, &olbn->lens, max_size, &to_receive,&olbn->metadata_size);
    if (first) {
      first = 0;
      rq->l_f = olbn->initial_off;
    }
    assert(cnt_blocks > 0);
    dllist_iat(*off_len_block_list, &olbn->link);
    total_cnt += olbn->metadata_size;
    total_to_receive += to_receive;
    if (crt_iot->crt_l_f > crt_iot->r_f) {
      request_queues_enum(&c_ctxt->task_queues, log_ios_rank);
      crt_iot = (iotask *)request_queues_peek_crt_queue(&c_ctxt->task_queues, log_ios_rank);
      assert((!crt_iot) || ((crt_iot) &&(crt_iot->l_f <= crt_iot->crt_l_f) && (crt_iot->crt_l_f <= crt_iot->r_f)));
    }
    max_size = MIN(c_ctxt->clarisse_pars.net_buf_data_size - total_cnt,
		   max_size - to_receive);
  } // while
  if (crt_iot) 
    rq->last_msg = 0;
  else 
    rq->last_msg = 1;
  rq->view_size = total_cnt;
  rq->count = total_to_receive;

  return total_cnt;
}

/*
// only in read: packs off-len lists s.t. the maximum data AND max off-len list 
// is less than net buf size
int  client_pack_metadata_listio_llint(client_iocontext_t *c_ctxt, struct rq_file_read *rq, struct dllist **off_len_block_list) {
  int total_cnt, max_size, total_to_receive;

  assert (((c_ctxt->crt_l_f >= ((iotask_p)c_ctxt->crt_iot)->l_f) && 
	   (c_ctxt->crt_l_f <= ((iotask_p)c_ctxt->crt_iot)->r_f)));
  total_cnt = 0; // Added for sending max c_ctxt->clarisse_pars.net_buf_size 
  total_to_receive = 0;
  rq->l_f = c_ctxt->crt_l_f;
  rq->view_size = 0;
  assert(total_cnt + 2 * (int) sizeof(clarisse_off) <= c_ctxt->clarisse_pars.net_buf_data_size);
  
  *off_len_block_list = (struct dllist *) clarisse_malloc(sizeof(struct dllist));
  dllist_init(*off_len_block_list);

  // while I can send at least one offset-len list
  max_size = c_ctxt->clarisse_pars.net_buf_data_size;
  while ((c_ctxt->crt_iot) && 
	 (total_cnt + 2 * (int) sizeof(clarisse_off) <= c_ctxt->clarisse_pars.net_buf_data_size) && 
	 (max_size > 0)) {
    long long int *offs, *lens;
    int cnt_blocks, metadata_size, to_receive;
    off_len_block_llint_node_t *olbn;

    olbn = (off_len_block_llint_node_t *)clarisse_malloc(sizeof(off_len_block_llint_node_t));
    
    cnt_blocks = get_offs_lens4(&c_ctxt->crt_l_f, ((iotask_p)c_ctxt->crt_iot)->r_f, c_ctxt->intermediary_target->distr, &offs, &lens, max_size, &to_receive,&metadata_size);
    assert(cnt_blocks > 0);
    olbn->offs = offs;
    olbn->lens = lens;
    olbn->metadata_size = metadata_size;
    dllist_iat(*off_len_block_list, &olbn->link);
    total_cnt += metadata_size;
    total_to_receive += to_receive;
    if (c_ctxt->crt_l_f > ((iotask_p)c_ctxt->crt_iot)->r_f) {
      c_ctxt->crt_iot = request_queues_enum(&c_ctxt->task_queues);
      if (c_ctxt->crt_iot)
	c_ctxt->crt_l_f = ((iotask_p)c_ctxt->crt_iot)->l_f;
    }
    max_size = MIN(c_ctxt->clarisse_pars.net_buf_data_size - total_cnt,
		   max_size - to_receive);
  } // while
  if (c_ctxt->crt_iot) 
    rq->last_msg = 0;
  else 
    rq->last_msg = 1;
  rq->view_size = total_cnt;
  rq->count = total_to_receive;

  return total_cnt;
}

// only in read: packs off-len lists s.t. the maximum data AND max off-len list 
// is less than net buf size
int  client_pack_metadata_listio(client_iocontext_t *c_ctxt, struct rq_file_read *rq, struct dllist **off_len_block_list) {
  int total_cnt, max_size, total_to_receive;
  int first;

  assert (((c_ctxt->crt_l_f >= ((iotask_p)c_ctxt->crt_iot)->l_f) && 
	   (c_ctxt->crt_l_f <= ((iotask_p)c_ctxt->crt_iot)->r_f)));
  total_cnt = 0; // Added for sending max c_ctxt->clarisse_pars.net_buf_size 
  total_to_receive = 0;
  //rq->l_f = c_ctxt->crt_l_f;
  rq->view_size = 0;
  assert(total_cnt + 2 * (int) sizeof(int) <= c_ctxt->clarisse_pars.net_buf_data_size);
  
  *off_len_block_list = (struct dllist *) clarisse_malloc(sizeof(struct dllist));
  dllist_init(*off_len_block_list);

  // while I can send at least one offset-len list
  first = 1;
  max_size = c_ctxt->clarisse_pars.net_buf_data_size;
  while ((c_ctxt->crt_iot) && 
	 (total_cnt + 2 * (int) sizeof(int) <= c_ctxt->clarisse_pars.net_buf_data_size) &&  
	 //(max_size > 0)) {
	 (max_size >= (int) (2 *sizeof(int)))) {
    int cnt_blocks, to_receive;
    off_len_block_node_t *olbn;

    olbn = (off_len_block_node_t *)clarisse_malloc(sizeof(off_len_block_node_t));    
    cnt_blocks = get_offs_lens6(&c_ctxt->crt_l_f, ((iotask_p)c_ctxt->crt_iot)->r_f, c_ctxt->intermediary_target->distr, &olbn->initial_off, &olbn->offs, &olbn->lens, max_size, &to_receive,&olbn->metadata_size);
    if (first) {
      first = 0;
      rq->l_f = olbn->initial_off;
    }
    assert(cnt_blocks > 0);
    dllist_iat(*off_len_block_list, &olbn->link);
    total_cnt += olbn->metadata_size;
    total_to_receive += to_receive;
    if (c_ctxt->crt_l_f > ((iotask_p)c_ctxt->crt_iot)->r_f) {
      c_ctxt->crt_iot = request_queues_enum(&c_ctxt->task_queues);
      if (c_ctxt->crt_iot)
	c_ctxt->crt_l_f = ((iotask_p)c_ctxt->crt_iot)->l_f;
    }
    max_size = MIN(c_ctxt->clarisse_pars.net_buf_data_size - total_cnt,
		   max_size - to_receive);
  } // while
  if (c_ctxt->crt_iot) 
    rq->last_msg = 0;
  else 
    rq->last_msg = 1;
  rq->view_size = total_cnt;
  rq->count = total_to_receive;

  return total_cnt;
}
*/
