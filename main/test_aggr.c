#include <stdio.h>
#include "aggr.h"
#include "util.h"

void test_bgq_include_bridge() {
  int bg_nodes_pset;
  //int *aggr_tab;	
  int *aggr_ranks;

  for (bg_nodes_pset = 1; bg_nodes_pset <= 1; bg_nodes_pset *= 4) {  
    int flag_iamserver;
    int nr_aggrs = get_aggregators(PLATFORM_BGQ_INCLUDE_BRIDGE, bg_nodes_pset, -1, &aggr_ranks, &flag_iamserver); 
    aggr_print(nr_aggrs, aggr_ranks);
    /*if (am_i_aggregator(naggr))
      printf("Aggregator %d for bg_node_pset=%d naggr=%d\n", r, bg_nodes_pset, naggr); */
    clarisse_free(aggr_ranks);
  }
}


void test_bgq_exclude_bridge() {
  int bg_nodes_pset;
  //int *aggr_tab;      
  int *aggr_ranks;
  int step, myrank;
  int flag_iamserver;

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);	
  for (bg_nodes_pset = 16; bg_nodes_pset <= 16; bg_nodes_pset *= 4) {
     for (step = 1; step <= 8; step *= 2) {	
	if (myrank == 0) 
		printf("%s: bg_nodes_pset=%d step =%d\n****************\n", __FUNCTION__, bg_nodes_pset, step);
        //int nr_aggrs = 
        get_aggregators(PLATFORM_BGQ_EXCLUDE_BRIDGE, bg_nodes_pset, step, &aggr_ranks, &flag_iamserver);
         //aggr_print(nr_aggrs, aggr_ranks);
        clarisse_free(aggr_ranks);
     }
  }
}   

void test_any0(){
  int i, nprocs;
  int *aggr_ranks;
  int flag_iamserver;

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  for (i = 1; i <= MIN(3, nprocs); i++) {  
    int nr_aggrs = get_aggregators(PLATFORM_ANY_UNIFORM, i, -1, &aggr_ranks, &flag_iamserver); 
    aggr_print(nr_aggrs, aggr_ranks);
    clarisse_free(aggr_ranks);
  }
}

int main(int argc, char *argv[]){
  

  PMPI_Init(&argc, &argv);
  
  //test_any0();
  //test_bgq_include_bridge();
  test_bgq_exclude_bridge();
  PMPI_Finalize();
  return 0;
}

