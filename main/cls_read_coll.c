#include <assert.h>
#include "buffer.h"
#include "map.h"
#include "client_iocontext.h"
#include "server_iocontext.h"
#include "buffer_pool.h"
#include "tasks.h"
#include "list.h"
#include "hdr.h"
#include "error.h"
#include "marshal.h"
#include "buffer.h"
#include "net_dt.h"
#include "util.h"
#include "clarisse_internal.h"

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern server_global_state_t server_global_state;
extern client_global_state_t client_global_state;

static int read_file_partitioning1(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v);
static int read_file_partitioning2(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v);
static void client_sendreq_read_round(client_iocontext_t *c_ctxt);
static void client_sendreq_read_round_listio(client_iocontext_t *c_ctxt);
int  client_recvret_read_round(client_iocontext_t *c_ctxt);
int client_task_to_read_rq(client_iocontext_t *c_ctxt, struct rq_file_read *rq, int log_ios_rank, struct dllist **off_len_block_list);
int client_pack_view_rd(client_iocontext_t *c_ctxt, struct rq_file_read *rq, int log_ios_rank);
int  client_pack_metadata_listio(client_iocontext_t *c_ctxt, int log_ios_rank, struct rq_file_read *rq, struct dllist **off_len_block_list);
static int serv_recv_serve_read_rq(client_iocontext_t *c_ctxt);
int serv_read_n_shares(int client_idx);
void serv_read_task(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_read_task_listio(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
static int serv_file_read(server_iocontext_t * s_ctxt, clarisse_off l, clarisse_off r, int client_idx, int source);


int MPI_File_read_at_all(MPI_File fh, MPI_Offset offset, void * buf, int count,
                         MPI_Datatype datatype, MPI_Status *status) {
  int ret;

#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_read_all[local_timing.cnt_read_all][0] = MPI_Wtime();
#endif   
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
  char *type_buf;
  int type_size;
  printf("READ_AT_ALL: OFFSET=%lld, COUNT=%d ", offset, count); 
  print_dt(datatype);
  type_buf = clarisse_malloc(1024);
  type_size = decode_pack_dt_out_buf(datatype, &type_buf);
  save_type_in_file("MEMORY", type_buf, type_size, 0);
  clarisse_free(type_buf);
#endif

  client_iocontext_t *c_ctxt;
  c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
  if (c_ctxt) {
    int bytes_xfered, datatype_size;
    if (c_ctxt->clarisse_pars.read_future == CLARISSE_READ_FUTURE_ENABLED)
      bytes_xfered = cls_read_coll_future(c_ctxt, (char *) buf, count,
					  datatype, CLARISSE_EXPLICIT_OFFSET, offset);
    else
      bytes_xfered = cls_read_coll(c_ctxt, (char *) buf, count,
				   datatype, CLARISSE_EXPLICIT_OFFSET, offset);
    
    MPI_Type_size(datatype, &datatype_size);
    MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
    ret = MPI_SUCCESS; 
  }
  else
    ret = PMPI_File_read_at_all(fh, offset, buf, count, datatype, status); 
#ifdef CLARISSE_TIMING
    if (local_timing.cnt_read_all < CLARISSE_MAX_CNT_TIMED_OPS) {
      int size, transfer_count;
    
      MPI_Get_count(status, datatype, &transfer_count);
      local_timing.time_read_all[local_timing.cnt_read_all][1] = MPI_Wtime();
      MPI_Type_size(datatype, &size);
      local_timing.size_read_all[local_timing.cnt_read_all] = (double) transfer_count * size;
      local_timing.cnt_read_all++;
    }
#endif  

  return ret;

}

int MPI_File_read_all(MPI_File fh, void *buf, int count, MPI_Datatype datatype, MPI_Status *status){
  int ret;

#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write_all < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_read_all[local_timing.cnt_read_all][0] = MPI_Wtime();
#endif   
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
  char *type_buf;
  int type_size;
  printf("READ_ALL: COUNT=%d ", count); 
  print_dt(datatype);
  type_buf = clarisse_malloc(1024);
  type_size = decode_pack_dt_out_buf(datatype, &type_buf);
  save_type_in_file("MEMORY", type_buf, type_size, 0);
  clarisse_free(type_buf);
#endif
  client_iocontext_t *c_ctxt;
  c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
  if (c_ctxt) {
    int bytes_xfered, datatype_size;

    if (c_ctxt->clarisse_pars.read_future == CLARISSE_READ_FUTURE_ENABLED)
      bytes_xfered = cls_read_coll_future(c_ctxt, (char *) buf, count,
					  datatype, CLARISSE_INDIVIDUAL,  c_ctxt->fp_ind);
    else
      bytes_xfered = cls_read_coll(c_ctxt, (char *) buf, count,
				 datatype, CLARISSE_INDIVIDUAL,  c_ctxt->fp_ind);
    MPI_Type_size(datatype, &datatype_size);
    MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
    ret = MPI_SUCCESS;
  }
  else
    ret = PMPI_File_read_all(fh, buf, count, datatype, status); 
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_read_all < CLARISSE_MAX_CNT_TIMED_OPS) {
    int size, transfer_count;

    MPI_Get_count(status, datatype, &transfer_count);
    local_timing.time_read_all[local_timing.cnt_read_all][1] = MPI_Wtime();
    MPI_Type_size(datatype, &size);
    local_timing.size_read_all[local_timing.cnt_read_all] = (double) transfer_count * size;
    local_timing.cnt_read_all++;
  }
#endif  

  return ret;

}

int cls_read_coll(client_iocontext_t *c_ctxt, 
		   void *buf, int count,
		   MPI_Datatype datatype, int file_ptr_type,
		   MPI_Offset offset)
{
  int bytes_xfered, buftype_size;
  clarisse_off l_v, r_v;

  c_ctxt->local_target = clarisse_target_alloc_init("", -1, CLARISSE_TARGET_MEMORY, distrib_get(datatype, 0), (void *)buf);
  MPI_Type_size(datatype, &buftype_size);
  // If not individual it can be explicit offset, use it 
  l_v = (file_ptr_type == CLARISSE_INDIVIDUAL) ? 
    func_sup(c_ctxt->fp_ind, c_ctxt->intermediary_target->distr):
    c_ctxt->etype_size * offset;
  r_v = l_v +  buftype_size * count - 1;

  if ((c_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING) || 
      (client_global_state.active_nr_servers == 1))
    bytes_xfered = read_file_partitioning1(c_ctxt, l_v, r_v);
  else
    bytes_xfered = read_file_partitioning2(c_ctxt, l_v, r_v);
  
  distrib_free(c_ctxt->local_target->distr);
  clarisse_free(c_ctxt->local_target);
  MPI_Type_size(datatype, &buftype_size);
  if (bytes_xfered != buftype_size * count)
    handle_error(MPI_ERR_OTHER, "cls_read_coll: bytes_xfered  != bufsize\n");    
  if (file_ptr_type == CLARISSE_INDIVIDUAL) 
    c_ctxt->fp_ind = func_1(r_v, c_ctxt->intermediary_target->distr) + 1; 
  
  client_global_state.coll_op_seq++;
  return bytes_xfered;
}

int cls_read_coll_future(client_iocontext_t *c_ctxt, 
			 void *buf, int count,
			 MPI_Datatype datatype, int file_ptr_type,
			 MPI_Offset offset)
{
  int bytes_xfered = 0, buftype_size, err;
  clarisse_off l_v, r_v, l_f, r_f;

  c_ctxt->local_target = clarisse_target_alloc_init("", -1, CLARISSE_TARGET_MEMORY, distrib_get(datatype, 0), (void *)buf);
  MPI_Type_size(datatype, &buftype_size);
  // If not individual it can be explicit offset, use it 
  l_v = (file_ptr_type == CLARISSE_INDIVIDUAL) ? 
    func_sup(c_ctxt->fp_ind, c_ctxt->intermediary_target->distr):
    c_ctxt->etype_size * offset;
  r_v = l_v +  buftype_size * count - 1;
  l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
  r_f = func_1(r_v, c_ctxt->intermediary_target->distr);

  // difference with round-based approach l_f and r_f
  create_io_tasks(c_ctxt, l_v, l_f, r_f, 0, 0, CLARISSE_STATIC_FILE_PARTITIONING);
  if (c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
    client_sendreq_read_round_listio(c_ctxt);
  else
    client_sendreq_read_round(c_ctxt);
  
  err = MPI_Waitall(c_ctxt->mpi_rq_pool.cnt, c_ctxt->mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
  if (err != MPI_SUCCESS)
    handle_error(err, "cls_read_coll_future: waitall error\n");
  free_mpi_rq(&c_ctxt->mpi_rq_map);   
  bytes_xfered += client_recvret_read_round(c_ctxt);

  distrib_free(c_ctxt->local_target->distr);
  clarisse_free(c_ctxt->local_target);
  MPI_Type_size(datatype, &buftype_size);
  if (bytes_xfered != buftype_size * count)
    handle_error(MPI_ERR_OTHER, "cls_read_coll_future: bytes_xfered  != bufsize\n");    
  //MPI_Barrier(c_ctxt->intracomm);
  if (file_ptr_type == CLARISSE_INDIVIDUAL) 
    c_ctxt->fp_ind = func_1(r_v, c_ctxt->intermediary_target->distr) + 1; 
  
  client_global_state.coll_op_seq++;
  return bytes_xfered;
}



static int read_file_partitioning1(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v){
  int bytes_xfered;
  clarisse_off l_f, r_f, crt_l_d, b, l_d, r_d;

  bytes_xfered = 0;
  b = (clarisse_off) c_ctxt->clarisse_pars.buffer_size;
  l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
  r_f = func_1(r_v, c_ctxt->intermediary_target->distr);
  MPI_Allreduce(&l_f, &l_d, 1, MPI_LONG_LONG_INT, MPI_MIN, c_ctxt->intracomm);
  MPI_Allreduce(&r_f, &r_d, 1, MPI_LONG_LONG_INT, MPI_MAX, c_ctxt->intracomm);
  crt_l_d = l_d;
    
  while (crt_l_d <= r_d) {
    int err;
    clarisse_off crt_r_d, srv_buf_capacity, crt_l_f, crt_r_f;
    // Assersion: (crt_r_f - crt_l_f  + 1 ) <= Maximum capacity of server buffer caches 
    srv_buf_capacity = (clarisse_off)c_ctxt->clarisse_pars.bufpool_size * b;
    crt_r_d = MIN((BEGIN_BLOCK(crt_l_d, b) + (clarisse_off) client_global_state.active_nr_servers * srv_buf_capacity - 1),
		  r_d);
    crt_l_f = MAX(l_f, crt_l_d);
    crt_r_f = MIN(r_f, crt_r_d);
    create_io_tasks(c_ctxt, //buf, c_ctxt->local_target->distr, 
		    l_v, crt_l_f, crt_r_f, 0, 0, CLARISSE_STATIC_FILE_PARTITIONING);
    if (c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
      client_sendreq_read_round_listio(c_ctxt);
    else
      client_sendreq_read_round(c_ctxt);
    /* aggregator */
    if(clarisse_am_i_coupled_server(c_ctxt))
      serv_recv_serve_read_rq(c_ctxt);
    err = MPI_Waitall(c_ctxt->mpi_rq_pool.cnt, c_ctxt->mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
    if (err != MPI_SUCCESS)
      handle_error(err, "cls_read_coll: waitall error\n");
    free_mpi_rq(&c_ctxt->mpi_rq_map);   
    bytes_xfered += client_recvret_read_round(c_ctxt);
    /* aggregator */
    if(clarisse_am_i_coupled_server(c_ctxt)){
      server_iocontext_t *s_ctxt = server_iocontext_find(c_ctxt->global_descr);
      err = MPI_Waitall(s_ctxt->client_rt_info[0].mpi_rq_pool.cnt, s_ctxt->client_rt_info[0].mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
      if (err != MPI_SUCCESS)
	handle_err(err, "cls_read_coll: SERV waitall error\n");
      free_mpi_rq(&s_ctxt->client_rt_info[0].mpi_rq_map);  
    }   
    crt_l_d = crt_r_d + 1;
    MPI_Barrier(c_ctxt->intracomm);
  }
  return bytes_xfered;
}

static int read_file_partitioning2(client_iocontext_t *c_ctxt, clarisse_off l_v, clarisse_off r_v){
  int bytes_xfered;
  clarisse_off l_f, r_f, b, part_block_len, l_d, r_d, crt_l_d, crt_l_f;

  assert(client_global_state.active_nr_servers > 1);

  bytes_xfered = 0;
  b = (clarisse_off)c_ctxt->clarisse_pars.buffer_size;
  l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
  r_f = func_1(r_v, c_ctxt->intermediary_target->distr);
  MPI_Allreduce(&l_f, &l_d, 1, MPI_LONG_LONG_INT, MPI_MIN, c_ctxt->intracomm);
  MPI_Allreduce(&r_f, &r_d, 1, MPI_LONG_LONG_INT, MPI_MAX, c_ctxt->intracomm);
  l_d = BEGIN_BLOCK(l_d, b);
  part_block_len = BEGIN_NEXT_BLOCK((r_d - l_d) / client_global_state.active_nr_servers, b);
  crt_l_f = l_f;
  crt_l_d = l_d;
  while (crt_l_d <= MIN(l_d + part_block_len - 1, r_d)){
    int err;
    if ((crt_l_f - crt_l_d) % part_block_len < b) {
      clarisse_off crt_r_f;
      /*int num_tasks, err;*/

      crt_r_f = MIN((END_BLOCK(crt_l_f, b) + ((clarisse_off) client_global_state.active_nr_servers - 1) * part_block_len),
		  r_f);
      //fprintf(stderr, "crt_l_f = %lld crt_r_f =%lld\n", crt_l_f, crt_r_f);
      //num_tasks = 
      create_io_tasks(c_ctxt, l_v, crt_l_f, crt_r_f, 
				  l_d, part_block_len,
				  CLARISSE_DYNAMIC_FILE_PARTITIONING);
      crt_l_f = BEGIN_NEXT_BLOCK(crt_l_f, b); 
    }
    if (c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
      client_sendreq_read_round_listio(c_ctxt);
    else
      client_sendreq_read_round(c_ctxt);
// aggregator 
    if(clarisse_am_i_coupled_server(c_ctxt))
      serv_recv_serve_read_rq(c_ctxt);
    err = MPI_Waitall(c_ctxt->mpi_rq_pool.cnt, c_ctxt->mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
    if (err != MPI_SUCCESS)
      handle_error(err, "cls_read_coll: CLI waitall error\n");
    free_mpi_rq(&c_ctxt->mpi_rq_map);   

    bytes_xfered += client_recvret_read_round(c_ctxt);
    if(clarisse_am_i_coupled_server(c_ctxt)) {
      server_iocontext_t *s_ctxt = server_iocontext_find(c_ctxt->global_descr);
      err = MPI_Waitall(s_ctxt->client_rt_info[0].mpi_rq_pool.cnt, s_ctxt->client_rt_info[0].mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
      if (err != MPI_SUCCESS)
         handle_err(err, "cls_read_coll: SERV waitall error\n");
      free_mpi_rq(&s_ctxt->client_rt_info[0].mpi_rq_map);  
    }
    crt_l_d = BEGIN_NEXT_BLOCK(crt_l_d, b);
    MPI_Barrier(c_ctxt->intracomm);
    //printf("New round\n");
  }
  return bytes_xfered;
}



static void client_sendreq_read_round(client_iocontext_t *c_ctxt){
  int err, ph_ios_rank, log_ios_rank, rq_type/*, myrank*/;
  char *data_rq;
  MPI_Request *mpi_rq;

  //MPI_Comm_rank(c_ctxt->intracomm, &myrank);
  //request_queues_set_first(&c_ctxt->task_queues, myrank);  

  rq_type = ((c_ctxt->clarisse_pars.read_future == CLARISSE_READ_FUTURE_ENABLED)? RQ_FILE_READ_COLL_FUTURE : RQ_FILE_READ_COLL);

  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
    ph_ios_rank = client_global_state.log2ph[log_ios_rank];
    if (ph_ios_rank != CLS_SERVER_INACTIVE) {
      if (c_ctxt->pending_ios_ret[log_ios_rank]) {
	struct rq_file_read *rq;
	int irq_max_size;
	struct dllist *off_len_block_list;
	MPI_Datatype d;

	mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
	off_len_block_list = NULL;
	irq_max_size = sizeof(struct rq_file_read) + CLARISSE_MAX_VIEW_SIZE + CLARISSE_MAX_TARGET_NAME_SIZE;
	data_rq = (char *) clarisse_malloc(irq_max_size);
	rq = (struct rq_file_read *)data_rq; 
	client_task_to_read_rq(c_ctxt, rq, log_ios_rank, &off_len_block_list);
	// mpi_rq is not used by now (if reallocation is done the pointer will be lost 
	d = net_dt_commit(&c_ctxt->net_dt);	
	save_mpi_rq(&c_ctxt->mpi_rq_map, mpi_rq, d, data_rq, off_len_block_list);
	//printf("Send request to %d\n", ph_ios_rank);
	err = MPI_Isend(data_rq, 1, d, ph_ios_rank, rq_type, c_ctxt->cli_serv_comm, mpi_rq);
	if (err != MPI_SUCCESS)
	  handle_error(err, "coll_read: send error\n");
	net_dt_reset(&c_ctxt->net_dt); // reinit the net datatype
      } 
      else {
	if (c_ctxt->clarisse_pars.coupleness == CLARISSE_COUPLED) {
	  // Sends just an int (used for termination) 
	  MPI_Request *mpi_rq;
	  
	  mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
	  err = MPI_Isend(&c_ctxt->global_descr, 1, MPI_INT, ph_ios_rank, rq_type, c_ctxt->cli_serv_comm, mpi_rq);
	  if (err != MPI_SUCCESS)
	    handle_error(err, "coll_read: send error\n");
	}
      }
    }
  }
}

/*
static void client_sendreq_read_allrounds(client_iocontext_t *c_ctxt){
  int err, ph_ios_rank, log_ios_rank;
  char *data_rq;
  MPI_Request *mpi_rq;


  while (c_ctxt->task_queues.nonempty_queues_enum) {
    nonempty_queues_t *q, *tmp;

    HASH_ITER(hh, c_ctxt->task_queues.nonempty_queues_enum, q, tmp) {
    //  while (!request_queues_empty(&c_ctxt->task_queues)) {

    //  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
    //ph_ios_rank = client_global_state.log2ph[log_ios_rank];
    //if (ph_ios_rank != CLS_SERVER_INACTIVE) {
      struct rq_file_read *rq;
      int irq_max_size;
      struct dllist *off_len_block_list;
      MPI_Datatype d;
      
      //log_ios_rank = ;
      assert(client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE);
      
      mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
      off_len_block_list = NULL;
      irq_max_size = sizeof(struct rq_file_read) + CLARISSE_MAX_VIEW_SIZE + CLARISSE_MAX_TARGET_NAME_SIZE;
      data_rq = (char *) clarisse_malloc(irq_max_size);
      rq = (struct rq_file_read *)data_rq; 
      client_task_to_read_rq(c_ctxt, rq, log_ios_rank, &off_len_block_list);
      // mpi_rq is not used by now (if reallocation is done the pointer will be lost 
      d = net_dt_commit(&c_ctxt->net_dt);	
      save_mpi_rq(&c_ctxt->mpi_rq_map, mpi_rq, d, data_rq, off_len_block_list);
      //printf("Send request to %d\n", ph_ios_rank);
      err = MPI_Isend(data_rq, 1, d, ph_ios_rank, RQ_FILE_READ_COLL, c_ctxt->cli_serv_comm, mpi_rq);
      if (err != MPI_SUCCESS)
	handle_error(err, "coll_read: send error\n");
      net_dt_reset(&c_ctxt->net_dt); // reinit the net datatype
    //} // if (ph_ios_rank != CLS_SERVER_INACTIVE)
    }
  } // while
}
*/


static void client_sendreq_read_round_listio(client_iocontext_t *c_ctxt){
  int err, ph_ios_rank, log_ios_rank, rq_type/*, myrank*/;
  char *data_rq;
  MPI_Request *mpi_rq;

  //MPI_Comm_rank(c_ctxt->intracomm, &myrank);  
  //request_queues_set_first(&c_ctxt->task_queues, myrank);  
  rq_type = ((c_ctxt->clarisse_pars.read_future == CLARISSE_READ_FUTURE_ENABLED)? RQ_FILE_READ_COLL_FUTURE : RQ_FILE_READ_COLL);

  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
    ph_ios_rank = client_global_state.log2ph[log_ios_rank];
    if (ph_ios_rank != CLS_SERVER_INACTIVE) {
      if (c_ctxt->pending_ios_ret[log_ios_rank]) {
	//iotask_p iot;
	struct rq_file_read *rq;
	int irq_max_size;
	struct dllist *off_len_block_list;
	MPI_Datatype d;
	//clarisse_off crt_l_f;
      
	irq_max_size = sizeof(struct rq_file_read) + CLARISSE_MAX_TARGET_NAME_SIZE;      
      //iot = (iotask_p)request_queues_peek_key(&c_ctxt->task_queues, log_ios_rank);
      //request_queues_init_enum(&c_ctxt->task_queues);
      //iot = (iotask_p)request_queues_enum(&c_ctxt->task_queues);
      //crt_l_f = iot->l_f;
      //while (iot) {
      
	request_queues_init_enum(&c_ctxt->task_queues, log_ios_rank);
	//c_ctxt->crt_iot = request_queues_enum(&c_ctxt->task_queues);
	//c_ctxt->crt_l_f = ((iotask_p)c_ctxt->crt_iot)->l_f;
	//while (c_ctxt->crt_iot){
	while(c_ctxt->task_queues.crt_elems[log_ios_rank]) {
	  mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
	  off_len_block_list = NULL;
	  data_rq = (char *) clarisse_malloc(irq_max_size);
	  rq = (struct rq_file_read *)data_rq; 
	  client_task_to_read_rq(c_ctxt, rq, log_ios_rank, &off_len_block_list);
	  off_len_pack(&c_ctxt->net_dt, off_len_block_list);
	  // mpi_rq is not used by now (if reallocation is done the pointer will be lost 
	  d = net_dt_commit(&c_ctxt->net_dt);	
	  save_mpi_rq(&c_ctxt->mpi_rq_map, mpi_rq, d, data_rq, off_len_block_list);
	  //printf("Send request to %d\n", ph_ios_rank);
	  err = MPI_Isend(data_rq, 1, d, ph_ios_rank, rq_type, c_ctxt->cli_serv_comm, mpi_rq);
	  if (err != MPI_SUCCESS)
	    handle_error(err, "coll_read: send error\n");
	  net_dt_reset(&c_ctxt->net_dt); // reinit the net datatype
	}
      } 
      else {
	if (c_ctxt->clarisse_pars.coupleness == CLARISSE_COUPLED) {
	  // Sends just an int (used for termination) 
	  MPI_Request *mpi_rq;
	  
	  mpi_rq = alloc_pool_get(&c_ctxt->mpi_rq_pool);
	  err = MPI_Isend(&c_ctxt->global_descr, 1, MPI_INT, ph_ios_rank, rq_type, c_ctxt->cli_serv_comm, mpi_rq);
	  if (err != MPI_SUCCESS)
	    handle_error(err, "coll_read: send error\n");
	}
      }
    }
  }
}

int client_recvret_read_round(client_iocontext_t *c_ctxt){
  int bytes_xfered = 0;
  /* Compute nodes recieve data */
  while ((c_ctxt->pending_ios) && (!request_queues_empty(&c_ctxt->task_queues))) {
    int err, received, total_cnt = 0, cnt;
    struct rt_file_read *rt;
    MPI_Status s;
    int_map_t *m;
    
    err = MPI_Recv(c_ctxt->net_buf, c_ctxt->clarisse_pars.net_buf_size, MPI_BYTE, MPI_ANY_SOURCE, RT_FILE_READ_COLL, c_ctxt->cli_serv_comm, &s);	
    if (err != MPI_SUCCESS)
      handle_err(err, "Error in receiving");
    HASH_FIND_INT(client_global_state.ph2log, &s.MPI_SOURCE, m);
    if (!m) 
      handle_err(MPI_ERR_OTHER, "Error: Not found ph2log mapping\n");
    rt = (struct rt_file_read *)c_ctxt->net_buf;
    MPI_Get_count(&s, MPI_BYTE, &cnt);
    received = cnt - sizeof(struct rt_file_read);
    //printf("CLIENT %d received rq from parent %d message=%d bytes_xfered=%d\n",myrank,  s.MPI_SOURCE, received  ,rt->bytes_xfered );
    if (rt->last_msg) {
      c_ctxt->pending_ios_ret[m->log_ios_rank] = 0;
      c_ctxt->pending_ios --;
      //my_fprintf(stderr, "Read: last message. Pending_ios=%d\n", c_ctxt->pending_ios);
    }
    while (received > 0) {
      int count; 
      iotask_p iot;
      
      iot = (iotask_p) request_queues_peek_key(&c_ctxt->task_queues, m->log_ios_rank);
      if (iot) {
	clarisse_off r_dt;
	if (received < iot->count) {
	  //if (rt->last_msg != CLARISSE_EOF)
	  //  handle_error(MPI_ERR_OTHER, "Received less than expected");
	  r_dt = func_1(func_sup(iot->l_dt, iot->buf_distr) + received - 1, iot->buf_distr); 
	}
	else
	  r_dt = iot->r_dt;
	count = sg_mem(c_ctxt->net_buf + sizeof(struct rt_file_read) + total_cnt,
		       //buf must represent the absolute position!!
		       iot->buf, iot->l_dt, r_dt, iot->buf_distr, SCATTER);
	received -= count;
	total_cnt += count;
	if (r_dt == iot->r_dt) {
	  //	  printf("Free task for ios %d\n",m->log_ios_rank );
	  request_queues_rm_queue(&c_ctxt->task_queues, m->log_ios_rank);
	  clarisse_free(iot);
	}
	else {
	  iot->l_dt = r_dt + 1;
	  iot->count -= count;
	  assert(iot->l_dt <= iot->r_dt);
	}
      }
      else {
	handle_error(MPI_ERR_OTHER, "Received more bytes than expected, not IOT");
      }
    }
    if (rt->last_msg == CLARISSE_EOF) 
      fprintf(stderr,"EOF reached, read less bytes than requested\n");
    bytes_xfered += total_cnt;
  }
  return bytes_xfered;
}

int client_task_to_read_rq(client_iocontext_t *c_ctxt, struct rq_file_read *rq, int log_ios_rank, struct dllist **off_len_block_list) {
  int nprocs, total_cnt;
  
  total_cnt = sizeof(struct rq_file_read);
  MPI_Comm_size(c_ctxt->cli_serv_comm, &nprocs);
  rq->global_descr = c_ctxt->global_descr; 
  if (c_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)
    rq->partitioning_stride_factor = client_global_state.active_nr_servers;
  else
    rq->partitioning_stride_factor = 1; 
  rq->nprocs = nprocs;
  
  if (c_ctxt->clarisse_pars.collective == CLARISSE_VB_COLLECTIVE) {
    rq->l_f = ((iotask_p)request_queues_peek_first(&c_ctxt->task_queues,
						   log_ios_rank))->l_f  ;//l_f;
    rq->r_f = ((iotask_p)request_queues_peek_last(&c_ctxt->task_queues, 
                                                  log_ios_rank))->r_f ; //r_f;
    rq->last_msg = 1;
  }
  total_cnt += client_pack_view_rd(c_ctxt, rq, log_ios_rank);
  if (total_cnt > 0)
    net_dt_update(&c_ctxt->net_dt, (char *)rq, total_cnt); 
  if (c_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE) 
    //bytes_xfered = 
    client_pack_metadata_listio(c_ctxt, log_ios_rank, rq, off_len_block_list);
  
  return 0;
}

//**********************************************
//**********************************************
// aggregator
// coupled
static int serv_recv_serve_read_rq(client_iocontext_t *c_ctxt)
{
  int arrived, nprocs;
  //  int myrank;
 
  MPI_Comm_size(c_ctxt->intracomm, &nprocs);
  //MPI_Comm_rank(fd->comm, &myrank);

  arrived = 0;
  while (arrived < nprocs) {
     int cnt, err;
     MPI_Status status;
  
     err = MPI_Recv(server_global_state.net_buf, server_global_state.clarisse_pars.net_buf_size, MPI_BYTE, MPI_ANY_SOURCE, RQ_FILE_READ_COLL, c_ctxt->cli_serv_comm, &status);
     //printf("Received request from %d\n",status.MPI_SOURCE);
     if (err != MPI_SUCCESS)
       handle_err(err, "Error in receiving");
     MPI_Get_count(&status, MPI_BYTE, &cnt);
     // TEMPORARY: next check needs to be changed
     if (cnt  > (int) sizeof(int)) {
       struct rq_file_read *rq; 
       server_iocontext_t * s_ctxt;

       rq = (struct rq_file_read *)server_global_state.net_buf; 
       s_ctxt = server_iocontext_find(rq->global_descr);
       s_ctxt->client_rt_info[0].partitioning_stride_factor = rq->partitioning_stride_factor;
       if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
	 serv_read_task_listio(s_ctxt, rq, 0 /*client_idx*/, status.MPI_SOURCE);
       else {
	 serv_read_task(s_ctxt, rq, 0 /*client_idx*/, status.MPI_SOURCE);
	 //      tasks += get_count(r->l_f, r->r_f, s_ctxt->remote_view_distr[/*ph2log_ios(c_ctxt->log2ph, client_global_state.active_nr_servers, s.MPI_SOURCE)*/ status.MPI_SOURCE]) / s_ctxt->clarisse_pars.net_buf_data_size + 1;
       }
       if (rq->last_msg)
	 arrived++;
     }
     else
       arrived++;
  
  }
  return serv_read_n_shares(0 /*client_idx*/);
}



// aggregator
void serv_read_task(server_iocontext_t * s_ctxt, struct rq_file_read  *r, int client_idx, int source) {
  if (r->first_msg && (r->view_size > 0)) {
    char *buf;
    MPI_Datatype dt;
    
    buf = (char *)(r + 1) + r->target_name_size;
    dt = reconstruct_dt(buf, r->view_size);
    distrib_update(s_ctxt->client_rt_info[client_idx].remote_view_distr[source], dt, r->displ);
#ifdef CLARISSE_VIEW_DEBUG
    int rank;
    MPI_Comm_rank(fd->comm, &rank);
    fprintf(stderr, "Proc %d (AGGREG) RECEIVED FROM %d ", rank, source);
    distrib_print(s_ctxt->client_rt_info[client_idx].remote_view_distr[source]);
#endif
    
  }
  if (r->l_f <= r->r_f) {
    iotask_p  iot = alloc_init_iotask(s_ctxt, source, r->l_f, r->r_f, client_idx);
    request_queues_insert(&server_global_state.read_task_queue, source, (void *)iot);
  }
}

void serv_read_future_task(server_iocontext_t *s_ctxt, struct rq_file_read *r, int client_idx, int source) {
  clarisse_off l_f;

  if (r->first_msg && (r->view_size > 0)) {
    char *buf;
    MPI_Datatype dt;
    
    buf = (char *)(r + 1) + r->target_name_size;
    dt = reconstruct_dt(buf, r->view_size);
    distrib_update(s_ctxt->client_rt_info[client_idx].remote_view_distr[source], dt, r->displ);
#ifdef CLARISSE_VIEW_DEBUG
    int rank;
    MPI_Comm_rank(fd->comm, &rank);
    fprintf(stderr, "Proc %d (AGGREG) RECEIVED FROM %d ", rank, source);
    distrib_print(s_ctxt->client_rt_info[client_idx].remote_view_distr[source]);
#endif
    
  }
  l_f = r->l_f;
  while (l_f <= r->r_f) {
    clarisse_off off, b;
    iotask_p  iot;
    future_access_t *read_future;

    b = s_ctxt->clarisse_pars.buffer_size;
    off = BEGIN_BLOCK(l_f, b);
    HASH_FIND_LLINT(s_ctxt->read_futures, &off, read_future);
    if (!read_future) {
      read_future  = (future_access_t *)clarisse_malloc(sizeof(future_access_t));
      read_future->block_off = off;
      dllist_init(&read_future->task_list);
      HASH_ADD_LLINT(s_ctxt->read_futures, block_off, read_future);
    }
    iot = alloc_init_iotask(s_ctxt, source, l_f, MIN(END_BLOCK(l_f, b) ,r->r_f), client_idx);    
    iot->s_ctxt = s_ctxt;
    dllist_iat(&read_future->task_list, &iot->link);

    l_f = BEGIN_BLOCK(l_f, b) + (clarisse_off) r->partitioning_stride_factor * b;
  }
}


void serv_read_task_listio(server_iocontext_t *s_ctxt, struct rq_file_read  *rq, int client_idx, int source) {
  int metadata_size, i, err, total_cnt;
  int *offs;
  int *lens;
  struct rt_file_read *ret;
  MPI_Request *mpi_rq;
  MPI_Datatype d;
  MPI_Comm comm;
  clarisse_off initial_off;
  
  total_cnt = 0;
  metadata_size = rq->view_size;
  offs = (int *) ((char *)(rq + 1) + rq->target_name_size);
  lens = (int *) ((char *)offs + metadata_size / 2);
  initial_off = rq->l_f;
  ret = (struct rt_file_read *)clarisse_malloc(sizeof(struct rt_file_read));
  net_dt_update(&s_ctxt->client_rt_info[client_idx].net_dt, (char *)ret, sizeof(struct rt_file_read));
  for (i = 0; i < metadata_size / (2 * (int)sizeof(int)); i++) {
    
    total_cnt += serv_file_read(s_ctxt, initial_off + offs[i], initial_off + offs[i] + lens[i] - 1, client_idx, source);
  }
  assert(total_cnt <= rq->count);
  d = net_dt_commit(&s_ctxt->client_rt_info[client_idx].net_dt);
  ret->confirmed = total_cnt; 
  ret->errn = 0;
  ret->last_msg = rq->last_msg;
  // clarisse_isend
  if (s_ctxt->clarisse_pars.coupleness == CLARISSE_COUPLED) 
    comm = server_global_state.intracomm;// s_ctxt->intracomm;
  else
    comm = server_global_state.intercomms[client_idx];
    
  mpi_rq = alloc_pool_get(&s_ctxt->client_rt_info[client_idx].mpi_rq_pool);
  save_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map, mpi_rq, d, (char *)ret, NULL);
  err = MPI_Isend(ret, 1, d, source, RT_FILE_READ_COLL, comm, mpi_rq);
  if (err != MPI_SUCCESS)
    handle_err(err, "RT_FILE_READ_COLL send error\n");    
  //free_datatype(d);
  net_dt_reset(&s_ctxt->client_rt_info[client_idx].net_dt);
}

void serv_read_task_listio_llint(server_iocontext_t *s_ctxt, struct rq_file_read  *rq, int client_idx, int source) {
  int metadata_size, i, err, total_cnt;
  clarisse_off *offs;
  clarisse_off *lens;
  struct rt_file_read *ret;
  MPI_Request *mpi_rq;
  MPI_Datatype d;
  MPI_Comm comm;

  total_cnt = 0;
  metadata_size = rq->view_size;
  offs = (clarisse_off *) ((char *)(rq + 1) + rq->target_name_size);
  lens = (clarisse_off *) ((char *)offs + metadata_size / 2);
  ret = (struct rt_file_read *)clarisse_malloc(sizeof(struct rt_file_read));
  net_dt_update(&s_ctxt->client_rt_info[client_idx].net_dt, (char *)ret, sizeof(struct rt_file_read));
  for (i = 0; i < metadata_size / (2 * (int)sizeof(clarisse_off)); i++) {

    total_cnt += serv_file_read(s_ctxt, offs[i], offs[i] + lens[i] - 1, client_idx, source);
  }
  assert(total_cnt <= rq->count);
  d = net_dt_commit(&s_ctxt->client_rt_info[client_idx].net_dt);
  ret->confirmed = total_cnt; 
  ret->errn = 0;
  ret->last_msg = rq->last_msg;
  // clarisse_isend
  if (s_ctxt->clarisse_pars.coupleness == CLARISSE_COUPLED) 
    comm = server_global_state.intracomm; //s_ctxt->intracomm;
  else
    comm = server_global_state.intercomms[client_idx];

  mpi_rq = alloc_pool_get(&s_ctxt->client_rt_info[client_idx].mpi_rq_pool);
  save_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map, mpi_rq, d, (char *)ret, NULL);
  err = MPI_Isend(ret, 1, d, source, RT_FILE_READ_COLL, comm, mpi_rq);
  if (err != MPI_SUCCESS)
      handle_err(err, "RT_FILE_READ_COLL send error\n");    
  //free_datatype(d);
  net_dt_reset(&s_ctxt->client_rt_info[client_idx].net_dt);
}



// aggregator
int serv_read_n_shares(int client_idx) {
  MPI_Datatype d;
  int b, err, sends;
  server_iocontext_t * s_ctxt;
  clarisse_off crt_r_f;
  struct rt_file_read *ret;
  char *data_rq;
  MPI_Request *mpi_rq;

  b = server_global_state.buf_pool->buffer_size;
  sends = 0;
  
  while (!request_queues_empty(&server_global_state.read_task_queue)) { 
    iotask_p iot;
    int expected, total_cnt, eof; 
 
    total_cnt = 0; 
    eof = 0;
    expected = 0;
    iot = (iotask_p) request_queues_peek_crt(&server_global_state.read_task_queue);   
    s_ctxt = iot->s_ctxt;
    data_rq = (char *)clarisse_malloc(sizeof(struct rt_file_read));
    ret = (struct rt_file_read *) data_rq;
    net_dt_update(&s_ctxt->client_rt_info[client_idx].net_dt, (char *)ret, sizeof(struct rt_file_read));
    
    while ((iot->l_f <= iot->r_f) && (!eof) && (total_cnt + expected < s_ctxt->clarisse_pars.net_buf_data_size)) {
      crt_r_f = MIN(iot->r_f, END_BLOCK(iot->l_f, b));
      crt_r_f = MIN(crt_r_f, func_1(func_sup(iot->l_f, s_ctxt->client_rt_info[client_idx].remote_view_distr[iot->proc_rank]) + (clarisse_off) (s_ctxt->clarisse_pars.net_buf_data_size - total_cnt - 1),
				    s_ctxt->client_rt_info[client_idx].remote_view_distr[iot->proc_rank]));
      
      expected = get_count(iot->l_f, crt_r_f, s_ctxt->client_rt_info[client_idx].remote_view_distr[iot->proc_rank]);
      if (expected > 0) {
	int confirmed;
	confirmed = serv_file_read(s_ctxt, iot->l_f, crt_r_f, client_idx, iot->proc_rank); 
	iot->confirmed += confirmed;
	total_cnt += confirmed;
	if (confirmed != expected) {
	  eof = 1;
	  printf("Encountered EOF\n");
	}	
      }
      if (crt_r_f == END_BLOCK(iot->l_f, b))
	iot->l_f = BEGIN_BLOCK(iot->l_f, b) + (clarisse_off) iot->s_ctxt->client_rt_info[client_idx].partitioning_stride_factor * (clarisse_off)b;     
      else
	iot->l_f = crt_r_f + 1;
    } // while
    if (eof)
      ret->last_msg = CLARISSE_EOF;
    else 
      if (iot->l_f > iot->r_f) 
	ret->last_msg = CLARISSE_LAST_MSG;
      else
	ret->last_msg = CLARISSE_NOT_LAST_MSG;
    ret->confirmed = total_cnt; 
    ret->errn = 0;
    d = net_dt_commit(&s_ctxt->client_rt_info[client_idx].net_dt);
    // clarisse_isend
    mpi_rq = alloc_pool_get(&s_ctxt->client_rt_info[client_idx].mpi_rq_pool);
    save_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map, mpi_rq, d, data_rq, NULL);
    err = MPI_Isend(data_rq, 1, d, iot->proc_rank, RT_FILE_READ_COLL, server_global_state.intercomms[client_idx], mpi_rq);
    if (err != MPI_SUCCESS)
      handle_err(err, "RT_FILE_READ_COLL send error\n");    
    net_dt_reset(&s_ctxt->client_rt_info[client_idx].net_dt);
    sends++;  
    if ((iot->l_f > iot->r_f) || eof) { 
      iot = request_queues_rm_crt(&server_global_state.read_task_queue);
      clarisse_free(iot);
    }
    else 
      request_queues_set_next(&server_global_state.read_task_queue);  
  }
  return sends;
}



void flush_dcache_buffers_future(server_iocontext_t *s_ctxt) {
  int client_idx = -1, err;
  buffer_unit  *bu, *tmp;

  // If I now exactly the buffer I could avoid this iteration
  HASH_ITER(hh, s_ctxt->dcache.bufs, bu, tmp) {  
    future_access_t *read_future;

    HASH_FIND_LLINT(s_ctxt->read_futures, &bu->offset, read_future);
    if (read_future) {
      HASH_DEL(s_ctxt->read_futures, read_future);
      while(!dllist_is_empty(&read_future->task_list)) {
	dllist_link *elm = dllist_rem_head(&read_future->task_list);
	iotask *iot = DLLIST_ELEMENT(elm, iotask, link);
	int expected;
	
	client_idx = iot->client_idx;
	expected = get_count(iot->l_f, iot->r_f, s_ctxt->client_rt_info[client_idx].remote_view_distr[iot->proc_rank]);
	assert(expected <= s_ctxt->clarisse_pars.net_buf_data_size);
	if (expected > 0) {
	  int confirmed, eof;
	  char *data_rq;
	  MPI_Request *mpi_rq;
	  struct rt_file_read *ret;
	  MPI_Datatype d;
	  
	  eof = 0;
	  data_rq = (char *)clarisse_malloc(sizeof(struct rt_file_read));
	  ret = (struct rt_file_read *) data_rq;
	  net_dt_update(&s_ctxt->client_rt_info[client_idx].net_dt, (char *)ret, sizeof(struct rt_file_read));
	  confirmed = serv_file_read(s_ctxt, iot->l_f, iot->r_f, client_idx, iot->proc_rank); 
	  iot->confirmed = confirmed;
	  if (confirmed != expected) {
	    eof = 1;
	    printf("Encountered EOF\n");
	  }
	  if (eof)
	    ret->last_msg = CLARISSE_EOF;
	  else {
	    if ((s_ctxt->read_futures == NULL) && (dllist_is_empty(&read_future->task_list)))
	      ret->last_msg = CLARISSE_LAST_MSG;
	    else
	      ret->last_msg = CLARISSE_NOT_LAST_MSG;
	  }
	  ret->confirmed = confirmed; 
	  ret->errn = 0;
	  d = net_dt_commit(&s_ctxt->client_rt_info[client_idx].net_dt);
	  mpi_rq = alloc_pool_get(&s_ctxt->client_rt_info[client_idx].mpi_rq_pool);
	  save_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map, mpi_rq, d, data_rq, NULL);
	  err = MPI_Isend(data_rq, 1, d, iot->proc_rank, RT_FILE_READ_COLL, server_global_state.intercomms[client_idx], mpi_rq);
	  if (err != MPI_SUCCESS)
	    handle_err(err, "RT_FILE_READ_COLL send error\n");    
	  net_dt_reset(&s_ctxt->client_rt_info[client_idx].net_dt);
	  
	  clarisse_free(iot);
	} //if (expected > 0)
      } // while
      clarisse_free(read_future);
      dcache_rm2(&s_ctxt->dcache, bu);
      bufpool_put(server_global_state.buf_pool, bu);
    } // if (read_future)
    else
      printf("Read not posted yet: data discarded\n");
  } // HASH_ITER

  if (client_idx != -1) {
    err = MPI_Waitall(s_ctxt->client_rt_info[client_idx].mpi_rq_pool.cnt, s_ctxt->client_rt_info[client_idx].mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
    if (err != MPI_SUCCESS)
      handle_err(err, "SERV waitall error\n");
    free_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map); 
  }
}


// aggregator
static int serv_file_read(server_iocontext_t * s_ctxt, clarisse_off l, clarisse_off r, int client_idx, int source) {
  int  to_read, ret, b; // global_descr,
  distrib *d;
  buffer_unit_p bu;
  clarisse_off beg_block; //, last_r;
  
  //    global_descr = s_ctxt->local_target->global_descr;
  d = s_ctxt->client_rt_info[client_idx].remote_view_distr[source];
  b = server_global_state.buf_pool->buffer_size;
  beg_block = BEGIN_BLOCK(l, b);
  

  to_read = func_inf(r,d) - func_sup(l,d) + 1;
  if (to_read <= 0) return 0;
  
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  bu = dcache_find_page(&s_ctxt->dcache, /*global_descr,*/ beg_block);
  if (!bu) {
    int n;

    bu = dcache_put_page(&s_ctxt->dcache, server_global_state.buf_pool, s_ctxt->local_target);
    if (bu == NULL)
      handle_err(MPI_ERR_OTHER, "OUT OF BUFFERS");
#ifdef CLARISSE_TIMING
    double t0, t1;
    
    t0 = MPI_Wtime();
#endif  	  
    if ((n = pread(s_ctxt->local_target->local_handle.fd, bu->buf, b, beg_block)) < 0) 
      handle_err(MPI_ERR_OTHER, "Error at preadn");
#ifdef CLARISSE_DCACHE_DEBUG
    bu_print(bu);
    printf("****pread\n");
#endif 
#ifdef CLARISSE_TIMING
    t1 = MPI_Wtime();
    local_timing.time_file_read += (t1 - t0);
    local_timing.cnt_file_read ++;
    local_timing.size_file_read += n;
#endif  	  
    bu->dirty = 0;
    bu->l_dirty = b;
    bu->r_dirty = -1;
    bu->global_descr = s_ctxt->local_target->global_descr;
    bu->offset = beg_block;
    dcache_insert(&s_ctxt->dcache, bu);
  }
  if (!bu) 
    handle_err(MPI_ERR_OTHER, "NULL buffer in clarisse_read_coll\n");
  
  // bu->buf address has to be updated to an absolute value
  // Bug: If bu is evicted from buffer cache and reused, the new data will be sent instead of the old one
  // Solutions: 1) Use the copy version 2) "pin" the blocks until the send is performed and/or allocate more buffers  
  ret = sg_mem_nocopy(&s_ctxt->client_rt_info[client_idx].net_dt, bu->buf - bu->offset /*func_inf(bu->offset, s_ctxt->local_target->distr)*/,  l, r, d,  GATHER_NOCOPY);  
  
#ifdef CLARISSE_DCACHE_DEBUG 
  bu_print(bu);
  printf("****sg_mem_nocopy\n");
#endif
  if (ret != to_read)
    handle_err(MPI_ERR_OTHER, "sg_mem returned a value different than expected");
  return ret;
}


