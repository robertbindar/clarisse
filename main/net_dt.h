#ifndef NET_DT_H
#define NET_DT_H

#include "map.h"
//#include "hdr.h"


// This structures is used for incrementally building a hindexed datatyped, subsequently used for transfers
struct net_dt {
  int count;
  int max_count;
  int incr;
  MPI_Aint base_address;
  int *block_lens;
  MPI_Aint *displs;
  int aggr_len;
};

long long int sg_mem_nocopy (struct net_dt *net_dt, char *inbuf, 
			     long long int l, long long int r, distrib *distr, 
			     int type);

/* Functions for manipulating the hindexed datatype used for transfers for
avoiding memcpy
*/
void net_dt_alloc(struct net_dt *net_dt);
void net_dt_init (struct net_dt *net_dt, int max_count, int incr);
void net_dt_init_alloc (struct net_dt *net_dt, int max_count, int incr);
void net_dt_free(struct net_dt *net_dt);
void net_dt_update(struct net_dt *net_dt, char *buf, int len);
void net_dt_reset(struct net_dt *net_dt);
void net_dt_fuse(struct net_dt *net_dt_dest, struct net_dt *net_dt_src);
MPI_Datatype net_dt_commit(struct net_dt *net_dt);

#endif




