#include <assert.h>
#include "client_iocontext.h"
#include "aggr.h"
#include "error.h"
#include "util.h" 

extern client_global_state_t client_global_state;
extern int i_am_client;
extern int i_am_server;
extern int *client_nprocs;
extern int nr_clients;

MPI_Comm cls_get_client_intracomm() {
  if (i_am_client)
    return client_global_state.intracomm;
  else
    return MPI_COMM_NULL; 
}

MPI_Comm cls_get_allclients_intracomm() {
  if (i_am_client)
    return client_global_state.allclients_intracomm;
  else
    return MPI_COMM_NULL; 
}

int cls_is_client(){
  return i_am_client;
}

int cls_is_server(){
  return i_am_server;
}

int cls_set_clients(int nr_cli, int *cli_nprocs) {
  if (client_nprocs != NULL) {
    fprintf(stderr, "Clients have already been initialized, call %s before MPI_Init\n", __FUNCTION__);
    return -1;
  }
  else {
    
    int i;
    nr_clients = nr_cli;
    client_nprocs = clarisse_malloc(nr_clients * sizeof(int));
    for (i = 0; i < nr_cli; i++)
      client_nprocs[i] = cli_nprocs[i];
    return 0;
  }
}

int cls_get_client_id(){
  return client_global_state.client_id;
}


static int  get_servers(int map_type, int *nr_servers, int **server_map) {
  int flag_iamserver;

  switch (map_type) {
  case CLARISSE_SERVER_MAP_UNIFORM: 
    *nr_servers = get_aggregators(PLATFORM_ANY_UNIFORM, *nr_servers, -1, server_map, &flag_iamserver);
    break;
  case CLARISSE_SERVER_MAP_ROMIO:
  case CLARISSE_SERVER_MAP_CUSTOM:
    
    break;
    /*case CLARISSE_SERVER_MAP_RANDOM: 
      break;*/
  case CLARISSE_SERVER_MAP_TOPOLOGY_EXCLUDE_BRIDGE:
    *nr_servers = get_aggregators(PLATFORM_BGQ_EXCLUDE_BRIDGE, client_global_state.clarisse_pars.bg_nodes_pset, client_global_state.clarisse_pars.aggregator_selection_step, server_map, &flag_iamserver);
    break;
  case CLARISSE_SERVER_MAP_TOPOLOGY_INCLUDE_BRIDGE: 
    *nr_servers = get_aggregators(PLATFORM_BGQ_INCLUDE_BRIDGE, client_global_state.clarisse_pars.bg_nodes_pset, -1, server_map, &flag_iamserver);
    break; 
  default:
    handle_err(MPI_ERR_OTHER, "get_servers: server_map not supported.");
  }

  return flag_iamserver;
}

// 012345678
//  S  S  
// for nr_clients = 2, client_nprocs[] ={3,4} 
//myrank group_index leader
//   0        1      0       
//   1        0      [1]
//   2        1      0
//   3        1      0
//   4        0      [1]
//   5        2      5
//   6        2      5
//   7        2      5
//   8        2      5

// leaders = 0,5

int assign_clients(unsigned char *client_ranks, int *client_nprocs, int *client_leaders){
  int i, j, client_memb_key, local_client_idx, flag_first_client_rank, ret;
  int myrank, nprocs;

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  client_memb_key = 1;
  local_client_idx = 0;
  flag_first_client_rank = 1;
  j = 0;
  for (i = 0; i < nprocs; i++) {
    if (client_ranks[i] == 0) { 
      if (flag_first_client_rank) {
	client_leaders[j++] = i;
	flag_first_client_rank = 0;
      }
      local_client_idx++;
    }
    if (i == myrank) {
      if (client_ranks[i] == 0)
	ret = client_memb_key;
      else
	ret = CLARISSE_SERVER_MEMBERSHIP_KEY; // server: 0
    }
    if (local_client_idx  == client_nprocs[client_memb_key - 1]) {
      local_client_idx = 0;
      flag_first_client_rank = 1;
      client_memb_key++;
    } 
  }
  return ret;
}



// Returns 1 if current rank is a server, 0 otherwise
int cls_assign_servers(int map_type, int nr_servers, int *server_map, int *server_leader, int *client_leaders) {
  int flag_iamserver, log_ios_rank, nprocs;
  unsigned char *client_ranks;
 
  flag_iamserver = get_servers(map_type, &nr_servers, &server_map);
  assert(nr_servers > 0);

  if (client_global_state.log2ph) {
    clarisse_free(client_global_state.log2ph);
    client_global_state.ph2log = NULL;
  }
  client_global_state.log2ph = clarisse_malloc(nr_servers * sizeof(int));
  memcpy(client_global_state.log2ph, server_map, nr_servers * sizeof(int));
  client_global_state.clarisse_pars.nr_servers = nr_servers;     
  client_global_state.active_nr_servers = nr_servers;  
  client_global_state.active_log_servers = (int *) clarisse_malloc(client_global_state.active_nr_servers *sizeof(int));
  if (client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM) { 
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    *server_leader = nprocs;
    client_ranks = (unsigned char *)clarisse_malloc(nprocs *sizeof(unsigned char));
    memset((void *)client_ranks, 0, nprocs *sizeof(unsigned char));
  }
  for (log_ios_rank = 0; log_ios_rank < nr_servers; log_ios_rank++) {
    int_map_t *m;
    
    m = (int_map_t *)clarisse_malloc(sizeof(int_map_t));
    m->log_ios_rank = log_ios_rank;
    client_global_state.active_log_servers[log_ios_rank] = log_ios_rank;
    if (client_global_state.clarisse_pars.coupleness != CLARISSE_COUPLED) { 
      // assign 1 if a physical rank is a server
      client_ranks[client_global_state.log2ph[log_ios_rank]] = 1;
      // MIN for enforcing that the first process from INTRACOMM is the leader
      *server_leader = MIN(*server_leader, client_global_state.log2ph[log_ios_rank]); 
      // For INTERCOMM, the intracomm is used so log_ios_rank are used as phys
      client_global_state.log2ph[log_ios_rank] = log_ios_rank;
    }
    m->ph_ios_rank = client_global_state.log2ph[log_ios_rank]; 
    HASH_ADD_INT(client_global_state.ph2log, ph_ios_rank, m); 
    //printf (" m->log_ios_rank=%d m->ph_ios_rank=%d\n", m->log_ios_rank,m->ph_ios_rank);
  }
  if (client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM) {
    int memb_key = assign_clients(client_ranks, client_nprocs, client_leaders);
    
    clarisse_free(client_ranks);
    //printf("Server leader = %d Client leader = %d\n", *server_leader, *client_leader);
    return memb_key;
  }
  else
    if (flag_iamserver)
      return CLARISSE_SERVER_MEMBERSHIP_KEY;
    else
      return CLARISSE_CLIENT_MEMBERSHIP_KEY;
}



// Returns 1 if current rank is a server, 0 otherwise
int OLDcls_assign_servers(int map_type, int nr_servers, int *server_map) {
  int log_ios_rank, myrank, ret, flag_iamserver;

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  ret = 0;
  assert(client_global_state.ph2log == NULL);

  if (map_type == CLARISSE_SERVER_MAP_TOPOLOGY_EXCLUDE_BRIDGE){
    if (client_global_state.log2ph) 
      clarisse_free(client_global_state.log2ph); 
    nr_servers = get_aggregators(PLATFORM_BGQ_EXCLUDE_BRIDGE, client_global_state.clarisse_pars.bg_nodes_pset, client_global_state.clarisse_pars.aggregator_selection_step, &client_global_state.log2ph, &flag_iamserver);
  }
  else   
      if (map_type == CLARISSE_SERVER_MAP_TOPOLOGY_INCLUDE_BRIDGE) {
	if (client_global_state.log2ph) 
      	   clarisse_free(client_global_state.log2ph);
	nr_servers = get_aggregators(PLATFORM_BGQ_INCLUDE_BRIDGE, client_global_state.clarisse_pars.bg_nodes_pset, -1, &client_global_state.log2ph, &flag_iamserver);
      }	
      else {	
        if ((nr_servers != client_global_state.clarisse_pars.nr_servers) || 
            (!client_global_state.log2ph))
           client_global_state.log2ph = clarisse_realloc(client_global_state.log2ph, nr_servers * sizeof(int));
      }
  assert(nr_servers > 0);
  client_global_state.clarisse_pars.nr_servers = nr_servers;     
  client_global_state.active_nr_servers = nr_servers;  

  client_global_state.active_log_servers = (int *) clarisse_malloc(client_global_state.active_nr_servers *sizeof(int));

  for (log_ios_rank = 0; log_ios_rank < nr_servers; log_ios_rank++) {
    int_map_t *m;
    switch (map_type) {
    case CLARISSE_SERVER_MAP_UNIFORM:
      client_global_state.log2ph[log_ios_rank] = log_ios_rank;
      break;
    case CLARISSE_SERVER_MAP_ROMIO:
    case CLARISSE_SERVER_MAP_CUSTOM:
      client_global_state.log2ph[log_ios_rank] = server_map[log_ios_rank];
      break;
      /*case CLARISSE_SERVER_MAP_RANDOM: 
	break;*/
    case CLARISSE_SERVER_MAP_TOPOLOGY_EXCLUDE_BRIDGE:
 	break;
    case CLARISSE_SERVER_MAP_TOPOLOGY_INCLUDE_BRIDGE: 
	break; 
    default:
      handle_err(MPI_ERR_OTHER, "cls_assign_server: server_map not supported.");
    }
    m = (int_map_t *)clarisse_malloc(sizeof(int_map_t));
    m->log_ios_rank = log_ios_rank;
    m->ph_ios_rank = client_global_state.log2ph[log_ios_rank];
    HASH_ADD_INT(client_global_state.ph2log, ph_ios_rank, m); 
    if (myrank == m->ph_ios_rank)
      ret = 1;
    client_global_state.active_log_servers[log_ios_rank] = log_ios_rank;
  }
  
  return ret;
}


int cls_reassign_servers(int map_type, int nr_servers, int *server_map) {
  int log_ios_rank, client_leader, server_leader;

  if (client_global_state.ph2log != NULL) {
    for (log_ios_rank = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++){
      int_map_t *m;
      HASH_FIND_INT(client_global_state.ph2log, &client_global_state.log2ph[log_ios_rank], m);
      if (m) {
	HASH_DEL(client_global_state.ph2log, m);
	clarisse_free(m);
      }
    }
  }
  assert(client_global_state.ph2log == NULL);
  
  return cls_assign_servers(map_type, nr_servers, server_map, &server_leader, &client_leader);

}


// Assigns servers to ranks of MPI_COMM_WORLD
// Returns 1 if current rank is a server, 0 otherwise
void cls_print_servers() {
  int log_ios_rank;

  printf("%d CLS servers, %d ACTIVE CLS servers: ", client_global_state.clarisse_pars.nr_servers, client_global_state.active_nr_servers);
  for (log_ios_rank = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++){
    printf("%d ", client_global_state.log2ph[log_ios_rank]);
  }
  printf(" MAPPING ACTIVE-NONACTIVE:");
  for (log_ios_rank = 0; log_ios_rank < client_global_state.active_nr_servers; log_ios_rank++){
    printf("%d ", client_global_state.active_log_servers[log_ios_rank]);
  }
  printf("\n");
}


int cls_get_nr_active_servers(){
  return client_global_state.active_nr_servers;
}
