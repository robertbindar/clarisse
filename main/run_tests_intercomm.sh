
export CLARISSE_DYN_PROCESS=false
#export CLARISSE_NR_SERVERS=1
FILENAME="abc"
MPIEXEC="mpiexec -l" 
max_processes=5
min_nr_servers=1
max_nr_servers=4

declare -a arr=(
#"./test1_open_close" 
#"./test2_write" 
#"./test3_write_read" 
#"./test4_write_read" 
#"./test5_read" 
#"./test6_write_write" 
#"./test7_setview_write_read"
"./test8_setview_write_read_nproc"
)
declare -a arr2=("lazy" "eager")
declare -a arr3=("false" "true")

for j in "${arr[@]}"
do
    for v in `seq $min_nr_servers $max_nr_servers`
    do
	echo CLARISSE_NR_SERVERS=$v
	export CLARISSE_NR_SERVERS=$v
	for i in `seq $((v+1)) $max_processes`
	do
	    #echo i=$i v=$v
	    for k in "${arr2[@]}"
	    do
		export CLARISSE_LAZY_EAGER_VIEW=$k
		if [ "$j" == "./test8_setview_write_read_nproc" ]
		then
		    for l in "${arr3[@]}"
		    do
			export TEST_PREEXISTING_FILE=$l
			#echo $v 
			${MPIEXEC} -n $i $j ${FILENAME}
			if [ $? -ne 0 ]
			then
			    exit $?
			fi
		    done
		else
		    #echo $v 
		    ${MPIEXEC} -n $i $j ${FILENAME}
		    if [ $? -ne 0 ]
		    then
			exit $?
		    fi
		fi
	    done
	done
    done
done
