#include "client_iocontext.h"
#include "hdr.h"
#include "util.h"

extern client_global_state_t client_global_state;
extern int i_am_client;

int MPI_File_set_size(MPI_File fh, MPI_Offset size) {
  int ret = MPI_SUCCESS;
  
  if (i_am_client) {
    client_iocontext_t *c_ctxt;
    c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
    
    if (c_ctxt) {
      ret = MPI_SUCCESS;
    }
    else
      ret = PMPI_File_set_size(fh, size);
  }
  return ret;
}
