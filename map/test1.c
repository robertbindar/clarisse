#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "map.h"

/* find_bin_search test */

MPI_Datatype get_contig();
MPI_Datatype get_lb_contig(); //1
MPI_Datatype get_contig_ub(); //2
MPI_Datatype get_simple_vec(); //3
MPI_Datatype get_displ_simple_vec(); //4
MPI_Datatype get_simple_struct(); //5
MPI_Datatype get_displ_displ_struct(); //6
MPI_Datatype get_vect_struct(); //7
MPI_Datatype get_darray();

#define MAX_CASES_PER_TEST 1024

int test_find_bin_search(const char *funname, char *testname, int test_cnt, MPI_Datatype *dt, long long int *offset_input, int map_shift, int *expected_output) {
  int i, ret = 0;
  distrib *distr = distrib_get(*dt, 0); 
  
  assert(test_cnt <= MAX_CASES_PER_TEST);

  for (i=0; i<test_cnt; i++) {
    int output;
    if ((output = find_bin_search(offset_input[i], distr, map_shift)) != expected_output[i]){
      printf("find_bin_search test %s:%s failed on input %d with output %d (expected %d)!!!\n", funname, testname, (int)offset_input[i], output, expected_output[i] );
      distrib_print(distr);
      distrib_draw(distr, 1);
      ret = -1;
      break;
    }
  }
  distrib_free(distr);
  MPI_Type_free(dt);
  return ret; 
}

int test1_1a() {
  MPI_Datatype dt = get_lb_contig();
  long long int offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {0,0,0,0,0,0,0,0};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search(__func__,"LB CONTIG MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_1b() {
  MPI_Datatype dt = get_lb_contig();
  long long int offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {0,0,0,0,1,1,1,1};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search(__func__,"LB CONTIG MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_2a() {
  MPI_Datatype dt = get_contig_ub();
  long long int offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {0,0,0,0,1,1,1,1};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search(__func__,"LB CONTIG MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_2b() {
  MPI_Datatype dt = get_contig_ub();
  long long int offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {1,1,1,1,1,1,1,1};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search(__func__,"CONTIG UB MAP_NEXT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}


int test1_3a() {
  MPI_Datatype dt = get_simple_vec();
  long long int offset_input[]={0,1,2};
  int  expected_output[]={0,1,1};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search(__func__,"SIMPLE VECTOR MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_3b() {
  MPI_Datatype dt = get_simple_vec();
  long long int offset_input[]={0,1,2};
  int  expected_output[]={1,1,2};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search(__func__,"SIMPLE VECTOR MAP_NEXT_BLOCK",3, &dt, offset_input, map_shift, expected_output);

}


// 0         1         
// 012345678901234
// ___[_*_**_*____]
//              1
//I:  012345678901 
//Oa: 000111223333
//Ob: 001122233333

int test1_4a() {
  MPI_Datatype dt = get_displ_simple_vec();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,0,1,1,1,2,2,3,3, 3, 3};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search(__func__,"DISPL SIMPLE VECT MAP_CURRENT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}

int test1_4b() {
  MPI_Datatype dt = get_displ_simple_vec();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,1,1,2,2,2,3,3,3, 3, 3};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search(__func__,"DISPL SIMPLE VECT MAP_NEXT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}

// 0         1         
// 01234567890123456789
// _______[___****____]
//                  1
//I:      012345678901 
//Oa:     000000001111
//Ob:     000011111111
int test1_5a() {
  MPI_Datatype dt = get_simple_struct();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,0,0,0,0,0,0,1,1, 1, 1};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search(__func__,"SIMPLE STRUCT MAP_CURRENT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}

int test1_5b() {
  MPI_Datatype dt = get_simple_struct();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,0,0,1,1,1,1,1,1, 1, 1};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search(__func__,"SIMPLE STRUCT MAP_NEXT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}

// 0         1         
// 01234567890123456789
// ________**
//                  
//I:       01 
//Oa:      00
//Ob:      01

int test1_6a() {
  MPI_Datatype dt = get_displ_displ_struct();
  long long int offset_input[]={0,1};
  int  expected_output[] =   {0,0};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search(__func__,"DISPL DISPL STRUCT MAP_CURRENT_BLOCK",2, &dt, offset_input, map_shift, expected_output);
}

int test1_6b() {
  MPI_Datatype dt = get_displ_displ_struct();
  long long int offset_input[]={0,1};
  int  expected_output[] =   {1,1};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search(__func__,"DISPL DISPL STRUCT MAP_NEXT_BLOCK",2, &dt, offset_input, map_shift, expected_output);
}

// 0         1         2         3         4         5         6         7         8         9         
// 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
// ********____****____****____****____*_______________________________________________________________
// ________________________****____****____****____*___________________________________________________
// ____________________________________****____****____****____*_______________________________________
// ________________________________________________****____****____****____*___________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// *___**__
//I: 2345678901 
//Oa:0000001111
//Ob:1111111111

int test1_7a() {
  MPI_Datatype dt = get_vect_struct();
  long long int offset_input[]={0,7,8,11,12,123,124,147,148,999,1000,1001,1004,1005,1006,1007};
  int  expected_output[] =   {0,0,1, 1, 1,  5,  5,  8,  8, 17,  17,  18,  18,  18,  19,  19};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search(__func__,"VECT STRUCT MAP_CURRENT_BLOCK",16, &dt, offset_input, map_shift, expected_output);
}
int test1_7b() {
  MPI_Datatype dt = get_vect_struct();
  long long int offset_input[]={0,7,8,11,12,123,124,147,148,999,1000,1001,1004,1005,1006,1007};
  int  expected_output[] =   {1,1,1, 1, 2,  5,  6,  8,  9, 17,  18,  18,  19,  19,  19,  19};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search(__func__,"VECT STRUCT MAP_NEXT_BLOCK",16, &dt, offset_input, map_shift, expected_output);
}



int main(int argc, char* argv[]) {
    int ret;
    MPI_Init(&argc, &argv);

    ret = test1_1a();
    if (ret >= 0) ret = test1_1b();
    if (ret >= 0) ret = test1_2a();
    if (ret >= 0) ret = test1_2b();
    if (ret >= 0) ret = test1_3a();
    if (ret >= 0) ret = test1_3b();
    if (ret >= 0) ret = test1_4a();
    if (ret >= 0) ret = test1_4b();
    if (ret >= 0) ret = test1_5a();
    if (ret >= 0) ret = test1_5b();
    if (ret >= 0) ret = test1_6a();
    if (ret >= 0) ret = test1_6b();
    if (ret >= 0) ret = test1_7a();
    if (ret >= 0) ret = test1_7b();
    
    if (ret >= 0) printf ("find_bin_search tests passed!!\n");

    MPI_Finalize();
    return ret;
}  // main 
