#ifndef _MAP_INCLUDE
#define _MAP_INCLUDE

#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>

#include "flatten.h"

#define MAX(a,b) \
  ({ __typeof__ (a) _a = (a);	 \
    __typeof__ (b) _b = (b);	 \
    _a > _b ? _a : _b; })

#define MIN(a,b)	       \
  ({ __typeof__ (a) _a = (a);	\
    __typeof__ (b) _b = (b);	\
    _a < _b ? _a : _b; })

typedef struct { 
  struct dllist_link link;
  long long int off;      
  long long int len;      
} off_len_node_t;

#define MAP_CURRENT_BLOCK 0
#define MAP_NEXT_BLOCK 1

int find_bin_search(long long int x, distrib *distr, int map_shift);
long long int find_size_bin_search(long long int x, distrib *distr);

long long int func_sup_per(long long int x, distrib *distr);
long long int func_sup(long long int x, distrib *distr);
long long int func_inf_per(long long int x, distrib *distr);
long long int func_inf(long long int x, distrib *distr);
long long int func_1(long long int x, distrib *distr);
long long int get_count(long long int l, long long int r,  distrib *distr);
#define get_next_data_l(l,d) func_1(func_sup((l),(d)),(d))
#define get_prev_data_r(r,d) func_1(func_inf((r),(d)),(d))

#define BEGIN_BLOCK(x,block_size) (((x)/(block_size))*(block_size))
#define END_BLOCK(x,block_size) (((x)/(block_size)+1)*(block_size)-1)
#define BEGIN_NEXT_BLOCK(x,block_size) (((x)/(block_size)+1)*(block_size))
#define CEIL_INT(x,y)  (((x)/(y)) + (((x)%(y))?1:0))

#define SCATTER 0
#define GATHER 1
#define SCATTER_NOCOPY 2
#define GATHER_NOCOPY 3
long long int sg_mem (char *outbuf, char *inbuf, 
		      long long int l, long long int r, distrib *distr, 
		      int type);
long long int sg_file (int fd, long long int file_displ, 
		       long long int l, long long int r, 
		       distrib *distr, char*out_buf, int type);
int get_offs_lens(long long int l, long long int r, distrib *distr, 
		  long long int **offs, long long int **lens, int *data_size);
int get_offs_lens2(long long int *start_l, long long int r, distrib *distr, 
		   long long int **offs, long long int **lens,
		   int max_size,
		   int *data_size, int *metadata_size);
int get_offs_lens3(long long int *start_l, long long int r, distrib *distr, 
		   struct dllist *off_len_list,
		   int max_size, int *data_size, int *metadata_size);
int get_offs_lens4(long long int *start_l, long long int r, distrib *distr, 
		   long long int **offs, long long int **lens,
		   int max_size,
		   int *data_size, int *metadata_size);
int get_offs_lens5(long long int *start_l, long long int r, distrib *distr, 
		   long long int* initial_offset, int **offs, int **lens,
		   int max_size,
		   int *data_size, int *metadata_size);
int get_offs_lens6(long long int *start_l, long long int r, distrib *distr, 
		   long long int* initial_offset, int **offs, int **lens,
		   int max_size,
		   int *data_size, int *metadata_size);

long long int writen(int fd,  const void *vptr, long long int n);
long long int pwriten(int fd, const void *vptr, long long int n, long long int offset);
long long int readn(int fd, void *vptr, long long int n);
long long int preadn(int fd, void *vptr, long long int n, long long int offset);

int count_contiguous_blocks_file(MPI_File fh, MPI_Offset foff1, MPI_Offset foff2);
int count_contiguous_blocks_memory(MPI_Datatype datatype, int count);


#endif
