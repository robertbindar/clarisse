#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "map.h"
#include "util.h"

/* Scatter/gather test */

MPI_Datatype get_contig();    //0
MPI_Datatype get_lb_contig(); //1
MPI_Datatype get_contig_ub(); //2
MPI_Datatype get_simple_vec(); //3
MPI_Datatype get_displ_simple_vec(); //4
MPI_Datatype get_simple_struct(); //5
MPI_Datatype get_displ_displ_struct(); //6
MPI_Datatype get_vect_struct(); //7
MPI_Datatype get_darray();

#define MAX_CASES_PER_TEST 1024

int test_get_offs_lens(const char *funname, char *testname, MPI_Datatype *dt, long long int l, long long int r, int max_size, int expected_idx, long long int expected_l, long long int *expected_offs, long long int *expected_lens, int expected_data_size, int expected_metadata_size) {
  int ret = 0, j;
  distrib *distr = distrib_get(*dt, 0);
  long long int *offs, *lens, initial_l = l;
  int data_size, metadata_size;
  int idx = get_offs_lens4(&l, r, distr, &offs, &lens, max_size, &data_size,&metadata_size);
  if ((idx == expected_idx) && (data_size == expected_data_size)) {
    for (j = 0; j < idx; j++) {
      if ((offs[j] != expected_offs[j]) || 
	  (lens[j] != expected_lens[j])) 
	break;
    }
  }
  if ((idx != expected_idx) || (j != idx) || (l != expected_l) || (data_size != expected_data_size) || (metadata_size != expected_metadata_size)){
    printf("get_offs_lens test %s:%s failed on inputs l=%lld (returned=%lld expected=%lld) r=%lld with output idx=%d (expected %d) data_size=%d (expected %d) metadata_size=%d (expected %d)!!!\n", funname, testname, initial_l, l, expected_l, r, idx, expected_idx, data_size, expected_data_size, metadata_size, expected_metadata_size);
    if (idx == expected_idx)
      for (j = 0; j < idx; j++) {
	printf("output_offs=%lld expected_offs=%lld\toutput_lens=%lld expected_lens=%lld\n", offs[j], expected_offs[j], lens[j], expected_lens[j]);
      }
    distrib_print(distr);
    distrib_draw(distr, 1);
    ret = -1;
  }
  if (idx > 0)
    clarisse_free(offs);
  if (idx > 0)
    clarisse_free(lens);
  distrib_free(distr);
  MPI_Type_free(dt);
  return ret; 
}



//  0         1         
//  012345678901234
//  **        
int test1_0() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 0, 2, 3, 4, 5};
  long long int r[]=           {15,16, 4, 4, 4, 4};
  int max_size[] =             {16,16,15,16,16,16};
  int  expected_idx[]=          {1, 1, 0, 1, 1, 0};
  int  expected_l[]=           {16,16, 2, 5, 5, 5};
  int expected_data_size[]=    {16,16, 0, 2, 1, 0};
  int expected_metadata_size[]={16,16, 0,16,16, 0};
  // last value does not matter as expected idx==0
  long long int expected_offs[6][1]={{0},{0},{-1},{3},{4},{1023333}};
  long long int expected_lens[6][1]={{16},{16},{0},{2},{1},{0}};
  

  for (i = 0; i < 6; i++) {
    MPI_Datatype dt = get_contig();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], (long long int *)expected_offs[i], (long long int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}


//  0         1         2
//  01234567890123456789012
//  -------[---****[---****                 

int test1_1() {
  int ret = 0;
  int i;
  long long int l[]=          {10,10,11,11,11,14,15,11};
  long long int r[]=          {10,14,90,90,20,14,14,21};
  int max_size[] =            {16,16,64,32,48,17,16,20};
  int expected_idx[]=         { 0, 1, 4, 2, 2, 1, 0, 1};
  int expected_l[]=           {10,15,43,27,21,15,15,19};
  int expected_data_size[]=   { 0, 4,16, 8, 6, 1, 0, 4};
  int expected_metadata_size[]={0,16,64,32,32,16, 0,16};
  // last value does not matter as expected idx==0
  long long int expected_offs[8][4]={{0},{11}, {11,19,27,35},{11,19},{11,19},{14},{1023333},{11}};
  long long int expected_lens[8][4]={{5},{ 4}, { 4, 4, 4, 4}, {4, 4}, {4, 2}, {1},{0},{4}};
  
  for (i = 0; i < 8; i++) {
    MPI_Datatype dt = get_lb_contig();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], (long long int *)expected_offs[i], (long long int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}

//  0         1         2         3 
//  0123456789012345678901234567890
//  ---****---****---****---****        
//            ]
int test1_2() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 2, 2, 6, 7, 8};
  long long int r[]=            {5,30,21, 7, 7,11};
  int max_size[] =             {16,16,32,16,16,16};
  int expected_idx[]=           {1, 1, 2, 1, 0, 1};
  int expected_l[]=             {6,10,17, 8, 7,12};
  int expected_data_size[]=     {3, 4, 8, 1, 0, 2};
  int expected_metadata_size[]={16,16,32,16, 0,16};
  // last value does not matter as expected idx==0
  long long int expected_offs[6][2]={{3},{3},{3,10},{6},{10003},{10}};
  long long int expected_lens[6][2]={{3},{4},{4, 4},{1},{0},{2}};
  
  for (i = 0; i < 6; i++) {
    MPI_Datatype dt = get_contig_ub();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], (long long int *)expected_offs[i], (long long int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)

    if (ret < 0)
      break;
  }
  
  return ret;
}


//  0         1         2
//  012345678901234567890123456
//  *-**-**-**-**-**-**-**-**-*        
int test1_3() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 0, 1,  1, 6, 7, 8};
  long long int r[]=            {2, 5, 6, 27, 7, 7,11};
  int max_size[] =             {16,48,32,128,17,16,16};
  //unoptimized
  //int  expected_idx[]=    {4,4,3,1,0,3};
  int expected_idx[]=           {1, 3, 2,  8, 1, 0, 1};
  int expected_l[]=             {2, 6, 7, 26, 8, 7,11};
  int expected_data_size[]=     {1, 4, 4, 16, 1, 0, 2};
  int expected_metadata_size[]={16,48,32,128,16, 0,16};
  // some values do not matter when expected idx==0
  // unoptimized
  //long long int expected_offs[6][4]={{0,2,3,5}, {2,3,5,6},{3,5,6},{6},{10003},{8,9,11}};
  long long int expected_offs[7][8]={{0},{0,2,5}, {2,5},{2,5,8,11,14,17,20,23},{6},{10003},{8}};
  // unoptimized
  //long long int expected_lens[6][4]={{1,1,1,1}, {1,1,1,1},{1,1,1},{1},{2},{1,1,1}};
  long long int expected_lens[7][8]={{1},{1,2,1}, {2,2},{2,2,2, 2, 2, 2, 2, 2},{1},{2},{2}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_simple_vec();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], (long long int *)expected_offs[i], (long long int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  return ret;
}


// 0         1         
// 012345678901234
// ___[_*_**_*____]
int test1_4() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 2, 3, 6, 7, 8, 5};
  long long int r[]=            {5, 6, 7, 7, 7,11, 7};
  int max_size[] =             {17,17,34,17,17,34,16};
  int expected_idx[]=           {1, 1, 2, 1, 1, 2, 1};
  int expected_l[]=             {6, 7, 8, 8, 8,12, 7};
  int expected_data_size[]=     {1, 1, 2, 1, 1, 2, 1};
  int expected_metadata_size[]={16,16,32,16,16,32,16};
  // some values do not matter when expected idx==0
  long long int expected_offs[7][2]={{5}, {5},{5, 7},{7},{7},{8,10},{5}};
  long long int expected_lens[7][2]={{1}, {1},{1, 1},{1},{1},{1,1} ,{1}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_displ_simple_vec();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], (long long int *)expected_offs[i], (long long int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  return ret;
}


// 0         1         
// 01234567890123456789
// _______[___****____]
int test1_5() {
  int ret = 0;
  int i;
  long long int l[]=         {10, 10,11,12,13,14,15};
  long long int r[]=         {10, 14,14,14,23,14,14};
  int max_size[] =            {16,16,16,16,32,16,16};
  int expected_idx[]=          {0, 1, 1, 1, 2, 1, 0};
  int expected_l[]=           {10,15,15,15,24,15,15};
  int expected_data_size[]=    {0, 4, 4, 3, 3, 1, 0};  
  int expected_metadata_size[]={0,16,16,16,32,16, 0};
  // last value does not matter as expected idx==0
  long long int expected_offs[7][2]={{0},{11}, {11},{12},{13,23},{14},{1023333}};
  long long int expected_lens[7][2]={{5},{ 4}, { 4}, {3}, {2, 1}, {1},{0}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_simple_struct();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], (long long int *)expected_offs[i], (long long int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}


// 0         1         
// 01234567890123456789
// ________************
int test1_6() {
  int ret = 0;
  int i;
  long long int l[]=           {10,10,11,12,13,14,15};
  long long int r[]=           {10,14,14,14,20,14,14};
  int max_size[] =             {16,16,16,16,16,16,16};
  int expected_idx[]=          { 1, 1, 1, 1, 1, 1, 0};
  int expected_l[]=            {11,15,15,15,21,15,15};
  int expected_data_size[]=     {1, 5, 4, 3, 8, 1, 0};  
  int expected_metadata_size[]={16,16,16,16,16,16, 0};
  // last value does not matter as expected idx==0
  long long int expected_offs[7][1]={{10},{10}, {11},{12},{13},{14},{1023333}};
  long long int expected_lens[7][1]={ {1},{ 5}, { 4}, {3}, {8}, {1},{0}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_displ_displ_struct();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], (long long int *)expected_offs[i], (long long int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}



/*

// 0         1         2         3         4         5         6         7         8         9         
// 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
// ********____****____****____****____*_______________________________________________________________
// ________________________****____****____****____*___________________________________________________
// ____________________________________****____****____****____*_______________________________________
// ________________________________________________****____****____****____*___________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// *___**__
//I:   12  
//O:   45  
int test1_7() {
  MPI_Datatype dt = get_vect_struct();
  long long int offset_input[]={0,7, 8,11,12,15,16,19,20, 21, 24, 25, 33, 50, 59,  60,  61,  62,  63, 112};
  int  expected_output[] =  {0,7, 8,11,12,15,16,19,20, 21, 24, 25, 33, 50, 59,  60,  61,  62,  63, 112}; 
  return test_sg_mem(__func__,"VECT STRUCT",18, &dt, offset_input, expected_output);
}

*/

int main(int argc, char* argv[]) {
    int ret;
    MPI_Init(&argc, &argv);

    ret = test1_0();
    if (ret >= 0) ret = test1_1();
    if (ret >= 0) ret = test1_2();     
    if (ret >= 0) ret = test1_3();
    if (ret >= 0) ret = test1_4();
    if (ret >= 0) ret = test1_5();
    if (ret >= 0) ret = test1_6();
    /*if (ret >= 0) ret = test1_7();

    */
    if (ret >= 0) printf ("get_offs_len4 tests passed!!\n");

    MPI_Finalize();
    return ret;
}  // main 
