#ifndef TEST_DATATPES_H
#define TEST_DATATYPES_H

MPI_Datatype get_contig();
MPI_Datatype get_lb_contig();
MPI_Datatype get_contig_ub();
MPI_Datatype get_simple_vec();
MPI_Datatype get_displ_simple_vec();
MPI_Datatype get_simple_struct();
MPI_Datatype get_displ_displ_struct();
MPI_Datatype get_vect_struct();
MPI_Datatype get_darray(MPI_Comm comm);
MPI_Datatype get_darray_4procs_1rank();
MPI_Datatype get_vec_nstride(MPI_Comm comm);

#endif
