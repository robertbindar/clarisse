#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "flatten.h"
//#include "adio.h"
//#include "adioi.h"
//#include "mpid_datatype.h"
#include "mpi.h"
#include "error.h"
#include "clarisse.h"
#include "util.h"

static int flatlist_keyval = MPI_KEYVAL_INVALID;
// global refcount is used for allocating deallocating flatlist_keyval
// is used as extra_state
static int flatlist_global_refcount = 0;

int flatlist_copy_attr_function(MPI_Datatype type,
                                int type_keyval,
                                void *extra_state,
                                void *attribute_val_in,
                                void *attribute_val_out,
                                int *flag)
{
  //printf("copy fn. called\n");
    if (type_keyval != flatlist_keyval) 
	handle_err(MPI_ERR_OTHER, "Error: type_keyval != flatlist_keyval\n");
    (*((int*)extra_state)) ++;
    ((flatlist_node *)attribute_val_in)->refcount ++;
    *(void **)attribute_val_out = attribute_val_in;
    *flag = 1;
    CLARISSE_UNUSED(type);
    return MPI_SUCCESS;
}

int flatlist_delete_attr_function(MPI_Datatype type,
                                  int type_keyval,
                                  void *attribute_val,
                                  void *extra_state)
{
  //printf("delete fn. called\n");
    if (type_keyval != flatlist_keyval) 
	handle_err(MPI_ERR_OTHER, "Error: type_keyval != flatlist_keyval\n");
    (*((int*)extra_state)) --;
    ((flatlist_node *)attribute_val)->refcount --;
    if (((flatlist_node *)attribute_val)->refcount == 0){
        free_flatlist(attribute_val);
        //printf("attribute freed\n");
    }
    if ((*((int*)extra_state)) == 0) {
      //printf("keyval freed\n");
        MPI_Type_free_keyval(&flatlist_keyval);
    }
    CLARISSE_UNUSED(type);
    return MPI_SUCCESS;
}




//struct dllist flatlist = DLLIST_INITIALIZER;

void free_datatype(MPI_Datatype type) {
    int ni, na, nd, combiner; 
    MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
    if (combiner != MPI_COMBINER_NAMED) {
      MPI_Type_free(&type);
    }
}


void free_flatlist(flatlist_node *flat){
  if (flat) {
      if (flat->blocklens) clarisse_free(flat->blocklens);
      if (flat->indices) clarisse_free(flat->indices);
      if (flat->incr_size) clarisse_free(flat->incr_size); 
      clarisse_free(flat);
  }
}

int contig_flatlist(flatlist_node *flat){
  return (flat->extent == flat->blocklens[0]);
}

flatlist_node *  get_flatlist(MPI_Datatype d){
  int foundflag;
  flatlist_node *flat;

  if (flatlist_keyval == MPI_KEYVAL_INVALID) {
    //printf("keyval allocated\n");
    MPI_Type_create_keyval(flatlist_copy_attr_function,
			   flatlist_delete_attr_function,
			   &flatlist_keyval,
			   &flatlist_global_refcount);
  }
    // get_attr recieves void**, while set_attr void*
  MPI_Type_get_attr(d, flatlist_keyval, &flat, &foundflag);
  if (!foundflag) {
    flat = (flatlist_node *) ROMIO_Flatten_datatype(d);
    // Add flat as an attibute of the datatype 
    flatlist_global_refcount ++;
    MPI_Type_set_attr(d, flatlist_keyval, flat);
  }
  assert(flat->extent >= 0);
  return flat;
}

flatlist_node *  flat_vector(long long int count, long long int blocklength, long long int stride){
  flatlist_node *flat;
  int i;

  assert(count >= 1);

  if (blocklength == stride) {
    blocklength *= count;
    stride *= count;
    count = 1;
  }
  flat = (flatlist_node *) clarisse_malloc(sizeof(flatlist_node));
  flat->refcount = 1;
  flat->count = count;
  flat->extent = (count - 1) *  stride + blocklength;
  alloc_flat_arrays(flat);
  for (i = 0; i < count; i++) {
    flat->blocklens[i] = blocklength;
    flat->indices[i] = i * stride;
    flat->incr_size[i] = i * blocklength;
  }
  flat->lb = 0;
  flat->id = -1;
  return flat;
}

/*
void delete_flatlist(int id)
{
  flatlist_node *flat = remove_flatlist(id);
  // flat is NULL if refcount was > 1
  if (flat)
    free_flatlist(flat);
}
*/

void print_dt(MPI_Datatype type) {
  static int nest=0;
  int ni, na, nd, combiner, *i,k; 
  MPI_Aint *a; 
  MPI_Datatype *d; 

  MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
  if (combiner != MPI_COMBINER_NAMED){
    if (ni > 0) i = (int *) clarisse_malloc(ni*sizeof(int));
    if (na > 0) a = (MPI_Aint *) clarisse_malloc(na*sizeof(MPI_Aint));
    if (nd > 0) d = (MPI_Datatype *) clarisse_malloc(nd*sizeof(MPI_Datatype));
    MPI_Type_get_contents(type, ni, na, nd, i, a, d); 
  }
  
  for (k=0;k<nest;k++) printf("  ");

  switch (combiner) {
  case  MPI_COMBINER_NAMED:
    printf("NAMED DTYPE=%x\n",type);
    break;
  case MPI_COMBINER_DUP:
    printf("DUP DTYPE\n");
    break;
  case MPI_COMBINER_CONTIGUOUS: 
    printf("CONTIGUOUS DTYPE count=%d\n",i[0]);
    break;
  case MPI_COMBINER_VECTOR:
    printf("VECTOR DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],i[2]);
    break;
  case MPI_COMBINER_HVECTOR_INTEGER:
    printf("HVECTOR_INTEGER DTYPE count=%d blocklen=%d stride=%lld \n",i[0],i[1],(long long int)a[0]);
    break;
  case MPI_COMBINER_HVECTOR: 
    printf("HVECTOR DTYPE count=%d blocklen=%d stride=%lld \n",i[0],i[1],(long long int)a[0]);
    break;
  case MPI_COMBINER_INDEXED:
    printf("INDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],i[1+i[0]+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_HINDEXED_INTEGER:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%lld ",k,i[1+k],(long long int)a[k]); 
    printf("\n");
    break;
    
  case MPI_COMBINER_HINDEXED:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%lld ",k,i[1+k],(long long int)a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_INDEXED_BLOCK:
    printf("HINDEXED_BLOCK DTYPE count=%d blocklen=%d ",i[0],i[1]);
    for (k=0;k<i[0];k++) 
      printf("[%d]disp=%d ",k,i[2+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_STRUCT_INTEGER:
  case MPI_COMBINER_STRUCT:
    printf("STRUCT DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%lld ",k,i[1+k],(long long int)a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_SUBARRAY:
    printf("SUBARRAY DTYPE ndims=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]size=%d subsize=%d start=%d",k,i[1+k],i[1+i[0]+k],i[1+2*i[0]+k]); 
 
    printf("order=%d\n",i[1+3*i[0]]);
    break;
  case MPI_COMBINER_DARRAY:
    printf("DARRAY DTYPE size=%d rank=%d ndims=%d ",i[0],i[1],i[2]);
    /*for (k=0;k<i[0];k++) 
      printf("[%d]gsize=%d distrib=%d darg=%d psize=%d",k,i[3+k],i[3+i[0]+k],i[3+2*i[0]+k],i[3+3*i[0]+k] );*/ 
    for (k=0;k<i[2];k++) 
      printf("[%d] gsize=%d distrib=%d darg=%d psize=%d ", k, i[3+k], i[3+i[2]+k], i[3+2*i[2]+k], i[3+3*i[2]+k] ); 
    printf("order=%d\n",i[3+4*i[2]]);
    break;
  case MPI_COMBINER_RESIZED:
     printf("RESIZED DTYPE lb=%d, extent=%d\n",i[0],i[1]);
     break;
  default:
    handle_error(MPI_ERR_LASTCODE,"Error: unkonwn combiner\n");
  }
  nest++;
  for (k=0;k<nd;k++)
    print_dt(d[k]);
  nest--;
  //  printf("DTYPE=%x\n",type);

  if (ni > 0) clarisse_free(i);  
  if (na > 0) clarisse_free(a);  
  if (nd > 0) clarisse_free(d);
  
}

void print_flatlist(flatlist_node *flat) {
  long long int i;
  printf ("FLATLIST %d: \n", flat->id);
  for (i=0; i < flat->count; i++) {
    printf ("\t%3lld-th: offset=%8lld len=%8lld incr_size=%8lld\n", i, flat->indices[i], flat->blocklens[i], flat->incr_size[i]); 
  }
}

/*
void print_flatlist_list() {
  struct dllist_link * aux;
  printf("PRINT FLATLIST LIST\n");
  for(aux = flatlist.head; aux != NULL; aux = aux->next) {
    flatlist_node *e = (flatlist_node *)DLLIST_ELEMENT(aux, flatlist_node, link);
    print_flatlist(e);
  }
}
*/

/* New function in ../test/flatten/datatypes.c */
void draw_flatlist(flatlist_node *flat, int rep, long long int displ) {
  long long int i,k,j;
  int line=100, idx=0, maxdraw=5000;

  printf("\n");
  for (i=0;i<line;i++)
    if (!(i%10))
      printf("%d",(int)i/10);
    else
      printf(" ");
  printf("\n");
  for (i=0;i< line ;i++)
    printf("%d",(int)i%10);
  printf("\n");

  for (i=0; i < displ + flat->indices[0]; i++) { 
      printf("_");
      if (!((++idx)%line)) { printf("\n"); }
      if (idx == maxdraw) return;
  }
  for (j = 0; j < rep; j++)
    for (i=0; i < flat->count; i++) {
	for (k=0;k < flat->blocklens[i]; k++) {
	    printf("*");
	    if (!((++idx)%line)) { printf("\n"); }
	    if (idx == maxdraw) return;
	}
	if (i < flat->count - 1)
	    for (k=0;k < flat->indices[i+1] - flat->indices[i] - flat->blocklens[i] ; k++)  {
		printf("_");
		if (!((++idx)%line)) { printf("\n"); }
		if (idx == maxdraw) return;
	    }
	 if (i  == flat->count - 1)
	    for (k=0;k < flat->extent - flat->indices[i] - flat->blocklens[i] ; k++)  {
		printf("_");
		if (!((++idx)%line)) { printf("\n"); }
		if (idx == maxdraw) return;
	    }


    }
  printf("\n");
}

// Returns 0 if equal and -1 if different
int cmp_flatlists(flatlist_node *flat_buf1, flatlist_node *flat_buf2) {
  long long int i;
  
  /*  if (flat_buf1->displ != flat_buf2->displ){
    printf("cmp_flatlists:Different displ\n");
    return -1;
    }*/
  /*  if (flat_buf1->rep != flat_buf2->rep){
    printf("cmp_flatlists:Different rep\n");
    return -1;
    }*/
  if (flat_buf1->count != flat_buf2->count) {
     printf("cmp_flatlists:Different number of flattened elements\n");
     return -1;
  }
  for (i=0; i < flat_buf1->count; i++) 
    if ((flat_buf1->indices[i] != flat_buf2->indices[i])||
	(flat_buf1->blocklens[i] != flat_buf2->blocklens[i])) {
      printf("cmp_flatlists:Different number of indices or blocklens\n");
      return -1;
    }
  return 0;
}

void distrib_init(distrib *distr, MPI_Datatype dt, long long int displ){
  distr->flat = get_flatlist(dt);
  distr->displ = distr->flat->lb + displ; //lb + displ
}




distrib *distrib_get(MPI_Datatype dt, long long int displ) {
  distrib *distr = (distrib *) clarisse_malloc(sizeof(distrib));
  if (distr) 
    distrib_init(distr, dt, displ);
  else
    handle_error(MPI_ERR_OTHER,"distrib_get: Out of memory");
  return distr;
}

distrib *distrib_get2(flatlist_node *flat, long long int displ){
  distrib *distr = (distrib *) clarisse_malloc(sizeof(distrib));
  if (distr) { 
    distr->flat = flat;
    distr->displ = flat->lb + displ; //lb + displ
  }
  else
    handle_error(MPI_ERR_OTHER,"distrib_get2: Out of memory");
  return distr;
}



void distrib_free_content(distrib *distr){
  CLARISSE_UNUSED(distr);
  //delete_flatlist(distr->flat->id);
}

void distrib_free(distrib *distr) {
  distrib_free_content(distr);
  clarisse_free(distr);
}

void distrib_update(distrib *distr, MPI_Datatype dt, long long int displ) {
  free_datatype(distr->flat->id);
  distrib_free_content(distr);
  distrib_init(distr, dt, displ);
}


void distrib_print(distrib *distr) {
  printf("PRINT DISTRIB\n\tdispl=%lld\n",distr->displ);
  print_flatlist(distr->flat);
}

void distrib_draw(distrib *distr, int rep){
  printf("DRAW DISTRIB\n\tdispl=%lld\n",distr->displ);
  draw_flatlist(distr->flat, rep, distr->displ);
}

int distrib_contig(distrib *distr) {
  return (contig_flatlist(distr->flat));// && (distr->displ == 0));
}
