#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "map.h"

/* Scatter/gather test */

MPI_Datatype get_contig();    //0
MPI_Datatype get_lb_contig(); //1
MPI_Datatype get_contig_ub(); //2
MPI_Datatype get_simple_vec(); //3
MPI_Datatype get_displ_simple_vec(); //4
MPI_Datatype get_simple_struct(); //5
MPI_Datatype get_displ_displ_struct(); //6
MPI_Datatype get_vect_struct(); //7
MPI_Datatype get_darray();

#define MAX_CASES_PER_TEST 1024

int test_gather_mem(const char *funname, char *testname, int test_cnt, MPI_Datatype *dt, long long int *l, long long int *r, char* inbuf, int *expected_cnt,char**expected_outbuf) {
  int i, ret = 0;
  distrib *distr = distrib_get(*dt, 0);
  
  assert(test_cnt <= MAX_CASES_PER_TEST);

  for (i=0; i<test_cnt; i++) {
    char outbuf[1024];
    int output = sg_mem(outbuf, inbuf, l[i],r[i], distr, GATHER);
    if (output != expected_cnt[i] || strncmp(outbuf, expected_outbuf[i],output)){
      outbuf[expected_cnt[i]]='\0';
      printf("sg_mem test %s:%s failed on inputs l=%lld r=%lld inbuf=%s with output cnt=%d (expected %d) outbuf=%s (expected %s)!!!\n", funname, testname, l[i], r[i], inbuf, output, expected_cnt[i], outbuf, expected_outbuf[i] );
      distrib_print(distr);
      distrib_draw(distr, 1);
      ret = -1;
      break;
    }
  }
  distrib_free(distr);
  MPI_Type_free(dt);
  return ret; 
}

int test_scatter_mem(const char *funname, char *testname, int test_cnt, MPI_Datatype *dt, long long int *l, long long int *r, char* outbuf, int *expected_cnt,char**expected_inbuf) {
  int i, ret = 0;
  distrib *distr = distrib_get(*dt, 0);
  
  assert(test_cnt <= MAX_CASES_PER_TEST);

  for (i=0; i<test_cnt; i++) {
    char inbuf[1024];
    int j, output;
    for (j = 0;j < 1024; j++) inbuf[j]='-';
    output = sg_mem(outbuf, inbuf, l[i],r[i], distr, SCATTER);
    if (output != expected_cnt[i] || strncmp(inbuf, expected_inbuf[i],output)){
      inbuf[r[i]+1]='\0';
      printf("sg_mem test %s:%s failed on inputs l=%lld r=%lld outbuf=%s with output cnt=%d (expected %d) inbuf=%s (expected %s)!!!\n", funname, testname, l[i], r[i], outbuf, output, expected_cnt[i], inbuf, expected_inbuf[i] );
      distrib_print(distr);
      distrib_draw(distr, 1);
      ret = -1;
      break;
    }
  }
  distrib_free(distr);
  MPI_Type_free(dt);
  return ret; 
}



//  0         1         
//  012345678901234
//  **        
int test1_0a() {
  MPI_Datatype dt = get_contig();
  char inbuf[]="01234567890";
  long long int l[]=      {0,1,2,3,4,5};
  long long int r[]=      {4,4,4,4,4,4};
  int  expected_cnt[]=    {5,4,3,2,1,0};
  char *expected_outbuf[]={"01234","1234","234","34","4",""};
  return test_gather_mem(__func__,"CONTIG",6, &dt, l,r,inbuf,expected_cnt, expected_outbuf);
}

int test1_0b() {
  MPI_Datatype dt = get_contig();
  char outbuf[]="01234567890";
  long long int l[]=      {0,1,2,3,4,5};
  long long int r[]=      {4,4,4,4,4,4};
  int  expected_cnt[]=    {5,4,3,2,1,0};
  char *expected_inbuf[]={"01234","-0123","--012","---01","----0","-----"};
  return test_scatter_mem(__func__,"CONTIG",6, &dt, l,r,outbuf,expected_cnt, expected_inbuf);
}

//  0         1         
//  012345678901234
//  -------[---****                 
int test1_1a() {
  MPI_Datatype dt = get_lb_contig();
  char inbuf[]="012345678901234567890";
  long long int l[]=      {10,11,12,13,14,15};
  long long int r[]=      {14,14,14,14,14,14};
  int  expected_cnt[]=    {4,4,3,2,1,0};
  char *expected_outbuf[]={"1234","1234","234","34","4",""};
  return test_gather_mem(__func__,"LB CONTIG",6, &dt, l,r,inbuf,expected_cnt, expected_outbuf);
}

int test1_1b() {
  MPI_Datatype dt = get_lb_contig();
  char outbuf[]="012345678901234567890";
  long long int l[]=      {10,11,12,13,14,15};
  long long int r[]=      {14,14,14,14,14,14};
  int  expected_cnt[]=    {4,4,3,2,1,0};
  char *expected_inbuf[]={"----------0123","----------0123","-----------012",
			  "------------01","-------------0","--------------"};
  return test_scatter_mem(__func__,"LB CONTIG",6, &dt, l,r,outbuf,expected_cnt, expected_inbuf);
}


//  0         1         
//  012345678901234
//  ---****---]        
int test1_2a() {
  MPI_Datatype dt = get_contig_ub();
  char inbuf[]="012345678901234567890";
  long long int l[]=      {0,2,3,6,7,8};
  long long int r[]=      {5,6,7,7,7,11};
  int  expected_cnt[]=    {3,4,4,1,0,2};
  char *expected_outbuf[]={"345","3456","3456","6","","01"};
  return test_gather_mem(__func__,"UB CONTIG",6, &dt, l,r,inbuf,expected_cnt, expected_outbuf);
}

int test1_2b() {
  MPI_Datatype dt = get_contig_ub();
  char outbuf[]="012345678901234567890";
  long long int l[]=      {0,2,3,6,7,8};
  long long int r[]=      {5,6,7,7,7,11};
  int  expected_cnt[]=    {3,4,4,1,0,2};
  char *expected_inbuf[]={"---012","---0123","---0123-",
			  "------0-","--------","----------01"};
  return test_scatter_mem(__func__,"UB CONTIG",6, &dt, l,r,outbuf,expected_cnt, expected_inbuf);
}

//  0         1         
//  012345678901234
//  *-**-**-**-*        
int test1_3a() {
  MPI_Datatype dt = get_simple_vec();
  char inbuf[]="012345678901234567890";
  long long int l[]=      {0,2,3,6,7,8};
  long long int r[]=      {5,6,7,7,7,11};
  int  expected_cnt[]=    {4,4,3,1,0,3};
  char *expected_outbuf[]={"0235","2356","356","6","","891"};
  return test_gather_mem(__func__,"SIMPLE VEC",6, &dt, l,r,inbuf,expected_cnt, expected_outbuf);
}

int test1_3b() {
  MPI_Datatype dt = get_simple_vec();
  char outbuf[]="012345678901234567890";
  long long int l[]=      {0,2,3,6,7,8};
  long long int r[]=      {5,6,7,7,7,11};
  int  expected_cnt[]=    {4,4,3,1,0,3};
  char *expected_inbuf[]={"0-12-3","--01-23","---0-12-",
			  "------0-","-------","-------01-2"};
  return test_scatter_mem(__func__,"SIMPLE VEC",6, &dt, l,r,outbuf,expected_cnt, expected_inbuf);
}

// 0         1         
// 012345678901234
// ___[_*_**_*____]
int test1_4a() {
  MPI_Datatype dt = get_displ_simple_vec();
  char inbuf[]="012345678901234567890";
  long long int l[]=      {0,2,3,6,7,8};
  long long int r[]=      {5,6,7,7,7,11};
  int  expected_cnt[]=    {1,1,2,1,1,2};
  char *expected_outbuf[]={"5","5","57","7","7","80"};
  return test_gather_mem(__func__,"DISPL SIMPLE VEC",6, &dt, l,r,inbuf,expected_cnt, expected_outbuf);
}

int test1_4b() {
  MPI_Datatype dt = get_displ_simple_vec();
  char outbuf[]="012345678901234567890";
  long long int l[]=      {0,2,3,6,7,8};
  long long int r[]=      {5,6,7,7,7,11};
  int  expected_cnt[]=    {1,1,2,1,1,2};
  char *expected_inbuf[]={"-----0","-----0-","-----0-1",
			  "-------0","-------0","--------0-1-"};
  return test_scatter_mem(__func__,"DISPL SIMPLE VEC",6, &dt, l,r,outbuf,expected_cnt, expected_inbuf);
}

// 0         1         
// 01234567890123456789
// _______[___****____]

int test1_5a() {
  MPI_Datatype dt = get_simple_struct();
  char inbuf[]="012345678901234567890";
  long long int l[]=      {10,11,12,13,14,15};
  long long int r[]=      {14,14,14,14,14,14};
  int  expected_cnt[]=    {4,4,3,2,1,0};
  char *expected_outbuf[]={"1234","1234","234","34","4",""};
  return test_gather_mem(__func__,"SIMPLE STRUCT",6, &dt, l,r,inbuf,expected_cnt, expected_outbuf);
}

int test1_5b() {
  MPI_Datatype dt = get_simple_struct();
  char outbuf[]="012345678901234567890";
  long long int l[]=      {10,11,12,13,14,15};
  long long int r[]=      {14,14,14,14,14,14};
  int  expected_cnt[]=    {4,4,3,2,1,0};
  char *expected_inbuf[]={"----------0123","----------0123","-----------012",
			  "------------01","-------------0","--------------"};
  return test_scatter_mem(__func__,"SIMPLE STRUCT",6, &dt, l,r,outbuf,expected_cnt, expected_inbuf);
}

// 0         1         
// 01234567890123456789
// ________************
int test1_6a() {
  MPI_Datatype dt = get_displ_displ_struct();
  char inbuf[]="012345678901234567890";
  long long int l[]=      {10,11,12,13,14,15};
  long long int r[]=      {14,14,14,14,14,14};
  int  expected_cnt[]=    {5,4,3,2,1,0};
  char *expected_outbuf[]={"01234","1234","234","34","4",""};
  return test_gather_mem(__func__,"DISPL DISPL STRUCT",6, &dt, l,r,inbuf,expected_cnt, expected_outbuf);
}

int test1_6b() {
  MPI_Datatype dt = get_displ_displ_struct();
  char outbuf[]="012345678901234567890";
  long long int l[]=      {10,11,12,13,14,15};
  long long int r[]=      {14,14,14,14,14,14};
  int  expected_cnt[]=    {5,4,3,2,1,0};
  char *expected_inbuf[]={"----------01234","-----------0123","------------012",
			  "-------------01","--------------0","---------------"};
  return test_scatter_mem(__func__,"DISPL DISPL STRUCT",6, &dt, l,r,outbuf,expected_cnt, expected_inbuf);
}


/*

// 0         1         2         3         4         5         6         7         8         9         
// 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
// ********____****____****____****____*_______________________________________________________________
// ________________________****____****____****____*___________________________________________________
// ____________________________________****____****____****____*_______________________________________
// ________________________________________________****____****____****____*___________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// *___**__
//I:   12  
//O:   45  
int test1_7() {
  MPI_Datatype dt = get_vect_struct();
  long long int offset_input[]={0,7, 8,11,12,15,16,19,20, 21, 24, 25, 33, 50, 59,  60,  61,  62,  63, 112};
  int  expected_output[] =  {0,7, 8,11,12,15,16,19,20, 21, 24, 25, 33, 50, 59,  60,  61,  62,  63, 112}; 
  return test_sg_mem(__func__,"VECT STRUCT",18, &dt, offset_input, expected_output);
}

*/

int main(int argc, char* argv[]) {
    int ret;
    MPI_Init(&argc, &argv);

    ret = test1_0a();
    if (ret >= 0) ret = test1_0b();
    if (ret >= 0) ret = test1_1a();       
    if (ret >= 0) ret = test1_1b();
    if (ret >= 0) ret = test1_2a();     
    if (ret >= 0) ret = test1_2b();
    if (ret >= 0) ret = test1_3a();
    if (ret >= 0) ret = test1_3b();
    if (ret >= 0) ret = test1_4a();
    if (ret >= 0) ret = test1_4b();
    if (ret >= 0) ret = test1_5a();
    if (ret >= 0) ret = test1_5b();
    if (ret >= 0) ret = test1_6a();
    if (ret >= 0) ret = test1_6b();
    /*
    if (ret >= 0) ret = test1_7a();
    if (ret >= 0) ret = test1_7b();

    */
    if (ret >= 0) printf ("sg_mem tests passed!!\n");

    MPI_Finalize();
    return ret;
}  // main 
