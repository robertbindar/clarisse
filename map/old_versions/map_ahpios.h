#ifndef _MAP_H
#define _MAP_H

#include <stdio.h>
//#include "mpi.h"
#include "adio.h"
#include "adioi.h"
#include <math.h>
#include <sys/types.h>
#include <unistd.h>

void print_flatlist(MPI_Datatype dt);
void draw_flatlist(MPI_Datatype dt, ADIO_Offset disp, int n);
int cmp_flatlists(MPI_Datatype dt1,MPI_Datatype dt2);
void handle_error(int errcode, char *str);
void print_dt(MPI_Datatype type);

/*#ifndef handle_error
#define ERROR(args...) \
do { \
  fprintf(stderr, "ERROR (%s:%d): ", __FILE__, __LINE__); \
  fprintf(stderr, args); \
} while(0)
#define  handle_error(errcode, string) {ERROR(string);handle_err(errcode, "");}
#endif
*/
#define ERROR(args...) \
do { \
  fprintf(stderr, "ERROR (%s:%d): ", __FILE__, __LINE__); \
  fprintf(stderr, args); \
} while(0)
#define handle_err(errcode, string) {ERROR(string);handle_error(errcode, "");}

typedef struct distrib{
  ADIO_Offset disp;
  MPI_Datatype filetype;
  ADIOI_Flatlist_node *flat_buf;
} distrib, *distrib_p;


ADIO_Offset func_sup( ADIO_Offset x,distrib_p d);
ADIO_Offset func_inf( ADIO_Offset x,distrib_p d);
ADIO_Offset func_1( ADIO_Offset x,distrib_p d);
ADIO_Offset get_count(ADIO_Offset l, ADIO_Offset r, distrib_p d);
#define get_next_data_l(l,d) func_1(func_sup((l),(d)),(d))
#define get_prev_data_r(r,d) func_1(func_inf((r),(d)),(d))

int find_size_bin_search(ADIO_Offset x, ADIOI_Flatlist_node *flat_buf);
int find_bin_search(ADIO_Offset x, ADIOI_Flatlist_node *flat_buf);
ADIOI_Flatlist_node * get_flatlist(MPI_Datatype dt);
distrib_p alloc_init_distrib(ADIO_Offset disp, MPI_Datatype dt);
void free_distrib(distrib_p d);
void update_distrib(distrib_p d, ADIO_Offset disp, MPI_Datatype dt);
void init_distrib(distrib_p d, ADIO_Offset disp, MPI_Datatype dt);


#define BEGIN_BLOCK(x,block_size) (((x)/(block_size))*(block_size))
#define END_BLOCK(x,block_size) (((x)/(block_size)+1)*(block_size)-1)
#define BEGIN_NEXT_BLOCK(x,block_size) (((x)/(block_size)+1)*(block_size))

#define SCATTER 0
#define GATHER 1
#define SCATTER_NOCOPY 2
#define GATHER_NOCOPY 3

int sg_mem (char *buf, ADIO_Offset l, ADIO_Offset r,  distrib_p d, char*out_buf, int type) ;
int sg_file (int fd, ADIO_Offset file_disp, ADIO_Offset l, ADIO_Offset r, distrib_p d, char*out_buf, int type);
int writen(int fd, const void *vptr, int n);
int writen_at(int fd, const void *vptr, int n, ADIO_Offset offset);
int readn(int fd, void *vptr, size_t n);
int readn_at(int fd, void *vptr, int n, ADIO_Offset offset);
#endif
int get_fsize(int fd);
