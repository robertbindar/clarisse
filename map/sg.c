#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "map.h"
#include "util.h"
//#include "adio.h"
//#include "adio_extern.h"

void handle_error(int errcode, char *str);

// Scatters/gathers data from/into outbuf 
// type: SCATTER/GATHER copy data
// type: SCATTER_NOCOPY/GATHER_NOCOPY save only the pointers to the data and 
//      blocklens from inbuf (can be used in hindexed operation)
//        
// l,r : absolute positions from the beginning of buffer buf
//        extent=7 
//disp=1 lb=4 up=12
//       -------------------
//   |   |     |     |     |
//   v   v     v     v     v
//  ______***____***____***___
//  0123456790123456790123456790
//  |--------------------------->
//  l=0 ->
// sg_mem(5,15) copies bytes 6, 7, 9, 14, 15 into outbuf 
//
// NOTE: If inbuf (for GATHER) or outbuf (for SCATTER) 
// ?? inbuf for both GATHER and SCATTER ??
// are buffers whose addresses are not 
// the beginning of the space, I need to substract the displacement
// from inbuf or outbuf before calling sg_mem
long long int sg_mem (char *outbuf, char *inbuf, 
	    long long int l, long long int r, distrib *distr, 
	    int type) {
  long long int cnt = 0;
  flatlist_node *flat = distr->flat;

  if (contig_flatlist(flat)) {
    cnt = r - l + 1;
    switch (type) {
    case SCATTER:
      //printf ("distr->displ %lld + BEGIN_BLOCK(l,extent) %lld + rel_l %lld  l %lld\n",distr->displ,  BEGIN_BLOCK(l,extent), rel_l,l);
      memcpy(inbuf + l, outbuf,  cnt);
      break;
    case GATHER:
      memcpy(outbuf, inbuf + l, cnt);
      break;
    }
    
  }
  else {
    if (r < distr->displ) return 0;
    else r -= distr->displ;
    l = (l < distr->displ) ? 0 : l - distr->displ;
    if (l > r) return 0;

    
    while (l <= r) {
      int i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      // if l is posisioned before or at the last active byte of current extent 
      if (i < flat->count ) {
	long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	
	while (rel_l <= rel_r) {
	  long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	  switch (type) {
	  case SCATTER:
	    //printf ("distr->displ %lld + BEGIN_BLOCK(l,extent) %lld + rel_l %lld  l %lld\n",distr->displ,  BEGIN_BLOCK(l,extent), rel_l,l);
	    memcpy(inbuf + distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l, outbuf + cnt,  crt_r - rel_l + 1);
	    break;
	  case GATHER:
	    memcpy(outbuf + cnt, inbuf + distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l, crt_r - rel_l + 1);
	    break;
	  case SCATTER_NOCOPY:
	  case GATHER_NOCOPY:
	    //update_net_dt((struct net_dt *) outbuf, inbuf + distr->displ + BEGIN_BLOCK(l, extent) + rel_l, crt_r - rel_l + 1);
	    break;
	  }	
	  cnt += (crt_r - rel_l + 1);
	  if ((++i) < flat->count )
	    rel_l =  flat->indices[i];
	  else
	    break;
	}
      }
      l = BEGIN_NEXT_BLOCK(l, flat->extent);
    }
  }
  return cnt;
}

// No minimum and no update of interval (start_l)
int get_offs_lens(long long int l, long long int r, distrib *distr, 
		  long long int **offs, long long int **lens, 
		  int *data_size) {
  
  *data_size = 0;
  if (l > r) {
    *offs = NULL;
    *lens = NULL;
    return 0;
  }
  else {
    flatlist_node *flat = distr->flat;

    if (contig_flatlist(flat)) {
      *offs = (long long int *)clarisse_malloc(sizeof(long long int));
      *lens = (long long int *)clarisse_malloc(sizeof(long long int));
      (*offs)[0] = l;
      (*lens)[0] = r - l + 1; 
      (*data_size) += ((*lens)[0]);
      return 1;
    }
    else {
      int idx = 0, max_idx, i; 
      if (r < distr->displ) return 0;
      else r -= distr->displ;
      l = (l < distr->displ) ? 0 : l - distr->displ;
      if (l > r) return 0;
      
      max_idx = distr->flat->count * CEIL_INT((r - BEGIN_BLOCK(l,  flat->extent) + 1), distr->flat->extent);
      *offs = (long long int *)clarisse_malloc(sizeof(long long int) * max_idx);
      *lens = (long long int *)clarisse_malloc(sizeof(long long int) * max_idx);
      i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      while (l <= r) {
	//int i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
	// if l is posisioned before or at the last active byte of current extent 
	if (i < flat->count ) {
	  long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	  long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	  while (rel_l <= rel_r) {
	    long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	    (*data_size) += (crt_r - rel_l + 1);
	    if ((idx > 0) && 
		((*offs)[idx - 1] +  (*lens)[idx -1 ] == distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l)) {
	      (*lens)[idx - 1] = (*lens)[idx - 1] + crt_r - rel_l + 1;
	    }
	    else {
	      (*offs)[idx] = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l;
	      (*lens)[idx] = crt_r - rel_l + 1;
	      idx++;
	      assert(idx <= max_idx);
	    }
	    if ((++i) < flat->count )
	      rel_l =  flat->indices[i];
	    else
	      break;
	  }
	}
	l = BEGIN_NEXT_BLOCK(l, flat->extent);
	i = 0;
      } //while (l <= r)
      
      if (idx == 0) {
	clarisse_free(*offs);
	clarisse_free(*lens);
	*offs = NULL;
	*lens = NULL;
      }
      return idx;
    }
  }
}

int get_offs_lens2(long long int *start_l, long long int r, distrib *distr, 
		   long long int **offs, long long int **lens,
		   int max_size,
		   int *data_size, int *metadata_size) {
  
  long long int l = *start_l;

  *data_size = 0;
  *metadata_size = 0;
  if (l > r) {
    *offs = NULL;
    *lens = NULL;
    return 0;
  }
  else {
    flatlist_node *flat = distr->flat;

    if (contig_flatlist(flat)) {
      if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) 
	return 0;
      else {
	int crt_data_size;

	*offs = (long long int *)clarisse_malloc(sizeof(long long int));
	*lens = (long long int *)clarisse_malloc(sizeof(long long int));
	
	(*metadata_size) += (2 * sizeof(long long int));
	crt_data_size = MIN(r - l + 1, max_size); 
	if (*data_size + *metadata_size + crt_data_size > max_size)
	  crt_data_size = max_size - *data_size - *metadata_size;
	(*offs)[0] = l;
	(*lens)[0] = crt_data_size; 
	(*data_size) += ((*lens)[0]);	
	(*start_l) += crt_data_size;
	return 1;
      }
    }
    else {
      int idx = 0, max_idx, i; 
      int finished = 0;

      if (r < distr->displ) return 0;
      else r -= distr->displ;
      l = (l < distr->displ) ? 0 : l - distr->displ;
      if (l > r) return 0;
      
      max_idx = distr->flat->count * CEIL_INT((r - BEGIN_BLOCK(l,  flat->extent) + 1), distr->flat->extent);
      *offs = (long long int *)clarisse_malloc(sizeof(long long int) * max_idx);
      *lens = (long long int *)clarisse_malloc(sizeof(long long int) * max_idx);
      i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      while ((l <= r) && (!finished)){
	// Can probably be moved outside loop and add an i = 0 at the end of the loop
	//int i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
	// if l is posisioned before or at the last active byte of current extent 
	if (i < flat->count ) {
	  long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	  long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	  while (rel_l <= rel_r) {
	    
	    long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	    int crt_data_size = crt_r - rel_l + 1;
	    // contiguous blocks, join them together: update length
	    if ((idx > 0) && 
		((*offs)[idx - 1] +  (*lens)[idx -1 ] == distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l)) {
	      if (*data_size + *metadata_size + crt_data_size > max_size) { 
		crt_data_size = max_size - *data_size - *metadata_size; 
		//crt_data_size can be 0 
		//assert(crt_data_size > 0);
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
		*start_l = (*offs)[idx - 1] + (*lens)[idx - 1];
		finished = 1;
	      }
	      else
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
	    }
	    // discontiguous blocks: save both offset length
	    else {
	      // I need at least one offset, one length and one byte 
	      if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) {
		*start_l = distr->displ + BEGIN_BLOCK(l,  flat->extent) /* l*/ + rel_l;
		finished = 1;
		break;
	      }
	      else {
		(*metadata_size) += (2 * sizeof(long long int));
		(*offs)[idx] = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l;
		if (*data_size + *metadata_size + crt_data_size > max_size) {
		  crt_data_size = max_size - *data_size - *metadata_size;
		  assert(crt_data_size > 0);
		  (*lens)[idx] = crt_data_size;
		  *start_l = (*offs)[idx] + (*lens)[idx];
		  finished = 1;
		}
		else 
		  (*lens)[idx] = crt_r - rel_l + 1;
		idx++;
		assert(idx <= max_idx);
	      }
	    }
	    (*data_size) += crt_data_size;
	    if (finished)
	      break;
	    else
	      if ((++i) < flat->count )
		rel_l =  flat->indices[i];
	      else
		break;
	    
	  } // while
	}
	if (!finished)
	  l = BEGIN_NEXT_BLOCK(l, flat->extent);
	i = 0;
      } // while ((*start_l <= r) && (!finished)) 
      
      if (idx == 0) {
	clarisse_free(*offs);
	clarisse_free(*lens);
	*offs = NULL;
	*lens = NULL;
      }
      else 
	if (!finished)
	  *start_l = distr->displ + r + 1;
      return idx;
    }
  }
}

// returns a list instead of an array

int get_offs_lens3(long long int *start_l, long long int r, distrib *distr, 
		   struct dllist *off_len_list,
		   int max_size, int *data_size, int *metadata_size) {
  
  long long int l = *start_l;

  dllist_init(off_len_list);
  //off_len_node_t 

  *data_size = 0;
  *metadata_size = 0;
  if (l > r) {
    return 0;
  }
  else {
    flatlist_node *flat = distr->flat;

    if (contig_flatlist(flat)) {
      if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) 
	return 0;
      else {
	int crt_data_size;
	off_len_node_t *off_len_node;
	off_len_node = (off_len_node_t *) clarisse_malloc(sizeof(off_len_node_t));
	
	(*metadata_size) += (2 * sizeof(long long int));
	crt_data_size = MIN(r - l + 1, max_size); 
	if (*data_size + *metadata_size + crt_data_size > max_size)
	  crt_data_size = max_size - *data_size - *metadata_size;
	off_len_node->off = l;
	off_len_node->len = crt_data_size; 
	dllist_iat(off_len_list, &off_len_node->link);
	(*data_size) += off_len_node->len;
	(*start_l) += crt_data_size;
	return 1;
      }
    }
    else {
      int idx = 0, max_idx, i; 
      int finished = 0;

      if (r < distr->displ) return 0;
      else r -= distr->displ;
      l = (l < distr->displ) ? 0 : l - distr->displ;
      if (l > r) return 0;
      
      max_idx = distr->flat->count * CEIL_INT((r - BEGIN_BLOCK(l,  flat->extent) + 1), distr->flat->extent);
      i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      while ((l <= r) && (!finished)){
	// Can probably be moved outside loop and add an i = 0 at the end of the loop
	//int i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
	// if l is posisioned before or at the last active byte of current extent 
	if (i < flat->count ) {
	  long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	  long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	  while (rel_l <= rel_r) {
	    off_len_node_t *off_len_node;
	    long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	    int crt_data_size = crt_r - rel_l + 1;
	    // contiguous blocks, join them together: update length
	    if (idx > 0)
	       off_len_node = DLLIST_ELEMENT(off_len_list->tail, off_len_node_t, link);
	    if ((idx > 0) && 
		(off_len_node->off + off_len_node->len == distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l)) {
	      	     
	      if (*data_size + *metadata_size + crt_data_size > max_size) { 
		crt_data_size = max_size - *data_size - *metadata_size; 
		//crt_data_size can be 0 
		//assert(crt_data_size > 0);
		off_len_node->len += crt_data_size;
		*start_l = off_len_node->off + off_len_node->len;
		finished = 1;
	      }
	      else
		off_len_node->len += crt_data_size;
	    }
	    // discontiguous blocks: save both offset length
	    else {
	      // I need at least one offset, one length and one byte 
	      if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) {
		*start_l = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l; //distr->displ + l;
		finished = 1;
		break;
	      }
	      else {
		off_len_node_t *off_len_node;
		off_len_node = (off_len_node_t *) clarisse_malloc(sizeof(off_len_node_t));
		(*metadata_size) += (2 * sizeof(long long int));
		off_len_node->off = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l;
		if (*data_size + *metadata_size + crt_data_size > max_size) {
		  crt_data_size = max_size - *data_size - *metadata_size;
		  assert(crt_data_size > 0);
		  off_len_node->len = crt_data_size;
		  *start_l = off_len_node->off + off_len_node->len;
		  finished = 1;
		}
		else 
		  off_len_node->len = crt_r - rel_l + 1;
		dllist_iat(off_len_list, &off_len_node->link);
		idx++;
		assert(idx <= max_idx);
	      }
	    }
	    (*data_size) += crt_data_size;
	    if (finished)
	      break;
	    else
	      if ((++i) < flat->count )
		rel_l =  flat->indices[i];
	      else
		break;
	    
	  } // while
	}
	if (!finished)
	  l = BEGIN_NEXT_BLOCK(l, flat->extent);
	i = 0;
      } // while ((*start_l <= r) && (!finished)) 
      
      if ((idx >0 )&& (!finished))
	*start_l = distr->displ + r + 1;
      return idx;
    }
  }
}


//data does not occupy space in off-len lists
//BUT: off-len lists must fit in max-size
//AND: represented data should be less or equal that max:size
int get_offs_lens4(long long int *start_l, long long int r, distrib *distr, 
		   long long int **offs, long long int **lens,
		   int max_size,
		   int *data_size, int *metadata_size) {
  
  long long int l = *start_l;

  *data_size = 0;
  *metadata_size = 0;
  if (l > r) {
    *offs = NULL;
    *lens = NULL;
    return 0;
  }
  else {
    flatlist_node *flat = distr->flat;

    if (contig_flatlist(flat)) {
      if (max_size - *metadata_size < (int) (2 * sizeof(long long int))) 
      //if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) 
	return 0;
      else {
	int crt_data_size;

	*offs = (long long int *)clarisse_malloc(sizeof(long long int));
	*lens = (long long int *)clarisse_malloc(sizeof(long long int));
	
	(*metadata_size) += (2 * sizeof(long long int));
	crt_data_size = MIN(r - l + 1, max_size);; 
	//	if (*data_size + *metadata_size + crt_data_size > max_size)
	//  crt_data_size = max_size - *data_size - *metadata_size;
	(*offs)[0] = l;
	(*lens)[0] = crt_data_size; 
	(*data_size) += ((*lens)[0]);	
	(*start_l) += crt_data_size;
	return 1;
      }
    }
    else {
      int idx = 0, max_idx, i; 
      int finished = 0;

      if (r < distr->displ) return 0;
      else r -= distr->displ;
      l = (l < distr->displ) ? 0 : l - distr->displ;
      if (l > r) return 0;
      
      //max_idx = distr->flat->count * CEIL_INT((r - BEGIN_BLOCK(l,  flat->extent) + 1), distr->flat->extent);
      max_idx = max_size / (2 * sizeof(long long int));
      *offs = (long long int *)clarisse_malloc(sizeof(long long int) * max_idx);
      *lens = (long long int *)clarisse_malloc(sizeof(long long int) * max_idx);
      i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      while ((l <= r) && (!finished)){
	if (i < flat->count ) {
	  long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	  long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	  while (rel_l <= rel_r) {
	    
	    long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	    int crt_data_size = crt_r - rel_l + 1;
	    // contiguous blocks, join them together: update length
	    if ((idx > 0) && 
		((*offs)[idx - 1] +  (*lens)[idx -1 ] == distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l)) {
	      //if (*data_size + *metadata_size + crt_data_size > max_size) { 
	      if (*data_size + crt_data_size > max_size) {
		//crt_data_size = max_size - *data_size - *metadata_size; 
		crt_data_size = max_size - *data_size; 
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
		*start_l = (*offs)[idx - 1] + (*lens)[idx - 1];
		finished = 1;
	      }
	      else 
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
	    }
	    // discontiguous blocks: save both offset length
	    else {
	      // I need at least one offset and  one length 
	      if (max_size - *metadata_size < (int) (2 * sizeof(long long int))) {
	      //if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) {
		*start_l = distr->displ + BEGIN_BLOCK(l,  flat->extent) /* l*/ + rel_l;
		finished = 1;
		break;
	      }
	      else {
		(*metadata_size) += (2 * sizeof(long long int));
		(*offs)[idx] = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l;
		//if (*data_size + *metadata_size + crt_data_size > max_size) {
		if (*data_size +crt_data_size > max_size) {
		  //		  crt_data_size = max_size - *data_size - *metadata_size;
		  crt_data_size = max_size - *data_size;
		  assert(crt_data_size > 0);
		  (*lens)[idx] = crt_data_size;
		  *start_l = (*offs)[idx] + (*lens)[idx];
		  finished = 1;
		}
		else
		  (*lens)[idx] = crt_r - rel_l + 1;
		idx++;
		assert(idx <= max_idx);
	      }
	    }
	    (*data_size) += crt_data_size;
	    if (finished)
	      break;
	    else
	      if ((++i) < flat->count )
		rel_l =  flat->indices[i];
	      else
		break;
	    
	  } // while
	}
	if (!finished)
	  l = BEGIN_NEXT_BLOCK(l, flat->extent);
	i = 0;
      } // while ((*start_l <= r) && (!finished)) 
      
      if (idx == 0) {
	clarisse_free(*offs);
	clarisse_free(*lens);
	*offs = NULL;
	*lens = NULL;
      }
      else 
	if (!finished)
	  *start_l = distr->displ + r + 1;
      return idx;
    }
  }
}


// Returns offs lens as integers and relative to intial offset
int get_offs_lens5(long long int *start_l, long long int r, distrib *distr, 
		   long long int* initial_offset, int **offs, int **lens,
		   int max_size,
		   int *data_size, int *metadata_size) {
  long long int l = *start_l;

  *data_size = 0;
  *metadata_size = 0;
  if (l > r) {
    *offs = NULL;
    *lens = NULL;
    return 0;
  }
  else {
    flatlist_node *flat = distr->flat;

    if (contig_flatlist(flat)) {
      if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(int))) 
	return 0;
      else {
	int crt_data_size;

	*offs = (int *)clarisse_malloc(sizeof(int));
	*lens = (int *)clarisse_malloc(sizeof(int));	
	(*metadata_size) += (2 * sizeof(int));
	crt_data_size = MIN(r - l + 1, max_size); 
	if (*data_size + *metadata_size + crt_data_size > max_size)
	  crt_data_size = max_size - *data_size - *metadata_size;
	*initial_offset = l;
	(*offs)[0] = 0;
	(*lens)[0] = crt_data_size; 
	(*data_size) += ((*lens)[0]);	
	(*start_l) += crt_data_size;
	return 1;
      }
    }
    else {
      int idx = 0, max_idx, i; 
      int finished = 0;

      if (r < distr->displ) return 0;
      else r -= distr->displ;
      l = (l < distr->displ) ? 0 : l - distr->displ;
      if (l > r) return 0;
      
      max_idx = distr->flat->count * CEIL_INT((r - BEGIN_BLOCK(l,  flat->extent) + 1), distr->flat->extent);
      *offs = (int *)clarisse_malloc(sizeof(int) * max_idx);
      *lens = (int *)clarisse_malloc(sizeof(int) * max_idx);
      i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      while ((l <= r) && (!finished)){
	// Can probably be moved outside loop and add an i = 0 at the end of the loop
	//int i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
	// if l is posisioned before or at the last active byte of current extent 
	if (i < flat->count ) {
	  long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	  long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	  while (rel_l <= rel_r) {
	    
	    long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	    int crt_data_size = crt_r - rel_l + 1;
	    // contiguous blocks, join them together: update length
	    if ((idx > 0) && 
		(*initial_offset + (*offs)[idx - 1] +  (*lens)[idx -1 ] == distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l)) {
	      if (*data_size + *metadata_size + crt_data_size > max_size) { 
		crt_data_size = max_size - *data_size - *metadata_size; 
		//crt_data_size can be 0 
		//assert(crt_data_size > 0);
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
		*start_l = *initial_offset + (*offs)[idx - 1] + (*lens)[idx - 1];
		finished = 1;
	      }
	      else
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
	    }
	    // discontiguous blocks: save both offset length
	    else {
	      // I need at least one offset, one length and one byte 
	      if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(int))) {
		*start_l = distr->displ + BEGIN_BLOCK(l,  flat->extent) /* l*/ + rel_l;
		finished = 1;
		break;
	      }
	      else {
		if (idx == 0)
		  *initial_offset = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l;
		(*metadata_size) += (2 * sizeof(int));
		(*offs)[idx] = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l - *initial_offset;
		if (*data_size + *metadata_size + crt_data_size > max_size) {
		  crt_data_size = max_size - *data_size - *metadata_size;
		  assert(crt_data_size > 0);
		  (*lens)[idx] = crt_data_size;
		  *start_l = *initial_offset + (*offs)[idx] + (*lens)[idx];
		  finished = 1;
		}
		else 
		  (*lens)[idx] = crt_r - rel_l + 1;
		idx++;
		assert(idx <= max_idx);
	      }
	    }
	    (*data_size) += crt_data_size;
	    if (finished)
	      break;
	    else
	      if ((++i) < flat->count )
		rel_l =  flat->indices[i];
	      else
		break;
	    
	  } // while
	}
	if (!finished)
	  l = BEGIN_NEXT_BLOCK(l, flat->extent);
	i = 0;
      } // while ((*start_l <= r) && (!finished)) 
      
      if (idx == 0) {
	clarisse_free(*offs);
	clarisse_free(*lens);
	*offs = NULL;
	*lens = NULL;
      }
      else 
	if (!finished)
	  *start_l = distr->displ + r + 1;
      return idx;
    }
  }
}

// Returns offs lens as integers and relative to intial offset
//data does not occupy space in off-len lists
//BUT: off-len lists must fit in max_size
//AND: represented data should be less or equal that max_size
int get_offs_lens6(long long int *start_l, long long int r, distrib *distr, 
		   long long int* initial_offset, int **offs, int **lens,
		   int max_size,
		   int *data_size, int *metadata_size) {
  
  long long int l = *start_l;

  *data_size = 0;
  *metadata_size = 0;
  if (l > r) {
    *offs = NULL;
    *lens = NULL;
    return 0;
  }
  else {
    flatlist_node *flat = distr->flat;

    if (contig_flatlist(flat)) {
      if (max_size - *metadata_size < (int) (2 * sizeof(int))) 
      //if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) 
	return 0;
      else {
	int crt_data_size;

	*offs = (int *)clarisse_malloc(sizeof(int));
	*lens = (int *)clarisse_malloc(sizeof(int));
	(*metadata_size) += (2 * sizeof(int));
	crt_data_size = MIN(r - l + 1, max_size);; 
	//	if (*data_size + *metadata_size + crt_data_size > max_size)
	//  crt_data_size = max_size - *data_size - *metadata_size;
	*initial_offset = l;
	(*offs)[0] = 0;
	(*lens)[0] = crt_data_size; 
	(*data_size) += ((*lens)[0]);	
	(*start_l) += crt_data_size;
	return 1;
      }
    }
    else {
      int idx = 0, max_idx, i; 
      int finished = 0;

      if (r < distr->displ) return 0;
      else r -= distr->displ;
      l = (l < distr->displ) ? 0 : l - distr->displ;
      if (l > r) return 0;
      
      //max_idx = distr->flat->count * CEIL_INT((r - BEGIN_BLOCK(l,  flat->extent) + 1), distr->flat->extent);
      max_idx = max_size / (2 * sizeof(int));
      *offs = (int *)clarisse_malloc(sizeof(int) * max_idx);
      *lens = (int *)clarisse_malloc(sizeof(int) * max_idx);
      i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      while ((l <= r) && (!finished)){
	if (i < flat->count ) {
	  long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	  long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	  while (rel_l <= rel_r) {
	    
	    long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	    int crt_data_size = crt_r - rel_l + 1;
	    // contiguous blocks, join them together: update length
	    if ((idx > 0) && 
		(*initial_offset + (*offs)[idx - 1] +  (*lens)[idx -1 ] == distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l)) {
	      //if (*data_size + *metadata_size + crt_data_size > max_size) { 
	      if (*data_size + crt_data_size > max_size) {
		//crt_data_size = max_size - *data_size - *metadata_size; 
		crt_data_size = max_size - *data_size; 
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
		*start_l = *initial_offset + (*offs)[idx - 1] + (*lens)[idx - 1];
		finished = 1;
	      }
	      else 
		(*lens)[idx - 1] = (*lens)[idx - 1] + crt_data_size;
	    }
	    // discontiguous blocks: save both offset length
	    else {
	      // I need at least one offset and  one length 
	      if (max_size - *metadata_size < (int) (2 * sizeof(int))) {
	      //if (max_size - *data_size - *metadata_size <= (int) (2 * sizeof(long long int))) {
		*start_l = distr->displ + BEGIN_BLOCK(l,  flat->extent) /* l*/ + rel_l;
		finished = 1;
		break;
	      }
	      else {
		if (idx == 0)
		  *initial_offset = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l;
		(*metadata_size) += (2 * sizeof(int));
		(*offs)[idx] = distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l - *initial_offset;
		//if (*data_size + *metadata_size + crt_data_size > max_size) {
		if (*data_size + crt_data_size > max_size) {
		  //		  crt_data_size = max_size - *data_size - *metadata_size;
		  crt_data_size = max_size - *data_size;
		  assert(crt_data_size > 0);
		  (*lens)[idx] = crt_data_size;
		  *start_l = *initial_offset + (*offs)[idx] + (*lens)[idx];
		  finished = 1;
		}
		else
		  (*lens)[idx] = crt_r - rel_l + 1;
		idx++;
		assert(idx <= max_idx);
	      }
	    }
	    (*data_size) += crt_data_size;
	    if (finished)
	      break;
	    else
	      if ((++i) < flat->count )
		rel_l =  flat->indices[i];
	      else
		break;
	    
	  } // while
	}
	if (!finished)
	  l = BEGIN_NEXT_BLOCK(l, flat->extent);
	i = 0;
      } // while ((*start_l <= r) && (!finished)) 
      
      if (idx == 0) {
	clarisse_free(*offs);
	clarisse_free(*lens);
	*offs = NULL;
	*lens = NULL;
      }
      else 
	if (!finished)
	  *start_l = distr->displ + r + 1;
      return idx;
    }
  }
}


static long long int sg_extent_file (int fd, long long int file_disp, long long int l, long long int r, distrib* distr, char*out_buf, int type) {
  flatlist_node *flat = distr->flat;
  long long int i = find_bin_search(l, distr, MAP_CURRENT_BLOCK);
  long long int cnt = 0;

  l = MAX (l, flat->indices[i]);

  while ((i < flat->count ) && (l <= r)) {
    long long int crt_r = MIN (r, flat->indices[i] + flat->blocklens[i] - 1);
    
    if (type) {
      if (pread(fd, out_buf + cnt, crt_r - l + 1, l + file_disp) != crt_r - l + 1)
	handle_error(MPI_ERR_OTHER, "read failed in sg_extent_file\n");
    }
    else
      if (pwrite(fd, out_buf + cnt, crt_r - l + 1, l + file_disp) != crt_r - l + 1)
	handle_error(MPI_ERR_OTHER, "read failed in sg_extent_file\n");
    cnt += (crt_r - l + 1);
    l =  flat->indices[++i];
  }
  return cnt;
}


static long long int get_fsize(int fd) {
    struct stat stat;
    if (fstat(fd,&stat)<0) {
	perror("file_interval_copy: fstat error");
	exit(1);
    }
    return stat.st_size;
}

// type 0: scatter 1: gather
// l,r : absolute positions in the buffer buf
// file_displ: subfile displacement (have to be substracted because a file consists of several subfiles)
long long int sg_file (int fd, long long int file_displ, 
		       long long int l, long long int r, 
		       distrib *distr, char*out_buf, int type) {
  long long int cnt = 0; 


  if (distrib_contig(distr)) {
    if (l > r) 
      return 0;
    if (type == SCATTER)
      cnt = pwrite(fd, out_buf, r - l + 1, l);
    else
      cnt = pread(fd, out_buf, r - l + 1, l);
    if (cnt < 0) {
      perror("File access error pwrite/pread");
      exit(1);
    }
  }
  else {
    flatlist_node *flat = distr->flat;
  
    if (type == GATHER) {
      long long int fsize = get_fsize(fd);
      if  (l - file_displ >= fsize) 
	return 0; //eof
      r = MIN(r, fsize + file_displ - 1); //eof reached as well
    }
    file_displ = distr->displ - file_displ;
    if (l > r) return 0;
    if (r < distr->displ) return 0;
    if (l < distr->displ) {
      l = 0;
    }
    else 
      l -= distr->displ;
    r -= distr->displ;
    while (l <= r) {
      long long int crt_r = (r < END_BLOCK(l,flat->extent))? (r % flat->extent) : flat->extent - 1; 
      // file_displ: displacement of the start of extent
      cnt += sg_extent_file(fd, l + file_displ - l % flat->extent, l % flat->extent, crt_r, distr, out_buf + cnt, type);
      
      l = BEGIN_NEXT_BLOCK(l,flat->extent);
    }  
  }
  return cnt;
}

long long int writen(int fd, const void *vptr, long long int n)
{
  long long int nleft;
  long long int nwritten;
  const char	*ptr;

  ptr = vptr;
  nleft = n;
  while (nleft > 0) {
    if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
      if (errno == EINTR) {
	fprintf(stderr, "writen EINTR\n");
	nwritten = 0;		/* and call write() again */
      }
      else
	return(-1);			/* error */
    }
    
    nleft -= nwritten;
    ptr   += nwritten;
  }
  return(n);
}

long long int pwriten(int fd, const void *vptr, long long int n, long long int offset)
{
    long long int m;
    char err_msg[128];


    //printf("Write to file %lld bytes at offset %lld\n", n, offset);

    if (lseek(fd, (loff_t)offset, SEEK_SET) < 0){
	sprintf(err_msg,"pwriten: seek error. fd:%d offset:%lld", fd, offset);
	handle_error(MPI_ERR_OTHER, err_msg);
	return -1;
    } 
    if ((m = writen(fd, vptr, n))<n) {
	sprintf(err_msg, "pwriten: Error at writing to file wrote %lld instead of %lld", m,n);
	handle_error(MPI_ERR_OTHER, err_msg);
    }
    return m;
}

long long int readn(int fd, void *vptr, long long int n)
{
    size_t	nleft;
    ssize_t	nread;
    char	*ptr;
    
    ptr = vptr;
    nleft = n;
    while (nleft > 0) {
      if ( (nread = read(fd, ptr, nleft)) < 0) {
	if (errno == EINTR){
	  fprintf(stderr, "READn EINTR\n");
	  nread = 0;		/* and call read() again */
	}
	else
	  return(-1);
      } else if (nread == 0)
	break;				/* EOF */
      
      nleft -= nread;
      ptr   += nread;
    }
    return(n - nleft);		/* return >= 0 */
}

long long int preadn(int fd, void *vptr, long long int n, long long int offset)
{
    long long int m;
    char err_msg[128];
    if (lseek(fd, (loff_t)offset, SEEK_SET) < 0){
      sprintf(err_msg,"preadn: seek error. fd:%d offset:%lld\n", fd, offset);
      handle_error(MPI_ERR_OTHER, err_msg);
      return-1;
    } 
    if ((m = readn(fd,vptr,n)) < n) {
      //fprintf(stderr, "preadn: Read from file %lld instead of %lld\n", m,n);
      //handle_error(MPI_ERR_OTHER, err_msg);
    }
    return m;
}


/*
int ADIO_Writen(ADIO_File fd, const void *vptr, long long int n)
{
  long long int nleft;
  int nwritten;
  const char	*ptr;
  ADIO_Status *status;
  int err;
    
  ptr = vptr;
  nleft = n;
  //printf ("writen:  nelem %d\n",n); 
  while (nleft > 0) {
    ADIO_WriteContig(fd,(char *)ptr,nleft,MPI_BYTE,ADIO_EXPLICIT_OFFSET,0,status,&err);
    MPI_Get_count(status, MPI_BYTE, &nwritten);
    if (err == EINTR) {
      fprintf(stderr, "writen EINTR\n");
      nwritten = 0;		// and call write() again 
    }
    else
      return(-1);			// error 
    
    nleft -= nwritten;
    ptr   += nwritten;
  }
  //    printf ("write n %d\n",n);
  return n;
}

int ADIO_Pwriten(ADIO_File fd, const void *vptr, long long int n, long long int offset)
{
  int m;
  char err_msg[128];
  ADIO_Status status;
  int err;
  
  //    printf ("writen_at: n %d offset %lld \n",n,offset); 
  ADIO_WriteContig(fd,vptr,n,MPI_BYTE, ADIO_EXPLICIT_OFFSET,offset,&status,&err);
  MPI_Get_count( &status,MPI_BYTE, &m);
  if (m < n) {
    sprintf(err_msg, "Writen_at: Error at writing to file write %d instead of %lld", m,n);
    handle_error(MPI_ERR_OTHER, err_msg);
  }
  return n;
}

int ADIO_Readn(ADIO_File fd, void *vptr, long long int n)
{
  long long int nleft;
  int nread;
  char	*ptr;
  ADIO_Status *status;
  int err;
    
  ptr = vptr;
  nleft = n;
  //printf ("readn: n %d\n",n);
  while (nleft > 0) {
    
    ADIO_ReadContig(fd,ptr,nleft,fd->etype,ADIO_EXPLICIT_OFFSET,0,status,&err);
    MPI_Get_count(status, fd->filetype, &nread);
    if (err != MPI_SUCCESS) {
      if (errno == EINTR){
	fprintf(stderr, "READn EINTR\n");
	nread = 0;		// and call read() again 
      }
      else
	return(-1);
    } else if (nread == 0)
      break;				// EOF 
    
    nleft -= nread;
    ptr   += nread;
  }
  return(n - nleft);		// return >= 0 
}

int ADIO_Preadn(ADIO_File fd, void *vptr, long long int n, long long int offset)
{
  long long int m;
  char err_msg[128];
  ADIO_Status status; 
  int err;
  if (n > 0) {
    ADIO_ReadContig(fd,vptr,n,MPI_BYTE,ADIO_EXPLICIT_OFFSET,offset,&status,&err);
    MPI_Get_count( &status, MPI_BYTE,(int *) &m);
    //	printf ("readn_at: n %d offset %lld m %d\n",n,offset,m);
    
    if (m < n) {
      sprintf(err_msg, "Readn_at: Error at reading file read %lld instead of %lld", m,n);
      handle_error(MPI_ERR_OTHER, err_msg);
    }
  }
  return m;
}

*/
