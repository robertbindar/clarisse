#http://www.cmake.org/Wiki/CMake:How_To_Find_Libraries
cmake_minimum_required (VERSION 2.8.3)
cmake_policy(VERSION 2.8.3)
project (map)

set(CMAKE_C_COMPILER mpicc)
set (SRCS romio_flatten.c romio_darray.c romio_subarray.c test_datatypes.c map.c flatten.c sg.c)
set (HDRS flatten.h map.h test_datatypes.h)
set (PROGS test1 test2 test3 test4 test5 test6 test7 test8 test9 test10 test11 test12 test13 test14 test15 test16)

include_directories (../util)
include_directories (../include)

#set compile flags
option(CLARISSE_TIMING "Use CLARISSE_TIMING" OFF)
option(CLARISSE_MEM_COUNT "Use CLARISSE_MEM_COUNT" OFF)
option(CLARISSE_MEM_DEBUG "Use CLARISSE_MEM_DEBUG" OFF)
option(TRACE_MEMALLOC "Use CLARISSE_MEMALLOC" OFF)
option(DEBUG "Use DEBUG" OFF)
option(COMPILER_DEBUG "Use COMPILER_DEBUG" ON)
if (CLARISSE_TIMING)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DCLARISSE_TIMING")
endif ()
if (CLARISSE_MEM_COUNT)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DCLARISSE_MEM_COUNT")
endif ()
if (CLARISSE_MEM_DEBUG)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DCLARISSE_MEM_DEBUG")
endif ()
if (TRACE_MEMALLOC)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DTRACE_MEMALLOC")
endif ()
if (DEBUG)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DDEBUG")
endif ()
if (COMPILER_DEBUG)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -Wall -Wextra")
endif ()

#add library
add_library(map ${SRCS})
set (EXTRA_LIBS ${EXTRA_LIBS} map util) 
set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "-dynamic")
if (CLARISSE_MEM_COUNT OR CLARISSE_MEM_DEBUG OR TRACE_MEMALLOC)
  set (EXTRA_LIBS ${EXTRA_LIBS} mem_catch)
endif ()

# add the search path for include files to enable other library see map
#target_include_directories (map PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})


# add the executables
foreach (PROG ${PROGS})
    add_executable(${PROG} ${PROG}.c)
    target_link_libraries(${PROG} LINK_PUBLIC ${EXTRA_LIBS})
    #target_link_libraries(${PROG} LINK_PUBLIC util map)
endforeach()


# add the install targets
install (TARGETS map DESTINATION lib)
install (FILES ${HDRS} DESTINATION include)
install (TARGETS ${PROGS} DESTINATION bin)


#install (FILES "${PROJECT_BINARY_DIR}/TutorialConfig.h"
#  DESTINATION include)


