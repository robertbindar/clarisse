#include <stdio.h>
#include "mpi.h"

MPI_Datatype get_contig() {
  MPI_Datatype t;
  MPI_Type_contiguous(2, MPI_CHAR, &t);
  MPI_Type_commit(&t);
  return t;
}
//           1         2
// 012345678901234567890123456789
// -------[---****

MPI_Datatype get_lb_contig() {
  int block_lengths[2]; 
  MPI_Aint displacements[2];
  MPI_Datatype typelist[2],t;
  block_lengths[0] = block_lengths[1] = 1;
  typelist[0] = MPI_LB;
  displacements[0] = 7; 
  typelist[1] = MPI_FLOAT;
  displacements[1] = 11;
  MPI_Type_struct(2, block_lengths, displacements, typelist, &t);
  MPI_Type_commit(&t);
  return t;
}
//           1         2
// 012345678901234567890123456789
// ---****---]

MPI_Datatype get_contig_ub() {
  int block_lengths[2]; 
  MPI_Aint displacements[2];
  MPI_Datatype typelist[2],t;
  block_lengths[0] = block_lengths[1] = 1;
  typelist[0] = MPI_FLOAT;
  displacements[0] = 3; 
  typelist[1] = MPI_UB;
  displacements[1] = 10;
  MPI_Type_struct(2, block_lengths, displacements, typelist, &t);
  MPI_Type_commit(&t);
  return t;
}

// *_*

MPI_Datatype get_simple_vec() {
  MPI_Datatype t;
  MPI_Type_vector(2, 1, 2, MPI_CHAR, &t);
  MPI_Type_commit(&t);
  return t;
}
//    l           u   
// 0123456789012345
// ---[-*-**-*    ]
MPI_Datatype get_displ_simple_vec() {
  int block_lengths[3];
  MPI_Aint displacements[3];
  MPI_Datatype typelist[3],t1,t;

  MPI_Type_vector(2, 1, 2, MPI_CHAR, &t1);
  //MPI_Type_commit(&t1);
	
  block_lengths[0] = 1;
  block_lengths[1] = 2;
  block_lengths[2] = 1;
  typelist[0] = MPI_LB;
  displacements[0] = 3;
  typelist[1] = t1;
  displacements[1] = 5;
  typelist[2] = MPI_UB;
  displacements[2] = 15;
  MPI_Type_struct(3, block_lengths, displacements, typelist, &t);
  MPI_Type_commit(&t);

  MPI_Type_free(&t1);
  return t;
}

//           1         2
// 012345678901234567890123456789
// -------[---****----]

MPI_Datatype get_simple_struct() {
  int block_lengths[3]; 
  MPI_Aint displacements[3];
  MPI_Datatype typelist[3],t;
  block_lengths[0] = block_lengths[1] = block_lengths[2] = 1;
  typelist[0] = MPI_LB;
  displacements[0] = 7; 
  typelist[1] = MPI_FLOAT;
  displacements[1] = 11;
  typelist[2] = MPI_UB;
  displacements[2] = 19; 
  MPI_Type_struct(3, block_lengths, displacements, typelist, &t);
  MPI_Type_commit(&t);
  return t;
}

//           1         2
// 012345678901234567890123456789
// ----d---****

MPI_Datatype get_displ_displ_struct() {
  int block_lengths[1]; 
  MPI_Aint displacements[1],lb,ub;
  MPI_Datatype typelist[1],t,t1;
  block_lengths[0] = 1;
  typelist[0] = MPI_SHORT;
  displacements[0] = 4; 
  MPI_Type_struct(1, block_lengths, displacements, typelist, &t);
  MPI_Type_commit(&t);
  
  typelist[0] = t;
  MPI_Type_struct(1, block_lengths, displacements, typelist, &t1);
  MPI_Type_commit(&t1);
  
  MPI_Type_lb(t1,&lb);
  MPI_Type_ub(t1,&ub);
  //printf("lb = %lld ub = %lld \n",(long long int)lb,(long long int)ub);
  MPI_Type_free(&t);
  return t1;
}

// Incomplete
//           1         2         3         4         5          6        7         8         9         
// 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
// ****----****----****----*---------------------------------------------------------------------------
// ****----****----****----*--------------------------------------------------------------------------- 


MPI_Datatype get_vect_struct() {
  int block_lengths[4]; 
  MPI_Aint displacements[4];
  MPI_Datatype typelist[4],filetype,t,t1;
  block_lengths[0] = block_lengths[1] = block_lengths[2] = block_lengths[3] = 1;
  typelist[0] = MPI_FLOAT;
  typelist[1] = MPI_FLOAT;
  typelist[2] = MPI_INT;
  typelist[3] = MPI_CHAR;
  displacements[0] = 0; displacements[1] = 2*sizeof(float); displacements[2] = 4*sizeof(float);   displacements[3] = 6*sizeof(float);
  MPI_Type_struct(4, block_lengths, displacements, typelist, &t);
  MPI_Type_commit(&t);
  MPI_Type_vector(4,1,4 ,t,&t1);
  MPI_Type_commit(&t1);
 
  displacements[0] = 0;displacements[1]=12;displacements[2]=1000;displacements[3]=1004;
  //displacements[2]=65536;displacements[3]=65800;
  typelist[0] = MPI_DOUBLE;
  typelist[1] = t1;
  typelist[2] = MPI_BYTE;
  typelist[3] = MPI_SHORT;
  
  MPI_Type_struct(4,block_lengths, displacements, typelist,&filetype);
  MPI_Type_commit(&filetype);
  MPI_Type_free(&t);
  MPI_Type_free(&t1);
  return filetype;
}

// TYpe used in coll_perf.c benchmark
// diffs: replaced MPI_INT by MPI_BYTE
// reduces dimention

#define COLL_PERF_SIZE 4

MPI_Datatype get_darray(MPI_Comm comm){
  MPI_Datatype newtype;
  int i, ndims, array_of_gsizes[3], array_of_distribs[3];
  int order, mynod, nprocs;
  int array_of_dargs[3], array_of_psizes[3];
  
  MPI_Comm_rank(comm, &mynod);
  MPI_Comm_size(comm, &nprocs);
  ndims = 3;
  order = MPI_ORDER_C;
  
  for (i=0; i<ndims; i++) {
    array_of_gsizes[i] = COLL_PERF_SIZE;
    array_of_distribs[i] = MPI_DISTRIBUTE_BLOCK;
    array_of_dargs[i] = MPI_DISTRIBUTE_DFLT_DARG;
    array_of_psizes[i] = 0;
  }
  MPI_Dims_create(nprocs, ndims, array_of_psizes);
  
  MPI_Type_create_darray(nprocs, mynod, ndims, array_of_gsizes,
			 array_of_distribs, array_of_dargs,
			 array_of_psizes, order, MPI_BYTE, &newtype);
  MPI_Type_commit(&newtype);
  
  return newtype;
}

MPI_Datatype get_darray_4procs_1rank(){
  MPI_Datatype newtype;
  int i, ndims, array_of_gsizes[3], array_of_distribs[3];
  int order, mynod=0, nprocs=4;
  int array_of_dargs[3], array_of_psizes[3];
  
  //MPI_Comm_rank(MPI_COMM_WORLD, &mynod);
  //MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  ndims = 3;
  order = MPI_ORDER_C;
  
  for (i=0; i<ndims; i++) {
    array_of_gsizes[i] = COLL_PERF_SIZE;
    array_of_distribs[i] = MPI_DISTRIBUTE_BLOCK;
    array_of_dargs[i] = MPI_DISTRIBUTE_DFLT_DARG;
    array_of_psizes[i] = 0;
  }
  MPI_Dims_create(nprocs, ndims, array_of_psizes);
  
  MPI_Type_create_darray(nprocs, mynod, ndims, array_of_gsizes,
			 array_of_distribs, array_of_dargs,
			 array_of_psizes, order, MPI_BYTE, &newtype);
  MPI_Type_commit(&newtype);
  
  return newtype;
}

// 3 procs writing 3 bytes with this view
// 001122001122

MPI_Datatype get_vec_nstride(MPI_Comm comm) {
  MPI_Datatype t;
  int nprocs;
  MPI_Comm_size(comm, &nprocs);
 
  MPI_Type_vector(2, 1, nprocs, MPI_BYTE, &t);
  MPI_Type_commit(&t);
  return t;
}
