#include <stdlib.h>
#include <stdio.h>
#include "flatten.h"

MPI_Datatype get_contig();
MPI_Datatype get_lb_contig();
MPI_Datatype get_contig_ub();
MPI_Datatype get_simple_vec();
MPI_Datatype get_displ_simple_vec();
MPI_Datatype get_simple_struct();
MPI_Datatype get_displ_displ_struct();
MPI_Datatype get_vect_struct();
MPI_Datatype get_darray();


void draw_type(MPI_Datatype dt, long long int displ, int rep) {
  distrib * distr = distrib_get(dt, displ);
  MPI_Aint extent, lb;
  MPI_Type_get_extent(dt, &lb,&extent);
  printf("REP=%d LB=%d EXTENT=%d\n", rep, (int)lb, (int)extent);
  
  distrib_print(distr);
  distrib_draw(distr, rep);
  
  free_datatype(dt);
  distrib_free(distr);
}

int main(int argc, char* argv[]) {
    MPI_Init(&argc, &argv);

    printf("\nDRAW BASIC FLOAT:\n");
    draw_type(MPI_FLOAT,0,1);
 
    printf("\nDRAW CONTIG:\n");
    draw_type(get_contig(),0,1);
    
    printf("\nDRAW LB CONTIG:\n");
    draw_type(get_lb_contig(),0,1);
  
    printf("\nDRAW CONTIG UB:\n");
    draw_type(get_contig_ub(),0,1);
  
    printf("\nDRAW SIMPLE VECTOR:\n");
    draw_type(get_simple_vec(),0,1);
 
    printf("\nDRAW DISPL SIMPLE VECTOR (between lb and ub):\n");
    draw_type(get_displ_simple_vec(),0,1);
    
    printf("\nDRAW SIMPLE STRUCT:\n");
    draw_type(get_simple_struct(),10,2);
    
    printf("\nDRAW DISPL DISPL STRUCT:\n");
    draw_type(get_displ_displ_struct(),0,1);
    
    printf("\nDRAW STRUCT OF VECTOR OF STRUCT:\n");
    draw_type(get_vect_struct(),0,1);
    
    MPI_Finalize();
    return 0;
}  // main 
