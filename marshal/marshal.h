#ifndef _MARSHAL_INCLUDE
#define _MARSHAL_INCLUDE

#include "mpi.h"
#include <math.h>
#include <stdio.h>


#define MAX_MARSHAL_BUF_SIZE 64*1024

void decode_dt(MPI_Datatype type);
int decode_pack_dt(MPI_Datatype type,char**buf,int displ);
int decode_pack_dt_out_buf(MPI_Datatype type, char**buf);
MPI_Datatype reconstruct_dt(char *buf, int buf_sz);
void save_type_in_file(char *type, char *buf, int size, long long int displ);
MPI_Datatype get_type_from_file(char *filename, long long int *displ);

void handle_error(int errcode, char *str);
void print_dt(MPI_Datatype type);

#endif
