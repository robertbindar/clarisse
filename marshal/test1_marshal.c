#include "marshal.h"
#include "flatten.h"
#include "test_datatypes.h"

extern char *packed_dt;
extern int pack_ptr;

#define LARGE 1000 
int ni, na, nd, combiner, i[LARGE]; 
MPI_Aint a[LARGE]; 
MPI_Datatype d[LARGE]; 

/* marshal/unmarshal test */

int test_decode_unpack(MPI_Datatype dt) {
  int ret;
  MPI_Datatype new_dt;
  //printf("FLATTENING\n");
  //printf("DECODING\n");
  pack_ptr = 0;
  decode_dt(dt);
  //printf("RECONSTRUCTING\n");

  new_dt = reconstruct_dt(packed_dt,pack_ptr);
  //new_dt = get_displ_simple_vec();

  //printf("COMPARING\n");
  ret = cmp_flatlists(get_flatlist(dt),get_flatlist(new_dt));
  //printf("RETURNING");
  if (ret) {
    ret = -1;
    print_dt(dt);
    print_flatlist(get_flatlist(dt));
    print_dt(new_dt);
    print_flatlist(get_flatlist(new_dt));
  }
  MPI_Type_free(&dt);
  MPI_Type_free(&new_dt);
  return ret;
}


int main(int argc, char* argv[]) {
  MPI_Info info;   
  int ret;
  MPI_Init(&argc, &argv);
  MPI_Info_create(&info);
  MPI_File_delete("/tmp/a", info);
  
  ret = test_decode_unpack(get_contig());
  if (ret >= 0) ret = test_decode_unpack(get_lb_contig()); //1
  if (ret >= 0) ret = test_decode_unpack(get_contig_ub()); //2
  if (ret >= 0) ret = test_decode_unpack(get_simple_vec()); //3
  
  if (ret >= 0) ret = test_decode_unpack(get_displ_simple_vec()); //4
  
  if (ret >= 0) ret = test_decode_unpack(get_simple_struct()); //5
  if (ret >= 0) ret = test_decode_unpack(get_displ_displ_struct()); //6
  if (ret >= 0) ret = test_decode_unpack(get_vect_struct()); //7
  if (ret >= 0) ret = test_decode_unpack(get_darray_4procs_1rank());//8 
  
  if (ret >= 0) 
    printf("Test marshal passed\n");
  MPI_Info_free(&info);
  MPI_Finalize();
  return 0;
}  // main 

