#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "marshal.h"
#include "mpi.h" 
#include "util.h" 

char static_packed_dt[MAX_MARSHAL_BUF_SIZE];
char *packed_dt = static_packed_dt;
int pack_ptr = 0;

// in flatten.c
void handle_error(int errcode, char *str); 

static void free_datatype(MPI_Datatype type) {
    int ni, na, nd, combiner; 
    MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
    if (combiner != MPI_COMBINER_NAMED)
	MPI_Type_free(&type);
}


void decode_dt(MPI_Datatype type) { 
  int ni, na, nd, combiner, *i, k; 
  MPI_Aint *a; 
  MPI_Datatype *d; 

  MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
  if (combiner != MPI_COMBINER_NAMED){
    if (ni > 0) i = (int *) clarisse_malloc(ni * sizeof(int));
    if (na > 0) a = (MPI_Aint *) clarisse_malloc(na * sizeof(MPI_Aint));
    if (nd > 0) d = (MPI_Datatype *) clarisse_malloc(nd * sizeof(MPI_Datatype));
    MPI_Type_get_contents(type, ni, na, nd, i, a, d); 
  }
  
  for (k=0;k<nd;k++) {
    decode_dt(d[k]);
    free_datatype(d[k]);
  }
  //  printf("DTYPE=%x\n",type);

  memcpy(packed_dt + pack_ptr,&combiner,sizeof(int));
  pack_ptr += sizeof(int);
  if (combiner == MPI_COMBINER_NAMED){
    memcpy(packed_dt + pack_ptr, &type, sizeof(MPI_Datatype));
    pack_ptr += sizeof(MPI_Datatype);
  }
  else {  
    if (ni > 0) {
      memcpy(packed_dt + pack_ptr, i, ni * sizeof(int));
      pack_ptr += ni * sizeof(int);
    }
    if (na > 0) {
      memcpy(packed_dt + pack_ptr, a, na * sizeof(MPI_Aint));
      pack_ptr += na * sizeof(MPI_Aint);    
    }

    /*    if (ni>1)
      printf("VECTOR: count=%d blocklen=%d stride=%d oldtype=%x\n",i[0],i[1],i[2],d[0]);
    */
    if (ni > 0) clarisse_free(i);  
    if (na > 0) clarisse_free(a);  
    if (nd > 0) clarisse_free(d);
  }
}
/*
void print_dt(MPI_Datatype type) {
  static int nest=0;
  int ni, na, nd, combiner, *i,k; 
  MPI_Aint *a; 
  MPI_Datatype *d; 

  MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
  if (combiner != MPI_COMBINER_NAMED){
    if (ni > 0) i = (int *) clarisse_malloc(ni*sizeof(int));
    if (na > 0) a = (MPI_Aint *) clarisse_malloc(na*sizeof(MPI_Aint));
    if (nd > 0) d = (MPI_Datatype *) clarisse_malloc(nd*sizeof(MPI_Datatype));
    MPI_Type_get_contents(type, ni, na, nd, i, a, d); 
  }
  
  for (k=0;k<nest;k++) printf("  ");

  switch (combiner) {
  case  MPI_COMBINER_NAMED:
    printf("NAMED DTYPE=%x\n",type);
    break;
  case MPI_COMBINER_DUP:
    printf("DUP DTYPE\n");
    break;
  case MPI_COMBINER_CONTIGUOUS: 
    printf("CONTIGUOUS DTYPE count=%d\n",i[0]);
    break;
  case MPI_COMBINER_VECTOR:
    printf("VECTOR DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],i[2]);
    break;
  case MPI_COMBINER_HVECTOR_INTEGER:
    printf("HVECTOR_INTEGER DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],a[0]);
    break;
  case MPI_COMBINER_HVECTOR: 
    printf("HVECTOR DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],a[0]);
    break;
  case MPI_COMBINER_INDEXED:
    printf("INDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],i[1+i[0]+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_HINDEXED_INTEGER:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],a[k]); 
    printf("\n");
    break;
    
  case MPI_COMBINER_HINDEXED:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_INDEXED_BLOCK:
    printf("HINDEXED_BLOCK DTYPE count=%d blocklen=%d ",i[0],i[1]);
    for (k=0;k<i[0];k++) 
      printf("[%d]disp=%d ",k,i[2+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_STRUCT_INTEGER:
  case MPI_COMBINER_STRUCT:
    printf("STRUCT DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_SUBARRAY:
    printf("SUBARRAY DTYPE ndims=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]size=%d subsize=%d start=%d",k,i[1+k],i[1+i[0]+k],i[1+2*i[0]+k]); 
 
    printf("order=%d\n",i[1+3*i[0]]);
    break;
  case MPI_COMBINER_DARRAY:
    printf("DARRAY DTYPE size=%d rank=%d ndims=%d ",i[0],i[1],i[2]);
    for (k=0;k<i[2];k++) 
      printf("[%d]gsize=%d distrib=%d darg=%d psize=%d", k, i[3+k], i[3+i[2]+k], i[3+2*i[2]+k], i[3+3*i[2]+k] ); 
 
    printf("order=%d\n",i[3+4*i[2]]);
    break;
  case MPI_COMBINER_RESIZED:
     printf("RESIZED DTYPE lb=%d, extent=%d\n",i[0],i[1]);
     break;
  default:
    handle_error(MPI_ERR_LASTCODE,"Error: unkonwn combiner\n");
  }
  nest++;
  for (k=0;k<nd;k++)
    print_dt(d[k]);
  nest--;
  //  printf("DTYPE=%x\n",type);

  if (ni > 0) clarisse_free(i);  
  if (na > 0) clarisse_free(a);  
  if (nd > 0) clarisse_free(d);
  
}
*/


#define MAX_STACK 100
static int sp;
static MPI_Datatype stack[MAX_STACK];

static void push(MPI_Datatype el){
  if (sp == MAX_STACK) 
    handle_error(MPI_ERR_LASTCODE,"Error: Datatype decoding stack full\n");
  else 
    stack[sp++] = el;
}

static MPI_Datatype pop(){
   if (sp == 0)
     handle_error(MPI_ERR_LASTCODE,"Error: Datatype decoding stack empty\n");
   else 
     return stack[--sp];
   return -1;
}


/**********************
Decodes the data type "type" and packs it POST-ORDER in the buf. 
If reused, the content of the buf is overwritten

**********************/
int decode_pack_dt(MPI_Datatype type,char**buf, int displ){
  pack_ptr = displ;
  decode_dt(type);
  *buf = packed_dt;
  return pack_ptr;
}

// uses a buffer allocated outside
int decode_pack_dt_out_buf(MPI_Datatype type, char**buf){
  char *aux;
  aux = packed_dt;
  packed_dt = *buf;
  pack_ptr = 0;
  decode_dt(type);
  *buf = packed_dt;
  packed_dt = aux;


  return pack_ptr;
}


/**********************
Reconstructs a datatype from a POST-ORDER packed datatype
**********************/
MPI_Datatype reconstruct_dt(char *buf, int buf_sz) {
  int buf_ptr = 0,combiner;
  MPI_Datatype newtype;
  sp = 0;
 
  while (buf_ptr < buf_sz) {
    memcpy(&combiner,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
        
    switch (combiner) {
    case  MPI_COMBINER_NAMED:
      memcpy(&newtype,buf+buf_ptr,sizeof(MPI_Datatype)); buf_ptr+=sizeof(MPI_Datatype);
      break;

    case MPI_COMBINER_DUP: {
      MPI_Datatype oldtype = pop();
      MPI_Type_dup(oldtype,&newtype);
      break;

    }
    case MPI_COMBINER_CONTIGUOUS: {
      MPI_Datatype oldtype = pop();
      int count;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_contiguous(count,oldtype,&newtype);
      free_datatype(oldtype);
      break;
    } 
    case MPI_COMBINER_VECTOR: {
      MPI_Datatype oldtype = pop();
      int count, blocklen,stride;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&blocklen,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&stride,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_vector(count, blocklen,stride,oldtype,&newtype); 
      free_datatype(oldtype);
      break;
    }
    case MPI_COMBINER_HVECTOR_INTEGER:
    case MPI_COMBINER_HVECTOR: {
      MPI_Datatype oldtype = pop();
      int count, blocklen;
      MPI_Aint stride;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&blocklen,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&stride,buf+buf_ptr,sizeof(MPI_Aint)); buf_ptr+=sizeof(MPI_Aint);
      MPI_Type_hvector (count, blocklen,stride,oldtype,&newtype);
      free_datatype(oldtype);
      break;
    }
    case MPI_COMBINER_INDEXED:{
      MPI_Datatype oldtype = pop();
      int count, *array_of_blocklengths, *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_blocklengths = (int*) clarisse_malloc(count*sizeof(int));
      array_of_displacements = (int*) clarisse_malloc(count*sizeof(int));
      memcpy(array_of_blocklengths,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      MPI_Type_indexed(count,array_of_blocklengths,array_of_displacements,oldtype,&newtype);
      free_datatype(oldtype);
      clarisse_free(array_of_blocklengths);
      clarisse_free(array_of_displacements);
      break;
    }
    case MPI_COMBINER_HINDEXED_INTEGER:
    case MPI_COMBINER_HINDEXED:{
      MPI_Datatype oldtype = pop();
      int count, *array_of_blocklengths; 
      MPI_Aint *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_blocklengths=(int*) clarisse_malloc(count*sizeof(int));
      array_of_displacements = (MPI_Aint*) clarisse_malloc(count*sizeof(MPI_Aint));
      memcpy(array_of_blocklengths,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(MPI_Aint));buf_ptr+=count*sizeof(MPI_Aint);
      MPI_Type_hindexed(count,array_of_blocklengths,array_of_displacements,oldtype,&newtype);
      free_datatype(oldtype);
      clarisse_free(array_of_blocklengths);
      clarisse_free(array_of_displacements);
      break;
   }
    case MPI_COMBINER_INDEXED_BLOCK:{
      MPI_Datatype oldtype = pop();
      int count, blocklength, *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_displacements = (int*) clarisse_malloc(count*sizeof(int));
      memcpy(&blocklength,buf+buf_ptr,sizeof(int));buf_ptr+=sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      MPI_Type_create_indexed_block(count,blocklength,array_of_displacements,oldtype,&newtype);
      free_datatype(oldtype);
      clarisse_free(array_of_displacements);
      break;
   }
    case MPI_COMBINER_STRUCT_INTEGER:
    case MPI_COMBINER_STRUCT:{
      MPI_Datatype *array_of_types;
      int count, *array_of_blocklengths,k; 
      MPI_Aint *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_blocklengths = (int*) clarisse_malloc(count*sizeof(int));
      array_of_displacements = (MPI_Aint*) clarisse_malloc(count*sizeof(MPI_Aint));
      array_of_types = (MPI_Datatype*) clarisse_malloc(count*sizeof(MPI_Datatype));
      for (k=0;k<count;k++) array_of_types[count-k-1] = pop();
      memcpy(array_of_blocklengths,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(MPI_Aint));buf_ptr+=count*sizeof(MPI_Aint);      
      MPI_Type_struct(count,array_of_blocklengths,array_of_displacements,array_of_types,&newtype);
      for (k=0;k<count;k++) free_datatype(array_of_types[count-k-1]);
      clarisse_free(array_of_blocklengths);
      clarisse_free(array_of_displacements);
      clarisse_free(array_of_types);
      break;
    }
    case MPI_COMBINER_SUBARRAY:{
      MPI_Datatype oldtype = pop();
      int ndims, *array_of_sizes, *array_of_subsizes, *array_of_starts, order;
      memcpy(&ndims,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_sizes = (int*) clarisse_malloc(ndims*sizeof(int));
      array_of_subsizes = (int*) clarisse_malloc(ndims*sizeof(int));
      array_of_starts = (int*) clarisse_malloc(ndims*sizeof(int));
      memcpy(array_of_sizes,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(array_of_subsizes,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(array_of_starts,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(&order,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_subarray (ndims,array_of_sizes,array_of_subsizes,array_of_starts,order,oldtype,&newtype);
      free_datatype(oldtype);
      clarisse_free(array_of_sizes);
      clarisse_free(array_of_subsizes);
      clarisse_free(array_of_starts);
      break;
   }
    case MPI_COMBINER_DARRAY:{
      MPI_Datatype oldtype = pop();
      int size, rank, ndims, *array_of_gsizes, *array_of_distribs, *array_of_dargs, *array_of_psizes, order;     
      memcpy(&size, buf + buf_ptr, sizeof(int)); buf_ptr += sizeof(int);
      memcpy(&rank, buf + buf_ptr, sizeof(int)); buf_ptr += sizeof(int);
      memcpy(&ndims, buf + buf_ptr, sizeof(int)); buf_ptr += sizeof(int);
      array_of_gsizes = (int*) clarisse_malloc(ndims * sizeof(int));
      array_of_distribs = (int*) clarisse_malloc(ndims * sizeof(int));
      array_of_dargs = (int*) clarisse_malloc(ndims * sizeof(int));
      array_of_psizes = (int*) clarisse_malloc(ndims * sizeof(int));
      memcpy(array_of_gsizes, buf + buf_ptr, ndims * sizeof(int)); buf_ptr += ndims*sizeof(int);
      memcpy(array_of_distribs,buf + buf_ptr, ndims * sizeof(int)); buf_ptr += ndims * sizeof(int);
      memcpy(array_of_dargs, buf + buf_ptr, ndims * sizeof(int)); buf_ptr += ndims * sizeof(int);
      memcpy(array_of_psizes, buf + buf_ptr, ndims * sizeof(int)); buf_ptr += ndims*sizeof(int);
      memcpy(&order, buf + buf_ptr, sizeof(int)); buf_ptr += sizeof(int);
      MPI_Type_create_darray(size, rank, ndims, array_of_gsizes, array_of_distribs,array_of_dargs, array_of_psizes, order, oldtype, &newtype); 
      free_datatype(oldtype);
      clarisse_free(array_of_gsizes);
      clarisse_free(array_of_distribs);
      clarisse_free(array_of_dargs);
      clarisse_free(array_of_psizes);
      break;
    }
#ifdef FORTRAN90
    case MPI_COMBINER_F90_REAL:{
      int p,r;
      memcpy(&p,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&r,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_f90_real(p,r,&newtype);
      break;
    }
    case MPI_COMBINER_F90_COMPLEX:{
      int p,r;
      memcpy(&p,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&r,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_f90_complex(p,r,&newtype);
      break;
    }
    case MPI_COMBINER_F90_INTEGER:{
      int r;
      memcpy(&r,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_f90_integer(r,&newtype);
      break;
    }
#endif
    case MPI_COMBINER_RESIZED:{
      MPI_Datatype oldtype = pop();
      MPI_Aint lb,extent;
      memcpy(&lb,buf+buf_ptr,sizeof(MPI_Aint)); buf_ptr+=sizeof(MPI_Aint);
      memcpy(&extent,buf+buf_ptr,sizeof(MPI_Aint)); buf_ptr+=sizeof(MPI_Aint);
      MPI_Type_create_resized(oldtype,lb,extent,&newtype);
      break;
    }

    default: {
      char err_msg[128];
      sprintf(err_msg, "Error: unkonwn combiner %d\n", combiner);
      handle_error(MPI_ERR_LASTCODE,err_msg);
    }
    }
    push(newtype);
    // Appearantly commit is needed only for types involved in communication
    //if (combiner != MPI_COMBINER_NAMED)
    //  MPI_Type_commit(&newtype);

  }
  if (sp!=1)
    handle_error(MPI_ERR_LASTCODE,"Error: stack has more than one element\n");
  return stack[sp-1];
}


void save_type_in_file(char *type, char *buf, int view_size, long long int displ){
  static int cnt = 0;
  char filename[256];
  int myrank, nprocs, fd;

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  sprintf(filename, "%s%d_R%d_N%d",type, cnt++, myrank, nprocs);

  if ((fd = open(filename,O_RDWR|O_CREAT|O_TRUNC, 0666)) < 0){
    perror("Dump view to file: Open Failed");
    exit(1);
    //handle_err(MPI_ERR_OTHER, "exit...");
  }
  
  if (write(fd, &displ, sizeof(long long int)) < 0){
    perror("Dump view to file: Displ Write Failed");
    exit(1);
    //handle_err(MPI_ERR_OTHER, "exit...");
  }

  if (write(fd, buf, view_size) < 0){
    perror("Dump view to file: Data type Write Failed");
    exit(1);
    //handle_err(MPI_ERR_OTHER, "exit...");
  }
  
  close(fd);

}

MPI_Datatype get_type_from_file(char *filename, long long int *displ) {
  int fd;
  struct stat stats;
  char buf[1024];

  if ((fd = open(filename, O_RDONLY))<0){
    perror("View replay: Open Failed");
    exit(1);
    //handle_err(MPI_ERR_OTHER, "exit...");
  }
  if (fstat(fd, &stats) < 0){
    perror("View replay: fstat Failed");
    exit(1);
    //handle_err(MPI_ERR_OTHER, "exit...");
  } 
  if (read(fd, displ, sizeof(long long int)) < 0){
    perror("View replay: Displ Read Failed");
    exit(1);
    //handle_err(MPI_ERR_OTHER, "exit...");
  }
  if (read(fd, buf, stats.st_size - sizeof(long long int)) < 0){
    perror("View replay: Datatype Read Failed");
    exit(1);
    //handle_err(MPI_ERR_OTHER, "exit...");
  }
  close(fd);
  return reconstruct_dt(buf, stats.st_size - sizeof(long long int));
}
