#include "decode.h"

extern ADIOI_Flatlist_node *ADIOI_Flatlist;

#define LARGE 1000 
int ni, na, nd, combiner, i[LARGE]; 
MPI_Aint a[LARGE]; 
MPI_Datatype d[LARGE]; 

/*
void print_flatlist(MPI_Datatype dt) {
  int b_index;
  ADIOI_Flatlist_node *flat_buf;
  flat_buf = ADIOI_Flatlist;
  while (flat_buf->type != dt) flat_buf = flat_buf->next;
  
  for (b_index=0; b_index < flat_buf->count; b_index++) {
    printf ("%3d-th: offset=%8lld len=%8d\n ",b_index, flat_buf->indices[b_index],flat_buf->blocklens[b_index]); 
  }
}


int cmp_flatlists(MPI_Datatype dt1,MPI_Datatype dt2) {
  int b_index;
  ADIOI_Flatlist_node *flat_buf1,*flat_buf2;
  flat_buf1 = ADIOI_Flatlist;
  while ((flat_buf1)&&(flat_buf1->type != dt1)&&(flat_buf1->type != dt2))
    flat_buf1 = flat_buf1->next;

  if (flat_buf1 == NULL)
    handle_error(MPI_ERR_LASTCODE,"None of 2 datatypes was flattened\n");
  flat_buf2 = flat_buf1;
  if (flat_buf2->type == dt1) {
    while ((flat_buf2)&&(flat_buf2->type != dt2)) flat_buf2 = flat_buf2->next;
    if (flat_buf2 == NULL)
      handle_error(MPI_ERR_LASTCODE,"2nd datatype was not flattened\n");
  }
  else {
    while ((flat_buf2)&&(flat_buf2->type != dt1)) flat_buf2 = flat_buf2->next;
    if (flat_buf2 == NULL)
      handle_error(MPI_ERR_LASTCODE,"1st datatypes was not flattened\n");
  }
  if (flat_buf1->count != flat_buf2->count)
     handle_error(MPI_ERR_LASTCODE,"cmp_flatlists:Different number of flattened elements\n");
  for (b_index=0; b_index < flat_buf1->count; b_index++) 
    if ((flat_buf1->indices[b_index] != flat_buf2->indices[b_index])||
	(flat_buf1->blocklens[b_index] != flat_buf2->blocklens[b_index]))
      handle_error(MPI_ERR_LASTCODE,"cmp_flatlists:Different number of indices or blocklens\n");
  return 0;
}
*/
static char static_packed_dt[65536];
static char *packed_dt = static_packed_dt;
static int pack_ptr = 0;


void decode_dt(MPI_Datatype type) {
  
  int ni, na, nd, combiner, *i,k; 
  MPI_Aint *a; 
  MPI_Datatype *d; 

  MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
  if (combiner != MPI_COMBINER_NAMED){
    if (ni > 0) i = (int *) ADIOI_clarisse_clarisse_malloc(ni*sizeof(int));
    if (na > 0) a = (MPI_Aint *) ADIOI_clarisse_clarisse_malloc(na*sizeof(MPI_Aint));
    if (nd > 0) d = (MPI_Datatype *) ADIOI_clarisse_clarisse_malloc(nd*sizeof(MPI_Datatype));
    MPI_Type_get_contents(type, ni, na, nd, i, a, d); 
  }
  
  for (k=0;k<nd;k++)
    decode_dt(d[k]);
  //  printf("DTYPE=%x\n",type);

  memcpy(packed_dt+pack_ptr,&combiner,sizeof(int));
  pack_ptr+=sizeof(int);
  if (combiner == MPI_COMBINER_NAMED){
    memcpy(packed_dt+pack_ptr,&type,sizeof(MPI_Datatype));
    pack_ptr+=sizeof(MPI_Datatype);
  }
  else {  
    if (ni > 0) {
      memcpy(packed_dt+pack_ptr,i,ni*sizeof(int));
      pack_ptr+=ni*sizeof(int);
    }
    if (na > 0) {
      memcpy(packed_dt+pack_ptr,a,na*sizeof(MPI_Aint));
      pack_ptr+=na*sizeof(MPI_Aint);    
    }
    /*    if (ni>1)
      printf("VECTOR: count=%d blocklen=%d stride=%d oldtype=%x\n",i[0],i[1],i[2],d[0]);
    */
    if (ni > 0) ADIOI_clarisse_clarisse_malloc(i);  
    if (na > 0) ADIOI_clarisse_clarisse_malloc(a);  
    if (nd > 0) ADIOI_clarisse_clarisse_malloc(d);
  }
}
/*
void print_dt(MPI_Datatype type) {
  static int nest=0;
  int ni, na, nd, combiner, *i,k; 
  MPI_Aint *a; 
  MPI_Datatype *d; 

  MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
  if (combiner != MPI_COMBINER_NAMED){
    if (ni > 0) i = (int *) ADIOI_clarisse_clarisse_malloc(ni*sizeof(int));
    if (na > 0) a = (MPI_Aint *) ADIOI_clarisse_clarisse_malloc(na*sizeof(MPI_Aint));
    if (nd > 0) d = (MPI_Datatype *) ADIOI_clarisse_clarisse_malloc(nd*sizeof(MPI_Datatype));
    MPI_Type_get_contents(type, ni, na, nd, i, a, d); 
  }
  
  for (k=0;k<nest;k++) printf("  ");

  switch (combiner) {
  case  MPI_COMBINER_NAMED:
    printf("NAMED DTYPE=%x\n",type);
    break;
  case MPI_COMBINER_DUP:
    printf("DUP DTYPE\n");
    break;
  case MPI_COMBINER_CONTIGUOUS: 
    printf("CONTIGUOUS DTYPE count=%d\n",i[0]);
    break;
  case MPI_COMBINER_VECTOR:
    printf("VECTOR DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],i[2]);
    break;
  case MPI_COMBINER_HVECTOR_INTEGER:
    printf("HVECTOR_INTEGER DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],a[0]);
    break;
  case MPI_COMBINER_HVECTOR: 
    printf("HVECTOR DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],a[0]);
    break;
  case MPI_COMBINER_INDEXED:
    printf("INDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],i[1+i[0]+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_HINDEXED_INTEGER:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],a[k]); 
    printf("\n");
    break;
    
  case MPI_COMBINER_HINDEXED:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_INDEXED_BLOCK:
    printf("HINDEXED_BLOCK DTYPE count=%d blocklen=%d ",i[0],i[1]);
    for (k=0;k<i[0];k++) 
      printf("[%d]disp=%d ",k,i[2+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_STRUCT_INTEGER:
  case MPI_COMBINER_STRUCT:
    printf("STRUCT DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_SUBARRAY:
    printf("SUBARRAY DTYPE ndims=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]size=%d subsize=%d start=%d",k,i[1+k],i[1+i[0]+k],i[1+2*i[0]+k]); 
 
    printf("order=%d\n",i[1+3*i[0]]);
    break;
  case MPI_COMBINER_DARRAY:
    printf("DARRAY DTYPE size=%d rank=%d ndims=%d ",i[0],i[1],i[2]);
    for (k=0;k<i[0];k++) 
      printf("[%d]gsize=%d distrib=%d darg=%d psize=%d",k,i[3+k],i[3+i[0]+k],i[3+2*i[0]+k],i[3+3*i[0]+k] ); 
 
    printf("order=%d\n",i[3+4*i[0]]);
    break;
  case MPI_COMBINER_RESIZED:
     printf("RESIZED DTYPE lb=%d, extent=%d\n",i[0],i[1]);
     break;
  default:
    handle_error(MPI_ERR_LASTCODE,"Error: unkonwn combiner\n");
  }
  nest++;
  for (k=0;k<nd;k++)
    print_dt(d[k]);
  nest--;
  //  printf("DTYPE=%x\n",type);

  if (ni > 0) ADIOI_clarisse_clarisse_malloc(i);  
  if (na > 0) ADIOI_clarisse_clarisse_malloc(a);  
  if (nd > 0) ADIOI_clarisse_clarisse_malloc(d);
  
}
*/


#define MAX_STACK 100
static int sp;
static MPI_Datatype stack[MAX_STACK];

static void push(MPI_Datatype el){
  if (sp == MAX_STACK) 
    handle_error(MPI_ERR_LASTCODE,"Error: Datatype decoding stack full\n");
  else 
    stack[sp++] = el;
}


/**********************
Decodes the data type "type" and packs it POST-ORDER in the buf. 
If reused, the content of the buf is overwritten

**********************/
int decode_pack_dt(MPI_Datatype type,char**buf, int displ){
  pack_ptr = displ;
  decode_dt(type);
  *buf = packed_dt;
  return pack_ptr;
}

// uses a buffer allocated outside
int decode_pack_dt_out_buf(MPI_Datatype type, char**buf){
  char *aux;
  aux = packed_dt;
  packed_dt = *buf;
  pack_ptr = 0;
  decode_dt(type);
  *buf = packed_dt;
  packed_dt = aux;
  return pack_ptr;
}

static MPI_Datatype pop(){
   if (sp == 0)
     handle_error(MPI_ERR_LASTCODE,"Error: Datatype decoding stack empty\n");
   else 
     return stack[--sp];
   return -1;
}


/**********************
Reconstructs a datatype from a POST-ORDER packed datatype
**********************/
MPI_Datatype reconstruct_dt(char *buf, int buf_sz) {
  int buf_ptr = 0,combiner;
  MPI_Datatype newtype;
  sp = 0;
 
  while (buf_ptr < buf_sz) {
    memcpy(&combiner,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
        
    switch (combiner) {
    case  MPI_COMBINER_NAMED:
      memcpy(&newtype,buf+buf_ptr,sizeof(MPI_Datatype)); buf_ptr+=sizeof(MPI_Datatype);
      break;

    case MPI_COMBINER_DUP: {
      MPI_Datatype oldtype = pop();
      MPI_Type_dup(oldtype,&newtype);
      break;

    }
    case MPI_COMBINER_CONTIGUOUS: {
      MPI_Datatype oldtype = pop();
      int count;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_contiguous(count,oldtype,&newtype);
      break;
    } 
    case MPI_COMBINER_VECTOR: {
      MPI_Datatype oldtype = pop();
      int count, blocklen,stride;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&blocklen,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&stride,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_vector(count, blocklen,stride,oldtype,&newtype); 
      break;
    }
    case MPI_COMBINER_HVECTOR_INTEGER:
    case MPI_COMBINER_HVECTOR: {
      MPI_Datatype oldtype = pop();
      int count, blocklen;
      MPI_Aint stride;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&blocklen,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&stride,buf+buf_ptr,sizeof(MPI_Aint)); buf_ptr+=sizeof(MPI_Aint);
      MPI_Type_hvector (count, blocklen,stride,oldtype,&newtype);
      break;

    }
    case MPI_COMBINER_INDEXED:{
      MPI_Datatype oldtype = pop();
      int count, *array_of_blocklengths, *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_blocklengths = (int*) ADIOI_clarisse_clarisse_malloc(count*sizeof(int));
      array_of_displacements = (int*) ADIOI_clarisse_clarisse_malloc(count*sizeof(int));
      memcpy(array_of_blocklengths,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      MPI_Type_indexed(count,array_of_blocklengths,array_of_displacements,oldtype,&newtype);
      ADIOI_clarisse_clarisse_malloc(array_of_blocklengths);
      ADIOI_clarisse_clarisse_malloc(array_of_displacements);
      break;
    }
    case MPI_COMBINER_HINDEXED_INTEGER:
    case MPI_COMBINER_HINDEXED:{
      MPI_Datatype oldtype = pop();
      int count, *array_of_blocklengths; 
      MPI_Aint *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_blocklengths=(int*) ADIOI_clarisse_clarisse_malloc(count*sizeof(int));
      array_of_displacements = (MPI_Aint*) ADIOI_clarisse_clarisse_malloc(count*sizeof(MPI_Aint));
      memcpy(array_of_blocklengths,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(MPI_Aint));buf_ptr+=count*sizeof(MPI_Aint);
      MPI_Type_hindexed(count,array_of_blocklengths,array_of_displacements,oldtype,&newtype);
      ADIOI_clarisse_clarisse_malloc(array_of_blocklengths);
      ADIOI_clarisse_clarisse_malloc(array_of_displacements);
      break;
   }
    case MPI_COMBINER_INDEXED_BLOCK:{
      MPI_Datatype oldtype = pop();
      int count, blocklength, *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_displacements = (int*) ADIOI_clarisse_clarisse_malloc(count*sizeof(int));
      memcpy(&blocklength,buf+buf_ptr,sizeof(int));buf_ptr+=sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      MPI_Type_create_indexed_block(count,blocklength,array_of_displacements,oldtype,&newtype);
      ADIOI_clarisse_clarisse_malloc(array_of_displacements);
      break;
   }
    case MPI_COMBINER_STRUCT_INTEGER:
    case MPI_COMBINER_STRUCT:{
      MPI_Datatype *array_of_types;
      int count, *array_of_blocklengths,k; 
      MPI_Aint *array_of_displacements;
      memcpy(&count,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_blocklengths = (int*) ADIOI_clarisse_clarisse_malloc(count*sizeof(int));
      array_of_displacements = (MPI_Aint*) ADIOI_clarisse_clarisse_malloc(count*sizeof(MPI_Aint));
      array_of_types = (MPI_Datatype*) ADIOI_clarisse_clarisse_malloc(count*sizeof(MPI_Datatype));
      for (k=0;k<count;k++) array_of_types[count-k-1] = pop();
      memcpy(array_of_blocklengths,buf+buf_ptr,count*sizeof(int));buf_ptr+=count*sizeof(int);
      memcpy(array_of_displacements,buf+buf_ptr,count*sizeof(MPI_Aint));buf_ptr+=count*sizeof(MPI_Aint);      
      MPI_Type_struct(count,array_of_blocklengths,array_of_displacements,array_of_types,&newtype);
      ADIOI_clarisse_clarisse_malloc(array_of_blocklengths);
      ADIOI_clarisse_clarisse_malloc(array_of_displacements);
      ADIOI_clarisse_clarisse_malloc(array_of_types);
      break;
    }
    case MPI_COMBINER_SUBARRAY:{
      MPI_Datatype oldtype = pop();
      int ndims, *array_of_sizes, *array_of_subsizes, *array_of_starts, order;
      memcpy(&ndims,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_sizes = (int*) ADIOI_clarisse_clarisse_malloc(ndims*sizeof(int));
      array_of_subsizes = (int*) ADIOI_clarisse_clarisse_malloc(ndims*sizeof(int));
      array_of_starts = (int*) ADIOI_clarisse_clarisse_malloc(ndims*sizeof(int));
      memcpy(array_of_sizes,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(array_of_subsizes,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(array_of_starts,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(&order,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_subarray (ndims,array_of_sizes,array_of_subsizes,array_of_starts,order,oldtype,&newtype); 
      ADIOI_clarisse_clarisse_malloc(array_of_sizes);
      ADIOI_clarisse_clarisse_malloc(array_of_subsizes);
      ADIOI_clarisse_clarisse_malloc(array_of_starts);
      break;
   }
    case MPI_COMBINER_DARRAY:{
      MPI_Datatype oldtype = pop();
      int size,rank,ndims, *array_of_gsizes, *array_of_distribs, *array_of_dargs, *array_of_psizes,order;     
      memcpy(&size,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&rank,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&ndims,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      array_of_gsizes = (int*) ADIOI_clarisse_clarisse_malloc(ndims*sizeof(int));
      array_of_distribs = (int*) ADIOI_clarisse_clarisse_malloc(ndims*sizeof(int));
      array_of_dargs = (int*) ADIOI_clarisse_clarisse_malloc(ndims*sizeof(int));
      array_of_psizes = (int*) ADIOI_clarisse_clarisse_malloc(ndims*sizeof(int));
      memcpy(array_of_gsizes,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(array_of_distribs,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(array_of_dargs,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(array_of_psizes,buf+buf_ptr,ndims*sizeof(int));buf_ptr+=ndims*sizeof(int);
      memcpy(&order,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_darray(size,rank,ndims,array_of_gsizes,array_of_distribs,array_of_dargs,array_of_psizes,order,oldtype,&newtype); 
      ADIOI_clarisse_clarisse_malloc(array_of_gsizes);
      ADIOI_clarisse_clarisse_malloc(array_of_distribs);
      ADIOI_clarisse_clarisse_malloc(array_of_dargs);
      ADIOI_clarisse_clarisse_malloc(array_of_psizes);
      break;
    }
#ifdef FORTRAN90
    case MPI_COMBINER_F90_REAL:{
      int p,r;
      memcpy(&p,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&r,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_f90_real(p,r,&newtype);
      break;
    }
    case MPI_COMBINER_F90_COMPLEX:{
      int p,r;
      memcpy(&p,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      memcpy(&r,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_f90_complex(p,r,&newtype);
      break;
    }
    case MPI_COMBINER_F90_INTEGER:{
      int r;
      memcpy(&r,buf+buf_ptr,sizeof(int)); buf_ptr+=sizeof(int);
      MPI_Type_create_f90_integer(r,&newtype);
      break;
    }
#endif
    case MPI_COMBINER_RESIZED:{
      MPI_Datatype oldtype = pop();
      MPI_Aint lb,extent;
      memcpy(&lb,buf+buf_ptr,sizeof(MPI_Aint)); buf_ptr+=sizeof(MPI_Aint);
      memcpy(&extent,buf+buf_ptr,sizeof(MPI_Aint)); buf_ptr+=sizeof(MPI_Aint);
      MPI_Type_create_resized(oldtype,lb,extent,&newtype);
      break;
    }

    default: {
      char err_msg[128];
      sprintf(err_msg, "Error: unkonwn combiner %d\n", combiner);
      handle_error(MPI_ERR_LASTCODE,err_msg);
    }
    }
    push(newtype);
    if (combiner != MPI_COMBINER_NAMED)
      MPI_Type_commit(&newtype);

  }
  if (sp!=1)
    handle_error(MPI_ERR_LASTCODE,"Error: stack has more than one element\n");
  return stack[sp-1];
}


void test_decode_unpack(MPI_Datatype dt) {
  MPI_Datatype new_dt;
  int ret;
  //printf("FLATTENING\n");
  ADIOI_Flatten_datatype(dt);
  //printf("DECODING\n");
  pack_ptr = 0;
  decode_dt(dt);
  //printf("RECONSTRUCTING\n");
  new_dt = reconstruct_dt(packed_dt,pack_ptr);
  //printf("FLATTENING\n");
  ADIOI_Flatten_datatype(new_dt);
    print_flatlist(dt);
   print_flatlist(new_dt);
  //printf("COMPARING\n");
  ret = cmp_flatlists(dt,new_dt);
  //printf("RETURNING");
  /*if (ret) 
    handle_error(MPI_ERR_LASTCODE,"Different FLATLISTS\n");
  else 
    printf("SIMILAR FLATLISTS\n");
  */
}
/*
void handle_error(int errcode, char *str) 
{
    char msg[MPI_MAX_ERROR_STRING];
    char   processor_name[MPI_MAX_PROCESSOR_NAME];
    int namelen,resultlen,my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Get_processor_name(processor_name,&namelen);
    MPI_Error_string(errcode, msg, &resultlen);
    fprintf(stderr, "Process %d on %s %s: %s\ ", my_rank, processor_name,str, msg);
    MPI_Abort(MPI_COMM_WORLD, 1);
}
*/

/*
int main(int argc, char* argv[]) {
    int my_rank,err_code;
    //MPI_Status status;
    
    MPI_Datatype column_mpi_t,nest;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    MPI_Type_vector(10, 1, 10, MPI_FLOAT, &column_mpi_t);
    MPI_Type_commit(&column_mpi_t);
    
    MPI_Type_vector(2, 1, 2, column_mpi_t,&nest);
    MPI_Type_commit(&nest);


    ADIO_Init(&argc, &argv,&err_code);
    ADIOI_Flatten_datatype(column_mpi_t);

    print_flatlist(column_mpi_t);

    decode_dt(nest);
    //decode_dt(MPI_FLOAT);
    decode_dt(reconstruct_dt( packed_dt,pack_ptr));

    MPI_Finalize();
    return 0;
}  // main 
*/
