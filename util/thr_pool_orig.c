/* Thread pool implementation from Pthreads Programming
Bradford Nichols, Dick Buttlar and Jacqueline Proulx Farrell
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "thr_pool.h"

void *tpool_thread(void *arg);

/* The tpool_init routine, shown in Example 3-23, initializes a thread
   pool. The routine sets the basic characteristics of the thread pool
   by copying into the tpoolt structure the values of its three input
   parameters (num_worker_threads,max_queue_size, and
   do_not_block_when_full). It also initializes the thread pool's
   state.
 */

void init_queue(tpool_t tpool){
  tpool->queue_head = NULL;	
  tpool->queue_tail = NULL;	
}

void tpool_init(tpool_t   *tpoolp,	
                int       num_worker_threads,	
                int       max_queue_size,	
                int       do_not_block_when_full)	
{	
   int i, rtn;	
   tpool_t tpool;	
   /* allocate a pool data structure */	
   if ((tpool = (tpool_t )malloc(sizeof(struct tpool))) == NULL)	
     perror("malloc"), exit(-1);	
   /* initialize the fields */	
   tpool->num_threads = num_worker_threads;	
   tpool->max_queue_size = max_queue_size;	
   tpool->do_not_block_when_full = do_not_block_when_full;	
   if ((tpool->threads =	
         (pthread_t *)malloc(sizeof(pthread_t)*num_worker_threads))	
           == NULL)	
     perror("malloc"), exit(-1);	
   tpool->cur_queue_size = 0;	
   init_queue(tpool);
   tpool->queue_closed = 0;	
   tpool->shutdown = 0;	
   if ((rtn = pthread_mutex_init(&(tpool->queue_lock), NULL)) != 0)	
        fprintf(stderr,"pthread_mutex_init %s",strerror(rtn)), exit(-1);	
   if ((rtn = pthread_cond_init(&(tpool->queue_not_empty), NULL)) != 0)	
        fprintf(stderr,"pthread_cond_init %s",strerror(rtn)), exit(-1);	
   if ((rtn = pthread_cond_init(&(tpool->queue_not_full), NULL)) != 0)	
        fprintf(stderr,"pthread_cond_init %s",strerror(rtn)), exit(-1);	
   if ((rtn = pthread_cond_init(&(tpool->queue_empty), NULL)) != 0)	
        fprintf(stderr,"pthread_cond_init %s",strerror(rtn)), exit(-1);	
   /* create threads */	
   for (i = 0; i != num_worker_threads; i++) {	
        if ((rtn = pthread_create(&(tpool->threads[i]),	
				  NULL,	
				  tpool_thread,	
				  (void *)tpool)) != 0)	
           fprintf(stderr,"pthread_create %d",rtn), exit(-1);	
   }	
   *tpoolp = tpool;	
}	
 
/* The tpool_thread routine, contains the logic each worker uses to
check the queue for work and take appropriate action depending upon
whether or not a request is available. It takes a single argument≈a
pointer to the tpool_t structure for the pool to which the thread
belongs.  
The body of the routine is a loop in which the worker checks
the request queue. If it's empty, the worker sleeps on the
queue_not_empty condition variable. It can be awakened by either a
shutdown request from tpool_destroy or a work item placed on its
request queue. When awakened by a shutdown request, the worker
exits. When awakened by a work request, however, it rechecks the
queue, removes the request from the queue's head, and executes the
routine specified in the request (using any associated argument). If
the worker finds that the queue was full before it removed the node
and knows that threads may be blocked waiting to add to the queue
(because the pool's do_not_block_when_full characteristic is not set),
it signals the queue_not_full condition. Likewise, if this thread
empties the queue, it signals queue_empty to allow a delayed shutdown
to proceed.
*/ 

tpool_work_t * get_work(tpool_t tpool){
  tpool_work_t *my_workp;
    
  my_workp = tpool->queue_head;	
  tpool->cur_queue_size--;	
  if (tpool->cur_queue_size == 0)	
    tpool->queue_head = tpool->queue_tail = NULL;	
  else	
    tpool->queue_head = my_workp->next;	
  
  return my_workp;
}


void *tpool_thread(void * arg) {	
  tpool_t tpool;
  tpool_work_t *my_workp;	
  
  tpool = (tpool_t)arg;
  for (;;) {	
    pthread_mutex_lock(&(tpool->queue_lock));	
    while ( (tpool->cur_queue_size == 0) && (!tpool->shutdown)) {	
      pthread_cond_wait(&(tpool->queue_not_empty),	
			&(tpool->queue_lock));	
    }	
    if (tpool->shutdown) {	
      pthread_mutex_unlock(&(tpool->queue_lock));	
      pthread_exit(NULL);	
    }	
    my_workp = get_work(tpool);

    if ((!tpool->do_not_block_when_full) &&	
	(tpool->cur_queue_size == (tpool->max_queue_size - 1)))	
      pthread_cond_broadcast(&(tpool->queue_not_full));	
    if (tpool->cur_queue_size == 0)	
      pthread_cond_signal(&(tpool->queue_empty));	
    pthread_mutex_unlock(&(tpool->queue_lock));	
    (*(my_workp->routine))(my_workp->arg);	
    free(my_workp);	
  }	
}

/*
The tpool_add_work routine checks the do_not_block_when_full flag and
examines the current size of the request queue. If the queue is full,
the routine either returns an error to its caller or suspends itself
on the queue_not_full condition, depending on the value of the pool's
do_not_block_when_full flag. In the latter case, the tpool_add_work
routine resumes when the condition is signaled; it queues the request
and returns to its caller.
*/


void add_queue(tpool_t tpool, void *routine, void *arg){
  tpool_work_t *workp;	

  /* allocate work structure */	
  workp = (tpool_work_t *)malloc(sizeof(tpool_work_t));	
  workp->routine = routine;	
  workp->arg = arg;	
  workp->next = NULL;	
  if (tpool->cur_queue_size == 0) {	
    tpool->queue_tail = tpool->queue_head = workp;	
    pthread_cond_broadcast(&tpool->queue_not_empty);	
  } else {	
    (tpool->queue_tail)->next = workp;	
    tpool->queue_tail = workp;	
  }	
  tpool->cur_queue_size++;	
}


int tpool_add_work(tpool_t tpool, void *routine, void *arg)	
{	

  pthread_mutex_lock(&tpool->queue_lock);	
  if ((tpool->cur_queue_size == tpool->max_queue_size) &&	
      tpool->do_not_block_when_full) {	
    pthread_mutex_unlock(&tpool->queue_lock);	
    return -1;	
  }	
  while ((tpool->cur_queue_size == tpool->max_queue_size) &&	
	 (!(tpool->shutdown || tpool->queue_closed))) {	
    pthread_cond_wait(&tpool->queue_not_full, &tpool->queue_lock);	
  }	
  if (tpool->shutdown || tpool->queue_closed) {	
    pthread_mutex_unlock(&tpool->queue_lock);	
    return -1;	
  }	

  add_queue(tpool, routine, arg);
  pthread_mutex_unlock(&tpool->queue_lock);	
  return 1;	
}

/*
tpool_destroy deallocates a thread pool. It sets the shutdownflag in
the tpool _t structure to indicate to workers (and threads calling
tpool_add_work) that the pool is being deactivated. Worker threads
exit when they find this flag set; the tpool_add_work routine returns
a -1 to its caller,

The tpool_destroy routine ensures that all threads are awake to see
the shutdown flag by signaling both the queue_not_empty and
queue_not_full conditions. Even still, some threads may be busy
completing their current requests; it may still be some time before
they learn that a shutdown has begun. To avoid interfering with
in-progress requests, tpool_destroy waits for all worker threads to
exit by calling pthread_join for each thread. When all workers have
departed, tpool_destroy frees the pool's data structures.

The current edition of our tpool_destroy routine is not without its
surprises. When it sets the shutdown flag, only those requests that
are currently in progress are completed. Any requests that are still
in the request queue are lost when the thread pool is
deallocated. Instead, it could disallow additions to the queue and
wait for the queue to empty before deactivating the thread pool. It
could also speed performance by canceling workers rather than waiting
for them to check the shutdown flag.

 */

void free_queue(tpool_t  tpool){
  tpool_work_t *cur_nodep;	

  while(tpool->queue_head != NULL) {	
    cur_nodep = tpool->queue_head->next;	
    tpool->queue_head = tpool->queue_head->next;	
    free(cur_nodep);	
  }	
}


int tpool_destroy(tpool_t    tpool,	
                  int        finish)	{	
  int          i, rtn;	
  if ((rtn = pthread_mutex_lock(&(tpool->queue_lock))) != 0)	
    fprintf(stderr,"pthread_mutex_lock %d",rtn), exit(-1);	
  /* Is a shutdown already in progress? */	
  if (tpool->queue_closed || tpool->shutdown) {	
    if ((rtn = pthread_mutex_unlock(&(tpool->queue_lock))) != 0)	
      fprintf(stderr,"pthread_mutex_unlock %d",rtn), exit(-1);	
    return 0;	
  }	
  tpool->queue_closed = 1;	
  /* If the finish flag is set, wait for workers to drain queue */	
  if (finish == 1) {	
    while (tpool->cur_queue_size != 0) {	
      if ((rtn = pthread_cond_wait(&(tpool->queue_empty),	
				   &(tpool->queue_lock))) != 0)	
	fprintf(stderr,"pthread_cond_wait %d",rtn), exit(-1);	
    }	
  }	
  tpool->shutdown = 1;	
  if ((rtn = pthread_mutex_unlock(&(tpool->queue_lock))) != 0)	
    fprintf(stderr,"pthread_mutex_unlock %d",rtn), exit(-1);	
  /* Wake up any workers so they recheck shutdown flag */	
  if ((rtn = pthread_cond_broadcast(&(tpool->queue_not_empty))) != 0)	
    fprintf(stderr,"pthread_cond_broadcast %d",rtn), exit(-1);	
  if ((rtn = pthread_cond_broadcast(&(tpool->queue_not_full))) != 0)	
    fprintf(stderr,"pthread_cond_broadcast %d",rtn), exit(-1);	
  /* Wait for workers to exit */	
  for(i=0; i < tpool->num_threads; i++) {	
    if ((rtn = pthread_join(tpool->threads[i],NULL)) != 0)	
      fprintf(stderr,"pthread_join %d",rtn), exit(-1);	
  }	
  /* Now free pool structures */	
  free(tpool->threads);	
  free_queue(tpool);
  free(tpool);	
  return 0;	
}
