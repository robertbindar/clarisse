#include <string.h>
#include <stdio.h>
#include "list.h"
#include "mpi.h"

struct i{ int i;}*i_p;

#define ITEM(it) ((struct i*)(((item_p)it)->data))->i 


void print_list(List_p l)
{
  item_p i;
  printf("List:\t");

  for(i=l->head;i!=NULL;i=i->next) {
    printf("%d\t",ITEM(i));
  }
}
    
//void* find( void* key, int (*cmp)(void*,void*),List_p l)

void dump_int_list(List_p l)
{
  item_p i;
  for(i=l->head;i!=NULL;i=i->next) {
    printf("%3lld ",(long long int)i->data);
  }
  printf("\n");
}
   


int main()
{
  char c;
  List l,l2;
  List_p l_p;
  int i=0,j=0;
  item_p items[6];

  init_list(&l);
  printf("e=enqueue d=dequeue i=insert a= append l to l p=print ?=is empty\n");
  scanf("%c",&c);
  printf("c=%c\n",c);
  while(c!='n')
    {
      switch(c)
	{
	case 'e': 
	  i_p=(struct i*)malloc(sizeof(struct i));
	  i_p->i=i++;
	  enqueue(i_p,&l);print_list(&l);
	  items[j++]=l.tail;
	  printf("l.tail=%p\n", l.tail);
	  break;
	case 'i': 
	  i_p=(struct i*)malloc(sizeof(struct i));
	  i_p->i=i++;
	  add_in_front(i_p,&l);print_list(&l);
	  break; 
	case 'p': 
	  print_list(&l);
	  break;
	case 'd':  
	  dequeue(&l);print_list(&l);
	  break;
	case 'a':
	  init_list(&l2);

	  i_p=(struct i*)malloc(sizeof(struct i));
	  i_p->i=10;
	  add_in_front(i_p,&l2);
	  
	  i_p=(struct i*)malloc(sizeof(struct i));
	  i_p->i=11;
	  add_in_front(i_p,&l2);
	  

	  //printf(dump_list(&l2));
	  l_p=list_append(&l,&l2);
	  print_list(l_p);
	  break;
	  
	case 'l':
	  printf("length=%d\n",l.length);
	  break;
	  
	case 'r':
	  printf("i="); fflush(stdout);
	  scanf("%d",&i);
	  printf("i=%d\n",i); fflush(stdout);
	  remove_item(items[i],&l);
	  printf("rm it=%p\n", items[i]);
	  print_list(&l);
	  break; 

	case '?':
	  if(empty_list(&l))
	    printf("Empty\n");
	  else
	    printf("Not Empty\n");
	}
      printf("e=enqueue d=dequeue i=insert p=print r=remove?=is empty\n");
      scanf("\n%c",&c);
      printf("c=%c\n",c);

    }
  return 0;
}
