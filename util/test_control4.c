#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include "mpi.h"
#include "error.h"

// Replace SIGALRM by pthread_wait_timedcond
// publisher and subscriber threads fused in one thread
// Implement one subscriber, multiple publishers first
// Assume non repeating subscribe by now



pthread_mutex_t     pub_mutex = PTHREAD_MUTEX_INITIALIZER;
int start_publisher_thread = 0;
pthread_cond_t      pub_cond_start_publisher = PTHREAD_COND_INITIALIZER;
int event_flag=0;
pthread_cond_t      pub_cond_wait_event = PTHREAD_COND_INITIALIZER;

#define CLARISSE_CTRL_TAG 29092014

#define CLARISSE_CTRL_MSG_DONE 0

#define CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY 1
#define CLARISSE_CTRL_MSG_SUBSCRIBE_LOAD 2
#define CLARISSE_CTRL_MSG_SUBSCRIBE_HEARTBEAT 3

#define CLARISSE_CTRL_MSG_PUBLISH_MEMORY 11
#define CLARISSE_CTRL_MSG_PUBLISH_LOAD 12
#define CLARISSE_CTRL_MSG_PUBLISH_HEARTBEAT 13

#define PUBLISH_INTERVAL_SEC 1

typedef struct {  
  int memory;
} clarisse_ctrl_msg_memory_t;


typedef struct {
  MPI_Comm comm;
  int dest;
} memory_params_t;

typedef struct {
  int type;
  union msg_t {
    clarisse_ctrl_msg_memory_t clarisse_ctrl_msg_memory;
    //clarisse_ctrl_msg_load_t clarisse_ctrl_msg_load;
    
    //clarisse_ctrl_msg_hearbeat_t clarisse_ctrl_msg_heartbeat;
    //void *clarisse_fd;
  } payload;
} clarisse_ctrl_msg_t;




void* publisher_thread(void *ptr){
  memory_params_t *mp = (memory_params_t *)ptr;
  clarisse_ctrl_msg_t msg;
  int mem = 0;
  int done = 0;
  struct timespec timeout;

  clock_gettime(CLOCK_REALTIME, &timeout);
  timeout.tv_sec += PUBLISH_INTERVAL_SEC;

  pthread_mutex_lock (&pub_mutex);
  while (start_publisher_thread == 0) {
#ifdef DEBUG
    printf("Thread %s waits for the condition to become true\n",__FUNCTION__);
#endif
    pthread_cond_wait (&pub_cond_start_publisher,&pub_mutex);
  }
  printf("Publisher start_publisher_thread=%d\n", start_publisher_thread);
  pthread_mutex_unlock (&pub_mutex);
  
  msg.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  while (!done) {
    int err;
#ifdef DEBUG
    printf("Polling\n");
#endif
    pthread_mutex_lock (&pub_mutex);
    while (!event_flag) {
      err = pthread_cond_timedwait (&pub_cond_wait_event,&pub_mutex, &timeout);  
      if (err == ETIMEDOUT) {
	pthread_mutex_unlock (&pub_mutex);
	printf("TIMEOUT, check again the condition\n");
	timeout.tv_sec += PUBLISH_INTERVAL_SEC;
	msg.payload.clarisse_ctrl_msg_memory.memory = mem;
	err = MPI_Send(&msg, sizeof(int) + sizeof(msg.payload.clarisse_ctrl_msg_memory), MPI_BYTE, mp->dest, CLARISSE_CTRL_TAG, mp->comm);
	if (err != MPI_SUCCESS)
	  handle_err(err, "MPI_Send error");
	mem++;
	pthread_mutex_lock (&pub_mutex);
      }
      else 
	if (err != 0) {
	  perror("pthread_cond_timedwait");
	  exit(1);
	}   
    }
    if (start_publisher_thread == 0)
      done = 1;
    else 
      event_flag = 0;
    pthread_mutex_unlock (&pub_mutex);
  }
  printf("PUBLISHER DONE\n");
  free(mp);
  return NULL;
}

void * controller_recv_thread(/*void *ptr*/)
{
  int done = 0;
  //int *pval = (int*)ptr;
  MPI_Comm comm = MPI_COMM_WORLD;
  double t0 = MPI_Wtime();

  while (!done) {
    clarisse_ctrl_msg_t msg;
    MPI_Status status;
#ifdef DEBUG
    printf("Controller recving ...\n");
#endif
    MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , comm, &status);
   
    switch (msg.type) {
    case CLARISSE_CTRL_MSG_DONE: 
      {
	int pending_comm = 1;
	printf("%9.6f: Controller recved DONE (msg type = %d) from %d\n", MPI_Wtime() - t0, msg.type, status.MPI_SOURCE);
	done = 1;
	//sleep(2);
	while (pending_comm){
#ifdef DEBUG
	  printf("Pending comm\n");
#endif
	  MPI_Iprobe(MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , comm, &pending_comm, &status);
	  if (pending_comm) {
	      MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , comm, &status);
	      printf("%9.6f: Controller recved msg type = %d from %d \n", MPI_Wtime() - t0, msg.type, status.MPI_SOURCE);
	  }
	}

	break;
      }
    case CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY: 
      {
	
	pthread_mutex_lock( &pub_mutex );
	start_publisher_thread = 1;
	pthread_cond_signal( &pub_cond_start_publisher);
	pthread_mutex_unlock( &pub_mutex );
	//tpool_add_work(global_thread_pool, publisher_thread, (void *)mp, (void*)0);
	break;
      }
    case CLARISSE_CTRL_MSG_PUBLISH_MEMORY: 
      {
	printf("%9.6f: Subscriber received meg type = %d from %d avail memory=%d\n", MPI_Wtime() - t0, CLARISSE_CTRL_MSG_PUBLISH_MEMORY, status.MPI_SOURCE, msg.payload.clarisse_ctrl_msg_memory.memory);
	break;
      }
    }
    
  }
  return NULL;
}

void my_busywaiting(int seconds){
  double start = MPI_Wtime();
  while ((MPI_Wtime() - start) < seconds);
}

// add tasks from a task
void test0(MPI_Comm comm){
  int myrank, nprocs, ret;
  pthread_t controller_tid, pub_tid;
  
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &nprocs);

  memory_params_t *mp = (memory_params_t *) malloc(sizeof( memory_params_t));
  mp->dest = 0; 
  mp->comm = comm;

  if ((ret = pthread_create(&controller_tid, NULL, controller_recv_thread, NULL)) != 0) {	
    fprintf(stderr,"pthread_create %d",ret); 
    exit(-1);
  }
  if (myrank != 0) {
    if ((ret = pthread_create(&pub_tid, NULL, publisher_thread, mp)) != 0) {	
      fprintf(stderr,"pthread_create %d",ret); 
      exit(-1); 
    }
  }

  if (myrank == 0) {
    int err, i;
    clarisse_ctrl_msg_t msg;

    msg.type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
    for (i = 1; i <  nprocs; i++) {
      err = MPI_Send(&msg, sizeof(int), MPI_BYTE, i, CLARISSE_CTRL_TAG, comm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Send error");
    }
  }
  my_busywaiting(3);
  if (myrank == 0) {
    int err, i;
    clarisse_ctrl_msg_t msg;
    msg.type = CLARISSE_CTRL_MSG_DONE;
    for (i = 0; i <  nprocs; i++) {
      err = MPI_Send(&msg, sizeof(int), MPI_BYTE, i, CLARISSE_CTRL_TAG, comm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Send error");
    }
  }
 
  if (myrank != 0) {
    pthread_mutex_lock (&pub_mutex);
    start_publisher_thread = 0;
    event_flag = 1;
    pthread_cond_signal( &pub_cond_wait_event );
    pthread_mutex_unlock (&pub_mutex);

    ret = pthread_join(pub_tid, NULL);
    if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); }
  }

  ret = pthread_join(controller_tid, NULL);
  if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); } 	
  //sleep(1);
  


}


int main(int argc, char **argv){
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  if (provided != MPI_THREAD_MULTIPLE)
    handle_err(MPI_ERR_OTHER, "The provided mode different from MPI_THREAD_MULTIPLE).");

  test0(MPI_COMM_WORLD);
  //test1();
  //test2();
  //test3();
  //test4();
  //test5();
  MPI_Finalize();
  return 0;
}
