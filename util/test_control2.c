#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <sys/time.h>
#include "mpi.h"
#include "error.h"

// Polling version (signal version does not work on BGQ)
// Implement one subscriber, multiple publishers first
// Assume non repeating subscribe by now


int arrived = 0;
pthread_mutex_t     mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t      cond = PTHREAD_COND_INITIALIZER;
sigset_t signal_set; // also shared

#define CLARISSE_CTRL_TAG 29062014

#define CLARISSE_CTRL_MSG_DONE 0

#define CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY 1
#define CLARISSE_CTRL_MSG_SUBSCRIBE_LOAD 2
#define CLARISSE_CTRL_MSG_SUBSCRIBE_HEARTBEAT 3

#define CLARISSE_CTRL_MSG_PUBLISH_MEMORY 11
#define CLARISSE_CTRL_MSG_PUBLISH_LOAD 12
#define CLARISSE_CTRL_MSG_PUBLISH_HEARTBEAT 13



typedef struct {  
  int memory;
} clarisse_ctrl_msg_memory_t;


typedef struct {
  MPI_Comm comm;
  int dest;
} memory_params_t;

typedef struct {
  int type;
  union msg_t {
    clarisse_ctrl_msg_memory_t clarisse_ctrl_msg_memory;
    //clarisse_ctrl_msg_load_t clarisse_ctrl_msg_load;
    
    //clarisse_ctrl_msg_hearbeat_t clarisse_ctrl_msg_heartbeat;
    //void *clarisse_fd;
  } payload;
} clarisse_ctrl_msg_t;


int flag = 0;

void sig_alrm()
{ 
  static int count=0;

  printf("caught SIGALRM, count = %d\n", count++);
  flag = 1;
}


void* publisher_task_mem(void *ptr){
  memory_params_t *mp = (memory_params_t *)ptr;
  clarisse_ctrl_msg_t msg;
  int mem = 0;
  int done = 0;

  pthread_mutex_lock (&mutex);
  while (arrived == 0) {
#ifdef DEBUG
    printf("Thread %s waits for the condition to become true\n",__FUNCTION__);
#endif
    pthread_cond_wait (&cond,&mutex);
  }
  pthread_mutex_unlock (&mutex);
  
  struct itimerval interv;
  //alarm(1);
  interv.it_interval.tv_sec = 1;
  interv.it_interval.tv_usec = 0;
  interv.it_value.tv_sec = 1;
  interv.it_value.tv_usec = 0;
  setitimer(ITIMER_REAL, &interv, NULL);
  struct sigaction act;
  act.sa_handler = sig_alrm;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;
  sigaction(SIGALRM, &act, NULL);
  
  msg.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  while (!done) {
    int err;
#ifdef DEBUG
    printf("Calling sigwait\n");
#endif
    
    while (!flag);
    flag = 0;
    printf("Polling interrupted\n");

    msg.payload.clarisse_ctrl_msg_memory.memory = mem;
    err = MPI_Send(&msg, sizeof(int) + sizeof(msg.payload.clarisse_ctrl_msg_memory), MPI_BYTE, mp->dest, CLARISSE_CTRL_TAG, mp->comm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");
    mem++;
    pthread_mutex_lock (&mutex);
    if (arrived == 0)
      done = 1;
    pthread_mutex_unlock (&mutex);
  }
  free(mp);
  return NULL;
}


void *publisher_recv(/*void *ptr*/)
{
  int done = 0;
  //int *pval = (int*)ptr;
  int type; 
  MPI_Comm comm = MPI_COMM_WORLD;

  while (!done) {
    MPI_Status status;
#ifdef DEBUG
    printf("Publisher recving ...\n");
#endif
    MPI_Recv(&type, 1, MPI_INT, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , comm, &status);
#ifdef DEBUG
    printf("Publisher recved msg type =%d\n", type);
#endif
    switch (type) {
    case CLARISSE_CTRL_MSG_DONE: 
      {
	done = 1;
	break;
      }
    case CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY: 
      {
	
	pthread_mutex_lock( &mutex );
	arrived = 1;
	pthread_cond_signal( &cond );
	pthread_mutex_unlock( &mutex );
	//tpool_add_work(global_thread_pool, publisher_task_mem, (void *)mp, (void*)0);
	break;
      }
    }
  }
  return NULL;
}

void * subscriber_recv(/*void *ptr*/)
{
  int done = 0;
  //int *pval = (int*)ptr;
  MPI_Comm comm = MPI_COMM_WORLD;
  double t0 = MPI_Wtime();

  while (!done) {
    clarisse_ctrl_msg_t msg;
    MPI_Status status;
#ifdef DEBUG
    printf("Subscriber recving ...\n");
#endif
    MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , comm, &status);
   
    switch (msg.type) {
    case CLARISSE_CTRL_MSG_DONE: 
      {
	int pending_comm = 1;
	printf("%9.6f: Subscriber recved DONE from %d\n", MPI_Wtime() - t0, status.MPI_SOURCE);
	done = 1;
	sleep(2);
	while (pending_comm){
#ifdef DEBUG
	  printf("Pending comm\n");
#endif
	  MPI_Iprobe(MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , comm, &pending_comm, &status);
	  if (pending_comm) {
	      MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , comm, &status);
	      printf("%9.6f: Subscriber recved msg type = %d from %d \n", MPI_Wtime() - t0, msg.type, status.MPI_SOURCE);
	  }
	}

	break;
      }
    case CLARISSE_CTRL_MSG_PUBLISH_MEMORY: 
      {
	printf("%9.6f: Subscriber received from %d avail memory=%d\n", MPI_Wtime() - t0, status.MPI_SOURCE, msg.payload.clarisse_ctrl_msg_memory.memory);
	break;
      }
    }
  }
  return NULL;
}


// add tasks from a task
void test0(MPI_Comm comm){
  int myrank, nprocs, ret, stat;
  pthread_t sub_thr_id, pub_thr_id, pub_task_thr_id;
  
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &nprocs);

  memory_params_t *mp = (memory_params_t *) malloc(sizeof( memory_params_t));
  mp->dest = 0; 
  mp->comm = comm;

  if (myrank == 0) {
    if ((ret = pthread_create(&sub_thr_id, NULL, subscriber_recv, NULL)) != 0){	
      fprintf(stderr,"pthread_create %d",ret);
      exit(-1);
    }
  }
  else {
    if ((ret = pthread_create(&pub_thr_id, NULL, publisher_recv, NULL)) != 0) {	
      fprintf(stderr,"pthread_create %d",ret); 
      exit(-1);
    }
    if ((ret = pthread_create(&pub_task_thr_id, NULL, publisher_task_mem, mp)) != 0) {	
      fprintf(stderr,"pthread_create %d",ret); 
      exit(-1); 
    }
  }

  if (myrank == 0) {
    int err, type, i;

    type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
    for (i = 1; i <  nprocs; i++) {
      err = MPI_Send(&type, 1, MPI_INT, i, CLARISSE_CTRL_TAG, comm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Send error");
    }
    sleep(3);
    type = CLARISSE_CTRL_MSG_DONE;
    for (i = 1; i <  nprocs; i++) {
      err = MPI_Send(&type, 1, MPI_INT, i, CLARISSE_CTRL_TAG, comm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Send error");
    }
    err = MPI_Send(&type, 1, MPI_INT, 0, CLARISSE_CTRL_TAG, comm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");
    
  }
  if (myrank == 0) {
    ret = pthread_join(sub_thr_id, (void **)&stat);
    if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); } 	
#ifdef DEBUG
    printf("Completed sub_thr join with status= %d\n",stat); 
#endif
  } 
  else {
    ret = pthread_join(pub_thr_id, (void **)&stat);
    if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); } 	
#ifdef DEBUG
    printf("Completed pub_thr join with status= %d\n", stat); 
#endif
    sleep(1);
    pthread_mutex_lock (&mutex);
    arrived = 0;
    pthread_mutex_unlock (&mutex);
    ret = pthread_join(pub_task_thr_id, (void **)&stat);
    if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); } 	
#ifdef DEBUG
    printf("Completed pub_task_thr join with status= %d\n", stat); 
#endif
  }


}

int main(int argc, char **argv){
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  if (provided != MPI_THREAD_MULTIPLE)
    handle_err(MPI_ERR_OTHER, "The provided mode different from MPI_THREAD_MULTIPLE).");

  test0(MPI_COMM_WORLD);
  //test1();
  //test2();
  //test3();
  //test4();
  //test5();
  MPI_Finalize();
  return 0;
}
