#include "alloc_pool.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

void test0(){
  alloc_pool_t a;
  char val1[]="abc";
  char val2[]="de";
  char *val3;
  char *val4;
  
  alloc_pool_init(&a, 2, 2, sizeof(char *));
  alloc_pool_print(&a);
  alloc_pool_get(&a);
  alloc_pool_get(&a);
  alloc_pool_print(&a);
  alloc_pool_get(&a);
  alloc_pool_get(&a);
  alloc_pool_print(&a);
  if ( alloc_pool_set_ith(&a, 0, val1) < 0){
    printf("alloc_pool_set_ith ERROR: exiting\n");
    exit(-1) ;
  }
  if ( alloc_pool_set_ith(&a, 2, val2) < 0){
    printf("alloc_pool_set_ith ERROR: exiting\n");
    exit(-1) ;
  }

  val3 = (char *)alloc_pool_get_ith(&a, 0);
  val4 = (char *)alloc_pool_get_ith(&a, 2);
  printf("Retrieved from pool: %s (wrote %s) and %s (wrote %s)\n", val3, val1, val4, val2);

}

void test1(){
  alloc_pool_t a;
  char val1[]="abc";
  char val2[]="de";
  char *val3;
  char *val4;
  
  alloc_pool_init(&a, 2, 2, sizeof(char *));
  alloc_pool_print(&a);
  val3 = (char *)alloc_pool_get_set(&a, val1);
  alloc_pool_get(&a);
  alloc_pool_print(&a);
  val4 = (char *)alloc_pool_get_set(&a, val2);
  printf("Retrieved from pool: %s (wrote %s) and %s (wrote %s)\n", val3, val1, val4, val2);  alloc_pool_get(&a);
  alloc_pool_print(&a);
  val3 = (char *)alloc_pool_get_ith(&a, 0);
  val4 = (char *)alloc_pool_get_ith(&a, 2);
  printf("Retrieved from pool: %s (wrote %s) and %s (wrote %s)\n", val3, val1, val4, val2);

}

void test2(){
  alloc_pool_t a;
  char *val1;
  char *val2;
  //char *val3;

  val1 = (char *) clarisse_malloc(sizeof(char)*4);
  strcpy(val1, "abc");
  alloc_pool_init(&a, 2, 2, sizeof(char *));
  alloc_pool_print(&a);
  val2 = (char *)alloc_pool_get_set(&a, (void *) val1);
  
  printf("Retrieved from pool: %s (wrote %s)\n", val2, val1);  
  
  //val3 = alloc_pool_get_ith(&a, 0);
  clarisse_free(val1);
  alloc_pool_free(&a);
#ifdef CLARISSE_MEM_DEBUG
  clarisse_print_leaks();
#endif
}

int main(){

  //char *c = clarisse_malloc(20);
  //clarisse_free(c);
  //test0();
  //test1();
  test2();
  //test3();
  //test4();
  return 0;
}
