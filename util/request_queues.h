#ifndef REQUEST_QUEUES_H
#define REQUEST_QUEUES_H

#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "uthash.h"

#define REQUEST_QUEUES_GET_QUEUE_IDX(key, h_p) (key%((long long int)((h_p)->nr_queues))) 
#define REQUEST_QUEUES_GET_QUEUE(key,h_p) (&(((h_p)->queues)[REQUEST_QUEUES_GET_QUEUE_IDX (key, h_p)]))


typedef struct queue_idx {
  struct dllist_link link;
  int idx;
} queue_idx, *queue_idx_p;

typedef struct elem {
  struct dllist_link link;
  void *data;
} elem, *elem_p;

typedef struct {
  UT_hash_handle hh;
  int idx;
} nonempty_queues_t;




#define ZERO_POLICY    0
#define RANDOM_POLICY  1
#define HASH_POLICY    2

typedef struct request_queues {
  struct dllist list; //list of non-empty queues sorted by id
  queue_idx_p crt_queue_idx; //pointer to the crt list element
  int nr_queues;
  struct dllist *queues; // array of nr_queues 
  elem **crt_elems; // current element in each queue (used for enumeration)
  nonempty_queues_t *nonempty_queues_enum;
  short type_next; // HASH_POLICY, RANDOM_POLICY, ZERO_POLICY    
} request_queues, *request_queues_p;

void request_queues_init(request_queues_p c_p, int nr_queues, short type);
void request_queues_free(request_queues_p c);
void request_queues_insert(request_queues_p c, int key, void* elem);
void* request_queues_peek_crt(request_queues_p c); // peek current elem
void* request_queues_peek_key(request_queues_p c, int key);
void* request_queues_peek_first(request_queues_p c, int i); // peek first elem
void* request_queues_peek_last(request_queues_p c, int i); // peek last elem
/*
rm_crt does not change queue for the ZERO and HASH policies but only for random (probably it should)
 */
void* request_queues_rm_crt(request_queues_p c); // get current elem
void *request_queues_rm_queue(request_queues_p c, int queue_nr); //remove first from queue
void request_queues_set_next(request_queues_p c); //set next elem
void request_queues_set_first(request_queues_p c, int i);
void request_queues_print(request_queues_p c);
char * request_queues_dump(request_queues_p c, char *buf);
dllist* request_queues_get_queue(request_queues_p c, int i);
#define request_queues_empty(c) (dllist_is_empty(&(c)->list))
void request_queues_init_enum(request_queues_p c, int i); //init enum from queue i
void *request_queues_enum(request_queues_p c, int i);//enum from queue i
void *request_queues_peek_crt_queue(request_queues_p c, int i);//crt elem from queue i

#endif
