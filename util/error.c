#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include "mpi.h"
#include "error.h"

void print_stack_trace() {
  void *stack_trace[MPI_MAX_ERROR_STRING];
  char **strings;
  int j, nptrs;
  nptrs = backtrace(stack_trace, MPI_MAX_ERROR_STRING);
  strings = backtrace_symbols(stack_trace, nptrs);
  if (strings == NULL) {
    perror("backtrace_symbols");
  }
  else {
    for (j = 0; j < nptrs; j++) {
      /*if (addr2line(icky_global_program_name, stack_traces[i]) != 0)
	{
	  printf("  error determining line # for: %s\n", strings[i]);
	  }*/
      printf("%s\n", strings[j]);
    }
    free(strings);
  }
}

void handle_error(int errcode, char *str) 
{
  char msg[MPI_MAX_ERROR_STRING];
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int namelen, resultlen, my_rank;
    
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Get_processor_name(processor_name,&namelen);
  MPI_Error_string(errcode, msg, &resultlen);
  
  fprintf(stderr,"Process %d on %s %s: %s\nStack trace:\n", my_rank, processor_name,str, msg);
  
  print_stack_trace();
  //NL_RETURN(0);  
  MPI_Abort(MPI_COMM_WORLD, 1);
}


