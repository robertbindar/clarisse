#include "thr_pool.h"
#include <stdio.h>
#include <unistd.h>

#define WAIT_WORKERS_TO_DRAIN_QUEUE 1

static pthread_mutex_t count_mutex = PTHREAD_MUTEX_INITIALIZER;
int count;


void task0(void *ptr){

  int *pval = (int*)ptr;

  printf("%s\n", __FUNCTION__);
  printf("slow task: count value is %d.\n",count);

  sleep(1);
  pthread_mutex_lock(&count_mutex);
  (*pval)++;
  count++;
  pthread_mutex_unlock(&count_mutex);
}

void task1(void *ptr)
{
  int *pval = (int*)ptr;
  int i;

  printf("%s\n", __FUNCTION__);

  for (i = 0; i < 10000000; i++) {
    (*pval)++;
  }
  pthread_mutex_lock(&count_mutex);
  count++;
  pthread_mutex_unlock(&count_mutex);
}

void task2(void *ptr)
{
  int *pval = (int*)ptr;
  int i;

  printf("%s\n", __FUNCTION__);
  for (i = 0; i < 2*100000000; i++) {
    (*pval)++;
  }
  pthread_mutex_lock(&count_mutex);
  count++;
  pthread_mutex_unlock(&count_mutex);
}

void task3(void *ptr)
{
  int *pval = (int*)ptr;
  int i;

  printf("%s\n", __FUNCTION__);
  for (i = 0; i < 1000; i++) {
    (*pval)++;
  }
  pthread_mutex_lock(&count_mutex);
  count++;
  pthread_mutex_unlock(&count_mutex);
}

tpool_t global_thread_pool;

void task4(void *ptr)
{
  int *pval = (int*)ptr;
  int i;
  int val;
  int TASKS=2;

  printf("%s\n", __FUNCTION__);
  for (i = 0; i < 1000; i++) {
    (*pval)++;
  }
  pthread_mutex_lock(&count_mutex);
  count++;
  pthread_mutex_unlock(&count_mutex);

  for (i = 0; i < TASKS; i++) {
    tpool_add_work(global_thread_pool, task2, (void *) &val, (void*)0);  
    //for (i = 0; i < TASKS; i++) 
    tpool_add_work(global_thread_pool, task3, (void *) &val, (void*)0);
  }
}





void test0(){
  tpool_t thread_pool;
  int val;
  int DO_NOT_BLOCK_WHEN_FULL = 1;
  int MAX_THREADS = 10;
  int MAX_TASKS_IN_SYSTEM = 10;

  printf("TEST %s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_TASKS_IN_SYSTEM, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_task_queue_create(thread_pool, (void*)0, 0);
  tpool_add_work(thread_pool, task0, (void *) &val, (void *)0); 
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

void test1(){
  tpool_t thread_pool;
  int val;
  int DO_NOT_BLOCK_WHEN_FULL = 1;
  int MAX_THREADS = 1;
  int MAX_TASKS_IN_SYSTEM = 1;
  
  sleep(10);
  printf("TEST %s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_TASKS_IN_SYSTEM, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_task_queue_create(thread_pool, (void*)0, 0);
  tpool_add_work(thread_pool, task0, (void *) &val, (void*)0); 
  tpool_add_work(thread_pool, task0, (void *) &val, (void*)0); 
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

void test2(){
  tpool_t thread_pool;
  int val;
  int DO_NOT_BLOCK_WHEN_FULL = 0;
  int MAX_THREADS = 1;
  int MAX_TASKS_IN_SYSTEM = 1;
  
  printf("TEST %s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_TASKS_IN_SYSTEM, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_task_queue_create(thread_pool, (void*)0, 0);
  tpool_task_queue_create(thread_pool, (void*)1, 0);
  tpool_add_work(thread_pool, task0, (void *) &val, (void*)0); 
  tpool_add_work(thread_pool, task0, (void *) &val, (void*)1); 
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

void test3(){
  tpool_t thread_pool;
  int val, i;
  int DO_NOT_BLOCK_WHEN_FULL = 0;
  int MAX_THREADS = 5;
  int MAX_TASKS_IN_SYSTEM = 10;
  
  printf("TEST %s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_TASKS_IN_SYSTEM, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_task_queue_create(thread_pool, (void*)1, 1);
  tpool_task_queue_create(thread_pool, (void*)2, 0);
  tpool_task_queue_create(thread_pool, (void*)3, 0);

  for (i = 0; i < 10; i++)
    if (i%2 == 0)
      tpool_add_work(thread_pool, task1, (void *) &val, (void*)1); 
  for (i = 0; i < 10; i++)
    //tpool_set_queue_priority(thread_pool, (void *)0, 1);
    if (i%3 == 0)
      tpool_add_work(thread_pool, task2, (void *) &val, (void*)2); 
  for (i = 0; i < 10; i++)
    if (i%4 == 0)
      tpool_add_work(thread_pool, task3, (void *) &val, (void*)3);
  /*
  tpool_task_queue_destroy(thread_pool, (void *)0);
  tpool_task_queue_destroy(thread_pool, (void *)1);
  tpool_task_queue_destroy(thread_pool, (void *)2);*/
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

// Test tpool wait for task queue empty
void test4(){
  tpool_t thread_pool;
  int val, i;
  int DO_NOT_BLOCK_WHEN_FULL = 0;
  int MAX_THREADS = 10;
  int MAX_TASKS_IN_SYSTEM = 100;
  
  printf("TEST %s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_TASKS_IN_SYSTEM, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_task_queue_create(thread_pool, (void*)1, 0);
  tpool_task_queue_create(thread_pool, (void*)2, 0);
  tpool_task_queue_create(thread_pool, (void*)3, 0);

  for (i = 0; i < 10; i++) 
      tpool_add_work(thread_pool, task1, (void *) &val, (void*)1); 
  tpool_wait_for_task_queue_empty(thread_pool, (void*)1);
  for (i = 0; i < 10; i++)
      tpool_add_work(thread_pool, task2, (void *) &val, (void*)2); 
  tpool_wait_for_task_queue_empty(thread_pool, (void*)2);
  for (i = 0; i < 10; i++)
      tpool_add_work(thread_pool, task3, (void *) &val, (void*)3);
  tpool_wait_for_task_queue_empty(thread_pool, (void*)3);
  
  /*
  tpool_task_queue_destroy(thread_pool, (void *)0);
  tpool_task_queue_destroy(thread_pool, (void *)1);
  tpool_task_queue_destroy(thread_pool, (void *)2);*/
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

// add tasks from a task
void test5(){
  int val, i;
  int DO_NOT_BLOCK_WHEN_FULL = 0;
  int MAX_THREADS = 2;
  int MAX_TASKS_IN_SYSTEM = 100;
  int TASKS=3;

  printf("TEST %s\n", __FUNCTION__);
  tpool_init(&global_thread_pool, MAX_THREADS, MAX_TASKS_IN_SYSTEM, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_task_queue_create(global_thread_pool, (void*)0, 0);
 
  for (i = 0; i < TASKS; i++) {
    tpool_add_work(global_thread_pool, task4, (void *) &val, (void*)0);
  }
  
  tpool_wait_for_task_queue_empty(global_thread_pool, (void*)0);
  tpool_destroy(global_thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

int main(){
  // test0();
  //test1();
  //test2();
  //test3();
  //test4();
  test5();
  return 0;
}
