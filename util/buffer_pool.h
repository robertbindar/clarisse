#ifndef _BUFFER_POOL_H
#define _BUFFER_POOL_H 1
#include <pthread.h>
#include "list.h"
#include "uthash.h"

#define STATIC_POOL 0
#define DYNAMIC_POOL 1

typedef struct buffer_unit{
  struct dllist_link link;
  struct dllist_link lru_link;
  UT_hash_handle hh;
  UT_hash_handle hh2;
  int global_descr;
  //clarisse_off idx;
  long long int offset; 
  short dirty;
  int l_dirty; // left of the dirty interval in page
  int r_dirty; // right of the dirty interval in page
  char *buf;
  
} buffer_unit, *buffer_unit_p; 

#define BPOOL_NONCONCURRENT_ACCESS 0
#define BPOOL_CONCURRENT_ACCESS 1


typedef struct buffer_pool {
  int concurrency;
  int buffer_size;
  int min_nr_buffers;   // min number of buffers
  int max_nr_buffers;   // max number
  int total_nr_buffers; // actual number
  int nr_free_buffers; 
  int incr_decr; // nr of buffers to increment or decrement
  short dyn_stat; // dyn=1 static=0
  struct dllist free_buf_list;
  //  struct dllist inuse_buf_list;
  pthread_cond_t  bufpool_not_empty;
  pthread_mutex_t bufpool_lock;
  int cnt_waiting;
} buffer_pool, *buffer_pool_p;


#define BUFFER_POOL_FULL(b_p) ((b_p)->nr_free_buffers == 0)
#define BUFFER_POOL_EMPTY(b_p) ((b_p)->nr_free_buffers == (b_p)->max_nr_buffers)

buffer_pool_p bufpool_init_static(int concurrency, int buffer_size, int nr_buffers);
buffer_pool_p bufpool_init_dynamic(int concurrency, int buffer_size, int min_nr_buffers, 
				   int max_nr_buffers, int incr_decr);
void bufpool_free(buffer_pool_p b);
buffer_unit_p bufpool_get(buffer_pool_p b_p);
void bufpool_put(buffer_pool_p b_p, buffer_unit_p bu);
int bufpool_empty(buffer_pool_p b_p);
void bufpool_print(buffer_pool_p b_p);

void bu_print(buffer_unit_p bu);
//buffer_unit_p bufpool_conc_put(buffer_pool_p b, buffer_unit_p bu);
//buffer_unit_p bufpool_conc_get(buffer_pool_p b);


#ifndef MAX
#define MAX(a,b) \
  ({ __typeof__ (a) _a = (a);	 \
    __typeof__ (b) _b = (b);	 \
    _a > _b ? _a : _b; })
#endif
#ifndef MIN
#define MIN(a,b) \
       ({ __typeof__ (a) _a = (a); \
           __typeof__ (b) _b = (b); \
         _a < _b ? _a : _b; })
#endif

#endif
