#ifndef _BUFFER_POOL_H
#define _BUFFER_POOL_H 1

#include "dllist.h"

#define STATIC_POOL 0
#define DYNAMIC_POOL 1

typedef struct buffer_pool {
    int buffer_size;
    int min_nr_buffers;
    int max_nr_buffers;   // max number
    int actual_nr_buffers; // actual number
    int nr_free_buffers; 
    int incr_decr; // nr of buffers to increment or decrement
    short dyn_stat; // dyn=1 static=0
    List_p buffers;
} buffer_pool, *buffer_pool_p;

void buffer_pool_init(buffer_pool_p b, int buffer_size, int min_nr_buffers, 
		      int max_nr_buffers, int incr_decr, short dyn_stat);
buffer_pool_p alloc_init_buffer_pool(int buffer_size, int min_nr_buffers, 
				     int max_nr_buffers, int incr_decr,
				     short dyn_stat);
char * get_buffer(buffer_pool_p b_p);
void put_buffer(char* buf, buffer_pool_p b_p);
void printf_buf_pool_state(buffer_pool_p b_p);
#endif
