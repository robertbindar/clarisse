#ifndef BUFFER_INCLUDE
#define BUFFER_INCLUDE

#include <stdio.h>
#include "ofile.h"

char * my_get_buffer(ADIO_File fd, ofile_info_p o);
void flush_buffer(ADIO_File fd,ofile_info_p o, ADIO_Offset offset);
void drop_buffer(ADIO_File fd,ofile_info_p o, ADIO_Offset offset);
void flush_buffers(ADIO_File fd,ofile_info_p o);
void drop_buffers_from(ADIO_File fd,ofile_info_p o, ADIO_Offset offset);

#endif /*BUFFER_H_*/
