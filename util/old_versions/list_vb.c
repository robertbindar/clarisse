#include "adio.h"
#include "adio_extern.h"
void init_list(List_p l)
{
  l->head=l->tail=NULL;
  l->length=0;
}


List_p alloc_init_list()
{
  List_p l;
  if ((l=(List_p)ADIOI_clarisse_malloc(sizeof(List)))==NULL){
    handle_err(MPI_ERR_OTHER,"alloc_init_list:Error by allocating list\n");
    return NULL;
  }
  l->head=l->tail=NULL;
  l->length=0;
  return l;
}  


//#define empty_list(l_p) ((l_p)->head==NULL)

int empty_list(List_p l)
{
  return (l->head==NULL);
}


int add_in_front(void *data,List_p l)
{
  if (l==NULL) {
    handle_err(MPI_ERR_OTHER,"list.c (add_in_front): Tried to add to NULL list");
    return -1;
  }
  else {
    item_p aux;
    if (!(aux=(item_p) ADIOI_clarisse_malloc(sizeof(item))))
      {
	handle_err(MPI_ERR_OTHER, "Allocation error");
	return -1;
      }
    aux->data=data;
    aux->next=l->head;
    l->head=aux;
    if(!l->tail)
      l->tail=aux;
    l->length++;
    return 0;
  }
}

void * dequeue (List_p l)
{
  if (l==NULL) {
    handle_err(MPI_ERR_OTHER,"list.c (dequeue): Tried to dequeue from NULL list");
    return NULL;
  }
  else {
    if (!l->head)
      return NULL;
    else {
      void* aux=l->head->data;
      item_p iaux=l->head;
      l->head=l->head->next;
      //printf("freeing %d\n",iaux);
      //fflush(stdout);
      ADIOI_clarisse_free(iaux);
      if(!l->head)
	l->tail=NULL;
      l->length--;
      return aux;
    }
  }
}

void * peek(List_p l) {
    if (l==NULL) {
	handle_err(MPI_ERR_OTHER, "list.c (peek): Tried to peek to NULL list");
	return NULL;
    }
    else {
	if (!l->head)
	    return NULL;
	else 
	    return l->head->data;
    }
}


int enqueue(void *data,List_p l){

  if (l==NULL) {
    handle_err(MPI_ERR_OTHER, "list.c (enqueue): Tried to enqueue in NULL list");
    return -1;
  }
  else {
    item_p aux;
    
    if (!(aux=(item_p) ADIOI_clarisse_malloc(sizeof(item)))){
      handle_err(MPI_ERR_OTHER, "Allocation error");
      return -1;
    }
    aux->data=data;
    aux->next=NULL;
    if (!l->tail)
      l->tail=l->head=aux;
    else 
      l->tail=l->tail->next=aux;
    l->length++;
    return 0;
  }    
}
/*******************************************
*******************************************
  RETURNS A POINTER TO THE ITEM
*******************************************
*******************************************/

item_p enqueue2(void *data,List_p l){
  if (l==NULL) {
    handle_err(MPI_ERR_OTHER,"list.c (enqueue2): Tried to enqueue in NULL list");
    return NULL;
  }
  else {
    item_p aux;
    
    if (!(aux=(item_p) ADIOI_clarisse_malloc(sizeof(item)))){
      handle_err(MPI_ERR_OTHER,"Allocation error");
      return NULL;
    }
    aux->data=data;
    aux->next=NULL;
    if (!l->tail)
      l->tail=l->head=aux;
    else 
      l->tail=l->tail->next=aux;
    l->length++;
    return aux;
  }    
}

/* doesn't free item: useful when void * is used simpy as value */
void * remove_item(item_p it_p,List_p l) {
  if (l==NULL) {
    handle_err(MPI_ERR_OTHER,"list.c (remove): Tried to remove from NULL list");
    return NULL;
  }
  else {

    if (empty_list(l)) {
      handle_err(MPI_ERR_OTHER,"Tried to remove from empty list");
      return NULL;
    }
    else {
      if (HEAD(l)==it_p) {
	  void *data;
	  HEAD(l)=HEAD(l)->next;
	  if (!HEAD(l))
	      TAIL(l)=NULL;
	  data=it_p->data;
//	  ADIOI_clarisse_free(it_p);
	  return data;
      }
      else {
	  item_p aux_p;
	  for(aux_p=HEAD(l);
	      (aux_p->next!=NULL)&&(aux_p->next!=it_p);
	      aux_p=aux_p->next);
	  if (aux_p->next==it_p) {
	      void *data=it_p->data;
	      aux_p->next=aux_p->next->next;
	      if (!aux_p->next)
		  TAIL(l)=aux_p;
//	      ADIOI_clarisse_free(it_p);
	      return data;
	  }
	  else {
	      handle_err(MPI_ERR_OTHER,"Tried to remove from list inexistent element");
	      return NULL;
	  }
      }
    }
  }  
} 
/* doesn't free data */
int remove_data(void* data, List_p l) {
  if (l==NULL) {
    handle_err(MPI_ERR_OTHER,"list.c (remove): Tried to remove from NULL list");
    return -1;
  }
  else {

    if (empty_list(l)) {
      handle_err(MPI_ERR_OTHER,"Tried to remove from empty list");
      return -1;
    }
    else {
      if (HEAD(l)->data==data) {
	  item_p it=HEAD(l);
	  HEAD(l)=HEAD(l)->next;
	  if (!HEAD(l))
	      TAIL(l)=NULL;
	  ADIOI_clarisse_free(it);
	  return 0;
      }
      else {
	  item_p aux_p;
	  for(aux_p=HEAD(l);
	      (aux_p->next!=NULL)&&(aux_p->next->data!=data);
	      aux_p=aux_p->next);
	  if (aux_p->next->data==data) {
	      item_p it=aux_p->next;
	      aux_p->next=aux_p->next->next;
	      if (!aux_p->next)
		  TAIL(l)=aux_p;
	      ADIOI_clarisse_free(it);
	      return 0;
	  }
	  else {
	      handle_err(MPI_ERR_OTHER,"Tried to remove from list inexistent element");
	      return -1;
	  }
      }
    }
  }  
} 

// append l2 to l1, return l1, doesn't free l2 

List_p list_append(List_p l1_p,List_p l2_p) {
  if (l1_p==NULL) {
    handle_err(MPI_ERR_OTHER,"list.c (list_append): First list NULL");
    return NULL;
  }
  else if (l2_p==NULL) return l1_p;
  else {
  /*  List_p l_p;
  if ((l_p=(List_p)ADIOI_clarisse_malloc(sizeof(List)))==NULL){
    handle_err(MPI_ERR_OTHER,"list_append:error at list allocation\n");
    return NULL;
  }
  init_list(l_p);
  */

    if (empty_list(l1_p)) { 
      if (!empty_list(l2_p)) {
	HEAD(l1_p)=HEAD(l2_p);
	TAIL(l1_p)=TAIL(l2_p);
      }
    }
    else { 
	if (!empty_list(l2_p)) {
	    NEXT(TAIL(l1_p))=HEAD(l2_p);
	    TAIL(l1_p)=TAIL(l2_p);
	}
	l1_p->length+=l2_p->length;
    }
    return l1_p;

  }
}

void free_list_content(List_p l_p) {
    if (l_p) {
	item_p it_p,next_p;
	it_p=HEAD(l_p);
	while(it_p!=NULL) {
	    next_p=it_p->next;
	    ADIOI_clarisse_free(it_p->data);
	    ADIOI_clarisse_free(it_p);
	    it_p=next_p;
	}
	HEAD(l_p)=TAIL(l_p)=NULL;
    }
}

void free_list(List_p l_p) {
    if (l_p) {   
	free_list_content(l_p);
	ADIOI_clarisse_free(l_p);
    }
 
}

void free_list_content2(List_p l_p) {
    if (l_p) {
	item_p it_p,next_p;
	it_p=HEAD(l_p);
	while(it_p!=NULL) {
	    next_p=it_p->next;
	    ADIOI_clarisse_free(it_p);
	    it_p=next_p;
	}
	HEAD(l_p)=TAIL(l_p)=NULL;
    }
}

void free_list2(List_p l_p) {
    if (l_p) {   
	free_list_content2(l_p);
	ADIOI_clarisse_free(l_p);
    }
 
}





     
/*
#define GET_HEAD(l_p) ((empty_list(l_p))?NULL:HEAD(l_p))
#define HAS_MORE_ELEMENTS(i_p) (i_p!=NULL)
#define NEXT_ELEMENT(i_p) (item_p->next)


item_p getHead(List_p l_p)
{
  if (empty_list(l_p))
    return NULL;
  else {
    return HEAD(l_p);
  }
}

int hasMoreElements(item_p item_p)
{
  return (item_p!=NULL);
}

item_p nextElement(item_p item_p)
{
  return item_p->next;
}

*/

struct i{ int i;}*i_p;

#define ITEM(it) ((struct i*)(((item_p)it)->data))->i 


char buffer[1024];
char * dump_list(List_p l)
{
  char buf[4];
  
  item_p i;

  strcpy(buffer,"List:\t");

  for(i=l->head;i!=NULL;i=i->next) {
    sprintf(buf,"%d\t",ITEM(i));
    strcat(buffer,buf);
  }
  sprintf(buf,"%s\n",buf);
  return buffer;
}
    
//void* find( void* key, int (*cmp)(void*,void*),List_p l)

void dump_int_list(List_p l)
{
  
  item_p i;

  for(i=l->head;i!=NULL;i=i->next) {
    printf("%3d ",i->data);
  }
  printf("\n");
}
   


int old_main()
{
  char c;
  List l,l2;
  List_p l_p;
  int i=0,j=0;
  
  item_p items[6];


  init_list(&l);

  printf("e=enqueue d=dequeue i=insert a= append l to l p=print ?=is empty\n");
  scanf("%c",&c);
  printf("c=%c\n",c);
  while(c!='n')
    {
      switch(c)
	{
	case 'e': 
	  i_p=(struct i*)ADIOI_clarisse_malloc(sizeof(struct i));
	  i_p->i=i++;
	  enqueue(i_p,&l);printf(dump_list(&l));
	  items[j++]=l.tail;
	  printf("l.tail=%p\n", l.tail);
	  break;
	case 'i': 
	  i_p=(struct i*)ADIOI_clarisse_malloc(sizeof(struct i));
	  i_p->i=i++;
	  add_in_front(i_p,&l);printf(dump_list(&l));
	  break; 
	case 'p': 
	  printf(dump_list(&l));
	  break;
	case 'd':  
	  dequeue(&l);printf(dump_list(&l));
	  break;
	case 'a':
	  init_list(&l2);

	  i_p=(struct i*)ADIOI_clarisse_malloc(sizeof(struct i));
	  i_p->i=10;
	  add_in_front(i_p,&l2);
	  
	  i_p=(struct i*)ADIOI_clarisse_malloc(sizeof(struct i));
	  i_p->i=11;
	  add_in_front(i_p,&l2);
	  

	  //printf(dump_list(&l2));
	  l_p=list_append(&l,&l2);
	  printf(dump_list(l_p));
	  break;
	  
	case 'l':
	  printf("length=%d\n",l.length);
	  break;
	  
	case 'r':
	  printf("i="); fflush(stdout);
	  scanf("%d",&i);
	  printf("i=%d\n",i); fflush(stdout);
	  remove_item(items[i],&l);
	  printf("rm it=%p\n", items[i]);
	  printf(dump_list(&l));
	  break; 

	case '?':
	  if(empty_list(&l))
	    printf("Empty\n");
	  else
	    printf("Not Empty\n");
	}
      printf("e=enqueue d=dequeue i=insert p=print r=remove?=is empty\n");
      scanf("\n%c",&c);
      printf("c=%c\n",c);

    }
  return 0;
}











