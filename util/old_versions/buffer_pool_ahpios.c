#include <signal.h>
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include "buffer_pool.h"
#include "adio.h"
#include "map.h"
// incr when nr_free_buffers==0, and actual_nr_buffers<max_nr_buffers
// decr when nr_free_buffers > min_nr_buffers (initial nr_free_buffers == min_nr_buffers)


void buffer_pool_init(buffer_pool_p b, int buffer_size, int min_nr_buffers, 
		      int max_nr_buffers, int incr_decr, short dyn_stat) {
  if (min_nr_buffers > max_nr_buffers) {
    handle_err(MPI_ERR_OTHER, "alloc_init_buffer_pool:min_nr_buffers>max_nr_buffers");
  }
  else {
    int i;
    b->buffer_size = buffer_size;
    b->min_nr_buffers = b->nr_free_buffers 
      = b->actual_nr_buffers = min_nr_buffers;
    b->max_nr_buffers = max_nr_buffers;    
    b->incr_decr = incr_decr;
    b->dyn_stat = dyn_stat;
    b->buffers = alloc_init_list();
    for (i = 0;i<b->nr_free_buffers;i++){
      char * buf = (char*)malloc(b->buffer_size);
      bzero(buf,b->buffer_size);
      enqueue(buf,b->buffers);
    }
  } 
}

buffer_pool_p alloc_init_buffer_pool(int buffer_size,
				     int min_nr_buffers, 
				     int max_nr_buffers,
				     int incr_decr,
				     short dyn_stat) {
  buffer_pool_p b = (buffer_pool_p)malloc(sizeof(buffer_pool));
  buffer_pool_init(b, buffer_size, min_nr_buffers, max_nr_buffers, incr_decr, dyn_stat);
  return b;
}

char * get_buffer(buffer_pool_p b_p) {
    if (b_p->nr_free_buffers>0) {
	b_p->nr_free_buffers--;
	return dequeue(b_p->buffers);
    }
    else if ((b_p->dyn_stat==STATIC_POOL)||
	     ((b_p->dyn_stat==DYNAMIC_POOL)&& 
	      (b_p->actual_nr_buffers+b_p->incr_decr>b_p->max_nr_buffers)))
    {
/*	My_Perror("get_buffer: no more buffers available");
	kill(getpid(),SIGSEGV);
	exit(1);*/
	return NULL;
    }
    else {
	char * buf;
	int i;
	b_p->actual_nr_buffers+=b_p->incr_decr;
	b_p->nr_free_buffers+=(b_p->incr_decr-1);
	for (i = 0;i<b_p->incr_decr-1;i++) {
	    buf = (char*)malloc(b_p->buffer_size);
	    bzero(buf,b_p->buffer_size);
	    enqueue(buf,b_p->buffers);
	}
	buf = (char*)malloc(b_p->buffer_size);
	bzero(buf,b_p->buffer_size);
	return buf;
    }
}
    
void put_buffer(char* buf, buffer_pool_p b_p) {
    if (b_p->nr_free_buffers < b_p->min_nr_buffers) {
	b_p->nr_free_buffers++;
	bzero(buf,b_p->buffer_size);
	enqueue(buf,b_p->buffers);
    }
    else {
	if (b_p->dyn_stat==DYNAMIC_POOL) {
	    int i;
	    b_p->actual_nr_buffers-=b_p->incr_decr;
	    b_p->nr_free_buffers-=(b_p->incr_decr-1);
	    free(buf);
	    for (i = 0;i<b_p->incr_decr-1;i++)
		free(dequeue(b_p->buffers));
	    
	}
	else {
	  char err_str[128];
	  sprintf(err_str,"Should never get here free_buf=%d min_nr_buf=%d",b_p->nr_free_buffers,b_p->min_nr_buffers);
	  handle_err(MPI_ERR_OTHER, err_str);
	}

    }

}

void printf_buf_pool_state(buffer_pool_p b_p) {
    printf("\nBUFF POOL STATE: actual_nr_buffers=%d nr_free_buffers=%d buffer_size=%d \n min_nr_buffers=%d max_nr_buffers=%d\n",
	   b_p->actual_nr_buffers, b_p->nr_free_buffers,b_p->buffer_size,
	   b_p->min_nr_buffers,b_p->max_nr_buffers);
}

