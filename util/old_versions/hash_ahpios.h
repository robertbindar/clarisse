#ifndef _HASH_H
#define _HASH_H 1

#include <stdlib.h>
#include "dllist.h"
#include "adio.h"
#include "adioi.h"

typedef struct hash {
    int max_elements;
    int crt_elements;
    struct dllist lru;
    int nr_buckets;
    struct dllist *buckets;
} hash, *hash_p;

typedef struct page_info{
  struct dllist_link link;
  struct dllist_link lru_link;
  int global_fd;
  ADIO_Offset idx;
  short dirty;
  int l_dirty; // left of the dirty interval in page
  int r_dirty; // right of the dirty interval in page 
  char *page;
} page_info,*page_info_p;


void hash_init(hash_p h, int nr_buckets, int max_elements);
hash_p hash_alloc_init(int nr_buckets,int max_elements);
void hash_free_content(hash_p h_p);
void hash_add(hash_p h_p, page_info_p p);
void hash_remove(hash_p h_p, page_info_p p);
void * hash_remove_lru(hash_p h_p);
#define GET_BUCKET_IDX(key, h_p) (key%((ADIO_Offset)((h_p)->nr_buckets))) 
#define GET_BUCKET(key,h_p) (&(((h_p)->buckets)[GET_BUCKET_IDX (key, h_p)]))
#define HASH_FULL(h_p) (h_p->crt_elements == h_p->max_elements)

inline void print_page_list(dllist * c_p);
inline void print_page_hash(hash_p h_p);


typedef struct buck_idx {
  struct dllist_link link;
  int idx;
} buck_idx, *buck_idx_p;

typedef struct elem {
  struct dllist_link link;
  void *data;
} elem, *elem_p;


#define ZERO_POLICY    0
#define RANDOM_POLICY  1
#define HASH_POLICY    2

typedef struct chash {
    struct dllist list;
    buck_idx_p crt_buck_idx; //pointer to the crt list element
    int nr_buckets;
    struct dllist *buckets; // array of nr_buckets 
    short type_next; // HASH_POLICY, RANDOM_POLICY, ZERO_POLICY 
    
} chash, *chash_p;

void chash_init(chash_p c_p,int nr_buckets, short type);
void chash_free(chash_p c);
void chash_insert(chash_p c,int key,void* elem);
void chash_insert_sorted(chash_p c,int key,void* elem);
void* chash_peek_crt(chash_p c); // get current elem
void* chash_peek_key(chash_p c,int key);
void* chash_remove_crt(chash_p c); // get current elem
void *chash_remove_buck(chash_p c, int buck_nr); //remove first from buck
void chash_set_next(chash_p c); //set next elem
void chash_set_first(chash_p c,int i);
void chash_dump(chash_p c);
char * chash_dump2(chash_p c, char *buf);
dllist* chash_get_bucket(chash_p c,int i);

#define chash_empty(c) (dllist_is_empty(&(c)->list))

#endif
