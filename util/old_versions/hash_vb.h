#ifndef HASH_INCLUDE

#define HASH_INCLUDE


#include <stdlib.h>
#include "ofile.h"
#include "dllist.h"

typedef struct hash {
    int max_elements;
    int crt_elements;
    struct dllist lru;
    int nr_buckets;
    struct dllist *buckets;
} hash, *hash_p;

typedef struct page_info{
  struct dllist_link link;
  struct dllist_link lru_link;
  int global_fd;
  ADIO_Offset idx;
  short dirty;
  int l_dirty; // left of the dirty interval in page
  int r_dirty; // right of the dirty interval in page
  char *page;
} page_info,*page_info_p;


void hash_init(hash_p h, int nr_buckets, int max_elements);
hash_p alloc_init_hash(int nr_buckets,int max_elements);
void free_hash_content(hash_p h_p);
void hash_insert(hash_p h_p, page_info_p p);
void hash_rm(hash_p h_p, page_info_p p);
page_info_p hash_rm_lru(hash_p h_p);
#define GET_BUCKET_IDX(key, h_p) (key%((ADIO_Offset)((h_p)->nr_buckets))) 
#define GET_BUCKET(key,h_p) (&(((h_p)->buckets)[GET_BUCKET_IDX (key, h_p)]))


inline void print_page_list(dllist * c_p);
inline void print_page_hash(hash_p h_p);


typedef struct buck_idx {
  struct dllist_link link;
  int idx;
} buck_idx, *buck_idx_p;

typedef struct elem {
  struct dllist_link link;
  void *data;
} elem, *elem_p;


#define ZERO_POLICY    0
#define RANDOM_POLICY  1
#define HASH_POLICY    2

typedef struct circ_hash {
    struct dllist list;
    buck_idx_p crt_buck_idx; //pointer to the crt list element
    int nr_buckets;
    struct dllist *buckets; // array of nr_buckets 
    short type_next; // HASH_POLICY, RANDOM_POLICY, ZERO_POLICY 
    
} circ_hash, *circ_hash_p;



void circ_hash_init(circ_hash_p c_p,int nr_buckets, short type);
void circ_hash_free(circ_hash_p c);
void circ_hash_insert(circ_hash_p c,int key,void* elem);
void circ_hash_insert_sorted(circ_hash_p c,int key,void* elem);
void* circ_hash_peek_crt(circ_hash_p c); // get current elem
void* circ_hash_peek_key(circ_hash_p c,int key);
void* circ_hash_rm_crt(circ_hash_p c); // get current elem
void *circ_hash_rm_buck(circ_hash_p c, int buck_nr); //remove first from buck
void circ_hash_set_next(circ_hash_p c); //set next elem
void circ_hash_set_first(circ_hash_p c,int i);
void circ_hash_dump(circ_hash_p c);
char * circ_hash_dump2(circ_hash_p c, char *buf);
dllist* circ_hash_get_bucket(circ_hash_p c,int i);

#define circ_hash_empty(c) (dllist_is_empty(&(c)->list))

void write_dirty_page(ADIO_File fd, ADIO_File fd_local, ADIO_Offset file_disp, page_info_p p);
page_info_p hash_insert_page(ADIO_File fd, hash_p h, ADIO_Offset idx, ofile_info_p o, char *page);
page_info_p find_page( hash_p h, int global_fd, ADIO_Offset idx);

#endif
