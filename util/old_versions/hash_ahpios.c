#include <signal.h>
#include <stdio.h>
#include "hash.h"
#include "dllist.h"
#include "map.h"


void hash_init(hash_p h, int nr_buckets, int max_elements) {
  int i;
  dllist_init(&h->lru);
  h->nr_buckets = nr_buckets;
  h->max_elements = max_elements;
  h->crt_elements = 0;
  h->buckets = (struct dllist*) malloc(nr_buckets*sizeof(struct dllist));
  for (i = 0; i < nr_buckets; i++)
    dllist_init(&h->buckets[i]);  
}


hash_p hash_alloc_init(int nr_buckets, int max_elements) {
  hash_p h=(hash_p) malloc(sizeof(hash));
  hash_init(h, nr_buckets, max_elements);
  return h;
}


static inline void free_bucket_content(ADIO_Offset index, hash_p h_p) {
  dllist *root = GET_BUCKET(index, h_p);
  while (!dllist_is_empty(root)) 
    free(dllist_rem_head(root));
}

void hash_free_content(hash_p h_p){
    ADIO_Offset i;   
    for (i = 0; i < h_p->nr_buckets; i++)
	free_bucket_content(i,h_p);
   
}

void hash_free(hash_p h_p) {
    hash_free_content(h_p);
    free(h_p);
}

void hash_add(hash_p h_p, page_info_p p) {
    if (h_p->crt_elements >=  h_p->max_elements) {
	char err_str[128];
	sprintf(err_str,"h_p->crt_elements = %d >= max_elements", h_p->crt_elements); 
	handle_err(MPI_ERR_OTHER, err_str);
    }
    else {
	h_p->crt_elements++;
	dllist_iat(GET_BUCKET(p->idx, h_p), (struct dllist_link *)p);
	dllist_iah(&h_p->lru, &p->lru_link);
    }
}

void hash_remove(hash_p h_p, page_info_p p) {
    if (h_p->crt_elements <= 0) {
	char err_str[128];
	sprintf(err_str,"hash_remove: h_p->crt_elements = %d <= 0", h_p->crt_elements); 
	handle_err(MPI_ERR_OTHER, err_str);
    }
    else {    
	dllist_rem(GET_BUCKET(p->idx, h_p), (struct dllist_link *)p);
	dllist_rem(&h_p->lru, &p->lru_link);
	h_p->crt_elements--;
    }
}

void * hash_remove_lru(hash_p h_p) {
    if (h_p->crt_elements <= 0) {
	char err_str[128];
	sprintf(err_str,"hash_remove_lru: h_p->crt_elements = %d <= 0", h_p->crt_elements); 
	handle_err(MPI_ERR_OTHER, err_str);
	return NULL;
    }
    else {
	struct dllist_link *lk = dllist_rem_tail(&h_p->lru);
	page_info_p p= (page_info_p) DLLIST_ELEMENT(lk, page_info, lru_link);
	dllist_rem(GET_BUCKET(p->idx ,h_p), (struct dllist_link *)p);
	h_p->crt_elements--;
	return p;
    }
}


/*********************************************************
*********************************************************
circ hash
*********************************************************
*********************************************************/

void chash_init(chash_p c, int nr_buckets, short type) {
    int i;
    dllist_init(&c->list);
    c->crt_buck_idx = NULL;
    c->nr_buckets = nr_buckets;
    c->buckets = (struct dllist*)malloc(nr_buckets * sizeof(struct dllist));
    for (i = 0;i < nr_buckets;i++)
	dllist_init(&c->buckets[i]);
    c->type_next = type;// HASH, RANDOM, INCREASING 
}

void chash_free(chash_p c) {
    free(c->buckets);
}

// inserts both in the list of tuples AND in the bucket
void chash_insert(chash_p c, int key, void *data) {
  struct dllist_link *aux;
  int idx = GET_BUCKET_IDX(key,c);
  elem_p e = (elem_p) malloc(sizeof(elem));
  e->data = data;
  dllist_iat(GET_BUCKET(key,c),(struct dllist_link *)e); //insert into bucket

  for (aux = c->list.head; aux != NULL && 
	 ((((buck_idx_p)aux)->idx) < idx) ; aux = aux->next);
  if ((!aux) ||                           // havent found any
      ((((buck_idx_p)aux)->idx) > idx)) {  // found one bigger 
    buck_idx_p b = (buck_idx_p) malloc(sizeof(struct buck_idx));
    b->idx = idx;
    if (!aux)
      dllist_iat(&c->list,(struct dllist_link *)b);
    else
      dllist_ibefore(&c->list, aux, (struct dllist_link *)b);   
    
    if (!c->crt_buck_idx)
      c->crt_buck_idx = b;
  }
}


void * chash_peek_crt(chash_p c) {
    return (c->crt_buck_idx)
      ? ((elem_p) c->buckets[c->crt_buck_idx->idx].head )->data
      : NULL;
}

void * chash_peek_key(chash_p c, int key) {
    return GET_BUCKET(key,c)->head
      ? ((elem_p)GET_BUCKET(key,c)->head)->data 
      : NULL ;
}



void chash_set_first(chash_p c, int i) {    
    if (c->crt_buck_idx) {
      int buck_idx;
      struct dllist_link *aux;
      if (c->type_next == ZERO_POLICY)
	buck_idx = 0;
      if (c->type_next == HASH_POLICY)
	buck_idx = i % c->nr_buckets;
      if (c->type_next == RANDOM_POLICY)
	buck_idx = random() % c->nr_buckets;
      for (aux = c->list.head; aux != NULL && 
	 ((((buck_idx_p)aux)->idx) < buck_idx) ; aux = aux->next);
      if (aux)
	c->crt_buck_idx = (buck_idx_p)aux;
      else
	c->crt_buck_idx = (buck_idx_p)c->list.head;
    }
}

static void go_next(chash_p c) {

    struct dllist_link **crt=(struct dllist_link **)&c->crt_buck_idx;
    if ((*crt)->next) {// there is a next
	*crt = (*crt)->next;
	//printf("client_id=%2d Hay next\n",client_id);
    }
    else {//there is no next
	*crt = c->list.head;
	//printf("client_id=%2d No next\n",client_id);
    }
}

void chash_set_next(chash_p c) {
    if (c->crt_buck_idx) {
	switch (c->type_next) {
	    case ZERO_POLICY: 
	    case HASH_POLICY:	
	    {
		go_next(c);
		break;
	    }
	    case RANDOM_POLICY:{
		chash_set_first(c,0);
		break;
	    }
	}
    }
}


void *chash_remove_crt(chash_p c) {
  buck_idx_p b = c->crt_buck_idx;
  if (b){
    elem_p e = (elem_p) dllist_rem_head(&c->buckets[b->idx]);
    void *data = e->data;
    if (dllist_is_empty(&c->buckets[b->idx])) {
      if (b->link.next)  // there is a next
	c->crt_buck_idx = (buck_idx_p) b->link.next;
      else
	if (c->list.head != (struct dllist_link *)b) // no next, first != crt
	  c->crt_buck_idx = (buck_idx_p) c->list.head;
	else // crt was the only one in the list
	  c->crt_buck_idx = NULL;
      dllist_rem(&c->list, (struct dllist_link *)b);
      free(b);
    }
    if (c->type_next == RANDOM_POLICY)
      chash_set_next(c);
    free(e);
    return data;
  }
  else 
    return NULL;
}

void *chash_remove_buck(chash_p c, int buck_idx) {
  
  if (c->crt_buck_idx)
    if (buck_idx == c->crt_buck_idx->idx) 
      return chash_remove_crt(c);
    else {
      elem_p e= (elem_p) dllist_rem_head(&c->buckets[buck_idx]);
      void *data = e->data;
      free(e);
      if (dllist_is_empty(&c->buckets[buck_idx])) {
	dllist_link *aux;
	for (aux = c->list.head; aux != NULL; aux = aux->next) {
	  buck_idx_p b = (buck_idx_p) aux; 
	  if (b->idx == buck_idx) {
	    dllist_rem(&c->list, (struct dllist_link *)b);
	    free(b);
	    break;
	  }
	}
      }
      return data;
    }
  else
    return NULL;
}





void chash_dump(chash_p c) {
    struct dllist_link *aux;
    if (c->crt_buck_idx) {
	printf("CIRC HASH CRT = %d\n",c->crt_buck_idx->idx);
	for (aux=c->list.head;aux!=NULL;aux=aux->next) 
	    printf("%d ",((buck_idx_p)aux)->idx);
	printf("\n");
    }
    else 
	printf("CIRC HASH EMPTY!\n");
}

char * chash_dump2(chash_p c, char *buf) {
    struct dllist_link *aux;
    if (c->crt_buck_idx) {
	sprintf(buf, "CIRC HASH CRT = %d\n",c->crt_buck_idx->idx);
	for (aux=c->list.head;aux!=NULL;aux=aux->next) 
	    sprintf(buf,"%s%d ",buf,((buck_idx_p)aux)->idx);
	sprintf(buf,"%s\n",buf);
    }
    else 
	sprintf(buf,"%sCIRC HASH EMPTY!\n",buf);
    return buf;
}


dllist * chash_get_bucket(chash_p c,int i) { 
    return (i<c->nr_buckets)?GET_BUCKET(i,c):NULL;}


