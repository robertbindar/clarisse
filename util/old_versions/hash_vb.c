#include "adio.h"
#include "adio_extern.h"


void hash_init(hash_p h, int nr_buckets, int max_elements) {
  int i;
  dllist_init(&h->lru);
  h->nr_buckets = nr_buckets;
  h->max_elements = max_elements;
  h->crt_elements = 0;
  h->buckets = (struct dllist*) ADIOI_clarisse_malloc(nr_buckets*sizeof(struct dllist));
  for (i = 0; i < nr_buckets; i++)
    dllist_init(&h->buckets[i]);  
}


hash_p alloc_init_hash(int nr_buckets, int max_elements) {
  hash_p h=(hash_p) ADIOI_clarisse_malloc(sizeof(hash));
  hash_init(h, nr_buckets, max_elements);
  return h;
}


void free_bucket_content(ADIO_Offset index, hash_p h_p) {
  dllist *root = GET_BUCKET(index, h_p);
  while (!dllist_is_empty(root)) 
    ADIOI_clarisse_free(dllist_rem_head(root));
}

void free_hash_content(hash_p h_p){
    ADIO_Offset i;   
    for (i = 0; i < h_p->nr_buckets; i++)
	free_bucket_content(i,h_p);
   
}

void free_hash(hash_p h_p) {
    free_hash_content(h_p);
    ADIOI_clarisse_free(h_p->buckets);
    //ADIOI_clarisse_free(h_p);
}

void hash_insert(hash_p h_p, page_info_p p) {
    if (h_p->crt_elements >=  h_p->max_elements) {
	char err_str[128];
	sprintf(err_str,"h_p->crt_elements = %d >= max_elements", h_p->crt_elements); 
	handle_err(MPI_ERR_OTHER, err_str);
    }
    else {
	h_p->crt_elements++;
	dllist_iat(GET_BUCKET(p->idx, h_p), (struct dllist_link *)p);
	dllist_iah(&h_p->lru, &p->lru_link);
    }
}

void write_dirty_page(ADIO_File fd, ADIO_File fd_local, ADIO_Offset file_disp, page_info_p p) {
    struct file_info *file_info = (struct file_info *)fd->adio_ptr;
    if (p->dirty) {
        int n;
        if ((n = writen_at(fd_local, p->page + p->l_dirty,  p->r_dirty - p->l_dirty + 1, file_disp + p->l_dirty)) != p->r_dirty - p->l_dirty + 1){
            char err_str[128];
            sprintf(err_str, "ERROR AT WRITING THE PAGE %p into file FD=%d SIZE=%d OFFSET=%lld,file_disp=%lld  p->r_dirty=%d p->l_dirty=%d p->r_dirty - p->l_dirty + 1=%d n=%d",
                    p->page, fd, file_info->block_size, p->idx * file_info->block_size - file_disp, file_disp,  p->r_dirty, p->l_dirty, p->r_dirty - p->l_dirty + 1, n);
            if (n<0)
                perror("Error writing the page");
            handle_err(MPI_ERR_OTHER, err_str);
        }
        p->dirty=0;
    }
}

page_info_p hash_insert_page(ADIO_File fd, hash_p h, ADIO_Offset idx, ofile_info_p o, char *page){
    struct file_info *file_info = (struct file_info *)fd->adio_ptr;
    page_info_p p;
    int error_code;
    ADIO_Offset i;
    ADIO_Fcntl_t *fcntl_struct;
    int b = file_info->block_size;
    
    p = (page_info_p) ADIOI_clarisse_malloc(sizeof(page_info));
 
    p->page = page;
    p->dirty = 0;
    p->l_dirty = file_info->block_size;
    p->r_dirty = -1;
    p->global_fd = o->global_fd;
    p->idx = idx;
    hash_insert(h, p);
   
    return p;
}

page_info_p find_page(hash_p h, int global_fd, ADIO_Offset idx){
    struct dllist_link *aux;
    for (aux = GET_BUCKET(idx, h)->head; aux != NULL;aux = aux->next) {
        page_info_p p = (page_info_p)aux;
        if (idx == p->idx) {
          dllist_move_to_head(&h->lru,&((page_info_p)aux)->lru_link);//move up in the lru list
          return p;
        }
    }
    return NULL;
}


void hash_rm(hash_p h_p, page_info_p p) {
    if (h_p->crt_elements <= 0) {
	char err_str[128];
	sprintf(err_str,"hash_rm: h_p->crt_elements = %d <= 0", h_p->crt_elements); 
	handle_err(MPI_ERR_OTHER, err_str);
    }
    else {    
	dllist_rem(GET_BUCKET(p->idx, h_p), (struct dllist_link *)p);
	dllist_rem(&h_p->lru, &p->lru_link);
	h_p->crt_elements--;
    }
}

page_info_p  hash_rm_lru(hash_p h_p) {
    if (h_p->crt_elements <= 0) {
	char err_str[128];
	sprintf(err_str,"hash_rm_lru: h_p->crt_elements = %d <= 0", h_p->crt_elements); 
	handle_err(MPI_ERR_OTHER, err_str);
	return NULL;
    }
    else {
	struct dllist_link *lk = dllist_rem_tail(&h_p->lru);
	page_info_p p= (page_info_p) DLLIST_ELEMENT(lk, page_info, lru_link);
	dllist_rem(GET_BUCKET(p->idx ,h_p), (struct dllist_link *)p);
	h_p->crt_elements--;
	return p;
    }
}

page_info_p  hash_lru(hash_p h_p) {
    if (h_p->crt_elements <= 0) {
	    return NULL;
    }
    else {
	struct dllist_link *lk = dllist_tail(&h_p->lru);
	return  (page_info_p) DLLIST_ELEMENT(lk, page_info, lru_link);
	
    }
}


/*********************************************************
*********************************************************
circ hash
*********************************************************
*********************************************************/

void circ_hash_init(circ_hash_p c, int nr_buckets, short type) {
    int i;
    dllist_init(&c->list);
    c->crt_buck_idx = NULL;
    c->nr_buckets = nr_buckets;
    c->buckets = (struct dllist*)ADIOI_clarisse_malloc(nr_buckets * sizeof(struct dllist));
    for (i = 0;i < nr_buckets;i++)
	dllist_init(&c->buckets[i]);
    c->type_next = type;// HASH, RANDOM, INCREASING 
}

void circ_hash_free(circ_hash_p c) {
    ADIOI_clarisse_free(c->buckets);
}

// inserts both in the list of tuples AND in the bucket
void circ_hash_insert(circ_hash_p c, int key, void *data) {
  struct dllist_link *aux;
  int idx = GET_BUCKET_IDX(key,c);
  elem_p e = (elem_p) ADIOI_clarisse_malloc(sizeof(elem));
  e->data = data;
  dllist_iat(GET_BUCKET(key,c),(struct dllist_link *)e); //insert into bucket

  for (aux = c->list.head; aux != NULL && 
	 ((((buck_idx_p)aux)->idx) < idx) ; aux = aux->next);
  if ((!aux) ||                           // havent found any
      ((((buck_idx_p)aux)->idx) > idx)) {  // found one bigger 
    buck_idx_p b = (buck_idx_p) ADIOI_clarisse_malloc(sizeof(struct buck_idx));
    b->idx = idx;
    if (!aux)
      dllist_iat(&c->list,(struct dllist_link *)b);
    else
      dllist_ibefore(&c->list, aux, (struct dllist_link *)b);   
    
    if (!c->crt_buck_idx)
      c->crt_buck_idx = b;
  }
}


void * circ_hash_peek_crt(circ_hash_p c) {
    return (c->crt_buck_idx)
      ? ((elem_p) c->buckets[c->crt_buck_idx->idx].head )->data
      : NULL;
}

void * circ_hash_peek_key(circ_hash_p c, int key) {
    return GET_BUCKET(key,c)->head
      ? ((elem_p)GET_BUCKET(key,c)->head)->data 
      : NULL ;
}



void circ_hash_set_first(circ_hash_p c, int i) {    
    if (c->crt_buck_idx) {
      int buck_idx;
      struct dllist_link *aux;
      if (c->type_next == ZERO_POLICY)
	buck_idx = 0;
      if (c->type_next == HASH_POLICY)
	buck_idx = i % c->nr_buckets;
      if (c->type_next == RANDOM_POLICY)
	buck_idx = random() % c->nr_buckets;
      for (aux = c->list.head; aux != NULL && 
	 ((((buck_idx_p)aux)->idx) < buck_idx) ; aux = aux->next);
      if (aux)
	c->crt_buck_idx = (buck_idx_p)aux;
      else
	c->crt_buck_idx = (buck_idx_p)c->list.head;
    }
}

static void go_next(circ_hash_p c) {

    struct dllist_link **crt=(struct dllist_link **)&c->crt_buck_idx;
    if ((*crt)->next) {// there is a next
	*crt = (*crt)->next;
	//printf("client_id=%2d Hay next\n",client_id);
    }
    else {//there is no next
	*crt = c->list.head;
	//printf("client_id=%2d No next\n",client_id);
    }
}

void circ_hash_set_next(circ_hash_p c) {
    if (c->crt_buck_idx) {
	switch (c->type_next) {
	    case ZERO_POLICY: 
	    case HASH_POLICY:	
	    {
		go_next(c);
		break;
	    }
	    case RANDOM_POLICY:{
		circ_hash_set_first(c,0);
		break;
	    }
	}
    }
}


void *circ_hash_rm_crt(circ_hash_p c) {
  buck_idx_p b = c->crt_buck_idx;
  if (b){
    elem_p e = (elem_p) dllist_rem_head(&c->buckets[b->idx]);
    void *data = e->data;
    if (dllist_is_empty(&c->buckets[b->idx])) {
      if (b->link.next)  // there is a next
	c->crt_buck_idx = (buck_idx_p) b->link.next;
      else
	if (c->list.head != (struct dllist_link *)b) // no next, first != crt
	  c->crt_buck_idx = (buck_idx_p) c->list.head;
	else // crt was the only one in the list
	  c->crt_buck_idx = NULL;
      dllist_rem(&c->list, (struct dllist_link *)b);
      ADIOI_clarisse_free(b);
    }
    if (c->type_next == RANDOM_POLICY)
      circ_hash_set_next(c);
    ADIOI_clarisse_free(e);
    return data;
  }
  else 
    return NULL;
}

void *circ_hash_rm_buck(circ_hash_p c, int buck_idx) {
  
  if (c->crt_buck_idx)
    if (buck_idx == c->crt_buck_idx->idx) 
      return circ_hash_rm_crt(c);
    else {
      elem_p e= (elem_p) dllist_rem_head(&c->buckets[buck_idx]);
      void *data = e->data;
      ADIOI_clarisse_free(e);
      if (dllist_is_empty(&c->buckets[buck_idx])) {
	dllist_link *aux;
	for (aux = c->list.head; aux != NULL; aux = aux->next) {
	  buck_idx_p b = (buck_idx_p) aux; 
	  if (b->idx == buck_idx) {
	    dllist_rem(&c->list, (struct dllist_link *)b);
	    ADIOI_clarisse_free(b);
	    break;
	  }
	}
      }
      return data;
    }
  else
    return NULL;
}





void circ_hash_dump(circ_hash_p c) {
    struct dllist_link *aux;
    if (c->crt_buck_idx) {
	printf("CIRC HASH CRT = %d\n",c->crt_buck_idx->idx);
	for (aux=c->list.head;aux!=NULL;aux=aux->next) 
	    printf("%d ",((buck_idx_p)aux)->idx);
	printf("\n");
    }
    else 
	printf("CIRC HASH EMPTY!\n");
}

char * circ_hash_dump2(circ_hash_p c, char *buf) {
    struct dllist_link *aux;
    if (c->crt_buck_idx) {
	sprintf(buf, "CIRC HASH CRT = %d\n",c->crt_buck_idx->idx);
	for (aux=c->list.head;aux!=NULL;aux=aux->next) 
	    sprintf(buf,"%s%d ",buf,((buck_idx_p)aux)->idx);
	sprintf(buf,"%s\n",buf);
    }
    else 
	sprintf(buf,"%sCIRC HASH EMPTY!\n",buf);
    return buf;
}


dllist * circ_hash_get_bucket(circ_hash_p c,int i) { 
    return (i<c->nr_buckets)?GET_BUCKET(i,c):NULL;}


