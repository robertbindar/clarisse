/* Thread pool implementation from Pthreads Programming
Bradford Nichols, Dick Buttlar and Jacqueline Proulx Farrell
*/

#ifndef THR_POOL_H
#define THR_POOL_H
#include <pthread.h>
#include "uthash.h"
#include "list.h"

/*
The focal point of a thread pool is the request queue. Each request describes a unit of work. (This description might be the name of a routine; it might be just a flag.) Worker threads continually monitor the queue for new work requests; the boss thread places new requests on the queue.	

A thread pool has some basic characteristics:	

 	∙		Number of worker threads. This limits the number of requests that can be in progress at the same time.	

 	∙		Request queue size. This limits the number of requests that can be waiting for service.	

 	∙		Behavior when all workers are occupied and the request queue is full. Some requesters may want to block until their requests can be queued and only then resume execution. Others may prefer immediate notification that the pool is full. (For instance, network-based applications typically depend on a status value to avoid "dropping requests on the floor" when the server is overloaded.)
 */

typedef struct tpool_work {	
  void (*routine)();	
  void *arg;	
  struct tpool_work *next;	
} tpool_work_t;	

typedef struct {
  void *queue_idx;
  tpool_work_t *queue_head;
  tpool_work_t *queue_tail;
  UT_hash_handle hh;
  //  UT_hash_handle hh2; // for empty queue hash
  struct dllist_link link; // for the priority list
  pthread_cond_t  queue_not_empty;
  int waiting_for_empty_queue;
  int tasks_in_progress; // nr of tasks in the queue or executing
  int priority;
} queue_t;

typedef struct tpool {	
  /* pool characteristics */	
  int num_threads;	
  int max_queue_size;	
  int do_not_block_when_full;	
  /* pool state */	
  pthread_t *threads;	
  int cur_queue_size;	
  //tpool_work_t *queue_head;	
  //tpool_work_t *queue_tail;	
  queue_t *work_queues;
  queue_t *empty_queues;
  queue_t *crt_queue;
  struct dllist priority_queue_list;
  pthread_mutex_t queue_lock;	
  pthread_cond_t  queue_not_empty;	
  pthread_cond_t  queue_not_full;	
  pthread_cond_t  queue_empty;	
  int queue_closed;	
  int shutdown;	
} *tpool_t;	

void tpool_init(tpool_t *tpoolp,	
		int num_worker_threads,	
		int max_queue_size,	
		int do_not_block_when_full);	

int tpool_add_work(tpool_t tpool,	
		   void *routine,	
		   void *arg, 
		   void *queue_idx);	

int tpool_destroy(tpool_t tpoolp, int finish);

void tpool_set_queue_priority(tpool_t tpool, void *queue_index, int priority);
void tpool_task_queue_create(tpool_t tpool, void *queue_index, int priority);
void tpool_task_queue_destroy(tpool_t tpool, void *queue_index);
int tpool_task_queue_empty(tpool_t tpool, void *queue_index);
void tpool_wait_for_task_queue_empty(tpool_t tpool, void *queue_index);

void tpool_print_queue(queue_t *q, char *msg);
void tpool_print(tpool_t tpool);
#endif
