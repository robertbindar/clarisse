#include <string.h>
#include <stdio.h>
#include "request_queues.h"

// Get from static pool until empty

struct dt {
  int x;
};

static void print_queue(struct dllist *dll, int i)
{
  struct dllist_link *aux;
  printf("Queue %d:\t",i);
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    printf("%d\t", ((struct dt *)
		    (DLLIST_ELEMENT(aux, struct elem, link)->data))->x);
  }
  printf("\n");
}

void myrequest_queues_print(request_queues_p c) {
    struct dllist_link *aux;
    
    if (c->crt_queue_idx) {
       int i;
       nonempty_queues_t *q;

       printf("REQUEST QUEUE CRT = %d\n",c->crt_queue_idx->idx);
       for (aux = c->list.head; aux != NULL; aux = aux->next) 
	 printf("%d ",((queue_idx_p)aux)->idx);
       printf("\n");
       for (i = 0; i < c->nr_queues; i++) {
	 print_queue(&c->queues[i],i);
       }
       printf("Non-empty queues for enumeration: ");
       for(q = c->nonempty_queues_enum; q != NULL; q = q->hh.next) {
	 printf("%d ",q->idx);
       }
       printf("\n");
    }
    else 
	printf("REQUEST QUEUE EMPTY!\n");
}

void test1(){ 
  int i;
  request_queues c;
  request_queues_init(&c, 3, ZERO_POLICY);

  for (i=0; i<10; i++){
    struct dt *d = (struct dt*) malloc(sizeof(d));
    d->x = i;
    myrequest_queues_print(&c);
    request_queues_insert(&c,i,d);
  }
  myrequest_queues_print(&c);
}

void test2( int policy){ 
  int i;
  request_queues c;
  request_queues_init(&c, 3, policy);

  for (i=0; i<10; i++){
    struct dt *d = (struct dt*) malloc(sizeof(d));
    d->x = i;
    request_queues_insert(&c,i,d);
  }
  myrequest_queues_print(&c);
  
  if (policy == HASH_POLICY)
    request_queues_set_first(&c,1);

  while (!(request_queues_empty(&c))) {
    request_queues_rm_crt(&c);
    myrequest_queues_print(&c);
  }
}

void test3( int policy){ 
  int i;
  request_queues c;
  request_queues_init(&c, 3, policy);

  for (i=0; i<10; i++){
    struct dt *d = (struct dt*) malloc(sizeof(d));
    d->x = i;
    request_queues_insert(&c,i,d);
  }
  myrequest_queues_print(&c);
  
  request_queues_init_enum(&c, 2);
  struct dt *d1 =request_queues_enum(&c, 2);
  while (d1){
    printf("Enumerated d->x=%d\n", d1->x);
    d1 =request_queues_enum(&c, 2);
  }
  request_queues_init_enum(&c, 2);
  myrequest_queues_print(&c);

  d1 =request_queues_enum(&c, 0);
  while (d1){
    printf("Enumerated d->x=%d\n", d1->x);
    d1 =request_queues_enum(&c, 0);
  }
  myrequest_queues_print(&c);

  d1 =request_queues_enum(&c, 1);
  while (d1){
    printf("Enumerated d->x=%d\n", d1->x);
    d1 =request_queues_enum(&c, 1);
  }
  myrequest_queues_print(&c);
}


void test4( int policy){ 
  int i;
  request_queues c;
  request_queues_init(&c, 3, policy);

  for (i=0; i<10; i++){
    struct dt *d = (struct dt*) malloc(sizeof(d));
    d->x = i;
    request_queues_insert(&c,i,d);
  }
  myrequest_queues_print(&c);
  
  for (i=0; i<3; i++){
    struct dt *d1 =  (struct dt*)  request_queues_peek_first(&c,i);
    printf("First element queue %d d->x=%d\n", i, d1->x);
  }
}




int main(){

  //test1();
  //test2(ZERO_POLICY);
  //test2(HASH_POLICY);
  //test2(RANDOM_POLICY);
  test3(RANDOM_POLICY);
  //test4(RANDOM_POLICY);
  return 0;
}
