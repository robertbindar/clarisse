#include <stdio.h>
#include "util.h"
#include "alloc_pool.h"

// simple pool allocated in one big memory chunk and reallocated
// when the max_cnt is reached

void alloc_pool_init(alloc_pool_t *a, int max_cnt, int incr, int alloc_size){
  a->cnt = 0;
  a->max_cnt = max_cnt;
  a->incr = incr;
  a->alloc_size = alloc_size;
  a->mem = clarisse_malloc(a->max_cnt * a->alloc_size);
}

void alloc_pool_free(alloc_pool_t *a){
  clarisse_free(a->mem);
}

void *alloc_pool_get(alloc_pool_t *a){
  void *ptr;

  if (a->cnt == a->max_cnt) {
    a->max_cnt += a->incr;
    a->mem = clarisse_realloc(a->mem, a->max_cnt * a->alloc_size);
  }
  ptr = (void *) (((char *)a->mem) + a->cnt * a->alloc_size);
  a->cnt++;
  return ptr;
}

void *alloc_pool_get_set(alloc_pool_t *a, void *val){
  void *ptr;

  ptr = alloc_pool_get(a);
  memcpy(ptr, val, a->alloc_size);
  return ptr;
}

void * alloc_pool_get_ith(alloc_pool_t *a, int i){
  if (i >= a->cnt) return NULL;
  return (void *) (a->mem + i * a->alloc_size);
}

int alloc_pool_set_ith(alloc_pool_t *a, int i, void *val){
  if (i >= a->cnt)
    return -1;
  else 
    memcpy((void *) (((char *)a->mem) + i * a->alloc_size), val, a->alloc_size);
  return 0;
}

void alloc_pool_print(alloc_pool_t *a){
  printf("ALLOC POOL: cnt=%d max_cnt=%d incr=%d alloc_size=%d\n",  a->cnt, a->max_cnt, a->incr, a->alloc_size);
}
