/* Thread pool implementation from Pthreads Programming
Bradford Nichols, Dick Buttlar and Jacqueline Proulx Farrell
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "thr_pool.h"

void *tpool_thread(void *arg);

/* The tpool_init routine, shown in Example 3-23, initializes a thread
   pool. The routine sets the basic characteristics of the thread pool
   by copying into the tpoolt structure the values of its three input
   parameters (num_worker_threads,max_queue_size, and
   do_not_block_when_full). It also initializes the thread pool's
   state.
 */

static void init_queue(tpool_t tpool){
  tpool->work_queues = NULL;
  tpool->empty_queues = NULL;
  tpool->crt_queue = NULL;
  dllist_init(&tpool->priority_queue_list);
  //tpool->queue_head = NULL;	
  //tpool->queue_tail = NULL;	
}

void tpool_init(tpool_t   *tpoolp,	
                int       num_worker_threads,	
                int       max_queue_size,	
                int       do_not_block_when_full)  {	
  int i, rtn;	
  tpool_t tpool;	
  /* allocate a pool data structure */	
  if ((tpool = (tpool_t )malloc(sizeof(struct tpool))) == NULL)	
    perror("malloc"), exit(-1);	
  /* initialize the fields */	
  tpool->num_threads = num_worker_threads;	
  tpool->max_queue_size = max_queue_size;	
  tpool->do_not_block_when_full = do_not_block_when_full;	
  if ((tpool->threads =	
       (pthread_t *)malloc(sizeof(pthread_t)*num_worker_threads))	
      == NULL)	
    perror("malloc"), exit(-1);	
  tpool->cur_queue_size = 0;	
  init_queue(tpool);
  tpool->queue_closed = 0;	
  tpool->shutdown = 0;	
  if ((rtn = pthread_mutex_init(&(tpool->queue_lock), NULL)) != 0)	
    fprintf(stderr,"pthread_mutex_init %s",strerror(rtn)), exit(-1);	
  if ((rtn = pthread_cond_init(&(tpool->queue_not_empty), NULL)) != 0)	
    fprintf(stderr,"pthread_cond_init %s",strerror(rtn)), exit(-1);	
  if ((rtn = pthread_cond_init(&(tpool->queue_not_full), NULL)) != 0)	
    fprintf(stderr,"pthread_cond_init %s",strerror(rtn)), exit(-1);	
  if ((rtn = pthread_cond_init(&(tpool->queue_empty), NULL)) != 0)	
    fprintf(stderr,"pthread_cond_init %s",strerror(rtn)), exit(-1);	
  /* create threads */	
  for (i = 0; i != num_worker_threads; i++) {	
    if ((rtn = pthread_create(&(tpool->threads[i]),	
			      NULL,	
			      tpool_thread,	
			      (void *)tpool)) != 0)	
      fprintf(stderr,"pthread_create %d",rtn), exit(-1);	
  }	
  *tpoolp = tpool;	
}	
 
/* The tpool_thread routine, contains the logic each worker uses to
check the queue for work and take appropriate action depending upon
whether or not a request is available. It takes a single argument≈a
pointer to the tpool_t structure for the pool to which the thread
belongs.  
The body of the routine is a loop in which the worker checks
the request queue. If it's empty, the worker sleeps on the
queue_not_empty condition variable. It can be awakened by either a
shutdown request from tpool_destroy or a work item placed on its
request queue. When awakened by a shutdown request, the worker
exits. When awakened by a work request, however, it rechecks the
queue, removes the request from the queue's head, and executes the
routine specified in the request (using any associated argument). If
the worker finds that the queue was full before it removed the node
and knows that threads may be blocked waiting to add to the queue
(because the pool's do_not_block_when_full characteristic is not set),
it signals the queue_not_full condition. Likewise, if this thread
empties the queue, it signals queue_empty to allow a delayed shutdown
to proceed.
*/ 

static tpool_work_t * get_work(tpool_t tpool){
  tpool_work_t *my_workp;
  queue_t *tmp;
  
#ifdef TPOOL_DEBUG  
  printf("Scheduled work from queue %p priority=%d\n", tpool->crt_queue->queue_idx, tpool->crt_queue->priority);
#endif
  my_workp = tpool->crt_queue->queue_head;
  tpool->crt_queue->queue_head = my_workp->next;
  tpool->cur_queue_size--;
  if (tpool->crt_queue->queue_head == NULL) {	
    //tpool->crt_queue->queue_head = 
    tpool->crt_queue->queue_tail = NULL;	
    //HASH_DEL(tpool->work_queues, tpool->crt_queue);
    if (tpool->crt_queue->priority > 0)
      dllist_rem_head(&tpool->priority_queue_list);
    if (tpool->priority_queue_list.head)
      tmp = DLLIST_ELEMENT(tpool->priority_queue_list.head, queue_t, link); 
    else
      tmp = tpool->crt_queue->hh.next;
#ifdef TPOOL_DEBUG  
    printf("EMPTY QUEUE %lld NEXT QUEUE %lld\n", 
	   (long long int )tpool->crt_queue->queue_idx,
	   ((tmp)?((long long int )tmp->queue_idx):-1));
#endif
    HASH_DEL(tpool->work_queues, tpool->crt_queue);
    HASH_ADD_PTR(tpool->empty_queues, queue_idx, tpool->crt_queue);
  }
  else {
    if (tpool->crt_queue->priority > 0)
      tmp = tpool->crt_queue;
    else
      tmp = tpool->crt_queue->hh.next;
  }
  // Wrap around if I reached last queue 
  if (tmp)
    tpool->crt_queue = tmp;
  else
    tpool->crt_queue = tpool->work_queues;
#ifdef TPOOL_DEBUG    
  printf("Current queue %lld\n", (tpool->crt_queue)?((long long int)(tpool->crt_queue->queue_idx)):-1);
#endif
  return my_workp;
}


void *tpool_thread(void * arg) {	
  tpool_t tpool;
  tpool_work_t *my_workp;	
  
  tpool = (tpool_t)arg;
  for (;;) {
    void *source_queue_idx;
    queue_t *source_queue;

    pthread_mutex_lock(&(tpool->queue_lock));	
    while ( (tpool->cur_queue_size == 0) && (!tpool->shutdown)) {	
      pthread_cond_wait(&(tpool->queue_not_empty),	
			&(tpool->queue_lock));	
    }	
    if (tpool->shutdown) {	
      pthread_mutex_unlock(&(tpool->queue_lock));	
      pthread_exit(NULL);	
    }	
    source_queue_idx = tpool->crt_queue->queue_idx;
    //tpool->crt_queue->tasks_in_progress++;
#ifdef TPOOL_DEBUG  
    printf("%s:Queue=%p tasks_in_progress=%d\n", __FUNCTION__, tpool->crt_queue, tpool->crt_queue->tasks_in_progress);
#endif
    // my_workp is taken from the crt_queue
    my_workp = get_work(tpool);
    if ((!tpool->do_not_block_when_full) &&	
	(tpool->cur_queue_size == (tpool->max_queue_size - 1)))	
      pthread_cond_broadcast(&(tpool->queue_not_full));	
    // Move the following one after work to finish after the last 
    // task has been finalized?
    if (tpool->cur_queue_size == 0)	
      pthread_cond_signal(&(tpool->queue_empty));	
    pthread_mutex_unlock(&(tpool->queue_lock));	
    //printf("Execute task\n");
    (*(my_workp->routine))(my_workp->arg);	
    free(my_workp);	
    pthread_mutex_lock(&(tpool->queue_lock));
    HASH_FIND_PTR(tpool->empty_queues, &source_queue_idx, source_queue); 
    if ((source_queue) &&
	(source_queue->waiting_for_empty_queue > 0) &&
	(source_queue->queue_head == NULL)){
      pthread_cond_broadcast(&source_queue->queue_not_empty);
    }
    // The queue can be also in work queues
    if (!source_queue)
      HASH_FIND_PTR(tpool->work_queues, &source_queue_idx, source_queue);
    source_queue->tasks_in_progress--;
#ifdef TPOOL_DEBUG  
    printf("Finished work from queue %p tasks_in_progress=%d\n", source_queue->queue_idx, source_queue->tasks_in_progress);
#endif
    assert(source_queue->tasks_in_progress >= 0);
    pthread_mutex_unlock(&(tpool->queue_lock));
  }	
}

/*
The tpool_add_work routine checks the do_not_block_when_full flag and
examines the current size of the request queue. If the queue is full,
the routine either returns an error to its caller or suspends itself
on the queue_not_full condition, depending on the value of the pool's
do_not_block_when_full flag. In the latter case, the tpool_add_work
routine resumes when the condition is signaled; it queues the request
and returns to its caller.
*/


queue_t *task_queue_create(void *queue_index, int priority){
  int rtn;
  queue_t *queue;
  
  queue = (queue_t *)malloc(sizeof(queue_t));
  queue->queue_idx = queue_index;
  queue->queue_tail = queue->queue_head = NULL;
  queue->priority = priority;
  // A thread may wait for a particular queue to be drained by seting 
  // queue->waiting_for_empty_queue to 1 and waiting at the condition
  // the last task taken from that queue needs to signal termination 
  // to the cond var. 
  if ((rtn = pthread_cond_init(&(queue->queue_not_empty), NULL)) != 0)	
    fprintf(stderr,"pthread_cond_init %s",strerror(rtn)), exit(-1);
  queue->waiting_for_empty_queue = 0;
  queue->tasks_in_progress = 0;
  return queue;
}


void tpool_task_queue_create(tpool_t tpool, void *queue_index, int priority){
  queue_t *queue;
  pthread_mutex_lock(&tpool->queue_lock);
  queue = task_queue_create(queue_index, priority);
  HASH_ADD_PTR(tpool->empty_queues, queue_idx, queue);
  pthread_mutex_unlock(&tpool->queue_lock);
}



 // Warning: any subsequent operation on the queue is not atomic
 // (i.e. even though the queue appears empty, the next time I need to
 // do something with it it can be non-empty
int tpool_task_queue_empty(tpool_t tpool, void *queue_index){
  queue_t *queue;

  pthread_mutex_lock(&tpool->queue_lock);
  HASH_FIND_PTR(tpool->empty_queues, &queue_index, queue);
  pthread_mutex_unlock(&tpool->queue_lock);
  return  (queue == NULL);
}

// caller waits until a particular queue is empty
// the thread that executes the last task from the queue will notify 
// all callers

void tpool_wait_for_task_queue_empty(tpool_t tpool, void *queue_index){
  queue_t *queue;

  pthread_mutex_lock(&tpool->queue_lock);
  HASH_FIND_PTR(tpool->work_queues, &queue_index, queue);
  if (!queue)
    HASH_FIND_PTR(tpool->empty_queues, &queue_index, queue);
#ifdef TPOOL_DEBUG  
  printf("%s queue=%p tasks_in_progress=%d\n",__FUNCTION__, queue, (queue)?queue->tasks_in_progress:-1);
#endif
  if ((queue) && (queue->tasks_in_progress > 0)) {
#ifdef TPOOL_DEBUG  
    printf("%s: WAITING for queue %p to become empty tasks_in_progress=%d\n",__FUNCTION__, queue, queue->tasks_in_progress);
#endif
    queue->waiting_for_empty_queue++;
    while (queue->tasks_in_progress > 0) {
      pthread_cond_wait(&(queue->queue_not_empty),	
			&(tpool->queue_lock));
      HASH_FIND_PTR(tpool->work_queues, &queue_index, queue);
      if (!queue)
	HASH_FIND_PTR(tpool->empty_queues, &queue_index, queue);
    }
    HASH_FIND_PTR(tpool->empty_queues, &queue_index, queue);
    queue->waiting_for_empty_queue--;
#ifdef TPOOL_DEBUG  
    printf("%s: ALL tasks finished\n",__FUNCTION__);
#endif
  }
  pthread_mutex_unlock(&tpool->queue_lock);
}



 // destroys an empty queue only
 // For an active queue it should block
 // waiting to be drained (the commented code does not do that
void tpool_task_queue_destroy(tpool_t tpool, void *queue_index){
  queue_t *queue;
  pthread_mutex_lock(&tpool->queue_lock);
  //HASH_FIND_PTR(tpool->work_queues, &queue_index, queue);
  //if (!queue) {
  HASH_FIND_PTR(tpool->empty_queues, &queue_index, queue);
  if (!queue) {
    //fprintf(stderr, "Error: queue %lld does not exist, have to create it\n", 
    //	    (long long int)queue_index);
    return;
    //exit(1);
  }
  else
    HASH_DEL(tpool->empty_queues, queue);
  /* }
  else {
    tpool_work_t *cur_nodep;

    while(queue->queue_head != NULL) {	
      cur_nodep = queue->queue_head->next;	
      queue->queue_head = queue->queue_head->next;	
      free(cur_nodep);	
    }
    HASH_DEL(tpool->work_queues, queue);
  }
  */
  free(queue);
  pthread_mutex_unlock(&tpool->queue_lock);
}


static void task_enqueue(tpool_t tpool, void *routine, void *arg, void *queue_index){
  tpool_work_t *workp;	
  queue_t *queue;

  /* allocate work structure */	
  workp = (tpool_work_t *)malloc(sizeof(tpool_work_t));	
  workp->routine = routine;	
  workp->arg = arg;	
  workp->next = NULL;	

  HASH_FIND_PTR(tpool->work_queues, &queue_index, queue);
  if (!queue) {
    HASH_FIND_PTR(tpool->empty_queues, &queue_index, queue);
    if (!queue) {
    /*
     queue = task_queue_create(queue_index, 0);
    queue->queue_tail = queue->queue_head = workp;
    HASH_ADD_PTR(tpool->work_queues, queue_idx,  queue);
    if (tpool->crt_queue == NULL)
      tpool->crt_queue = queue;
    */
      fprintf(stderr, "Error: queue %lld does not exist, have to create it\n", 
	      (long long int)queue_index);
      exit(1);
    }
    else {
      HASH_DEL(tpool->empty_queues, queue);
      HASH_ADD_PTR(tpool->work_queues, queue_idx, queue);
    } 
  }
  //printf("Add task to queue =%d\n", (int)queue->queue_idx);
  if ((tpool->crt_queue == NULL) ||
      (queue->priority > tpool->crt_queue->priority)){
    tpool->crt_queue = queue;
    //printf("New crt queue =%d\n", (int)queue->queue_idx);
  }
  // If it was empty, has priority add it to the sorted priority list
  if ((queue->priority > 0) &&
      (queue->queue_head == NULL)) {
    struct dllist_link *aux;
    for(aux = tpool->priority_queue_list.head; 
	((aux!= NULL) &&
	 ((DLLIST_ELEMENT(aux, queue_t, link))->priority > queue->priority)); 
	aux = aux->next);
    if (aux)
      dllist_ibefore(&tpool->priority_queue_list, aux, &queue->link);
    else
      dllist_iat(&tpool->priority_queue_list, &queue->link);
  }
#ifdef TPOOL_DEBUG  
  printf("Inserted queue %lld into the priority list\n", (long long int)queue->queue_idx);
#endif

  if (queue->queue_tail)
    (queue->queue_tail)->next = workp;
  else
    queue->queue_head = workp;
  queue->queue_tail = workp;
  queue->tasks_in_progress++;


#ifdef TPOOL_DEBUG  
  printf("Insert work into queue %p tasks_in_progress=%d\n", queue->queue_idx, queue->tasks_in_progress);
#endif  
  if (tpool->cur_queue_size == 0) {	
    pthread_cond_broadcast(&tpool->queue_not_empty);	
  } 
  tpool->cur_queue_size++;	
}


int tpool_add_work(tpool_t tpool, void *routine, void *arg, void* queue_index)	
{	

  pthread_mutex_lock(&tpool->queue_lock);	
  if ((tpool->cur_queue_size == tpool->max_queue_size) &&	
      tpool->do_not_block_when_full) {	
    pthread_mutex_unlock(&tpool->queue_lock);	
    return -1;	
  }	
  while ((tpool->cur_queue_size == tpool->max_queue_size) &&	
	 (!(tpool->shutdown || tpool->queue_closed))) {	
    pthread_cond_wait(&tpool->queue_not_full, &tpool->queue_lock);	
  }	
  if (tpool->shutdown || tpool->queue_closed) {	
    pthread_mutex_unlock(&tpool->queue_lock);	
    return -1;	
  }	

  task_enqueue(tpool, routine, arg, queue_index);
  pthread_mutex_unlock(&tpool->queue_lock);	
  return 1;	
}

//By now just adds the queue in a queue of lists 
void tpool_set_queue_priority(tpool_t tpool, void* queue_index, int priority){
  queue_t *queue;

  pthread_mutex_lock(&tpool->queue_lock);
  HASH_FIND_PTR(tpool->work_queues, &queue_index, queue);
  if (queue){
    HASH_FIND_PTR(tpool->empty_queues, &queue_index, queue);
    if (queue) {
      int old_priority;
  
      old_priority = queue->priority; 
      queue->priority = priority;
      if (old_priority == 0) {
	if (priority > 0)
	  dllist_iat(&tpool->priority_queue_list, &queue->link);
      }
      else
	if (priority == 0)
	  dllist_rem(&tpool->priority_queue_list, &queue->link);
      if (&queue->link == tpool->priority_queue_list.head)
	tpool->crt_queue = queue;
    }
  }
  pthread_mutex_unlock(&tpool->queue_lock);
}

/*
tpool_destroy deallocates a thread pool. It sets the shutdownflag in
the tpool _t structure to indicate to workers (and threads calling
tpool_add_work) that the pool is being deactivated. Worker threads
exit when they find this flag set; the tpool_add_work routine returns
a -1 to its caller,

The tpool_destroy routine ensures that all threads are awake to see
the shutdown flag by signaling both the queue_not_empty and
queue_not_full conditions. Even still, some threads may be busy
completing their current requests; it may still be some time before
they learn that a shutdown has begun. To avoid interfering with
in-progress requests, tpool_destroy waits for all worker threads to
exit by calling pthread_join for each thread. When all workers have
departed, tpool_destroy frees the pool's data structures.

The current edition of our tpool_destroy routine is not without its
surprises. When it sets the shutdown flag, only those requests that
are currently in progress are completed. Any requests that are still
in the request queue are lost when the thread pool is
deallocated. Instead, it could disallow additions to the queue and
wait for the queue to empty before deactivating the thread pool. It
could also speed performance by canceling workers rather than waiting
for them to check the shutdown flag.

 */

static void free_queues(tpool_t  tpool){
  tpool_work_t *cur_nodep;	
  queue_t *q, *tmp;

  for(q = tpool->work_queues; q != NULL;) {
    while(q->queue_head != NULL) {	
      cur_nodep = q->queue_head->next;	
      q->queue_head = q->queue_head->next;	
      free(cur_nodep);	
    }	
    tmp = q->hh.next;
    free(q);
    q = tmp;
  }
  
  for(q = tpool->empty_queues; q != NULL;) {
    tmp = q->hh.next;
    HASH_DEL(tpool->empty_queues, q);
    free(q);
    q = tmp;
  }
}


int tpool_destroy(tpool_t    tpool,	
                  int        finish)	{	
  int          i, rtn;	
  if ((rtn = pthread_mutex_lock(&(tpool->queue_lock))) != 0)	
    fprintf(stderr,"pthread_mutex_lock %d",rtn), exit(-1);	
  /* Is a shutdown already in progress? */	
  if (tpool->queue_closed || tpool->shutdown) {	
    if ((rtn = pthread_mutex_unlock(&(tpool->queue_lock))) != 0)	
      fprintf(stderr,"pthread_mutex_unlock %d",rtn), exit(-1);	
    return 0;	
  }	
  tpool->queue_closed = 1;	
  /* If the finish flag is set, wait for workers to drain queue */	
  if (finish == 1) {	
    while (tpool->cur_queue_size != 0) {	
      if ((rtn = pthread_cond_wait(&(tpool->queue_empty),	
				   &(tpool->queue_lock))) != 0)	
	fprintf(stderr,"pthread_cond_wait %d",rtn), exit(-1);	
    }	
  }	
  tpool->shutdown = 1;	
  //if ((rtn = pthread_mutex_unlock(&(tpool->queue_lock))) != 0)	
  //  fprintf(stderr,"pthread_mutex_unlock %d",rtn), exit(-1);	
  /* Wake up any workers so they recheck shutdown flag */	
  if ((rtn = pthread_cond_broadcast(&(tpool->queue_not_empty))) != 0)	
    fprintf(stderr,"pthread_cond_broadcast %d",rtn), exit(-1);	
  if ((rtn = pthread_cond_broadcast(&(tpool->queue_not_full))) != 0)	
    fprintf(stderr,"pthread_cond_broadcast %d",rtn), exit(-1);	

  if ((rtn = pthread_mutex_unlock(&(tpool->queue_lock))) != 0)	
    fprintf(stderr,"pthread_mutex_unlock %d",rtn), exit(-1);	
  /* Wait for workers to exit */	
  for(i=0; i < tpool->num_threads; i++) {	
    if ((rtn = pthread_join(tpool->threads[i],NULL)) != 0)	
      fprintf(stderr,"pthread_join %d",rtn), exit(-1);	
  }	
  /* Now free pool structures */	
  free(tpool->threads);	
  free_queues(tpool);
  free(tpool);	
  return 0;	
}

void tpool_print_queue(queue_t *q, char *msg) {
  //tpool_work_t *aux;
  printf("\n\tQUEUE %s: ", msg);
  printf("ptr=%p queue_idx=%p tasks_in_progress=%d waiting_for_empty_queue=%d\n", q, q->queue_idx, q->tasks_in_progress, q->waiting_for_empty_queue);
  //for (aux = q->queue_head; aux != NULL; aux = aux->next)
}

void tpool_print(tpool_t tpool){
  queue_t *q;
  printf("\nTPOOL print\n");
  printf("\tcur_queue_size=%d\n", tpool->cur_queue_size);
  tpool_print_queue(tpool->crt_queue, "crt_queue");
  printf("\n\tWORK QUEUES:");
  for (q = tpool->work_queues; q != NULL; q = q->hh.next)
    tpool_print_queue(q, "work_queue");
  printf("\n\tEMPTY QUEUES:");
  for (q = tpool->empty_queues; q != NULL; q = q->hh.next)
    tpool_print_queue(q, "work_queue");
  
}
