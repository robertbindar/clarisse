#include <assert.h>
#include "request_queues.h"
#include "util.h"


/*********************************************************
Request queues. 

One queue is currently active and the enumeration will return an item 
from that queue and then make another queue active based on the 
following policies:
ZERO: starts with queue zero and goes up incrementally (wraps around at last)
HASH: starts with queue i % n and goes up incrementally (wraps around at last)
RANDOM: randomly selects the next queue
*********************************************************
*********************************************************/

void request_queues_init(request_queues_p c, int nr_queues, short type) {
    int i;
    dllist_init(&c->list);
    c->crt_queue_idx = NULL;
    c->nr_queues = nr_queues;
    c->queues = (struct dllist*)clarisse_malloc(nr_queues * sizeof(struct dllist));
    c->crt_elems  = (elem **)clarisse_malloc(nr_queues * sizeof(elem *));
    c->nonempty_queues_enum = NULL;
    for (i = 0;i < nr_queues;i++) {
      c->crt_elems[i] = NULL;
      dllist_init(&c->queues[i]);
    }
    c->type_next = type;// HASH, RANDOM, INCREASING 
}

void request_queues_free(request_queues_p c) {
  if (c->nr_queues > 0)
    clarisse_free(c->queues);
}

// inserts both in the list of tuples AND in the queue
// Need to rewrite the linear search
void request_queues_insert(request_queues_p c, int key, void *data) {
  struct dllist_link *aux;
  int idx = REQUEST_QUEUES_GET_QUEUE_IDX(key, c);
  elem_p e = (elem_p) clarisse_malloc(sizeof(elem));
  nonempty_queues_t *q; 

  e->data = data;
  if (dllist_is_empty(&c->queues[idx]))
    c->crt_elems[idx] = e;
  dllist_iat(&c->queues[idx],(struct dllist_link *)e); //insert into queue

  for (aux = c->list.head; aux != NULL && 
	 ((((queue_idx_p)aux)->idx) < idx) ; 
       aux = aux->next);
  if ((!aux) ||                           // havent found any
      ((((queue_idx_p)aux)->idx) > idx)) {  // found one bigger 
    queue_idx_p b = (queue_idx_p) clarisse_malloc(sizeof(struct queue_idx));
    b->idx = idx;
    if (!aux)
      dllist_iat(&c->list,(struct dllist_link *)b);
    else
      dllist_ibefore(&c->list, aux, (struct dllist_link *)b);   
    
    if (!c->crt_queue_idx)
      c->crt_queue_idx = b;
  }
  HASH_FIND_INT(c->nonempty_queues_enum, &idx, q);
  if (!q) {
    q = (nonempty_queues_t *) clarisse_malloc(sizeof(nonempty_queues_t));
    q->idx = idx;
    HASH_ADD_INT(c->nonempty_queues_enum, idx, q);
  }
}

void * request_queues_peek_crt(request_queues_p c) {
    return (c->crt_queue_idx)
      ? ((elem_p) c->queues[c->crt_queue_idx->idx].head )->data
      : NULL;
}

void * request_queues_peek_key(request_queues_p c, int key) {
    return REQUEST_QUEUES_GET_QUEUE(key,c)->head
      ? ((elem_p)REQUEST_QUEUES_GET_QUEUE(key,c)->head)->data 
      : NULL ;
}

void* request_queues_peek_first(request_queues_p c, int i){
  if (i >= c->nr_queues) {
    fprintf(stderr, "request_queues_peek_first: i >= c->nr_queues ");
    return NULL;
  }
  else
    return (c->queues[i].head)?
      ((elem_p)c->queues[i].head)->data:
      NULL;
} // peek first elem
void* request_queues_peek_last(request_queues_p c, int i){
  if (i >= c->nr_queues) {
    fprintf(stderr, "request_queues_peek_last: i >= c->nr_queues ");
    return NULL;
  }
  else  
    return (c->queues[i].tail)?
      ((elem_p)c->queues[i].tail)->data:
      NULL;
} // peek last elem


void request_queues_set_first(request_queues_p c, int i) {    
  if (c->crt_queue_idx) {
    int queue_idx;
    struct dllist_link *aux;
    if (c->type_next == ZERO_POLICY)
      queue_idx = 0;
    if (c->type_next == HASH_POLICY)
      queue_idx = i % c->nr_queues;
    if (c->type_next == RANDOM_POLICY)
      queue_idx = random() % c->nr_queues;
    for (aux = c->list.head; aux != NULL && 
	   ((((queue_idx_p)aux)->idx) < queue_idx) ; aux = aux->next);
    if (aux)
      c->crt_queue_idx = (queue_idx_p)aux;
    else
      c->crt_queue_idx = (queue_idx_p)c->list.head;
  }
}

static void go_next(request_queues_p c) {
    struct dllist_link **crt=(struct dllist_link **)&c->crt_queue_idx;
    if ((*crt)->next) {// there is a next
	*crt = (*crt)->next;
	//printf("client_id=%2d Hay next\n",client_id);
    }
    else {//there is no next
	*crt = c->list.head;
	//printf("client_id=%2d No next\n",client_id);
    }
}

void request_queues_set_next(request_queues_p c) {
  if (c->crt_queue_idx) {
    switch (c->type_next) {
    case ZERO_POLICY: 
    case HASH_POLICY:	{
      go_next(c);
      break;
    }
    case RANDOM_POLICY:{
      request_queues_set_first(c,0);
      break;
    }
    }
  }
}
/*
rm_crt does not change the queue for the ZERO and HASH policies but only for random (probably it should)
 */
void *request_queues_rm_crt(request_queues_p c) {
  queue_idx_p b = c->crt_queue_idx;
  if (b){
    elem_p e = (elem_p) dllist_rem_head(&c->queues[b->idx]);
    void *data = e->data;
    nonempty_queues_t *q; 
    // If I have just removed the current, set a new current
    if (c->crt_elems[b->idx] == e)
      c->crt_elems[b->idx] = (elem_p) c->queues[b->idx].head;

    if (dllist_is_empty(&c->queues[b->idx])) {
      if (b->link.next)  // there is a next
	c->crt_queue_idx = (queue_idx_p) b->link.next;
      else
	if (c->list.head != (struct dllist_link *)b) // no next, first != crt
	  c->crt_queue_idx = (queue_idx_p) c->list.head;
	else // crt was the only one in the list
	  c->crt_queue_idx = NULL;
      dllist_rem(&c->list, (struct dllist_link *)b);
      HASH_FIND_INT(c->nonempty_queues_enum, &b->idx, q);
      //assert(q != NULL);
      if (q)
	HASH_DEL(c->nonempty_queues_enum, q);
      clarisse_free(b);
    }
    if (c->type_next == RANDOM_POLICY)
      request_queues_set_next(c);
    clarisse_free(e);
    return data;
  }
  else 
    return NULL;
}

void *request_queues_rm_queue(request_queues_p c, int queue_idx) {  
  if (c->crt_queue_idx)
    if (queue_idx == c->crt_queue_idx->idx) 
      return request_queues_rm_crt(c);
    else {
      elem_p e = (elem_p) dllist_rem_head(&c->queues[queue_idx]);
      void *data = e->data;
      clarisse_free(e);
      if (dllist_is_empty(&c->queues[queue_idx])) {
	dllist_link *aux;
	for (aux = c->list.head; aux != NULL; aux = aux->next) {
	  queue_idx_p b = (queue_idx_p) aux; 
	  if (b->idx == queue_idx) {
	    nonempty_queues_t *q; 
	    dllist_rem(&c->list, (struct dllist_link *)b);
	    HASH_FIND_INT(c->nonempty_queues_enum, &b->idx, q);
	    //assert(q != NULL);
	    if (q)
	      HASH_DEL(c->nonempty_queues_enum, q);
	    clarisse_free(b);
	    break;
	  }
	}
      }
      return data;
    }
  else
    return NULL;
}


void request_queues_print(request_queues_p c) {
    struct dllist_link *aux;
    
    if (c->crt_queue_idx) {
      printf("REQUEST QUEUE CRT = %d\n",c->crt_queue_idx->idx);
      for (aux = c->list.head; aux != NULL; aux = aux->next) 
	printf("%d ",((queue_idx_p)aux)->idx);
      printf("\n");
    }
    else 
	printf("REQUEST QUEUE EMPTY!\n");
}

char * request_queues_dump(request_queues_p c, char *buf) {
    struct dllist_link *aux;
    if (c->crt_queue_idx) {
      sprintf(buf, "REQUEST QUEUE CRT = %d ACTIVE QUEUE: ",c->crt_queue_idx->idx);
      for (aux = c->list.head; aux != NULL; aux = aux->next) 
	sprintf(buf,"%s%d ", buf, ((queue_idx_p)aux)->idx);
      sprintf(buf, "%s\n", buf);
    }
    else 
      sprintf(buf, "%sREQUEST QUEUE EMPTY!\n", buf);
    return buf;
}


dllist * request_queues_get_queue(request_queues_p c, int idx) { 
    return (idx < c->nr_queues) ? &c->queues[idx] : NULL;
}

void request_queues_init_enum(request_queues_p c, int idx) {
  nonempty_queues_t *q; 
  c->crt_elems[idx] = (elem_p) c->queues[idx].head;
  HASH_FIND_INT(c->nonempty_queues_enum, &idx, q);
  if (!q) {
    q = (nonempty_queues_t *) clarisse_malloc(sizeof(nonempty_queues_t));
    q->idx = idx;
    HASH_ADD_INT(c->nonempty_queues_enum, idx, q);
  }
}

void *request_queues_enum(request_queues_p c, int idx) {
  if (c->crt_elems[idx]) {
    void *data;
    data = c->crt_elems[idx]->data;
    c->crt_elems[idx] = (elem_p)c->crt_elems[idx]->link.next;
    if (c->crt_elems[idx] == NULL) {
      nonempty_queues_t *q; 
      HASH_FIND_INT(c->nonempty_queues_enum, &idx, q);
      assert(q != NULL);
      HASH_DEL(c->nonempty_queues_enum, q);
    }
    return data;
  }
  else
    return NULL;
}

void *request_queues_peek_crt_queue(request_queues_p c, int idx){//crt elem from queue i
  return (c->crt_elems[idx]) ? (c->crt_elems[idx]->data) : NULL;
}
