#include <string.h>
#include <stdio.h>
#include "list.h"


struct el{ 
  struct dllist_link link;
  int i;
};

void print_list(struct dllist *dll)
{
  struct dllist_link *aux;
  printf("List:\t");
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    printf("%d\t", (DLLIST_ELEMENT(aux, struct el, link))->i);
  }
  printf("\n");
}


struct el * find_int_elem(struct dllist *dll, int i) {
  struct dllist_link * aux;
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    struct el *e = DLLIST_ELEMENT(aux, struct el, link);
    if (e->i == i) {
      return e;
    }
  }
  return NULL;
}

struct el * remove_int_elem(struct dllist *dll, int i) {
  struct dllist_link * aux;
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    struct el *e = DLLIST_ELEMENT(aux, struct el, link);
    if (e->i == i) {
      dllist_rem(dll, aux);
      return e;
    }
  }
  return NULL;
}
    

int main()
{
  char c;
  int i=0;
  struct dllist dll;

  dllist_init(&dll);

  printf("e=enqueue d=dequeue h=remove_head i=insert a= append l to l p=print ?=is empty\n");
  scanf("%c",&c);
  //printf("c=%c\n",c);
  while(c!='n')
    {
      switch(c)
	{
	case 'e': { 
	  struct el *e=(struct el *) malloc(sizeof(struct el));
	  e->i=i++;
	  dllist_iat(&dll,&e->link);
	  print_list(&dll);
	  break;
	}
	case 'i': {
	  struct el *e=(struct el *) malloc(sizeof(struct el));
	  e->i=i++;
	  dllist_iah(&dll,&e->link);
	  print_list(&dll);
	  break;
	}
	case 'p': 
	  print_list(&dll);
	  break;
	case 'd':
	  dllist_rem_tail(&dll);
	  print_list(&dll);
	  break;
	case 'h':
	  dllist_rem_head(&dll);
	  print_list(&dll);
	  break;
	case 'a':
	  printf("Append not implemented\n");
	  break;
	  
	case 'l':
	  printf("length not implemented\n");
	  break;
	  
	case 'r':
	  printf("Remove element with i="); fflush(stdout);
	  scanf("%d",&i);
	  printf("i=%d\n",i); fflush(stdout);
	  remove_int_elem(&dll, i);
	  print_list(&dll);
	  break; 

	case '?':
	  if(dllist_is_empty(&dll))
	    printf("Empty\n");
	  else
	    printf("Not Empty\n");
	}
      printf("e=enqueue d=dequeue h=remove_head i=insert p=print r=remove ?=is empty\n");
      scanf("\n%c",&c);
      //printf("c=%c\n",c);

    }
  return 0;
}
