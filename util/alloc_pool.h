#ifndef _ALLOC_POOL_H
#define _ALLOC_POOL_H 1

typedef struct{
  int cnt;
  int max_cnt;
  int incr;
  int alloc_size;
  void *mem;
} alloc_pool_t; 


void alloc_pool_init(alloc_pool_t *a, int max_cnt, int incr, int alloc_size);
void alloc_pool_free(alloc_pool_t *a);
void *alloc_pool_get(alloc_pool_t *a);
void *alloc_pool_get_set(alloc_pool_t *a, void *val);
void * alloc_pool_get_ith(alloc_pool_t *a, int i);
int alloc_pool_set_ith(alloc_pool_t *a, int i, void *val);
void alloc_pool_print(alloc_pool_t *a);
#endif
