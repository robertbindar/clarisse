#ifndef _ERROR_INCLUDE
#define _ERROR_INCLUDE

void handle_error(int errcode, char *str);
//void print_dt(MPI_Datatype type);

#define ERROR(args...) \
do { \
  fprintf(stderr, "ERROR (%s:%d): ", __FILE__, __LINE__); \
  fprintf(stderr, args); \
} while(0)
#define handle_err(errcode, string) {ERROR(string);handle_error(errcode, "");}

#ifdef DEBUG
#define DEBUG_TEST 1
#else
#define DEBUG_TEST 0
#endif
#define debug_printf(fmt, ...) \
        do { if (DEBUG_TEST) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); } while (0)

/*#define debug_printf(fmt, ...)					\
            do { if (DEBUG) fprintf(stderr, fmt, __VA_ARGS__); } while (0)
*/

// e-mail : kangmo@nanolat.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// c-callstack.h : Show Java-like callstack in C/C++ projects.
//

#if defined(NDEBUG) /* Release Mode */

#define NL_RETURN(rc) return (rc)

#else /* Debug Mode */

#define NL_RETURN(rc)                                       \
   {                                                          \
     if ((rc)) {                                              \
       fprintf( stderr,                                       \
                "Error(code:%d) at : %s (%s:%d)\n",           \
                (rc), __FUNCTION__, __FILE__, __LINE__);      \
     }                                                        \
     return (rc);                                             \
   }

#endif

#endif
